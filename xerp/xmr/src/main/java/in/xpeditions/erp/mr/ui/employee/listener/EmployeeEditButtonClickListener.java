/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author xpeditions
 *
 */
public class EmployeeEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5428318104135590926L;
	private EmployeeDataChangeObserver countryDataChangeObserver;

	public EmployeeEditButtonClickListener(
			EmployeeDataChangeObserver countryDataChangeObserver) {
				this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.countryDataChangeObserver.notifyEditButtonClick();
		button.setEnabled(true);
	}

}
