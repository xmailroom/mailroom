package in.xpeditions.erp.mr.ui.deliverysheet;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.shared.ui.label.ContentMode;
import com.vaadin.ui.Component;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;

public class DeliverySheetViewContainer extends HorizontalLayout implements DeliverySheetDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2877701049396734390L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private DeliverySheet deliverySheet;

	public DeliverySheetViewContainer(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheetDataChangeObserver.addListener(this);
		this.setSizeFull();
		this.createViewContainer();
	}

	private void createViewContainer() {
		VerticalLayout viewLayout = new VerticalLayout();
		viewLayout.setVisible(false);

		this.addComponent(viewLayout);
		this.handleSelectionChange(this.deliverySheet);
	}

	private void handleSelectionChange(DeliverySheet deliverySheet) {
		this.deliverySheet = deliverySheet;
		this.refreshViewContainer();
	}

	private void refreshViewContainer() {
		if (this.deliverySheet != null) {
			try {
				this.deliverySheet = new DeliverySheetManager().getBy(this.deliverySheet);
			} catch (Exception e) {
			}
			VerticalLayout oViewLayout = (VerticalLayout) this.getComponent(0);
			this.removeComponent(oViewLayout);

			VerticalLayout outerViewLayout = new VerticalLayout();
			outerViewLayout.setSizeFull();
			this.addComponent(outerViewLayout, 0);

			outerViewLayout.addComponent(this.deliverySheetLayout());
		} else {
			VerticalLayout oViewLayout = (VerticalLayout) this.getComponent(0);
			oViewLayout.setVisible(false);
		}

	}

	private Component deliverySheetLayout() {
		HorizontalLayout viewLayout = new HorizontalLayout();
		viewLayout.setWidth("40%");
		viewLayout.setSpacing(true);

		VerticalLayout leftLayout = new VerticalLayout();
		leftLayout.setWidth("60%");
		viewLayout.addComponent(leftLayout);
		if (XERPUtil
				.hasPermissions(new String[] { MailRoomPermissionEnum.VIEW_DELIVERY_SHEET.asPermission().getName() })) {
			String desc = "<strong> " + this.deliverySheet.getNumber() + " - "
					+ XERPUtil.formatDate(this.deliverySheet.getDate(), DataHolder.commonUserDateFormat) + "</strong>";
			desc += "</br>";
			desc += XERPUtil.formatDate(this.deliverySheet.getDate(), DataHolder.commonUserDateFormat);
			Label name = new Label();
			name.setContentMode(ContentMode.HTML);
			name.setValue(desc);
			name.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_NORMAL);
			leftLayout.addComponent(name);
		}
		return viewLayout;
	}

	@Override
	public void onUpdate(DeliverySheet deliverySheet) {
		this.handleSelectionChange(deliverySheet);

	}

	@Override
	public void onSelectionChange(DeliverySheet deliverySheet) {
		this.handleSelectionChange(deliverySheet);

	}

	@Override
	public void onEditButtonClick(DeliverySheet deliverySheet) {

	}

	@Override
	public void onConfirm(BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		// TODO Auto-generated method stub
		
	}

}
