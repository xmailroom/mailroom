/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DivisionDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1481465115340366641L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private Division division;

	public DivisionDeleteButtonClickListener(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		String pleaseConfirmTxt = bundle.getString(MailRoomMessageConstants.DIVISION_DELETE_CONFIRM_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = bundle.getString(MailRoomMessageConstants.DIVISION_DELETE_CONFIRM_MESSAGE);
		String captionYes = bundle.getString(MailRoomMessageConstants.DIVISION_DELETE_CONFIRM_BUTTON_CAPTION_YES);
		String captionNo = bundle.getString(MailRoomMessageConstants.DIVISION_DELETE_CONFIRM_BUTTON_CAPTION_NO);
		DivisionDeleteConfirmListener countryDeleteConfirmListener = new DivisionDeleteConfirmListener(this.division, this.divisionDataChangeObserver);
		ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt, doYouWantToDeleteTxt + this.division.getName() + "?", captionYes, captionNo, countryDeleteConfirmListener);
		event.getButton().setEnabled(true);
	}

	public void setDivision(Division division) {
		this.division = division;
	}

}
