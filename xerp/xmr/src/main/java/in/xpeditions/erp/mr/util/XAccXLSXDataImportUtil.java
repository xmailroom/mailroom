/**
 * 
 */
package in.xpeditions.erp.mr.util;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.poi.xssf.usermodel.XSSFCell;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.acc.entity.Company;
import in.xpeditions.erp.acc.entity.Country;
import in.xpeditions.erp.acc.entity.District;
import in.xpeditions.erp.acc.entity.State;
import in.xpeditions.erp.acc.manager.BranchManager;
import in.xpeditions.erp.acc.manager.CompanyManager;
import in.xpeditions.erp.acc.manager.CountryManager;
import in.xpeditions.erp.acc.manager.DistrictManager;
import in.xpeditions.erp.acc.manager.StateManager;
import in.xpeditions.erp.acc.util.XAccXLSXUtil;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.entity.util.EmployeeNature;
import in.xpeditions.erp.mr.entity.util.GenderEnum;
import in.xpeditions.erp.mr.manager.CostCentreManager;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.manager.EmployeeManager;
import in.xpeditions.erp.mr.manager.FloorManager;

/**
 * @author Saran
 *
 */
public class XAccXLSXDataImportUtil {

	public static void imporCountrytData(File file) throws XERPException, IOException {
		int startingRow = 1;
		int sheetNo = 0;

		int nColIdx=0;
		
		int country_name_index = ++nColIdx;	//1
		int state_name_index = ++nColIdx;	//2
		int district_name_index = ++nColIdx;	//3;
		int pincode_name_index = ++nColIdx;	//4;
		
		CountryManager countryManager = new CountryManager();
		StateManager stateManager = new StateManager();
		DistrictManager districtManager = new DistrictManager();
		for (int i = startingRow; i < XAccXLSXUtil.extract(file, sheetNo).size(); i++) {
			List<XSSFCell> row = XAccXLSXUtil.extract(file, sheetNo).get(i);
			
			XSSFCell countryNameCell = row.get(country_name_index);
			String countryName = XAccXLSXUtil.getCellValueAsString(countryNameCell);
			if (countryName == null) {
				continue;
			}
			countryName = countryName.trim();
			Country country = countryManager.getByName(countryName);
			if (country == null) {
				country = new Country();
				String firstLetter = countryName.substring(0, 1);
				firstLetter = firstLetter.toUpperCase();
				String remaingLetters = countryName.substring(1, countryName.length());
				remaingLetters = remaingLetters.toLowerCase();
				
				String name = firstLetter+remaingLetters;
				
				country.setCode(name);
				country.setName(name);
			}
			
			XSSFCell stateNameCell = row.get(state_name_index);
			String stateName = XAccXLSXUtil.getCellValueAsString(stateNameCell);
			if (stateName == null) {
				continue;
			}
			stateName = stateName.trim();
			State state = stateManager.getByName(stateName);
			if (state == null) {
				state = new State();
				String firstLetter = stateName.substring(0, 1);
				firstLetter = firstLetter.toUpperCase();
				String remaingLetters = stateName.substring(1, stateName.length());
				remaingLetters = remaingLetters.toLowerCase();
				
				String name = firstLetter+remaingLetters;
				state.setCode(name);
				state.setName(name);
			}
			state.setCountry(country);
			
			XSSFCell districtNameCell = row.get(district_name_index);
			String districtName = XAccXLSXUtil.getCellValueAsString(districtNameCell);
			if (districtName == null) {
				continue;
			}
			
			XSSFCell districtPincodeCell = row.get(pincode_name_index);
			String districtPincode = XAccXLSXUtil.getCellValueAsString(districtPincodeCell);
			if (districtPincode == null) {
				continue;
			}
			
			districtName = districtName.trim();
			districtPincode = districtPincode.replace(".0", "");
			districtPincode = districtPincode.trim();
			District district = districtManager.getByPincode(districtPincode);
			if (district == null) {
				district = new District();
				String firstLetter = districtName.substring(0, 1);
				firstLetter = firstLetter.toUpperCase();
				String remaingLetters = districtName.substring(1, districtName.length());
				remaingLetters = remaingLetters.toLowerCase();
				
				String name = firstLetter+remaingLetters;
				
				district.setCode(name);
				district.setName(name);
				
				district.setPincode(districtPincode);
			}
			district.setCountry(country);
			district.setState(state);
			
			countryManager.bulkSave(country, state, district);
		}
	}

	public static void imporEmployeetData(File file) throws XERPException, IOException {
		int startingRow = 1;
		int sheetNo = 0;

		int nColIdx=0;
		
		int company_name_index = ++nColIdx;	//1
		int branch_name_index = ++nColIdx;	//2
		int employment_nature_index = ++nColIdx;	//3;
		int department_index = ++nColIdx;	//4;
		int costCenter_index = ++nColIdx;	//5;
		int name_index = ++nColIdx;	//6;
		int employeeId_index = ++nColIdx;	//7;
		int emailId_index = ++nColIdx;	//8;
		int empExtn_index = ++nColIdx;	//9;
		int cabin_index = ++nColIdx;	//10;
		int buildingName_index = ++nColIdx;	//11;
		int floor_index = ++nColIdx;	//12;
		int location_index = ++nColIdx;	//13;
		int mobileNumber_index = ++nColIdx;	//14;
		int gender_index = ++nColIdx;	//15;
		int band_index = ++nColIdx;	//16;
		
		CompanyManager companyManager = new CompanyManager();
		BranchManager branchManager = new BranchManager();
		DepartmentManager departmentManager = new DepartmentManager();
		CostCentreManager costCentreManager = new CostCentreManager();
		FloorManager floorManager = new FloorManager();
		EmployeeManager employeeManager = new EmployeeManager();
		
		for (int i = startingRow; i < XAccXLSXUtil.extract(file, sheetNo).size(); i++) {
			List<XSSFCell> row = XAccXLSXUtil.extract(file, sheetNo).get(i);
			
			Employee employee = new Employee();
			
			XSSFCell companyNameCell = row.get(company_name_index);
			String comapnyName = XAccXLSXUtil.getCellValueAsString(companyNameCell);
			if (comapnyName == null) {
				continue;
			}
			Company company = companyManager.getByName(comapnyName);
			if (company == null) {
				Notification.show("Please configure company details first and then proceed.", Type.ERROR_MESSAGE);
				return;
			}
			
			XSSFCell branchNameCell = row.get(branch_name_index);
			String branchName = XAccXLSXUtil.getCellValueAsString(branchNameCell);
			if (branchName == null) {
				continue;
			}
			
			Branch branch = branchManager.getByName(branchName);
			if (branch == null) {
				Notification.show("Please configure branch details first and then proceed.", Type.ERROR_MESSAGE);
				return;
			}
			employee.setBranch(branch);
			
			XSSFCell employmentNatureCell = row.get(employment_nature_index);
			String employmentNature = XAccXLSXUtil.getCellValueAsString(employmentNatureCell);
			if (employmentNature == null) {
				employee.setEmployeeNature(null);
			} else {
				EmployeeNature employeeNature = employmentNature.equals(EmployeeNature.PERMANENT.getName()) ? EmployeeNature.PERMANENT : EmployeeNature.TEMPORARY;
				employee.setEmployeeNature(employeeNature);
			}
			
			XSSFCell departmentCell = row.get(department_index);
			String departmentName = XAccXLSXUtil.getCellValueAsString(departmentCell);
			if (departmentName == null) {
				continue;
			}
			Department department = departmentManager.getByName(departmentName);
			List<Floor> floors = null;
			Division division = null;
			if (department == null) {
				division = new Division();
				division.setBranch(branch);
				division.setCode(departmentName);
				division.setName(departmentName);
				department = new Department();
				department.setDivision(division);
				department.setCode(departmentName);
				department.setName(departmentName);
			} else {
				floors = department.getFloors();
				if (floors == null) {
					floors = new ArrayList<Floor>();
				}
			}
			employee.setDepartment(department);
			
			XSSFCell costCenterCell = row.get(costCenter_index);
			String costCenterName = XAccXLSXUtil.getCellValueAsString(costCenterCell);
			if (costCenterName == null) {
				continue;
			}
			CostCentre costCentre = costCentreManager.getByName(costCenterName);
			if (costCentre == null) {
				costCentre = new CostCentre();
				costCentre.setDepartment(department);
				costCentre.setCode(costCenterName);
				costCentre.setName(costCenterName);
			}
			employee.setCostCetre(costCentre);
			
			XSSFCell employeeNameCell = row.get(name_index);
			String employeeName = XAccXLSXUtil.getCellValueAsString(employeeNameCell);
			if (employeeName == null) {
				continue;
			}
			employee.setName(employeeName);
			
			XSSFCell employeeIdCell = row.get(employeeId_index);
			String employeeId = XAccXLSXUtil.getCellValueAsString(employeeIdCell);
			if (employeeId == null) {
				continue;
			}
			employee.setEmployeeId(employeeId);
			
			XSSFCell emailIdCell = row.get(emailId_index);
			String emailId = XAccXLSXUtil.getCellValueAsString(emailIdCell);
			employee.setEmailId(emailId);
			
			XSSFCell extnCell = row.get(empExtn_index);
			String empExtn = XAccXLSXUtil.getCellValueAsString(extnCell);
			employee.setExtension(empExtn);
			
			XSSFCell cabinCell = row.get(cabin_index);
			String cabin = XAccXLSXUtil.getCellValueAsString(cabinCell);
			employee.setCabin(cabin);
			
			XSSFCell builidingNameCell = row.get(buildingName_index);
			String buildingName = XAccXLSXUtil.getCellValueAsString(builidingNameCell);
			employee.setBuildingName(buildingName);
			
			XSSFCell floorCell = row.get(floor_index);
			String floorName = XAccXLSXUtil.getCellValueAsString(floorCell);
			if (floorName == null) {
				continue;
			}
			Floor floor = floorManager.getByName(floorName);
			
			if (!floors.contains(floor)) {
				floors.add(floor);
				department.setFloors(floors);
			}
			/*List<Floor> employeeFloors = new ArrayList<Floor>();
			employeeFloors.add(floor);*/
			employee.setFloor(floor);
			
			
			XSSFCell locationCell = row.get(location_index);
			String locationName = XAccXLSXUtil.getCellValueAsString(locationCell);
			employee.setLocation(locationName);
			
			XSSFCell mobileNumberCell = row.get(mobileNumber_index);
			double mobileNumber = XAccXLSXUtil.getCellValueAsDouble(mobileNumberCell);
			employee.setMobileNumber(mobileNumber);
			
			XSSFCell genderCell = row.get(gender_index);
			String genderName = XAccXLSXUtil.getCellValueAsString(genderCell);
			if (genderName == null) {
				employee.setGender(GenderEnum.MALE);
			} else {
				GenderEnum genderEnum = genderName.equals(GenderEnum.MALE.getName()) ? GenderEnum.MALE : GenderEnum.FEMALE;
				employee.setGender(genderEnum);
			}
			
			XSSFCell bandCell = row.get(band_index);
			String bandName = XAccXLSXUtil.getCellValueAsString(bandCell);
			employee.setBand(bandName);
			
			employeeManager.bulkSave(floor, division, department, costCentre, employee);
		}
	}

}
