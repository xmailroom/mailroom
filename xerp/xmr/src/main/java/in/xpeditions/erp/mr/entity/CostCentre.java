/**
 * 
 */
package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPPrimaryEntity;

/**
 * @author Saran
 *
 */
@XERPPrimaryEntity
@Entity
@Table(name = CostCentre.TABLE_NAME)
public class CostCentre implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1921780093620591738L;

	public static final String TABLE_NAME = "mr_costcentre";

	private static final String COLUMN_ID = "id";

	public static final String COLUMN_CODE = "code";

	public static final String COLUMN_NAME = "name";

	public static final String COLUMN_DEPARTMENT = "department";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = CostCentre.COLUMN_ID, unique = true, nullable = false)
	private long id;
	
	@Column (name = CostCentre.COLUMN_CODE, unique = true)
	private String code;
	
	@Column (name = CostCentre.COLUMN_NAME, unique = true, nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = CostCentre.COLUMN_DEPARTMENT)
	private Department department;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CostCentre other = (CostCentre) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
