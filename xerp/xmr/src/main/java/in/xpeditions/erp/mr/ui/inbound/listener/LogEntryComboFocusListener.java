package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.LogEntrySelectionWindow;

public class LogEntryComboFocusListener implements FocusListener, InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -613927487631874156L;


	private XERPComboBox logEntryComboBox;

	@Override
	public void focus(FocusEvent event) {
		this.logEntryComboBox = (XERPComboBox) event.getSource();
		LogEntry logEntry = (LogEntry) this.logEntryComboBox.getValue();
		if (logEntry == null) {
			InboundDataChangeObserver inboundDataChangeObserver = new InboundDataChangeObserver();
			inboundDataChangeObserver.addListener(this);
			LogEntrySelectionWindow logEntrySelectionWindow = new LogEntrySelectionWindow(inboundDataChangeObserver,
					null);
			logEntrySelectionWindow.open();
		}
	}

	@Override
	public void onUpdate(Inbound inbound) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		this.logEntryComboBox.setValue(logEntry);
	}

	@Override
	public void onEmployeeChange(Employee employee) {
		
	}

}
