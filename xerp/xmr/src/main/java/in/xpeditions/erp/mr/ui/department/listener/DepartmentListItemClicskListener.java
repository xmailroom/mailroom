/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public class DepartmentListItemClicskListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5324511041396066920L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;

	public DepartmentListItemClicskListener(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<Department> nestingBeanItem = (NestingBeanItem<Department>) event.getItem();
		Department department = nestingBeanItem.getBean();
		this.departmentDataChangeObserver.notifySelectionChange(department);
	}

}
