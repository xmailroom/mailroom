/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.CostCentre;

/**
 * @author Saran
 *
 */
public class CostcentreDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7787244991691631810L;
	
	private List<CostcentreDataChangeListener> costcentreDataChangeListeners;

	public CostcentreDataChangeObserver() {
		this.costcentreDataChangeListeners = new ArrayList<CostcentreDataChangeListener>();
	}

	public void add(CostcentreDataChangeListener costcentreDataChangeListener) {
		this.costcentreDataChangeListeners.add(costcentreDataChangeListener);
	}

	public void remove(CostcentreDataChangeListener departmentDataChangeListener) {
		this.costcentreDataChangeListeners.remove(departmentDataChangeListener);
	}

	public void notifySelectionChange(CostCentre costCentre) {
		for (CostcentreDataChangeListener costcentreDataChangeListener : costcentreDataChangeListeners) {
			costcentreDataChangeListener.onSelectionChange(costCentre);
		}
	}

	public void notifyUpdate(CostCentre costCentre) {
		for (CostcentreDataChangeListener costcentreDataChangeListener : costcentreDataChangeListeners) {
			costcentreDataChangeListener.onUpdate(costCentre);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (CostcentreDataChangeListener costcentreDataChangeListener : costcentreDataChangeListeners) {
			costcentreDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifyEditButtonClick() {
		for (CostcentreDataChangeListener costcentreDataChangeListener : costcentreDataChangeListeners) {
			costcentreDataChangeListener.onEditButtonClick();
		}
	}

}
