package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.manager.InboundManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class InboundDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -976198990625798388L;

	private ResourceBundle bundle;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private Inbound inbound;

	public InboundDeleteConfirmListener(ResourceBundle bundle, InboundDataChangeObserver inboundDataChangeObserver,
			Inbound inbound) {
		this.bundle = bundle;
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.inbound = inbound;
	}

	@Override
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			InboundManager inboundManager = new InboundManager();
			try {
				inboundManager.delete(this.inbound);
				Notification.show(
						this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_DELETED_SUCCESSFULLY));
				this.inboundDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_FOREIGN_KEY_VALUE)
						+ " '" + this.inbound.getAwbNo() + "' " + "' Dated '"
						+ XERPUtil.formatDate(this.inbound.getDate(), DataHolder.commonUserDateTimeFormat) + "' ?"
						+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_HAS_REFERENCE);
				Notification.show(this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_DELETE_FAILED),
						subMessage, Type.ERROR_MESSAGE);
			} catch (XERPException e) {
				String subMessage = "<br/>" + e.getMessage();
				Notification.show(this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_DELETE_FAILED),
						subMessage, Type.ERROR_MESSAGE);

			}
		}
	}

}
