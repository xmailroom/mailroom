/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.CostCentre;

/**
 * @author Saran
 *
 */
public class CostCentreResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3103139359670214185L;
	private CostCentre costCentre;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostCentreResetButtonClickListener(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.costcentreDataChangeObserver.notifySelectionChange(this.costCentre);
		event.getButton().setEnabled(true);
	}

	public void setCostCentre(CostCentre costCentre) {
		this.costCentre = costCentre;
	}

}
