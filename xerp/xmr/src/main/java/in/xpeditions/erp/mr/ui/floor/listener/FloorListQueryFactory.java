/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author xpeditions
 *
 */
public class FloorListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -778041010293651816L;
	private String searchString;

	public FloorListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		FloorListQuery countryListQuery = new FloorListQuery(searchString, queryDefinition);
		return countryListQuery;
	}
}