/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class DivisionEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7833762900778794802L;
	private DivisionDataChangeObserver divisionDataChangeObserver;

	public DivisionEditButtonClickListener(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.divisionDataChangeObserver.notifyEditButtonClick();
		button.setEnabled(true);
	}

}
