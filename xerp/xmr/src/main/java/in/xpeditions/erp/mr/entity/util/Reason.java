package in.xpeditions.erp.mr.entity.util;

public enum Reason {
	DISCREPANCY("Discrepancy"), 
	NOT_IN_SEAT("Not in Seat"), 
	OUT_FOR_LUNCH("Out for Lunch"), 
	REDIRECTED("Redirected"), 
	REFUSED("Refused"), 
	RESIGNED("Resigned"), 
	RETIRED("Retired"), 
	SHIFTED("Shifted"), 
	TIME_CONSTRAINT("Time Constraint"), 
	ON_LEAVE("On Leave"), 
	WEEKLY_OFF("Weekly Off"), 
	DOUBLE_ENTRY("Double Entry"), 
	WRONG_ENTRY("Wrong Entry"), 
	DEPT_CLOSED("dept Closed"), 
	UNCLAIMED("Unclaimed"), 
	BUSY_WITH_CUSTOMER("Busy With Customer");

	public static final String CAPTION_PROPERTY_ID = "name";

	private String name;

	private Reason(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static Reason get(int i) {
		return Reason.values()[i];
	}

}
