package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.mr.entity.Employee;

public class EmployeeValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8207612578333798448L;

	private XERPTextField branchField;

	public EmployeeValueChangeListener(XERPTextField branchField) {
		this.branchField = branchField;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox employeeCombo = (XERPComboBox) event.getProperty();
		Employee employee = (Employee) employeeCombo.getValue();
		if (employee != null) {
			this.branchField.setValue(employee.getBranch().getName());
		} else {
			this.branchField.setValue("");
		}
	}

}
