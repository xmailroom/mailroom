package in.xpeditions.erp.mr.ui.deliverysheet;

import java.util.Date;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPDateField;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.XERPWindow;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetSaveConfirmClickListener;

public class ReceivedByEditWindow extends XERPWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2986895104091215142L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private BeanFieldGroup<DeliverySheet> deliverySheetBinder;

	private ResourceBundle bundle;

	private VerticalLayout verticalLayout;

	public ReceivedByEditWindow(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		super(true);
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheetBinder = deliverySheetBinder;
		this.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_HEADER);
		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.setCaption(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_RECEIVED_BY));
		this.setModal(true);
		this.setWidth("500px");
		this.setHeight("200px");
		this.verticalLayout = new VerticalLayout();
		this.verticalLayout.setMargin(new MarginInfo(false, false, false, true));

		FormLayout formLayout = new FormLayout();
		formLayout.setSpacing(true);
		this.verticalLayout.addComponent(formLayout);

		XERPTextField receivedByField = new XERPTextField();
		receivedByField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_200_PX);
		receivedByField.setRequired(true);
		receivedByField.setCaption(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_RECEIVED_BY));
		this.deliverySheetBinder.bind(receivedByField, DeliverySheet.COLUMN_RECEIVED_BY);
		formLayout.addComponent(receivedByField);

		XERPDateField receivedTimeField = new XERPDateField();
		receivedTimeField.setValue(new Date(System.currentTimeMillis()));
		this.deliverySheetBinder.bind(receivedTimeField, DeliverySheet.COLUMN_RECEIVED_TIME);

		XERPButton okButton = new XERPButton();
		okButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_SAVE));
		okButton.setIcon(FontAwesome.CHECK);
		okButton.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_FRIENDLY);
		okButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		okButton.addClickListener(new DeliverySheetSaveConfirmClickListener(this.deliverySheetDataChangeObserver, this,
				this.deliverySheetBinder));
		this.verticalLayout.addComponent(okButton);

	}

	public void open() {
		this.setContent(this.verticalLayout);
		UI current = UI.getCurrent();
		current.addWindow(this);
		current.setFocusedComponent(this);
	}

}
