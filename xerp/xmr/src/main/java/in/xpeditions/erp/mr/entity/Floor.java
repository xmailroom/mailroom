/**
 * 
 */
package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPPrimaryEntity;

/**
 * @author Saran
 *
 */
@XERPPrimaryEntity
@Entity
@Table(name = Floor.TABLE_NAME)
public class Floor implements Serializable {

	/**
		 * 
		 */
	private static final long serialVersionUID = -3953622875447101997L;

	public static final String TABLE_NAME = "mr_floor";

	private static final String COLUMN_ID = "id";

	public static final String COLUMN_NAME = "name";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Floor.COLUMN_ID, unique = true, nullable = false)
	private long id;
	
	@Column (name = Floor.COLUMN_NAME, unique = true)
	private String name;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	
}
