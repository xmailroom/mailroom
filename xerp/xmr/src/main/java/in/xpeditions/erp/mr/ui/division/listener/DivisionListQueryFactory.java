/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author Saran
 *
 */
public class DivisionListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8996127144319814023L;
	private String searchString;

	public DivisionListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		DivisionListQuery divisionListQuery = new DivisionListQuery(searchString, queryDefinition);
		return divisionListQuery;
	}

}
