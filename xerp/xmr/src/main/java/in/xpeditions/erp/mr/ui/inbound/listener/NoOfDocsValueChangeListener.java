package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.HashMap;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.To;

public class NoOfDocsValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3840921243781941295L;

	private HashMap<BeanFieldGroup<To>, Integer> noOfDocsMap;

	private BeanFieldGroup<To> toBinder;

	public NoOfDocsValueChangeListener(HashMap<BeanFieldGroup<To>, Integer> noOfDocsMap, BeanFieldGroup<To> toBinder) {
		this.noOfDocsMap = noOfDocsMap;
		this.toBinder = toBinder;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Object value = event.getProperty().getValue();
		int noOfDocs = XERPUtil.sum(value, 0).intValue();
		this.noOfDocsMap.put(this.toBinder, noOfDocs);
	}

}
