package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.LogEntryDAOManager;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

public class LogEntryManager {
	public void save(LogEntry logEntry) throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		logEntryDAOManager.save(logEntry);
	}

	public void update(LogEntry logEntry) throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		logEntryDAOManager.update(logEntry);
	}

	public List<LogEntry> list() throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		return logEntryDAOManager.list();
	}

	public LogEntry getBy(LogEntry logEntry) throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		return logEntryDAOManager.getBy(logEntry);
	}

	public void delete(LogEntry logEntry) throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		logEntryDAOManager.delete(logEntry);
	}

	public List<Object[]> listInbound() throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		return logEntryDAOManager.listInbound();
	}

	public List<LogEntry> listPending() throws XERPException {
		LogEntryDAOManager logEntryDAOManager = (LogEntryDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY);
		return logEntryDAOManager.listPending();
	}

}
