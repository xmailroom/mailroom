/**
 * 
 */
package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.annotations.XERPPrimaryEntity;
import in.xpeditions.erp.mr.entity.util.EmployeeNature;
import in.xpeditions.erp.mr.entity.util.GenderEnum;

/**
 * @author Saran
 *
 */
@XERPPrimaryEntity
@Entity
@Table (name = Employee.TABLE_NAME)
public class Employee implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -79029053656414556L;
	
	public static final String TABLE_NAME = "mr_employee";
	
	private static final String COLUMN_ID = "id";
	
	public static final String COLUMN_NAME = "name";
	
	public static final String COLUMN_BRANCH = "branch";
	
	public static final String COLUMN_DIVISION = "division";
	
	public static final String COLUMN_DEPARTMENT = "department";
	
	public static final String COLUMN_COSTCENTER = "costCetre";
	
	public static final String COLUMN_EMPLOYEE_NATURE = "employeeNature";
	
	public static final String COLUMN_EMPLOYEEID = "employeeId";
	
	public static final String COLUMN_EMAIL = "email";
	
	public static final String COLUMN_EXTENSION = "extension";
	
	public static final String COLUMN_CABIN = "cabin";
	
	public static final String COLUMN_BUILDING_NAME = "buildingName";
	
	public static final String COLUMN_FLOOR = "floor";
	
	/*private static final String JOIN_TABLE_EMPLOYEE_FLOOR = "mr_employee_floor";

	private static final String JOIN_COLUMN_EMPLOYEE_ID = "empId";

	private static final String INVERSE_JOIN_COLUMN_FLOOR_ID = "floorId";*/
	
	public static final String COLUMN_LOCATION = "location";
	
	public static final String COLUMN_MOBILE_NUMBER = "mobileNumber";
	
	public static final String COLUMN_GENDER= "gender";
	
	public static final String COLUMN_BAND = "band";
	
	public static final String COLUMN_ACTIVE = "active";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Employee.COLUMN_ID, unique = true, nullable = false)
	private long id;
	
	@Column(name = Employee.COLUMN_NAME, nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = Employee.COLUMN_BRANCH)
	private Branch branch;
	
	@ManyToOne
	@JoinColumn(name = Employee.COLUMN_DIVISION)
	private Division division;
	
	@ManyToOne
	@JoinColumn(name = Employee.COLUMN_DEPARTMENT)
	private Department department;
	
	@ManyToOne
	@JoinColumn(name = Employee.COLUMN_COSTCENTER)
	private CostCentre costCetre;
	
	@Column(name = Employee.COLUMN_EMPLOYEE_NATURE, nullable = false)
	private EmployeeNature employeeNature;
	
	@Column(name = Employee.COLUMN_EMPLOYEEID, unique = true, nullable = false)
	private String employeeId;
	
	@Column(name = Employee.COLUMN_EMAIL, nullable = false)
	private String emailId;
	
	@Column(name = Employee.COLUMN_EXTENSION)
	private String extension;
	
	@Column(name = Employee.COLUMN_CABIN)
	private String cabin;
	
	@Column(name = Employee.COLUMN_BUILDING_NAME)
	private String buildingName;
	
	/*@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = Employee.JOIN_TABLE_EMPLOYEE_FLOOR, joinColumns = { @JoinColumn(name = Employee.JOIN_COLUMN_EMPLOYEE_ID) }, inverseJoinColumns = { @JoinColumn(name = Employee.INVERSE_JOIN_COLUMN_FLOOR_ID) })
	private List<Floor> floors;*/
	
	@ManyToOne
	@JoinColumn(name = Employee.COLUMN_FLOOR)
	private Floor floor;
	
	@Column(name = Employee.COLUMN_LOCATION)
	private String location;
	
	@Column(name = Employee.COLUMN_MOBILE_NUMBER)
	private double mobileNumber;
	
	@Column(name = Employee.COLUMN_GENDER)
	private GenderEnum gender;
	
	@Column(name = Employee.COLUMN_BAND)
	private String band;
	
	@Column(name = Employee.COLUMN_ACTIVE)
	private boolean active;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the branch
	 */
	public Branch getBranch() {
		return branch;
	}

	/**
	 * @param branch the branch to set
	 */
	public void setBranch(Branch branch) {
		this.branch = branch;
	}

	/**
	 * @return the division
	 */
	public Division getDivision() {
		return division;
	}

	/**
	 * @param division the division to set
	 */
	public void setDivision(Division division) {
		this.division = division;
	}

	/**
	 * @return the department
	 */
	public Department getDepartment() {
		return department;
	}

	/**
	 * @param department the department to set
	 */
	public void setDepartment(Department department) {
		this.department = department;
	}

	/**
	 * @return the costCetre
	 */
	public CostCentre getCostCetre() {
		return costCetre;
	}

	/**
	 * @param costCetre the costCetre to set
	 */
	public void setCostCetre(CostCentre costCetre) {
		this.costCetre = costCetre;
	}

	/**
	 * @return the employeeNature
	 */
	public EmployeeNature getEmployeeNature() {
		return employeeNature;
	}

	/**
	 * @param employeeNature the employeeNature to set
	 */
	public void setEmployeeNature(EmployeeNature employeeNature) {
		this.employeeNature = employeeNature;
	}

	/**
	 * @return the employeeId
	 */
	public String getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId the employeeId to set
	 */
	public void setEmployeeId(String employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the emailId
	 */
	public String getEmailId() {
		return emailId;
	}

	/**
	 * @param emailId the emailId to set
	 */
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	/**
	 * @return the extension
	 */
	public String getExtension() {
		return extension;
	}

	/**
	 * @param extension the extension to set
	 */
	public void setExtension(String extension) {
		this.extension = extension;
	}

	/**
	 * @return the cabin
	 */
	public String getCabin() {
		return cabin;
	}

	/**
	 * @param cabin the cabin to set
	 */
	public void setCabin(String cabin) {
		this.cabin = cabin;
	}

	/**
	 * @return the buildingName
	 */
	public String getBuildingName() {
		return buildingName;
	}

	/**
	 * @param buildingName the buildingName to set
	 */
	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	/**
	 * @return the floors
	 */
	/*public List<Floor> getFloors() {
		return floors;
	}

	*//**
	 * @param floors the floors to set
	 *//*
	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}*/

	/**
	 * @return the floor
	 */
	public Floor getFloor() {
		return floor;
	}

	/**
	 * @param floor the floor to set
	 */
	public void setFloor(Floor floor) {
		this.floor = floor;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the mobileNumber
	 */
	public double getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(double mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/**
	 * @return the gender
	 */
	public GenderEnum getGender() {
		return gender;
	}

	/**
	 * @param gender the gender to set
	 */
	public void setGender(GenderEnum gender) {
		this.gender = gender;
	}

	/**
	 * @return the band
	 */
	public String getBand() {
		return band;
	}

	/**
	 * @param band the band to set
	 */
	public void setBand(String band) {
		this.band = band;
	}

	/**
	 * @return the active
	 */
	public boolean isActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(boolean active) {
		this.active = active;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
