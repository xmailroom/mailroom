package in.xpeditions.erp.mr.general;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.event.ShortcutListener;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

import in.xpeditions.erp.commons.annotations.XERPMenuProvider;
import in.xpeditions.erp.commons.ui.XERPMenuCreator;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.menu.handler.CostcentreMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.DepartmentMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.DivisionMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.EmployeeMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.FloorMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.InboundLogMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.InboundMenuHandler;
import in.xpeditions.erp.mr.ui.menu.handler.MailRoomMenuChangeHandler;
import in.xpeditions.erp.mr.ui.menu.handler.MailRoomMenuShortcutListener;
import in.xpeditions.erp.mr.ui.menu.handler.ServiceProviderMenuHandler;

/**
 * @author prince
 *
 */
@XERPMenuProvider(menuName = "Mail Room Menu")
public class MailRoomMenuCreator implements XERPMenuCreator {

	private MenuBar menuBar;
	private Layout container;
	private Label breadCumbLabel;

	public MailRoomMenuCreator(MenuBar menuBar, Layout container, Label breadCumbLabel) {
		this.menuBar = menuBar;
		this.container = container;
		this.breadCumbLabel = breadCumbLabel;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see in.xpeditions.erp.commons.ui.XERPMenuCreator#createMenus()
	 */
	public void createMenus() {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		MailRoomMenuChangeHandler menuChangeHandler = new MailRoomMenuChangeHandler(container, breadCumbLabel);
		
		MenuItem mMasterMenu = this.menuBar.addItem("Masters",
				null);
		mMasterMenu.setStyleName("menu-float-left");
		Command divisionMenuHandler = new DivisionMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION.asPermission().getDisplayName(),
				divisionMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION
						.asPermission().getName(), null);
		
		Command departmentMenuHandler = new DepartmentMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT
				.asPermission().getDisplayName(),
				departmentMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT
						.asPermission().getName(), null);
		
		Command costcentreMenuHandler = new CostcentreMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE
				.asPermission().getDisplayName(),
				costcentreMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE
						.asPermission().getName(), null);
		
		Command floorMenuHandler = new FloorMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR
				.asPermission().getDisplayName(),
				floorMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR
						.asPermission().getName(), null);
		
		Command employeeMenuHandler = new EmployeeMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE
				.asPermission().getDisplayName(),
				employeeMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE
						.asPermission().getName(), null);
		
		Command serviceProviderMenuHandler = new ServiceProviderMenuHandler(menuChangeHandler);
		mMasterMenu.addItem(MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER
				.asPermission().getDisplayName(),
				serviceProviderMenuHandler, MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER
						.asPermission().getName(), null);
		
		MenuItem masterMenu = this.menuBar.addItem(bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM),
				null);
		masterMenu.setStyleName("menu-float-left");

		/*SPMenuHandler spMenuHandler = new SPMenuHandler(menuChangeHandler);
		masterMenu.addItem(bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_SP), spMenuHandler,
				MailRoomPermissionEnum.MENU_MAIL_ROOM_SP.asPermission().getName(), null);*/

		masterMenu.setStyleName("menu-float-left");
		InboundLogMenuHandler inboundLogMenuHandler = new InboundLogMenuHandler(menuChangeHandler);
		MenuItem inboundLogMenuItem = masterMenu.addItem(
				bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND_LOG), inboundLogMenuHandler,
				MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND_LOG.asPermission().getName(), null);

		ShortcutListener menuShortCutListener = new MailRoomMenuShortcutListener("", KeyCode.L,
				new int[] { ModifierKey.ALT }, inboundLogMenuHandler, inboundLogMenuItem);
		this.menuBar.addShortcutListener(menuShortCutListener);

		InboundMenuHandler inboundMenuHandler = new InboundMenuHandler(menuChangeHandler);
		MenuItem inboundMenuItem = masterMenu.addItem(
				bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND), inboundMenuHandler,
				MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND.asPermission().getName(), null);

		menuShortCutListener = new MailRoomMenuShortcutListener("", KeyCode.I, new int[] { ModifierKey.ALT },
				inboundMenuHandler, inboundMenuItem);
		this.menuBar.addShortcutListener(menuShortCutListener);

	}
}