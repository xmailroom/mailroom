package in.xpeditions.erp.mr.ui.deliverysheet;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.ui.XERPWindow;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;

public class DeliverySheetEditWindow extends XERPWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2986895104091215142L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private DeliverySheet deliverySheet;

	private ResourceBundle bundle;

	private DeliverySheetEditContainer deliverySheetEditComponent;

	public DeliverySheetEditWindow(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			DeliverySheet deliverySheet) {
		super(true);
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheet = deliverySheet;
		this.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_HEADER);
		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.setCaption(this.bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_DELIVERY_SHEET));
		this.setModal(true);
		this.setWidth("100%");
		this.setHeight("100%");
		this.deliverySheetEditComponent = new DeliverySheetEditContainer(this.deliverySheetDataChangeObserver, this,
				this.deliverySheet);
	}

	public void open() {
		this.setContent(this.deliverySheetEditComponent);
		UI current = UI.getCurrent();
		current.addWindow(this);
		current.setFocusedComponent(this);
	}

}
