/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class EmployeeMoveDepartmentButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1238746296199435439L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;

	public EmployeeMoveDepartmentButtonClickListener(EmployeeDataChangeObserver employeeDataChangeObserver) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.employeeDataChangeObserver.notifyMoveEmployee();
		button.setEnabled(true);
	}

}
