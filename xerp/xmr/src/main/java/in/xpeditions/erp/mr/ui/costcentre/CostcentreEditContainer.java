/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre;

import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDangerButton;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostCentreDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostCentreEditButtonClickListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostCentreResetButtonClickListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostCentreSaveButtonClickListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreDataChangeListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreDataChangeObserver;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author Saran
 *
 */
public class CostcentreEditContainer extends VerticalLayout implements CostcentreDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8575751396269790146L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;
	private boolean canAdd;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	private BeanFieldGroup<CostCentre> binder;
	private CostCentre costCentre;
	private XERPFriendlyButton saveButton;
	private CostCentreDeleteButtonClickListener costCentreDeleteButtonClickListener;
	private CostCentreResetButtonClickListener costCentreResetButtonClickListener;

	public CostcentreEditContainer(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
		this.costcentreDataChangeObserver.add(this);
		
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent()
				.getPage()
				.setTitle(
						bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS_COSTCENTRE));
		canAdd = XERPUtil.hasPermission(MailRoomPermissionEnum.ADD_COSTCENTRE
				.asPermission().getName());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_COSTCENTRE
				.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_COSTCENTRE
				.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_COSTCENTRE
				.asPermission().getName());
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		this.setMargin(marginInfo);
		createEditContainer(bundle);
	}

	private void createEditContainer(ResourceBundle bundle) {
		this.binder = new BeanFieldGroup<CostCentre>(CostCentre.class);
		this.binder.setBuffered(true);

		this.costCentre = initialize();
		HorizontalLayout editBarLayout = new HorizontalLayout();
		MarginInfo editMarginInfo = new MarginInfo(true, true, false, false);
		editBarLayout.setMargin(editMarginInfo);
		editBarLayout.setVisible(false);
		this.addComponent(editBarLayout, 0);
		this.setComponentAlignment(editBarLayout, Alignment.TOP_RIGHT);

		String editActions[] = {
				MailRoomPermissionEnum.EDIT_COSTCENTRE.asPermission().getName(),
				MailRoomPermissionEnum.DELETE_COSTCENTRE.asPermission().getName() };
		CostCentreEditButtonClickListener costCentreEditButtonClickListener = new CostCentreEditButtonClickListener(
				this.costcentreDataChangeObserver);
		XERPDangerButton editButton = new XERPDangerButton(editActions);
		createFormButton(editButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE_EDIT),
				editBarLayout, FontAwesome.EDIT,
				costCentreEditButtonClickListener, true);

		FormLayout formLayout = new FormLayout();

		DepartmentManager departmentManager = new DepartmentManager();
		List<Department> departments = null;
		try {
			departments = departmentManager.listDepartments(0, 0, null, null, null);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		BeanItemContainer<Department> departmentContainer = new BeanItemContainer<Department>(
				Department.class);
		departmentContainer.addAll(departments);

		XERPComboBox departmentfield = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_COSTCENTRE_DEPARTMENT),
				XMailroomFormItemConstants.COSTCENTRE_DEPARTMENT, departmentContainer,
				XMailroomFormItemConstants.COSTCENTRE_DEPARTMENT_NAME, null, null, true, bundle.getString(MailRoomMessageConstants.COSTCENTRE_DEPARTMENT_NAME_REQUIRED));
		departmentfield.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(departmentfield);

		XERPTextField nameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_COSTCETRE_NAME),
				CostCentre.COLUMN_NAME,
				CostCentre.COLUMN_NAME,
				true, bundle.getString(MailRoomMessageConstants.NAME_REQUIRED));
		nameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		nameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		formLayout.addComponent(nameField);

		XERPTextField codeField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_COSTCENTRE_CODE),
				CostCentre.COLUMN_CODE,
				CostCentre.COLUMN_CODE,
				false, "");
		codeField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		formLayout.addComponent(codeField);

		this.addComponent(formLayout, 1);

		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		this.addComponent(buttonBarLayoutContainer, 2);
		this.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(false);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		CostCentreSaveButtonClickListener costCentreSaveButtonClickListener = new CostCentreSaveButtonClickListener(
				this.costcentreDataChangeObserver, this.binder, this.canEdit);
		String saveActions[] = {
				MailRoomPermissionEnum.ADD_COSTCENTRE.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_COSTCENTRE.asPermission().getName() };
		saveButton = new XERPFriendlyButton(saveActions);
		createFormButton(saveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE),
				buttonBarLayout, FontAwesome.SAVE,
				costCentreSaveButtonClickListener, true);
		saveButton.setClickShortcut(KeyCode.ENTER);

		this.costCentreDeleteButtonClickListener = new CostCentreDeleteButtonClickListener(
				this.costcentreDataChangeObserver);
		String deleteActions[] = { MailRoomPermissionEnum.DELETE_COSTCENTRE
				.asPermission().getName() };
		XERPDangerButton deleteButton = new XERPDangerButton(deleteActions);
		createFormButton(deleteButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_DELETE),
				buttonBarLayout, FontAwesome.TRASH_O,
				costCentreDeleteButtonClickListener, true);

		this.costCentreResetButtonClickListener = new CostCentreResetButtonClickListener(
				this.costcentreDataChangeObserver);
		String resetActions[] = {
				MailRoomPermissionEnum.ADD_COSTCENTRE.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_COSTCENTRE.asPermission().getName() };
		XERPPrimaryButton resetButton = new XERPPrimaryButton(resetActions);
		createFormButton(resetButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_RESET),
				buttonBarLayout, FontAwesome.UNDO,
				costCentreResetButtonClickListener, true);

		handleSelectionChange(this.costCentre);
	}

	private void handleSelectionChange(CostCentre costCentre) {
		costCentreContainerFieldProperties(false);
		if (costCentre == null) {
			costCentre = initialize();
		}
		this.costCentre = costCentre;
		this.binder.setItemDataSource(costCentre);
		if (costCentre.getId() > 0) {
			HorizontalLayout editBarLayout = (HorizontalLayout) this
					.getComponent(0);
			editBarLayout.setVisible(true);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(false);
		}
		costCentreContainerFieldProperties(true);
		handleButtons(costCentre);
	}

	private CostCentre initialize() {
		CostCentre costCentre = new CostCentre();
		costCentre.setCode("");
		costCentre.setName("");
		return costCentre;
	}
	
	private void costCentreContainerFieldProperties(boolean readOnly) {
		Collection<Field<?>> fields = this.binder.getFields();
		for (Field<?> field : fields) {
			if (field instanceof XERPComboBox) {
				if (((XERPComboBox) field).size() == 1) {
					((XERPComboBox) field).setValue(((XERPComboBox) field)
							.getItemIds().iterator().next());
					field.setReadOnly(true);
					continue;
				} else {
					field.setReadOnly(readOnly);
				}
			}
			field.setReadOnly(readOnly);
		}
	}
	
	private void handleButtons(CostCentre costCentre) {
		boolean isExistState = costCentre.getId() > 0;
		HorizontalLayout buttonBarLayoutContainer = (HorizontalLayout) this
				.getComponent(2);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarLayoutContainer
				.getComponent(0);
		XERPDangerButton deleteButton = (XERPDangerButton) buttonBarLayout
				.getComponent(1);
		deleteButton.setVisible(isExistState && canDelete);
		if (isExistState) {
			this.costCentreDeleteButtonClickListener.setCostCentre(costCentre);
		}
		this.costCentreResetButtonClickListener.setCostCentre(costCentre);
	}
	
	private XERPTextField createFormTextField(String caption,
			String propertyId, String inputPrompt, boolean isRequired, String errorMessage) {
		XERPTextField textField = new XERPTextField();
		textField.setCaption(caption);
		textField.setRequired(isRequired);
		textField.setRequiredError(errorMessage);
		textField.setImmediate(true);
		textField.setInputPrompt(inputPrompt);
		textField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		binder.bind(textField, propertyId);
		return textField;
	}

	private XERPComboBox createFormCombobox(String caption, String propertyId,
			BeanItemContainer<?> itemContainer, String itemCaptionPropertyId,
			Layout layout, String defaultValue, boolean isRequired, String errorMessage) {
		XERPComboBox comboBox = new XERPComboBox();
		comboBox.setCaption(caption);
		comboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		comboBox.setNullSelectionAllowed(false);
		comboBox.setImmediate(true);
		comboBox.setContainerDataSource(itemContainer);
		comboBox.setRequired(isRequired);
		if (isRequired) {
			comboBox.setRequiredError(errorMessage);
		}
		comboBox.setItemCaptionPropertyId(itemCaptionPropertyId);
		this.binder.bind(comboBox, propertyId);
		if (defaultValue != null && !defaultValue.isEmpty()) {
			comboBox.setValue(defaultValue);
		}
		if (layout != null) {
			layout.addComponent(comboBox);
		}
		return comboBox;
	}

	private void createFormButton(XERPButton button, String caption,
			Layout layout, FontAwesome icon, ClickListener buttonClickListener,
			boolean isEnabled) {
		button.setCaption(caption);
		button.setDisableOnClick(true);
		button.addClickListener(buttonClickListener);
		button.setIcon(icon);
		button.setEnabled(isEnabled);
		button.setImmediate(true);
		layout.addComponent(button);
	}

	@Override
	public void onSelectionChange(CostCentre costCentre) {
		handleSelectionChange(costCentre);
	}

	@Override
	public void onUpdate(CostCentre costCentre) {
		if (costCentre != null && costCentre.getId() > 0) {
			handleSelectionChange(costCentre);
		} else {
			costCentre = initialize();
			this.costCentre = costCentre;
			this.binder.setItemDataSource(costCentre);
			HorizontalLayout editBarContainer = (HorizontalLayout) this
					.getComponent(0);
			editBarContainer.setVisible(false);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(true);
			onEditButtonClick();
		}
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEditButtonClick() {
		boolean readOnly = canView && (canEdit || canAdd) ? !canView : canView;
		costCentreContainerFieldProperties(readOnly);
		HorizontalLayout buttonBarContainer = (HorizontalLayout) this
				.getComponent(2);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
				.getComponent(0);
		buttonBarLayout.setVisible(true);
		handleButtons(costCentre);
	}

}
