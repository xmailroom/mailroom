/**
 * 
 */
package in.xpeditions.erp.mr.ui.department;

import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPCheckBox;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDangerButton;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.DivisionManager;
import in.xpeditions.erp.mr.manager.FloorManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDataChangeListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDataChangeObserver;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentEditButtonClickListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentResetButtonClickListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentSaveButtonClickListener;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class DepartmentEditContainer extends VerticalLayout implements DepartmentDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2342963996225045918L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private boolean canAdd;
	private boolean canEdit;
	private boolean canView;
	private BeanFieldGroup<Department> binder;
	private Department department;
	private XERPDangerButton editButton;
	private Table floorTable;
	private List<Floor> floors;
	private XERPFriendlyButton saveButton;
	private XERPDangerButton deleteButton;
	private DepartmentDeleteButtonClickListener departmentDeleteButtonClickListener;
	private DepartmentResetButtonClickListener departmentResetButtonClickListener;
	
	public DepartmentEditContainer(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
		this.departmentDataChangeObserver.add(this);
		
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent()
				.getPage()
				.setTitle(
						bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS_DEPARTMENT));
		canAdd = XERPUtil.hasPermission(MailRoomPermissionEnum.ADD_DEPARTMENT
				.asPermission().getName());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_DEPARTMENT
				.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_DEPARTMENT
				.asPermission().getName());
		/* this.setSizeFull(); */
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		this.setMargin(marginInfo);
		createEditContainer(bundle);
	}

	private void createEditContainer(ResourceBundle bundle) {
		this.binder = new BeanFieldGroup<Department>(Department.class);
		this.binder.setBuffered(true);

		this.department = initialize();

		HorizontalLayout editBarLayout = new HorizontalLayout();
		MarginInfo editMarginInfo = new MarginInfo(true, true, false, false);
		editBarLayout.setMargin(editMarginInfo);
		editBarLayout.setVisible(false);
		this.addComponent(editBarLayout);
		this.setComponentAlignment(editBarLayout, Alignment.TOP_RIGHT);

		String editActions[] = {
				MailRoomPermissionEnum.EDIT_DEPARTMENT.asPermission().getName(),
				MailRoomPermissionEnum.DELETE_DEPARTMENT.asPermission()
						.getName() };
		DepartmentEditButtonClickListener departmentEditButtonClickListener = new DepartmentEditButtonClickListener(
				this.departmentDataChangeObserver);
		editButton = new XERPDangerButton(editActions);
		createFormButton(editButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE_EDIT),
				editBarLayout, FontAwesome.EDIT,
				departmentEditButtonClickListener, true);		

		FormLayout formLayout = new FormLayout();
		
		DivisionManager divisionManager = new DivisionManager();
		List<Division> listAllDivisions = null;
		try {
			listAllDivisions = divisionManager.listDivisions(0, 0, null, null, null);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		BeanItemContainer<Division> divisionContainer = new BeanItemContainer<Division>(Division.class);
		divisionContainer.addAll(listAllDivisions);
		
		XERPComboBox divisionCombo = createFormCombobox(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_DEPARTMENT_DIVISION),
				XMailroomFormItemConstants.DEPARTMENT_DIVISION, divisionContainer, XMailroomFormItemConstants.DEPARTMENT_DIVISION_NAME,
				null, null, true, bundle.getString(MailRoomMessageConstants.DEPARTMENT_DIVISION_NAME_REQUIRED));
		formLayout.addComponent(divisionCombo);

		XERPTextField nameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_DEPARTMENT_NAME),
				XMailroomFormItemConstants.DIVISION_NAME,
				bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_DEPARTMENT_NAME),
				true, bundle.getString(MailRoomMessageConstants.DEPARTMENT_NAME_REQUIRED));
		nameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		nameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(nameField);

		XERPTextField codeField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_DEPARTMENT_CODE),
				XMailroomFormItemConstants.DEPARTMENT_CODE,
				bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_DEPARTMENT_CODE),
				true, bundle.getString(MailRoomMessageConstants.DEPARTMENT_CODE_REQUIRED));
		codeField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		codeField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		formLayout.addComponent(codeField);

		this.addComponent(formLayout);
		
		HorizontalLayout tableLayout = new HorizontalLayout();
		this.addComponent(tableLayout);
		floorTable = new Table();
		tableLayout.addComponent(floorTable);
		floorTable.setWidth("250px");
		floorTable.setHeight("250px");
		
		floorTable.addContainerProperty(XMailroomTableItemConstants.DEPARTMENT_FLOOR_SELECT, XERPCheckBox.class, null);
		floorTable.addContainerProperty(XMailroomTableItemConstants.DEPARTMENT_FLOOR_NAME, String.class, "");
		
		floorTable.setColumnHeader(XMailroomTableItemConstants.DEPARTMENT_FLOOR_SELECT, bundle.getString(MailRoomMessageConstants.LIST_HEADER_DEPARTMENT_FLOOR));
		floorTable.setColumnHeader(XMailroomTableItemConstants.DEPARTMENT_FLOOR_NAME, bundle.getString(MailRoomMessageConstants.LIST_HEADER_DEPARTMENT_FLOOR_NAME));
		
		floorTable.setColumnWidth(XMailroomTableItemConstants.DEPARTMENT_FLOOR_SELECT, 50);
		
		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		/* buttonBarLayoutContainer.setWidth("100%"); */
		this.addComponent(buttonBarLayoutContainer);
		this.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(false);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		DepartmentSaveButtonClickListener divisionSaveButtonClickListener = new DepartmentSaveButtonClickListener(
				this.departmentDataChangeObserver, this.binder, this.canEdit, this.floorTable);
		String saveActions[] = {
				MailRoomPermissionEnum.ADD_DEPARTMENT.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_DEPARTMENT.asPermission().getName() };
		this.saveButton = new XERPFriendlyButton(saveActions);
		createFormButton(saveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE),
				buttonBarLayout, FontAwesome.SAVE,
				divisionSaveButtonClickListener, true);
		this.saveButton.setClickShortcut(KeyCode.ENTER, null);
		
		this.departmentDeleteButtonClickListener = new DepartmentDeleteButtonClickListener(
				this.departmentDataChangeObserver);
		String deleteActions[] = { MailRoomPermissionEnum.DELETE_DEPARTMENT
				.asPermission().getName() };
		deleteButton = new XERPDangerButton(deleteActions);
		createFormButton(deleteButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_DELETE),
				buttonBarLayout, FontAwesome.TRASH_O,
				departmentDeleteButtonClickListener, true);

		this.departmentResetButtonClickListener = new DepartmentResetButtonClickListener(
				this.departmentDataChangeObserver);
		String resetActions[] = {
				MailRoomPermissionEnum.ADD_DEPARTMENT.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_DEPARTMENT.asPermission().getName() };
		XERPPrimaryButton resetButton = new XERPPrimaryButton(resetActions);
		createFormButton(resetButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_RESET),
				buttonBarLayout, FontAwesome.UNDO,
				departmentResetButtonClickListener, true);

		handleSelectionChange(this.department);
	}

	private void handleSelectionChange(Department department) {
		if (department == null) {
			department = initialize();
		}
		this.department = department;
		this.binder.setItemDataSource(department);
		if (department.getId() > 0) {
			HorizontalLayout editBarLayout = (HorizontalLayout) this
					.getComponent(0);
			editBarLayout.setVisible(true);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(3);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(false);
			
			FloorManager floorManager = new FloorManager(); 
			try {
				this.floors = floorManager.listBy(department);
			} catch (XERPException e) {
				e.printStackTrace();
			}
		
		}
		refreshFloorTable();
		departmentContainerFieldProperties(true);
		handleButtons(department);
		
	}

	private void refreshFloorTable() {
		this.floorTable.removeAllItems();
		FloorManager floorManager = new FloorManager();
		List<Floor> allFloors = null; 
		try {
			allFloors = floorManager.listAll();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		if (allFloors != null && allFloors.size() > 0) {
			for (Floor floor : allFloors) {
				Object[] objects = new Object[2];
				XERPCheckBox floorCheckBox = new XERPCheckBox();
				floorCheckBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
				floorCheckBox.setData(floor);
				
				if (floors != null && floors.size() > 0) {
					for (Floor deptFloor : floors) {
						if (deptFloor.getId() == floor.getId()) {
							floorCheckBox.setValue(true);
						}
					}
				}
				objects[0] = floorCheckBox;
				
				String floorName = floor.getName();
				objects[1] = floorName;
				
				this.floorTable.addItem(objects, floorCheckBox);
			}
		}
	}

	private void handleButtons(Department department) {
		boolean isExistDepartment = department.getId() > 0;
		this.deleteButton.setVisible(isExistDepartment);
		if (isExistDepartment) {
			this.departmentDeleteButtonClickListener.setDepartment(department);
		}
		this.departmentResetButtonClickListener.setDepartment(department);		
	}

	private void departmentContainerFieldProperties(boolean readOnly) {
		Collection<Field<?>> fields = this.binder.getFields();
		for (Field<?> field : fields) {
			field.setReadOnly(readOnly);
		}
		this.floorTable.setEnabled(!readOnly);
	}

	private Department initialize() {
		Department department = new Department();
		department.setCode("");
		department.setDivision(new Division());
		department.setName("");
		return department;
	}
	
	private void createFormButton(XERPButton button, String caption,
			Layout layout, FontAwesome icon, ClickListener buttonClickListener,
			boolean isEnabled) {
		button.setCaption(caption);
		button.setDisableOnClick(true);
		button.addClickListener(buttonClickListener);
		button.setIcon(icon);
		button.setEnabled(isEnabled);
		button.setImmediate(true);
		layout.addComponent(button);
	}
	
	private XERPComboBox createFormCombobox(String caption, String propertyId,
			BeanItemContainer<?> itemContainer, String itemCaptionPropertyId,
			Layout layout, String defaultValue, boolean isRequired,
			String errorMessage) {
		XERPComboBox comboBox = new XERPComboBox();
		comboBox.setCaption(caption);
		comboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		comboBox.setNullSelectionAllowed(false);
		comboBox.setImmediate(true);
		comboBox.setContainerDataSource(itemContainer);
		comboBox.setRequired(isRequired);
		if (isRequired) {
			comboBox.setRequiredError(errorMessage);
		}
		comboBox.setItemCaptionPropertyId(itemCaptionPropertyId);
		this.binder.bind(comboBox, propertyId);
		if (defaultValue != null && !defaultValue.isEmpty()) {
			comboBox.setValue(defaultValue);
		}
		if (layout != null) {
			layout.addComponent(comboBox);
		}
		return comboBox;
	}
	
	private XERPTextField createFormTextField(String caption,
			String propertyId, String inputPrompt, boolean isRequired,
			String errorMessage) {
		XERPTextField textField = new XERPTextField();
		textField.setCaption(caption);
		textField.setRequired(isRequired);
		textField.setRequiredError(errorMessage);
		textField.setImmediate(true);
		textField.setInputPrompt(inputPrompt);
		binder.bind(textField, propertyId);
		return textField;
	}

	@Override
	public void onUpdate(Department department) {
		if (department != null && department.getId() > 0) {
			handleSelectionChange(department);
		} else {
			department = initialize();
			this.department = department;
			this.binder.setItemDataSource(department);
			HorizontalLayout editBarContainer = (HorizontalLayout) this
					.getComponent(0);
			editBarContainer.setVisible(false);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(3);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(true);
			onEditButtonClick();
		}
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelectionChange(Department department) {
		handleSelectionChange(department);
	}

	@Override
	public void onEditButtonClick() {
		boolean readOnly = canView && (canEdit || canAdd) ? !canView : canView;
		departmentContainerFieldProperties(readOnly);
		HorizontalLayout buttonBarContainer = (HorizontalLayout) this
				.getComponent(3);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
				.getComponent(0);
		buttonBarLayout.setVisible(true);
		handleButtons(department);
	}

}
