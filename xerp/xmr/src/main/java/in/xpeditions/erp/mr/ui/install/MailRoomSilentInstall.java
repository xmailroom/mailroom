/**
 * 
 */
package in.xpeditions.erp.mr.ui.install;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Component;

import in.xpeditions.erp.commons.annotations.XERPInstallUIProvider;
import in.xpeditions.erp.commons.ui.XERPInstallUICreator;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;

/**
 * @author prince
 * 
 */
@XERPInstallUIProvider
public class MailRoomSilentInstall implements XERPInstallUICreator {

	/*
	 * (non-Javadoc)
	 * 
	 * @see in.xpeditions.erp.commons.ui.XERPInstallUICreator#createComponent()
	 */
	@Override
	public Component createComponent() {
		return null;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see in.xpeditions.erp.commons.ui.XERPInstallUICreator#doInstall()
	 */
	@Override
	public boolean doInstall() {
		VaadinSession session = VaadinSession.getCurrent();
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(session.getLocale());

		/*SPUtil spUtil = new SPUtil();
		List<SP> sps = spUtil.listOfServiceProviders(bundle);

		SPManager spManager = new SPManager();
		try {
			spManager.saveOrUpdate(sps);
		} catch (XERPConstraintViolationException e) {
			String message = bundle.getString(MailRoomMessageConstants.NOTIFICATION_SP_SAVE_FAILED);
			String subMessage = bundle.getString(
					MailRoomMessageConstants.NOTIFICATION_SP_DUPILCATE_VALUE_NAME) + " '" + e.getTitle()
					+ "' " + bundle.getString(MailRoomMessageConstants.NOTIFICATION_SP_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} catch (XERPException e) {
			Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_SP_SAVE_FAILED),
					bundle.getString(MailRoomMessageConstants.NOTIFICATION_SP_REQUIRED),
					Type.ERROR_MESSAGE);
			return false;
		}*/

		return true;
	}

	@Override
	public Component showPreviousUI() {
		return null;
	}

	@Override
	public boolean doInstallForBranchFinancialYearDB() {
		return false;
	}

}