package in.xpeditions.erp.mr.entity.util;

public enum DeliveryStatus {
	UNDELIVERED("Undelivered"), DELIVERED("Delivered");

	public static final String CAPTION_PROPERTY_ID = "name";

	private String name;

	private DeliveryStatus(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static DeliveryStatus get(int i) {
		return DeliveryStatus.values()[i];
	}

}
