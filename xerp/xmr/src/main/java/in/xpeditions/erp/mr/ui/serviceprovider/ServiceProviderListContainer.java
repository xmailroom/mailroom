/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.acc.resources.i18n.I18NAccResourceBundle;
import in.xpeditions.erp.acc.resources.i18n.XAccMessageConstants;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDataChangeListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDataChangeObserver;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderListItemClickListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderListContainer extends VerticalLayout implements ServiceProviderDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7372471904016649781L;
	
	private ServiceProviderDataChangeObserver serviceProviderDataChangeObserver;
	private Table table;
	private String searchString;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	
	public ServiceProviderListContainer(ServiceProviderDataChangeObserver serviceProviderDataChangeObserver) {
		this.serviceProviderDataChangeObserver = serviceProviderDataChangeObserver;
		this.serviceProviderDataChangeObserver.add(this);
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18NAccResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_SERVICE_PROVIDER.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_SERVICE_PROVIDER.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_SERVICE_PROVIDER.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		ServiceProviderListItemClickListener countryListItemClickListener = new ServiceProviderListItemClickListener(this.serviceProviderDataChangeObserver);
		table.addItemClickListener(countryListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void createSearchSection(ResourceBundle bundle) {
		ServiceProviderAddNewButtonClickListener countryAddNewButtonClickListener = new ServiceProviderAddNewButtonClickListener(this.serviceProviderDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_SERVICE_PROVIDER.asPermission().getName()};
		XERPPrimaryButton newCountryButton = new XERPPrimaryButton(actions);
		newCountryButton.setCaption(bundle.getString(XAccMessageConstants.BUTTON_ADD_NEW_COUNTRY));
		newCountryButton.setSizeFull();
		newCountryButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newCountryButton.addClickListener(countryAddNewButtonClickListener);
		this.addComponent(newCountryButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(XAccMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		ServiceProviderSearchTextShortCutListener countrySearchTextShortCutListener = new ServiceProviderSearchTextShortCutListener(prompt, KeyCode.ENTER, this.serviceProviderDataChangeObserver);
		searchTextField.addShortcutListener(countrySearchTextShortCutListener);
		
		ServiceProviderSearchTextBlurListener countrySearchTextBlurListener = new ServiceProviderSearchTextBlurListener(this.serviceProviderDataChangeObserver, countrySearchTextShortCutListener);
		searchTextField.addBlurListener(countrySearchTextBlurListener);
		
		ServiceProviderSearchTextFocusListener countrySearchTextFocusListener = new ServiceProviderSearchTextFocusListener(countrySearchTextShortCutListener);
		searchTextField.addFocusListener(countrySearchTextFocusListener);
		this.addComponent(searchTextField);
	}
	
	private void refreshTable(String searchString) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillServiceProviderList(table, searchString);
		}
	}


	@Override
	public void onUpdate(ServiceProvider serviceProvider) {
		refreshTable(this.searchString);
//		if (country != null && country.getId() > 0) {
//		}
	}

	@Override
	public void onSelectionChange(ServiceProvider serviceProvider) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSearchTextValueChange(String searchString) {
		this.searchString = searchString;
		refreshTable(this.searchString);
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}
}