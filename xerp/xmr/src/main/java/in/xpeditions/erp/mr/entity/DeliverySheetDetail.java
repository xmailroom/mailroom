package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;
import in.xpeditions.erp.mr.entity.util.DeliveryStatus;
import in.xpeditions.erp.mr.entity.util.Reason;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = DeliverySheetDetail.TABLE_NAME)
public class DeliverySheetDetail implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -2282471757945320499L;

	public static final String TABLE_NAME = "xmr_delivery_sheet_detail";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_INBOUND = "inbound";

	public static final String COLUMN_DELIVERY_STATUS = "deliveryStatus";

	public static final String COLUMN_REASON = "reason";

	public static final String COLUMN_REMARKS = "remarks";

	public static final String COLUMN_DELIVERY_SHEET = "deliverySheet";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = DeliverySheetDetail.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@ManyToOne
	@JoinColumn(name = DeliverySheetDetail.COLUMN_INBOUND, nullable = false)
	private Inbound inbound;

	@Column(name = DeliverySheetDetail.COLUMN_DELIVERY_STATUS, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private DeliveryStatus deliveryStatus;

	@Column(name = DeliverySheetDetail.COLUMN_REASON)
	private Reason reason;

	@Column(name = DeliverySheetDetail.COLUMN_REMARKS, nullable = false)
	private String remarks;

	@ManyToOne
	@JoinColumn(name = DeliverySheetDetail.COLUMN_DELIVERY_SHEET, nullable = false)
	private DeliverySheet deliverySheet;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the inbound
	 */
	public Inbound getInbound() {
		return inbound;
	}

	/**
	 * @param inbound
	 *            the inbound to set
	 */
	public void setInbound(Inbound inbound) {
		this.inbound = inbound;
	}

	/**
	 * @return the deliveryStatus
	 */
	public DeliveryStatus getDeliveryStatus() {
		return deliveryStatus;
	}

	/**
	 * @param deliveryStatus
	 *            the deliveryStatus to set
	 */
	public void setDeliveryStatus(DeliveryStatus deliveryStatus) {
		this.deliveryStatus = deliveryStatus;
	}

	/**
	 * @return the reason
	 */
	public Reason getReason() {
		return reason;
	}

	/**
	 * @param reason
	 *            the reason to set
	 */
	public void setReason(Reason reason) {
		this.reason = reason;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the deliverySheet
	 */
	public DeliverySheet getDeliverySheet() {
		return deliverySheet;
	}

	/**
	 * @param deliverySheet
	 *            the deliverySheet to set
	 */
	public void setDeliverySheet(DeliverySheet deliverySheet) {
		this.deliverySheet = deliverySheet;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliverySheetDetail other = (DeliverySheetDetail) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
