/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.io.Serializable;

import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public interface DepartmentDataChangeListener extends Serializable {

	public void onUpdate(Department department);

	public void onSearchTextValueChange(String value);

	public void onSelectionChange(Department department);

	public void onEditButtonClick();

}
