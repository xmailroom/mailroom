/**
 * 
 */
package in.xpeditions.erp.mr.ui.department;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDataChangeObserver;

/**
 * @author Saran
 *
 */
public class DepartmentHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5477070870056626358L;
	
	public DepartmentHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        DepartmentDataChangeObserver departmentDataChangeObserver = new DepartmentDataChangeObserver();
        DepartmentListContainer departmentListContainer = new DepartmentListContainer(departmentDataChangeObserver);
        containerSplitter.setFirstComponent(departmentListContainer);
        
        DepartmentEditContainer departmentEditContainer = new DepartmentEditContainer(departmentDataChangeObserver);
        containerSplitter.setSecondComponent(departmentEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
