/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider;

import java.util.Arrays;
import java.util.Collection;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDangerButton;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDataChangeListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDataChangeObserver;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderEditButtonClickListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderResetButtonClickListener;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderSaveButtonClickListener;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author xpeditions
 * 
 */
public class ServiceProviderEditContainer extends VerticalLayout implements ServiceProviderDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484020088110920562L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;
	private BeanFieldGroup<ServiceProvider> binder;
	private ServiceProvider serviceProvider;
	private XERPFriendlyButton saveButton;
	private ServiceProviderDeleteButtonClickListener serviceProviderDeleteButtonClickListener;
	private ServiceProviderResetButtonClickListener serviceProviderResetButtonClickListener;
	private boolean canAdd;
	private boolean canEdit;
	private boolean canView;
	private XERPDangerButton editButton;
	private XERPDangerButton deleteButton;

	public ServiceProviderEditContainer(
			ServiceProviderDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
		this.countryDataChangeObserver.add(this);
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent()
				.getPage()
				.setTitle(
						bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS_SERVICE_PROVIDER));
		canAdd = XERPUtil.hasPermission(MailRoomPermissionEnum.ADD_SERVICE_PROVIDER
				.asPermission().getName());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_SERVICE_PROVIDER
				.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_SERVICE_PROVIDER
				.asPermission().getName());
		/* this.setSizeFull(); */
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		this.setMargin(marginInfo);
		createEditContainer(bundle);
	}

	private void createEditContainer(ResourceBundle bundle) {
		this.binder = new BeanFieldGroup<ServiceProvider>(ServiceProvider.class);
		this.binder.setBuffered(true);

		this.serviceProvider = initialize();

		HorizontalLayout editBarLayout = new HorizontalLayout();
		MarginInfo editMarginInfo = new MarginInfo(true, true, false, false);
		editBarLayout.setMargin(editMarginInfo);
		editBarLayout.setVisible(false);
		this.addComponent(editBarLayout);
		this.setComponentAlignment(editBarLayout, Alignment.TOP_RIGHT);

		String editActions[] = {
				MailRoomPermissionEnum.EDIT_SERVICE_PROVIDER.asPermission().getName(),
				MailRoomPermissionEnum.DELETE_SERVICE_PROVIDER.asPermission()
						.getName() };
		ServiceProviderEditButtonClickListener countryEditButtonrClickListener = new ServiceProviderEditButtonClickListener(
				this.countryDataChangeObserver);
		editButton = new XERPDangerButton(editActions);
		createFormButton(editButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE_EDIT),
				editBarLayout, FontAwesome.EDIT,
				countryEditButtonrClickListener, true);		

		FormLayout formLayout = new FormLayout();
		
		BeanItemContainer<ModeEnum> modeContainer = new BeanItemContainer<ModeEnum>(ModeEnum.class);
		modeContainer.addAll(Arrays.asList(ModeEnum.values()));
		
		XERPComboBox modeCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_MODE),
				XMailroomFormItemConstants.SERVICE_PROVIDER_MODE, modeContainer, null, null, null, true,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_MODE_CANNOT_EMPTY));
		formLayout.addComponent(modeCombo);
		
		BeanItemContainer<TypeEnum> typeContainer = new BeanItemContainer<TypeEnum>(TypeEnum.class);
		typeContainer.addAll(Arrays.asList(TypeEnum.values()));
		
		XERPComboBox typeCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_TYPE),
				XMailroomFormItemConstants.SERVICE_PROVIDER_TYPE, typeContainer, null, null, null, true,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_TYPE_CANNOT_EMPTY));
		formLayout.addComponent(typeCombo);

		XERPTextField nameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_NAME),
				XMailroomFormItemConstants.SERVICE_PROVIDER_NAME,
				ServiceProvider.COLUMN_NAME,
				true, bundle.getString(MailRoomMessageConstants.NAME_REQUIRED));
		nameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		nameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(nameField);
		
		XERPTextField contactNameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_CONTACT_NAME),
				XMailroomFormItemConstants.SERVICE_PROVIDER_CONTACT_NAME,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_CONTACT_NAME),
				false, null);
		contactNameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		contactNameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(contactNameField);
		
		XERPTextField mobileNumberField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_MOBILE_NUMBER),
				XMailroomFormItemConstants.SERVICE_PROVIDER_MOBILE_NUMBER,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_SERVICE_PROVIDER_MOBILE_NUMBER),
				false, null);
		mobileNumberField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		mobileNumberField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(mobileNumberField);

		/*XERPTextField codeField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_COUNTRY_CODE),
				XMailroomFormItemConstants.COUNTRY_CODE,
				bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_COUNTRY_CODE),
				true, bundle.getString(MailRoomMessageConstants.COUNTRY_CODE_REQUIRED));
		codeField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		codeField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		formLayout.addComponent(codeField);*/

		this.addComponent(formLayout);

		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		/* buttonBarLayoutContainer.setWidth("100%"); */
		this.addComponent(buttonBarLayoutContainer);
		this.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(false);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		ServiceProviderSaveButtonClickListener countrySaveButtonClickListener = new ServiceProviderSaveButtonClickListener(
				this.countryDataChangeObserver, this.binder, this.canEdit);
		String saveActions[] = {
				MailRoomPermissionEnum.ADD_SERVICE_PROVIDER.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_SERVICE_PROVIDER.asPermission().getName() };
		this.saveButton = new XERPFriendlyButton(saveActions);
		createFormButton(saveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE),
				buttonBarLayout, FontAwesome.SAVE,
				countrySaveButtonClickListener, true);
		this.saveButton.setClickShortcut(KeyCode.ENTER, null);
		
		this.serviceProviderDeleteButtonClickListener = new ServiceProviderDeleteButtonClickListener(
				this.countryDataChangeObserver);
		String deleteActions[] = { MailRoomPermissionEnum.DELETE_SERVICE_PROVIDER
				.asPermission().getName() };
		deleteButton = new XERPDangerButton(deleteActions);
		createFormButton(deleteButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_DELETE),
				buttonBarLayout, FontAwesome.TRASH_O,
				serviceProviderDeleteButtonClickListener, true);

		this.serviceProviderResetButtonClickListener = new ServiceProviderResetButtonClickListener(
				this.countryDataChangeObserver);
		String resetActions[] = {
				MailRoomPermissionEnum.ADD_SERVICE_PROVIDER.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_SERVICE_PROVIDER.asPermission().getName() };
		XERPPrimaryButton resetButton = new XERPPrimaryButton(resetActions);
		createFormButton(resetButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_RESET),
				buttonBarLayout, FontAwesome.UNDO,
				serviceProviderResetButtonClickListener, true);

		handleSelectionChange(this.serviceProvider);
	}

	private void handleSelectionChange(ServiceProvider serviceProvider) {
		if (serviceProvider == null) {
			serviceProvider = initialize();
		}
		this.serviceProvider = serviceProvider;
		this.binder.setItemDataSource(serviceProvider);
		if (serviceProvider.getId() > 0) {
			HorizontalLayout editBarLayout = (HorizontalLayout) this
					.getComponent(0);
			editBarLayout.setVisible(true);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(false);
		}
		serviceProviderContainerFieldProperties(true);
		handleButtons(serviceProvider);
	}

	private void serviceProviderContainerFieldProperties(boolean readOnly) {
		Collection<Field<?>> fields = this.binder.getFields();
		for (Field<?> field : fields) {
			field.setReadOnly(readOnly);
		}
	}

	private void handleButtons(ServiceProvider serviceProvider) {
		boolean isExistServiceProvider = serviceProvider.getId() > 0;
		this.deleteButton.setVisible(isExistServiceProvider);
		if (isExistServiceProvider) {
			this.serviceProviderDeleteButtonClickListener.setServiceProvider(serviceProvider);
		}
		this.serviceProviderResetButtonClickListener.setServiceProvider(serviceProvider);
	}

	private XERPTextField createFormTextField(String caption,
			String propertyId, String inputPrompt, boolean isRequired,
			String errorMessage) {
		XERPTextField textField = new XERPTextField();
		textField.setCaption(caption);
		textField.setRequired(isRequired);
		textField.setRequiredError(errorMessage);
		textField.setImmediate(true);
		textField.setInputPrompt(inputPrompt);
		binder.bind(textField, propertyId);
		return textField;
	}
	
	private XERPComboBox createFormCombobox(String caption, String propertyId,
			BeanItemContainer<?> itemContainer, String itemCaptionPropertyId,
			Layout layout, String defaultValue, boolean isRequired, String errorMessage) {
		XERPComboBox comboBox = new XERPComboBox();
		comboBox.setCaption(caption);
		comboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		comboBox.setNullSelectionAllowed(false);
		comboBox.setImmediate(true);
		comboBox.setContainerDataSource(itemContainer);
		comboBox.setRequired(isRequired);
		if (isRequired) {
			comboBox.setRequiredError(errorMessage);
		}
		comboBox.setItemCaptionPropertyId(itemCaptionPropertyId);
		this.binder.bind(comboBox, propertyId);
		if (defaultValue != null && !defaultValue.isEmpty()) {
			comboBox.setValue(defaultValue);
		}
		if (layout != null) {
			layout.addComponent(comboBox);
		}
		return comboBox;
	}

	private void createFormButton(XERPButton button, String caption,
			Layout layout, FontAwesome icon, ClickListener buttonClickListener,
			boolean isEnabled) {
		button.setCaption(caption);
		button.setDisableOnClick(true);
		button.addClickListener(buttonClickListener);
		button.setIcon(icon);
		button.setEnabled(isEnabled);
		button.setImmediate(true);
		layout.addComponent(button);
	}

	private ServiceProvider initialize() {
		ServiceProvider serviceProvider = new ServiceProvider();
		serviceProvider.setName("");
		serviceProvider.setMode(ModeEnum.COURIER);
		serviceProvider.setType(TypeEnum.INBOUND);
		serviceProvider.setContactName("");
		serviceProvider.setMobileNumber("");
		return serviceProvider;
	}

	@Override
	public void onUpdate(ServiceProvider serviceProvider) {
		if (serviceProvider != null && serviceProvider.getId() > 0) {
			handleSelectionChange(serviceProvider);
		} else {
			serviceProvider = initialize();
			this.serviceProvider = serviceProvider;
			this.binder.setItemDataSource(serviceProvider);
			HorizontalLayout editBarContainer = (HorizontalLayout) this
					.getComponent(0);
			editBarContainer.setVisible(false);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(true);
			onEditButtonClick();
		}
	}

	@Override
	public void onSelectionChange(ServiceProvider serviceProvider) {
		handleSelectionChange(serviceProvider);
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEditButtonClick() {
		boolean readOnly = canView && (canEdit || canAdd) ? !canView : canView;
		serviceProviderContainerFieldProperties(readOnly);
		HorizontalLayout buttonBarContainer = (HorizontalLayout) this
				.getComponent(2);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
				.getComponent(0);
		buttonBarLayout.setVisible(true);
		handleButtons(serviceProvider);
	}
}