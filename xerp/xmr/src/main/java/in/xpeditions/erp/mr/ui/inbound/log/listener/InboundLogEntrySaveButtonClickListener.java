package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class InboundLogEntrySaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6692446945510350816L;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private ResourceBundle bundle;

	private BeanFieldGroup<LogEntry> logEntryBinder;

	public InboundLogEntrySaveButtonClickListener(InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver,
			ResourceBundle bundle, BeanFieldGroup<LogEntry> logEntryBinder) {
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.bundle = bundle;
		this.logEntryBinder = logEntryBinder;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		String message = "", subMessage = "";
		try {
			this.logEntryBinder.commit();
		} catch (CommitException e) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}
		LogEntry logEntry = this.logEntryBinder.getItemDataSource().getBean();
		if (logEntry.getServiceProviderId() <= 0) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}
		LogEntryManager logEntryManager = new LogEntryManager();
		try {
			if (logEntry.getId() > 0) {
				logEntryManager.update(logEntry);
			} else {
				logEntry.setCreatedTime(System.currentTimeMillis());
				logEntryManager.save(logEntry);
			}
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_SAVED_SUCCESSFULLY);
			Notification.show(message, Notification.Type.WARNING_MESSAGE);
			this.inboundLogEntryDataChangeObserver.notifyUpdate(logEntry);
		} catch (XERPConstraintViolationException e) {
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_SAVE_FAILED);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DUPILCATE_VALUE_NAME)
					+ " '" + e.getTitle() + "' "
					+ this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);

		} catch (XERPException e) {
			message = "XERPException";
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}

		button.setEnabled(true);
	}

}
