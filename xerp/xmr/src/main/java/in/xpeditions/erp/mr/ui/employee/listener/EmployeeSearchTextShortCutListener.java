package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author xpeditions
 *
 */
public class EmployeeSearchTextShortCutListener  extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3828091810523726811L;
	
	private EmployeeDataChangeObserver employeeDataChangeObserver;

	public EmployeeSearchTextShortCutListener(String prompt, int keyCode, EmployeeDataChangeObserver employeeDataChangeObserver) {
		super(prompt, keyCode, null);
		this.employeeDataChangeObserver = employeeDataChangeObserver;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if ((target instanceof XERPTextField)) {
			XERPTextField field = (XERPTextField) target;
			String value = field.getValue();
			this.employeeDataChangeObserver.notifySearchTextValueChange(value);
		}
	}
}