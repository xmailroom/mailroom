package in.xpeditions.erp.mr.ui.inbound;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.Property;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPTableFilterDecorator;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.manager.InboundManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundListItemChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundListItemClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundListItemShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.listener.TableFoucsShortcutListener;

public class InboundListContainer extends VerticalLayout implements InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4164886019105933683L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private ResourceBundle bundle;

	private FilterTable table;

	private IndexedContainer container;

	private InboundListItemShortcutListener inboundListItemShortcutListener;

	private InboundListItemClickListener inboundListItemClickListener;

	public InboundListContainer(InboundDataChangeObserver inboundDataChangeObserver) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.inboundDataChangeObserver.addListener(this);

		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent().getPage().setTitle(this.bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND));
		this.createListLayout();
	}

	private void createListLayout() {
		boolean canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_INBOUND_LOG.asPermission().getName());
		final DecimalFormat decimalFormat = new DecimalFormat("#");
		this.table = new FilterTable() {

			/**
			 * 
			 */
			private static final long serialVersionUID = -2061140714490593231L;

			@Override
			protected String formatPropertyValue(Object rowId, Object colId, Property<?> property) {
				Object object = property.getValue();
				if (object instanceof Long) {
					double dValue = XERPUtil.parseDouble(object.toString());
					return XERPUtil.decimalFormat(decimalFormat, dValue);
				}
				return super.formatPropertyValue(rowId, colId, property);
			}

		};
		this.addComponent(this.table);
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.setFilterBarVisible(true);
		this.table.setSortEnabled(true);
		this.table.setFilterDecorator(new XERPTableFilterDecorator());
		this.table.setWidth("300px");
		this.table.setHeight("300px");
		this.table.setImmediate(true);
		this.table.setVisible(canView);
		this.table.setPageLength(15);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);
		this.table.addShortcutListener(new TableFoucsShortcutListener("ctrl+left", KeyCode.ARROW_LEFT,
				new int[] { ModifierKey.CTRL }, this.table));

		container = new IndexedContainer();
		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO),
				Long.class, 0);
		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE), String.class,
				"");
		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				HorizontalLayout.class, null);

		this.table.setContainerDataSource(container);

		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE), Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), Align.LEFT);

		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE), 135);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), 83);

		this.inboundListItemShortcutListener = new InboundListItemShortcutListener("", KeyCode.ENTER,
				new int[] { ModifierKey.ALT });

		this.inboundListItemClickListener = new InboundListItemClickListener(this.bundle,
				this.inboundDataChangeObserver);

		this.table.addValueChangeListener(new InboundListItemChangeListener(this.inboundDataChangeObserver,
				this.inboundListItemClickListener, this.inboundListItemShortcutListener, this.table));
		this.table.addItemClickListener(inboundListItemClickListener);

		this.refreshTable();
	}

	private void refreshTable() {
		this.container.removeAllItems();
		InboundManager inboundManager = new InboundManager();
		List<Object[]> objects = new ArrayList<Object[]>();
		try {
			objects = inboundManager.listSQL();
		} catch (XERPException e) {
		}
		Inbound inbound = null;
		for (Object[] object : objects) {
			inbound = this.init(inbound, object);
			// String serviceProviderName = object[8] + "";
			HorizontalLayout actionLayout = new HorizontalLayout();
			Label label = new Label();
			label.addShortcutListener(this.inboundListItemShortcutListener);
			actionLayout.addComponent(label);

			this.table.addItem(new Object[] { inbound.getLogEntry().getBatchNo(),
					XERPUtil.formatDate(inbound.getDate(), DataHolder.commonUserDateTimeFormat) + "", actionLayout },
					inbound);
		}

	}

	private Inbound init(Inbound inbound, Object[] object) {
		inbound = new Inbound();
		int i = 0;
		inbound.setId(XERPUtil.sum(object[i++], 0).longValue());
		LogEntry logEntry = new LogEntry();
		logEntry.setBatchNo(XERPUtil.sum(object[i++], 0).longValue());
		inbound.setLogEntry(logEntry);
		inbound.setDate(XERPUtil.sum(object[i++], 0).longValue());
		inbound.setMode(ModeEnum.get(XERPUtil.sum(object[i++], 0).intValue()));
		inbound.setRemarks(object[i++] + "");
		i++;
		// inbound.setsTypeEnum(TypeEnum.get(XERPUtil.sum(object[i++],
		// 0).intValue()));
		inbound.setServiceProviderId(XERPUtil.sum(object[i++], 0).longValue());
		i++;// Service Provider Name
		return inbound;
	}

	@Override
	public void onUpdate(Inbound inbound) {
		this.refreshTable();
		this.table.setCurrentPageFirstItemId(inbound);
		this.table.select(inbound);
		this.table.focus();
	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEmployeeChange(Employee employee) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		// TODO Auto-generated method stub
		
	}

}
