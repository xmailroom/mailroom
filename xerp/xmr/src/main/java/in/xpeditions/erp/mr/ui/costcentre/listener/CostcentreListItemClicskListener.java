/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.CostCentre;

/**
 * @author Saran
 *
 */
public class CostcentreListItemClicskListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3043490779276062011L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostcentreListItemClicskListener(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<CostCentre> nestingBeanItem = (NestingBeanItem<CostCentre>) event.getItem();
		CostCentre costCentre = nestingBeanItem.getBean();
		this.costcentreDataChangeObserver.notifySelectionChange(costCentre);
	}

}
