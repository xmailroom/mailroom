package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPTextField;

public class DataEntryFoucsShortcutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2139512120099008669L;

	private XERPTextField awbField;

	public DataEntryFoucsShortcutListener(String caption, int keyCode, int[] modifierKeys, XERPTextField awbField) {
		super(caption, keyCode, modifierKeys);
		this.awbField = awbField;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.awbField.focus();
	}

}
