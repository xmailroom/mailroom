package in.xpeditions.erp.mr.util;

import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.To;

public class InboundUtil {

	public static Inbound copy(Inbound inbound, To to, int noOfDocs) {
		Inbound iInbound = new Inbound();
		iInbound.setAwbNo(inbound.getAwbNo());
		iInbound.setBulk(inbound.isBulk());
		iInbound.setCreatedTime(inbound.getCreatedTime());
		iInbound.setDate(inbound.getDate());
		iInbound.setLogEntry(inbound.getLogEntry());
		iInbound.setMode(inbound.getMode());
		iInbound.setRemarks(inbound.getRemarks());
		iInbound.setServiceProviderId(inbound.getServiceProviderId());
		iInbound.setsType(inbound.getsType());
		iInbound.setxFrom(inbound.getxFrom());
		iInbound.setxTo(to);
		iInbound.setNoOfDocs(noOfDocs);
		return iInbound;
	}

}
