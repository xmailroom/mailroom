/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public class DivisionListItemClickListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 768135839541110568L;
	private DivisionDataChangeObserver divisionDataChangeObserver;

	public DivisionListItemClickListener(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<Division> nestingBeanItem = (NestingBeanItem<Division>) event.getItem();
		Division division = nestingBeanItem.getBean();
		this.divisionDataChangeObserver.notifySelectionChange(division);
	}

}
