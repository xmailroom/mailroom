package in.xpeditions.erp.mr.ui.floor.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.ShortcutListener;

/**
 * @author xpeditions
 *
 */
public class FloorSearchTextShortCutListener  extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3828091810523726811L;
	
	private FloorDataChangeObserver countryDataChangeObserver;

	public FloorSearchTextShortCutListener(String prompt, int keyCode, FloorDataChangeObserver countryDataChangeObserver) {
		super(prompt, keyCode, null);
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if ((target instanceof XERPTextField)) {
			XERPTextField field = (XERPTextField) target;
			String value = field.getValue();
			this.countryDataChangeObserver.notifySearchTextValueChange(value);
		}
	}
}