package in.xpeditions.erp.mr.ui.inbound;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.ui.XERPWindow;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;

public class EmployeeSelectionWindow extends XERPWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6243206581573927493L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private Employee employee;

	private ResourceBundle bundle;

	private EmployeeSelectionContainer employeeSelectionContainer;

	public EmployeeSelectionWindow(InboundDataChangeObserver inboundDataChangeObserver, Employee employee) {
		super(false);
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.employee = employee;
//		this.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_HEADER);
		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.setCaption(this.bundle.getString(MailRoomMessageConstants.WINDOW_HEADING_SELECT_EMPLOYEE));
		this.setModal(true);
		this.setWidth("50%");
		this.setHeight("60%");
		this.employeeSelectionContainer = new EmployeeSelectionContainer(this.inboundDataChangeObserver, this.bundle,
				this, this.employee);
	}

	public void open() {
		this.setContent(this.employeeSelectionContainer);
		UI current = UI.getCurrent();
		current.addWindow(this);
		current.setFocusedComponent(this);
		this.employeeSelectionContainer.table.focus();
	}

}
