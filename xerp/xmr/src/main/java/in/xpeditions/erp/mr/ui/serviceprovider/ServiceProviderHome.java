/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderDataChangeObserver;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8373592946196286721L;
	
	public ServiceProviderHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        ServiceProviderDataChangeObserver serviceProviderDataChangeObserver = new ServiceProviderDataChangeObserver();
        
        ServiceProviderListContainer serviceProviderListContainer = new ServiceProviderListContainer(serviceProviderDataChangeObserver);
        containerSplitter.setFirstComponent(serviceProviderListContainer);
        
        ServiceProviderEditContainer serviceProviderEditContainer = new ServiceProviderEditContainer(serviceProviderDataChangeObserver);
        containerSplitter.setSecondComponent(serviceProviderEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
