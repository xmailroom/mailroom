/**
 * 
 */
package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.dao.manager.CostCentreDAOManager;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class CostCentreDAOManagerHibernate implements CostCentreDAOManager {

	@Override
	public void save(CostCentre costCentre) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.save(costCentre);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void update(CostCentre costCentre) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.update(costCentre);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public Long getCostCentreCount(String searchString) throws XERPException {
		String hql = "select count(c) from CostCentre c left join c.department d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " c.name LIKE '" + searchString + "%' OR d.name LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCentre> listCostCentres(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		String hql = "select c from CostCentre c left join c.department d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " c.name LIKE '" + searchString + "%' OR c.name LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		String sortCrit = "";
		if (sortProperties != null) {
			for (int i = 0; i < sortProperties.length; i++) {
				Object sObject = sortProperties[i];
				if (sObject.equals(XMailroomTableItemConstants.COSTCENTRE_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " c.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				} 
				if (sObject.equals(XMailroomTableItemConstants.COSTCENTRE_DEPARTMENT_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " c.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				}
			}
			sortCrit = sortCrit.isEmpty() ? "" : " ORDER BY " + sortCrit;
		}
		if(sortCrit.isEmpty()) {
			sortCrit = " ORDER BY c.name";
		}
		hql += sortCrit;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			if (count > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(count);
			}
			List<CostCentre> costCentres = query.list();
			return costCentres;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(CostCentre costCentre) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.delete(costCentre);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CostCentre> listByDepartment(Department department) throws XERPException {
		String hql = "select c from CostCentre c where c.department=:department";
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("department", department);
			List<CostCentre> costCentres = query.list();
			return costCentres;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public CostCentre getByName(String costCenterName) throws XERPException {
		String hql = "select c from CostCentre c where c.name=:costCenterName";
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("costCenterName", costCenterName);
			return (CostCentre) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
