package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.Constants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.dao.manager.InboundDAOManager;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.ServiceProvider;

public class InboundDAOManagerHibernate implements InboundDAOManager {

	@Override
	public void save(Inbound inbound) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			String HQL = "update " + LogEntry.class.getSimpleName() + " le set le." + LogEntry.COLUMN_NO_OF_ENTRY
					+ " = (" + LogEntry.COLUMN_NO_OF_ENTRY + " + 1) where le.id=:id";
			Query query = session.createQuery(HQL);
			query.setParameter("id", inbound.getLogEntry().getId());
			query.executeUpdate();
			session.save(inbound);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void bulkSave(List<Inbound> inbounds) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			String HQL = "update " + LogEntry.TABLE_NAME + " le set le." + LogEntry.COLUMN_NO_OF_ENTRY + " = ("
					+ LogEntry.COLUMN_NO_OF_ENTRY + " + 1) where le.id=:id";
			Query query = session.createSQLQuery(HQL);
			query.setParameter("id", inbounds.get(0).getLogEntry().getId());
			query.executeUpdate();
			for (Inbound inbound : inbounds) {
				session.save(inbound);
			}
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(Inbound inbound) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.update(inbound);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Inbound> list() throws XERPException {
		String hql = "SELECT le FROM " + Inbound.class.getSimpleName() + " le";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Inbound getBy(Inbound inbound) throws XERPException {
		String hql = "SELECT le FROM " + Inbound.class.getSimpleName() + " AS le WHERE le.id=:id";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			query.setParameter("id", inbound.getId());
			return (Inbound) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Inbound inbound) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.delete(inbound);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listSQL() throws XERPException {
		String db1 = DataHolder.common_db_name;
		String db2 = (String) XERPUtil.getSessionAttribute(Constants.SESSION_DBNAME);
		String s = "";
		s += " i." + Inbound.COLUMN_ID + " as iId,";
		s += " le." + LogEntry.COLUMN_BATCH_NO + " as batchNo,";
		s += " i." + Inbound.COLUMN_DATE + " as iDate,";
		s += " i." + Inbound.COLUMN_MODE + " as iMode,";
		s += " le." + LogEntry.COLUMN_BATCH_NO + " as bNo,";
		s += " IFNULL(i." + Inbound.COLUMN_REMARKS + ", '') as r,";
		s += " i." + Inbound.COLUMN_TYPE + " as sType,";
		s += " sp." + ServiceProvider.COLUMN_ID + " as spId,";
		s += " sp." + ServiceProvider.COLUMN_NAME + " as spName";
		String SQL = " SELECT " + s + " FROM " + db2 + "." + Inbound.TABLE_NAME + " i" + " INNER JOIN " + db1 + "."
				+ ServiceProvider.TABLE_NAME + " sp ON i." + Inbound.COLUMN_SERVICE_PROVIDER + " = sp." + ServiceProvider.COLUMN_ID
				+ " INNER JOIN " + LogEntry.TABLE_NAME + " le on le." + LogEntry.COLUMN_ID + " = "
				+ Inbound.COLUMN_LOG_ENTRY;
		;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createSQLQuery(SQL);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
