/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class CostcentreSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6828856028673292693L;
	private CostcentreSearchTextShortCutListener costcentreSearchTextShortCutListener;

	public CostcentreSearchTextFocusListener(
			CostcentreSearchTextShortCutListener costcentreSearchTextShortCutListener) {
		this.costcentreSearchTextShortCutListener = costcentreSearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(costcentreSearchTextShortCutListener);
	}

}
