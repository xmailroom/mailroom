/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderAddNewButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2198214663685807650L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;

	public ServiceProviderAddNewButtonClickListener(ServiceProviderDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.countryDataChangeObserver.notifyUpdate(null);
		event.getButton().setEnabled(true);
	}
}