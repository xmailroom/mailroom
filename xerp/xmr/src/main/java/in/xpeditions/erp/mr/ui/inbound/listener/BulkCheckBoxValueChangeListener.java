package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.Property;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Table;

import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.mr.entity.To;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class BulkCheckBoxValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6547702759726250641L;

	private XERPTextField nosField;

	private Table toTable;

	private ResourceBundle bundle;

	private HashMap<BeanFieldGroup<To>, Integer> noOfShipMap;

	public BulkCheckBoxValueChangeListener(XERPTextField nosField, Table toTable, ResourceBundle bundle,
			HashMap<BeanFieldGroup<To>, Integer> noOfShipMap) {
		this.nosField = nosField;
		this.toTable = toTable;
		this.bundle = bundle;
		this.noOfShipMap = noOfShipMap;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void valueChange(ValueChangeEvent event) {
		boolean value = (boolean) event.getProperty().getValue();
		if (!value) {
			this.nosField.setValue(1 + "");
			this.toTable.setVisibleColumns(
					new Object[] { this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS) });
			List<?> itemIds = (List<?>) this.toTable.getItemIds();
			Property<?> itemProperty = this.toTable.getItem(itemIds.get(0))
					.getItemProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_SHIPMENT));
			XERPTextField noOfShipField = (XERPTextField) itemProperty.getValue();
			noOfShipField.setValue(1 + "");
			this.noOfShipMap.put((BeanFieldGroup<To>) itemIds.get(0), 1);
		} else {
			this.toTable.setVisibleColumns(
					new Object[] { this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_SHIPMENT),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO),
							this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS),
							this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION) });
		}
		this.nosField.setVisible(value);
	}

}
