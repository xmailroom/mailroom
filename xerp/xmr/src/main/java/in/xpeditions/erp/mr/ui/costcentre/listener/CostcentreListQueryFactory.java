/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author Saran
 *
 */
public class CostcentreListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1372087919587755157L;
	private String searchString;

	public CostcentreListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		CostCentreListQuery costCentreListQuery = new CostCentreListQuery(searchString, queryDefinition);
		return costCentreListQuery;
	}

}
