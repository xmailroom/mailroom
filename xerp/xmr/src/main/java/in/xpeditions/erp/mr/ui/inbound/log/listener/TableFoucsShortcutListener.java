package in.xpeditions.erp.mr.ui.inbound.log.listener;

import org.tepi.filtertable.FilterTable;

import com.vaadin.event.ShortcutListener;

public class TableFoucsShortcutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2139512120099008669L;

	private FilterTable table;

	public TableFoucsShortcutListener(String caption, int keyCode, int[] modifierKeys, FilterTable table) {
		super(caption, keyCode, modifierKeys);
		this.table = table;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.table.focus();
	}

}
