package in.xpeditions.erp.mr.ui.inbound;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.ui.XERPWindow;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;

public class LogEntrySelectionWindow extends XERPWindow {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6243206581573927493L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private LogEntry logEntry;

	private ResourceBundle bundle;

	private LogEntrySelectionContainer inboundLogSelectionContainer;

	public LogEntrySelectionWindow(InboundDataChangeObserver inboundDataChangeObserver, LogEntry logEntry) {
		super(false);
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.logEntry = logEntry;
		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.setCaption(this.bundle.getString(MailRoomMessageConstants.WINDOW_HEADING_SELECT_EMPLOYEE));
		this.setModal(true);
		this.setWidth("50%");
		this.setHeight("60%");
		this.inboundLogSelectionContainer = new LogEntrySelectionContainer(this.inboundDataChangeObserver, this.bundle,
				this, this.logEntry);
	}

	public void open() {
		this.setContent(this.inboundLogSelectionContainer);
		UI current = UI.getCurrent();
		current.addWindow(this);
		current.setFocusedComponent(this);
		this.inboundLogSelectionContainer.table.focus();
	}

}
