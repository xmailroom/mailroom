/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author xpeditions
 *
 */
public class EmployeeAddNewButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2198214663685807650L;
	private EmployeeDataChangeObserver countryDataChangeObserver;

	public EmployeeAddNewButtonClickListener(EmployeeDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.countryDataChangeObserver.notifyUpdate(null);
		event.getButton().setEnabled(true);
	}
}