/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.CostCentreDAOManager;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class CostCentreManager {

	public void save(CostCentre costCentre) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		costCentreDAOManager.save(costCentre);
	}

	public void update(CostCentre costCentre) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		costCentreDAOManager.update(costCentre);
	}

	public Long getCostCentreCount(String searchString) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		return costCentreDAOManager.getCostCentreCount(searchString);
	}

	public List<CostCentre> listCostCentres(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		return costCentreDAOManager.listCostCentres(startIndex, count, sortProperties, sortPropertyAscendingStates,
				searchString);
	}

	public void delete(CostCentre costCentre) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		costCentreDAOManager.delete(costCentre);
	}

	public List<CostCentre> listByDepartment(Department department) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		return costCentreDAOManager.listByDepartment(department);
	}

	public CostCentre getByName(String costCenterName) throws XERPException {
		CostCentreDAOManager costCentreDAOManager = (CostCentreDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE);
		return costCentreDAOManager.getByName(costCenterName);
	}

}
