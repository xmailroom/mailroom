package in.xpeditions.erp.mr.hibernate.manager;

import java.sql.Date;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.Constants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.dao.manager.DeliverySheetDAOManager;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.entity.DeliverySheetDetail;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.entity.From;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.To;
import in.xpeditions.erp.mr.entity.util.DeliveryStatus;
import in.xpeditions.erp.mr.entity.util.ModeEnum;

public class DeliverySheetDAOManagerHibernate implements DeliverySheetDAOManager {

	@Override
	public void save(DeliverySheet deliverySheet) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			String SQL = "select (ifnull(max(ds." + DeliverySheet.COLUMN_NUMBER + "), 0) + 1) from "
					+ DeliverySheet.TABLE_NAME + " ds";
			Query query = session.createSQLQuery(SQL);
			Object object = query.uniqueResult();
			deliverySheet.setNumber(XERPUtil.sum(object, 0).longValue());
			session.save(deliverySheet);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(DeliverySheet deliverySheet) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.update(deliverySheet);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeliverySheet> list() throws XERPException {
		String hql = "SELECT le FROM " + DeliverySheet.class.getSimpleName() + " le";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public DeliverySheet getBy(DeliverySheet deliverySheet) throws XERPException {
		String hql = "SELECT le FROM " + DeliverySheet.class.getSimpleName() + " AS le WHERE le.id=:id";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			query.setParameter("id", deliverySheet.getId());
			return (DeliverySheet) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(DeliverySheet deliverySheet) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.delete(deliverySheet);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listSQL() throws XERPException {
		String s = "";
		s += " ds." + DeliverySheet.COLUMN_ID + " as dId";
		s += ", ds." + DeliverySheet.COLUMN_NUMBER + " as dNo";
		s += ", ds." + DeliverySheet.COLUMN_DATE + " as dDate";
		s += ", ds." + DeliverySheet.COLUMN_RECEIVED_BY + " as receivedBy";
		s += ", ds." + DeliverySheet.COLUMN_RECEIVED_TIME + " as receivedTime";
		s += ", count(DISTINCT dsd." + DeliverySheetDetail.COLUMN_INBOUND + ") as noOfInbounds";
		s += ", sum(i." + Inbound.COLUMN_NO_OF_DOCS + ") as noOfDocs";
		s += ", SUM(CASE WHEN IFNULL(dsd." + DeliverySheetDetail.COLUMN_DELIVERY_STATUS + ", 0) = "
				+ DeliveryStatus.DELIVERED.ordinal() + " THEN 1 ELSE 0 END) as delivered";
		String SQL = " select " + s + " from " + DeliverySheet.TABLE_NAME + " ds";
		SQL += " INNER JOIN " + DeliverySheetDetail.TABLE_NAME + " dsd on dsd."
				+ DeliverySheetDetail.COLUMN_DELIVERY_SHEET + "=ds." + DeliverySheet.COLUMN_ID;
		SQL += " INNER JOIN " + Inbound.TABLE_NAME + " i on dsd." + DeliverySheetDetail.COLUMN_INBOUND + "=i."
				+ Inbound.COLUMN_ID;
		SQL += " GROUP BY ds." + DeliverySheet.COLUMN_ID;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createSQLQuery(SQL);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DeliverySheetDetail> listDeliverySheetDetail(DeliverySheet deliverySheet) throws XERPException {
		String HQL = "SELECT dsd FROM " + DeliverySheetDetail.class.getSimpleName() + " AS dsd WHERE dsd."
				+ DeliverySheetDetail.COLUMN_DELIVERY_SHEET + "=:" + DeliverySheetDetail.COLUMN_DELIVERY_SHEET;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(HQL);
			query.setParameter(DeliverySheetDetail.COLUMN_DELIVERY_SHEET, deliverySheet);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listDeliverySheetDetailSQL(DeliverySheet deliverySheet) throws XERPException {
		String db1 = DataHolder.common_db_name;
		String db2 = (String) XERPUtil.getSessionAttribute(Constants.SESSION_DBNAME);
		String s = "";
		s += " dsd." + DeliverySheetDetail.COLUMN_ID + " as dsId";
		s += ", i." + Inbound.COLUMN_ID + " as iId";
		s += ", i." + Inbound.COLUMN_AWB_NO + " as awb";
		s += ", i." + Inbound.COLUMN_DATE + " as iDate";
		s += ", i." + Inbound.COLUMN_MODE + " as iMode";
		s += ", sp." + ServiceProvider.COLUMN_NAME + " as spName";
		s += ", CASE WHEN i." + Inbound.COLUMN_MODE + " = " + ModeEnum.INTERBRANCH.ordinal() + " THEN fe."
				+ Employee.COLUMN_NAME + " ELSE xFrom." + From.COLUMN_FROM_NAME + " END AS xFrom";
		s += ", CASE WHEN i." + Inbound.COLUMN_MODE + " = " + ModeEnum.INTERBRANCH.ordinal() + " THEN te."
				+ Employee.COLUMN_NAME + " ELSE xTo." + From.COLUMN_FROM_NAME + " END AS xTo";
		s += ", d." + Department.COLUMN_NAME + " AS department";
		s += ", i." + Inbound.COLUMN_NO_OF_DOCS + " AS noOfDocs";
		String SQL = " SELECT " + s;
		SQL += " FROM " + db2 + "." + Inbound.TABLE_NAME + " i";
		SQL += " INNER JOIN " + db2 + "." + From.TABLE_NAME + " xFrom ON i.xFrom = xFrom.id";
		SQL += " INNER JOIN " + db1 + "." + Employee.TABLE_NAME + " fe ON xFrom." + Employee.COLUMN_EMPLOYEEID
				+ " = fe.id";
		SQL += " INNER JOIN " + db2 + "." + To.TABLE_NAME + " xTo ON i.xTo = xTo.id";
		SQL += " INNER JOIN " + db1 + "." + Employee.TABLE_NAME + " te ON xTo." + Employee.COLUMN_EMPLOYEEID
				+ " = te.id";
		SQL += " INNER JOIN " + db1 + "." + Department.TABLE_NAME + " d ON te." + Employee.COLUMN_DEPARTMENT
				+ " = d.id";
		SQL += " INNER JOIN " + db1 + "." + ServiceProvider.TABLE_NAME + " sp ON i." + Inbound.COLUMN_SERVICE_PROVIDER
				+ " = sp." + ServiceProvider.COLUMN_ID;
		SQL += " INNER JOIN " + db2 + "." + DeliverySheetDetail.TABLE_NAME + " dsd ON dsd."
				+ DeliverySheetDetail.COLUMN_INBOUND + " = i.id";
		SQL += " where dsd." + DeliverySheetDetail.COLUMN_DELIVERY_SHEET + "=:"
				+ DeliverySheetDetail.COLUMN_DELIVERY_SHEET;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createSQLQuery(SQL);
			query.setParameter(DeliverySheetDetail.COLUMN_DELIVERY_SHEET, deliverySheet.getId());
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listReadyForDeliverySQL(Date from, Date to, Floor floor, Department department)
			throws XERPException {
		String db1 = DataHolder.common_db_name;
		String db2 = (String) XERPUtil.getSessionAttribute(Constants.SESSION_DBNAME);
		String s = "";
		s += " 0 as dsId";
		s += ", i." + Inbound.COLUMN_ID + " as iId";
		s += ", i." + Inbound.COLUMN_AWB_NO + " as awb";
		s += ", i." + Inbound.COLUMN_DATE + " as iDate";
		s += ", i." + Inbound.COLUMN_MODE + " as iMode";
		s += ", sp." + ServiceProvider.COLUMN_NAME + " as spName";
		s += ", CASE WHEN i." + Inbound.COLUMN_MODE + " = " + ModeEnum.INTERBRANCH.ordinal() + " THEN fe."
				+ Employee.COLUMN_NAME + " ELSE xFrom." + From.COLUMN_FROM_NAME + " END AS xFrom";
		s += ", CASE WHEN i." + Inbound.COLUMN_MODE + " = " + ModeEnum.INTERBRANCH.ordinal() + " THEN te."
				+ Employee.COLUMN_NAME + " ELSE xTo." + From.COLUMN_FROM_NAME + " END AS xTo";
		s += ", d." + Department.COLUMN_NAME + " AS department";
		s += ", i." + Inbound.COLUMN_NO_OF_DOCS + " AS noOfDocs";
		String SQL = " SELECT " + s;
		SQL += " FROM " + db2 + "." + Inbound.TABLE_NAME + " i";
		SQL += " INNER JOIN " + db2 + "." + From.TABLE_NAME + " xFrom ON i.xFrom = xFrom.id";
		SQL += " INNER JOIN " + db1 + "." + Employee.TABLE_NAME + " fe ON xFrom." + Employee.COLUMN_EMPLOYEEID
				+ " = fe.id";
		SQL += " INNER JOIN " + db2 + "." + To.TABLE_NAME + " xTo ON i.xTo = xTo.id";
		SQL += " INNER JOIN " + db1 + "." + Employee.TABLE_NAME + " te ON xTo." + Employee.COLUMN_EMPLOYEEID
				+ " = te.id";
		SQL += " INNER JOIN " + db1 + "." + Department.TABLE_NAME + " d ON te." + Employee.COLUMN_DEPARTMENT
				+ " = d.id";
		SQL += " INNER JOIN " + db1 + "." + ServiceProvider.TABLE_NAME + " sp ON i." + Inbound.COLUMN_SERVICE_PROVIDER
				+ " = sp." + ServiceProvider.COLUMN_ID;
		SQL += " LEFT JOIN " + db2 + "." + DeliverySheetDetail.TABLE_NAME + " dsd ON dsd."
				+ DeliverySheetDetail.COLUMN_INBOUND + " = i.id";
		SQL += " where IFNULL(dsd.id, 0) = 0";
		if (from != null && to != null) {
			SQL += " and DATE_FORMAT(FROM_UNIXTIME(" + Inbound.COLUMN_DATE + "/1000), '%Y-%m-%d') between '" + from
					+ "' and '" + to + "'";
		}
		if (floor != null) {
			SQL += " and te." + Employee.COLUMN_FLOOR + "=:" + Employee.COLUMN_FLOOR;
		}
		if (department != null) {
			SQL += " and d." + Employee.COLUMN_DEPARTMENT + "=:" + Employee.COLUMN_DEPARTMENT;
		}
		SQL += " GROUP BY i." + Inbound.COLUMN_ID;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createSQLQuery(SQL);
			if (floor != null) {
				query.setParameter(Employee.COLUMN_FLOOR, floor.getId());
			}
			if (department != null) {
				query.setParameter(Employee.COLUMN_DEPARTMENT, department.getId());
			}
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public DeliverySheetDetail getDeliverySheetDetail(DeliverySheetDetail deliverySheetDetail) throws XERPException {
		String hql = "SELECT dsd FROM " + DeliverySheetDetail.class.getSimpleName() + " AS dsd WHERE dsd."
				+ DeliverySheetDetail.COLUMN_ID + "=:" + DeliverySheetDetail.COLUMN_ID;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			query.setParameter(DeliverySheetDetail.COLUMN_ID, deliverySheetDetail.getId());
			return (DeliverySheetDetail) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
