/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public class EmployeeDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1632562329617975130L;
	
	private List<EmployeeDataChangeListener> countryDataChangeListeners;
	
	public EmployeeDataChangeObserver() {
		this.countryDataChangeListeners = new ArrayList<EmployeeDataChangeListener>();
	}
	
	public void add(EmployeeDataChangeListener countryDataChangeListener) {
		this.countryDataChangeListeners.add(countryDataChangeListener);
	}
	
	public void remove(EmployeeDataChangeListener countryDataChangeListener) {
		this.countryDataChangeListeners.remove(countryDataChangeListener);
	}

	public void notifyUpdate(Employee employee) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onUpdate(employee);
		}
	}

	public void notifySelectionChange(Employee employee) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onSelectionChange(employee);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifyEditButtonClick() {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onEditButtonClick();
		}
	}

	public void notifyOnBranchChange(Branch branch) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onBranchChange(branch);
		}
	}

	public void notifyDivisionChange(Division division) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onDivisionChange(division);
		}
	}

	public void notiyDepartmentChange(Department department) {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onDepartmentChange(department);
		}
	}

	public void notifyMoveEmployee() {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onMoveEmployee();
		}
	}

	public void notifyMoveWindowClose() {
		for (EmployeeDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onMoveWindowClose();
		}
	}

	public void notifyOnBranchMoveChange(Branch branch, XERPComboBox divisionCombo) {
		for (EmployeeDataChangeListener employeeDataChangeListener : countryDataChangeListeners) {
			employeeDataChangeListener.onBranchMoveChange(branch, divisionCombo);
		}
	}

	public void notifyDivisionMoveChange(Division division, XERPComboBox departmentCombo) {
		for (EmployeeDataChangeListener employeeDataChangeListener : countryDataChangeListeners) {
			employeeDataChangeListener.onDivisionMoveChange(division, departmentCombo);
		}
	}

	public void notiyDepartmentMoveChange(Department department, XERPComboBox costCentreCombo) {
		for (EmployeeDataChangeListener employeeDataChangeListener : countryDataChangeListeners) {
			employeeDataChangeListener.onDepartmentMoveChange(department, costCentreCombo);
		}
	}
}