/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.FloorManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author xpeditions
 *
 */
public class FloorSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1724832640663001003L;
	
	private FloorDataChangeObserver countryDataChangeObserver;
	private BeanFieldGroup<Floor> binder;
	private boolean canEdit;

	public FloorSaveButtonClickListener(FloorDataChangeObserver countryDataChangeObserver, BeanFieldGroup<Floor> binder, boolean canEdit) {
		this.countryDataChangeObserver = countryDataChangeObserver;
		this.binder = binder;
		this.canEdit = canEdit;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		FloorManager floorManager = new FloorManager();
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		Floor floor = this.binder.getItemDataSource().getBean();
		if (floor.getName() == null || floor.getName().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.FLOOR_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NAME_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		try {
			
			if (floor.getId() > 0) {
				if (canEdit) {
					floorManager.update(floor);
					Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_FLOOR_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				floorManager.save(floor);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_FLOOR_SAVED_SUCCESSFULLY));
			}
			this.countryDataChangeObserver.notifyUpdate(floor);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.FLOOR_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DUPILCATE_VALUE_NAME) + " '" + e.getTitle() + "' "+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}  catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.FLOOR_SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} 
		event.getButton().setEnabled(true);
	}
}