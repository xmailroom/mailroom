/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDataChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDataChangeObserver;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeListItemClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author xpeditions
 *
 */
public class EmployeeListContainer extends VerticalLayout implements EmployeeDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7372471904016649781L;
	
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private Table table;
	private String searchString;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	
	public EmployeeListContainer(EmployeeDataChangeObserver employeeDataChangeObserver) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.employeeDataChangeObserver.add(this);
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_EMPLOYEE.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_EMPLOYEE.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_EMPLOYEE.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		EmployeeListItemClickListener employeeListItemClickListener = new EmployeeListItemClickListener(this.employeeDataChangeObserver);
		table.addItemClickListener(employeeListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void createSearchSection(ResourceBundle bundle) {
		EmployeeAddNewButtonClickListener countryAddNewButtonClickListener = new EmployeeAddNewButtonClickListener(this.employeeDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_EMPLOYEE.asPermission().getName()};
		XERPPrimaryButton newCountryButton = new XERPPrimaryButton(actions);
		newCountryButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_ADD_NEW_EMPLOYEE));
		newCountryButton.setSizeFull();
		newCountryButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newCountryButton.addClickListener(countryAddNewButtonClickListener);
		this.addComponent(newCountryButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		EmployeeSearchTextShortCutListener countrySearchTextShortCutListener = new EmployeeSearchTextShortCutListener(prompt, KeyCode.ENTER, this.employeeDataChangeObserver);
		searchTextField.addShortcutListener(countrySearchTextShortCutListener);
		
		EmployeeSearchTextBlurListener countrySearchTextBlurListener = new EmployeeSearchTextBlurListener(this.employeeDataChangeObserver, countrySearchTextShortCutListener);
		searchTextField.addBlurListener(countrySearchTextBlurListener);
		
		EmployeeSearchTextFocusListener countrySearchTextFocusListener = new EmployeeSearchTextFocusListener(countrySearchTextShortCutListener);
		searchTextField.addFocusListener(countrySearchTextFocusListener);
		this.addComponent(searchTextField);
	}
	
	private void refreshTable(String searchString) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillEmployeeList(table, searchString);
		}
	}


	@Override
	public void onUpdate(Employee employee) {
		refreshTable(this.searchString);
//		if (country != null && country.getId() > 0) {
//		}
	}

	@Override
	public void onSelectionChange(Employee employee) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSearchTextValueChange(String searchString) {
		this.searchString = searchString;
		refreshTable(this.searchString);
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBranchChange(Branch branch) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDivisionChange(Division division) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDepartmentChange(Department department) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMoveEmployee() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onMoveWindowClose() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onBranchMoveChange(Branch branch, XERPComboBox divisionCombo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDivisionMoveChange(Division division, XERPComboBox departmentCombo) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onDepartmentMoveChange(Department department, XERPComboBox costCentreCombo) {
		// TODO Auto-generated method stub
		
	}
}