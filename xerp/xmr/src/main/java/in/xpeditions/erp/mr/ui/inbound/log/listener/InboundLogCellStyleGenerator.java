package in.xpeditions.erp.mr.ui.inbound.log.listener;

import com.vaadin.ui.CustomTable;
import com.vaadin.ui.CustomTable.CellStyleGenerator;

import in.xpeditions.erp.commons.ui.util.XERPStyle;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.util.InboundLogEntryUtil;

public class InboundLogCellStyleGenerator implements CellStyleGenerator {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7449409749000451047L;

	@Override
	public String getStyle(CustomTable source, Object itemId, Object propertyId) {
		LogEntry logEntry = (LogEntry) itemId;
		int pending = logEntry.getNoOfShip() - logEntry.getNoOfEntry();
		long days = InboundLogEntryUtil.getDifferenceInDays(System.currentTimeMillis(), logEntry.getDate());
		if (days >= 1 && pending > 0) {
			return XERPStyle.BG_ERROR_RED.getName();
		}
		if (pending == 0) {
			return XERPStyle.BG_GRAY.getName();
		} else if (pending >= 10) {
			return XERPStyle.BG_ORANGE.getName();
		} else if (pending >= 1) {
			return XERPStyle.BG_YELLOW.getName();
		}

		return null;
	}

}
