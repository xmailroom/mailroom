/**
 * 
 */
package in.xpeditions.erp.mr.importdata.ui.listener;

import java.io.File;
import java.io.IOException;
import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.util.XAccXLSXDataImportUtil;

/**
 * @author Saran
 *
 */
public class XAccMasterDataImportButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6721632268217519014L;
	private File file;
	private XERPComboBox masterCombo;

	public XAccMasterDataImportButtonClickListener(XERPComboBox masterCombo) {
		this.masterCombo = masterCombo;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		if (file == null) {
			String message = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE);
			String subMessage = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_PLEASE_SELECT_FILE);
			Notification.show(message,
					"\n" + subMessage,
					Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		Object value = this.masterCombo.getValue();
		if (value == null) {
			String message = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE);
			String subMessage = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_PLEASE_CHOOSE_MASTER_TYPE);
			Notification.show(message,
					"\n" + subMessage,
					Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		
		try {
			if (value.equals("Country")) {
				XAccXLSXDataImportUtil.imporCountrytData(file);
			} else if (value.equals("Employee")) {
				XAccXLSXDataImportUtil.imporEmployeetData(file);
			}
			String message = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_ACCOUNT_SUCCESS);
			Notification.show(message);
			event.getButton().setEnabled(true);
		} catch (XERPException e) {
			String message = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE);
			Notification.show(message,
					"\n" + e.getMessage(),
					Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
		} catch (IOException e) {
			String message = bundle
					.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE);
			Notification.show(message,
					"\n" + e.getMessage(),
					Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
		}
	}

	public void setImportFile(File file) {
		this.file = file;
		
	}

}
