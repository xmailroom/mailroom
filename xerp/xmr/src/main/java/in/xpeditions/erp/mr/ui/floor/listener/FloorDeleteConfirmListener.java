/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import in.xpeditions.erp.acc.resources.i18n.I18NAccResourceBundle;
import in.xpeditions.erp.acc.resources.i18n.XAccMessageConstants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.FloorManager;

/**
 * @author xpeditions
 *
 */
public class FloorDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8461449962417029054L;
	private Floor floor;
	private FloorDataChangeObserver countryDataChangeObserver;

	public FloorDeleteConfirmListener(Floor floor, FloorDataChangeObserver countryDataChangeObserver) {
		this.floor = floor;
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void onClose(ConfirmDialog confirmDialog) {
		if (confirmDialog.isConfirmed()) {
			ResourceBundle bundle = I18NAccResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
			FloorManager floorManager = new FloorManager();
			try {
				floorManager.delete(floor);
				Notification.show(bundle.getString(XAccMessageConstants.NOTIFICATION_COUNTRY_DELETED_SUCCESSFULLY));
				this.countryDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = bundle.getString(XAccMessageConstants.NOTIFICATION_COUNTRY_FOREIGN_KEY_VALUE) + " '" + floor.getName() + "' "+ bundle.getString(XAccMessageConstants.NOTIFICATION_COUNTRY_HAS_REFERENCE);
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(XAccMessageConstants.NOTIFICATION_COUNTRY_DELETE_FAILED), "<br/>" + subMessage , Notification.Type.ERROR_MESSAGE, true).show(page);
			} catch (XERPException e) {
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(XAccMessageConstants.NOTIFICATION_COUNTRY_DELETE_FAILED), "<br/>" + e.getMessage() , Notification.Type.ERROR_MESSAGE, true).show(page);
			}
		}
	}
}