package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ResourceBundle;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.log.InboundLogEntryListContainer;
import in.xpeditions.erp.mr.util.InboundLogEntryUtil;

public class InboundLogEntryListItemClickListener implements ItemClickListener, ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -368457497331522653L;

	private LogEntry logEntry;

	private InboundLogEntryListContainer inboundLogEntryListContainer;

	private ResourceBundle bundle;

	public InboundLogEntryListItemClickListener(ResourceBundle bundle,
			InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver,
			InboundLogEntryListContainer inboundLogEntryListContainer) {
		this.bundle = bundle;
		this.inboundLogEntryListContainer = inboundLogEntryListContainer;
	}

	@Override
	public void itemClick(ItemClickEvent event) {
		if (event.isDoubleClick()) {
			InboundLogEntryUtil.takeAction(this.logEntry, this.inboundLogEntryListContainer, this.bundle);
		}

	}

	public void setFocusedItem(LogEntry logEntry) {
		this.logEntry = logEntry;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		InboundLogEntryUtil.takeAction(this.logEntry, this.inboundLogEntryListContainer, this.bundle);
	}

}
