package in.xpeditions.erp.mr.ui.inbound.log;

import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDateField;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPTextArea;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.converter.ServiceProviderToLongConverter;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.log.listener.ClearButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.DataEntryFoucsShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryDataChangeListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntrySaveButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.ServiceProviderModeValueChangeListener;

public class InboundLogEntryEditContainer extends VerticalLayout implements InboundLogEntryDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6832096888591057770L;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private LogEntry logEntry;

	private BeanFieldGroup<LogEntry> logEntryBinder;

	private ResourceBundle bundle;

	private XERPComboBox spComboBox;

	private XERPTextField inBatchNoField;

	private XERPFriendlyButton deleteButton;

	private InboundLogDeleteButtonClickListener inboundLogDeleteButtonClickListener;

	public InboundLogEntryEditContainer(InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver,
			LogEntry logEntry) {
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.inboundLogEntryDataChangeObserver.addListener(this);
		this.logEntry = logEntry;

		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.createEditContainer();
	}

	private void init() {
		if (this.logEntry == null) {
			this.logEntry = new LogEntry();
			this.logEntry.setMode(ModeEnum.COURIER);
			this.logEntry.setsType(TypeEnum.INBOUND);
			this.logEntry.setNoOfShip(1);
			this.logEntry.setNoOfEntry(0);
			this.logEntry.setDate(System.currentTimeMillis());
			this.logEntry.setRemarks("");
		}
	}

	private void createEditContainer() {
		this.init();
		this.spComboBox = new XERPComboBox();

		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setStyleName(" v-app xerp-valo-facebook xerphomeui xerp-overflow-auto");

		VerticalLayout dataEntryLayout = new VerticalLayout();
		verticalLayout.addComponent(dataEntryLayout);
		dataEntryLayout.setSpacing(true);
		verticalLayout.setComponentAlignment(dataEntryLayout, Alignment.MIDDLE_CENTER);

		HorizontalLayout desigLayout = this.logEntryLayout();
		dataEntryLayout.addComponent(desigLayout);

		HorizontalLayout saveLayout = new HorizontalLayout();
		verticalLayout.addComponent(saveLayout);
		verticalLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_LEFT);
		saveLayout.setSpacing(true);
		saveLayout.setId("xerp-save-button-layout");

		String[] saveActions = new String[] { MailRoomPermissionEnum.ADD_INBOUND_LOG.asPermission().getName(),
				MailRoomPermissionEnum.ADD_INBOUND_LOG.asPermission().getName() };
		XERPFriendlyButton saveButton = new XERPFriendlyButton(saveActions);
		saveButton.setIcon(FontAwesome.SAVE);
		saveButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_SAVE));
		saveButton.setDisableOnClick(true);
		saveButton.setEnabled(true);
		saveButton.setImmediate(true);
		saveButton.addClickListener(new InboundLogEntrySaveButtonClickListener(this.inboundLogEntryDataChangeObserver,
				this.bundle, this.logEntryBinder));
		saveButton.setClickShortcut(KeyCode.ENTER, ModifierKey.CTRL);
		saveLayout.addComponent(saveButton);

		String[] deleteActions = new String[] { MailRoomPermissionEnum.DELETE_INBOUND_LOG.asPermission().getName() };
		this.deleteButton = new XERPFriendlyButton(deleteActions);

		Button button = new Button();
		button.setDisableOnClick(true);
		button.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_CLEAR));
		button.setIcon(FontAwesome.TIMES_CIRCLE);
		button.addStyleName("small");
		button.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_PRINT);
		button.addClickListener(new ClearButtonClickListener(this.logEntryBinder, this.deleteButton));

		saveLayout.addComponent(button);

		this.deleteButton.setIcon(FontAwesome.TRASH_O);
		this.deleteButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_DELETE));
		this.deleteButton.setDisableOnClick(true);
		this.deleteButton.setEnabled(true);
		this.deleteButton.setImmediate(true);
		this.deleteButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
		this.inboundLogDeleteButtonClickListener = new InboundLogDeleteButtonClickListener(
				this.inboundLogEntryDataChangeObserver, this.bundle, this.logEntry);
		this.deleteButton.addClickListener(this.inboundLogDeleteButtonClickListener);
		this.deleteButton.setVisible(false);
		saveLayout.addComponent(this.deleteButton);

		this.addComponent(verticalLayout);
	}

	private HorizontalLayout logEntryLayout() {

		this.logEntryBinder = new BeanFieldGroup<LogEntry>(LogEntry.class);
		this.logEntryBinder.setItemDataSource(this.logEntry);

		HorizontalLayout logEntryLayout = new HorizontalLayout();

		FormLayout formLayout = new FormLayout();
		logEntryLayout.addComponent(formLayout);

		HorizontalLayout batchLayout = new HorizontalLayout();
		batchLayout.setHeight("30px");
		formLayout.addComponent(batchLayout);
		batchLayout.setSpacing(true);
		batchLayout.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_BATCH_NO));
		formLayout.addComponent(batchLayout);

		this.inBatchNoField = new XERPTextField();
		this.inBatchNoField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		this.inBatchNoField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.logEntryBinder.bind(this.inBatchNoField, LogEntry.COLUMN_BATCH_NO);
		this.inBatchNoField.setEnabled(false);
		batchLayout.addComponent(this.inBatchNoField);
		batchLayout.setComponentAlignment(this.inBatchNoField, Alignment.TOP_CENTER);

		Label dLabel = new Label();
		dLabel.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_DATE));
		dLabel.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		batchLayout.addComponent(dLabel);
		batchLayout.setComponentAlignment(dLabel, Alignment.TOP_CENTER);

		XERPDateField dateField = new XERPDateField();
		dateField = new XERPDateField();
		dateField.setRequired(true);
		dateField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_160_PX);
		dateField.setDateFormat(DataHolder.commonUserDateTimeFormat);
		dateField.setResolution(Resolution.SECOND);
		dateField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.logEntryBinder.bind(dateField, LogEntry.COLUMN_DATE);
		batchLayout.addComponent(dateField);
		batchLayout.setComponentAlignment(dateField, Alignment.TOP_CENTER);

		BeanItemContainer<ServiceProvider> spContainer = new BeanItemContainer<ServiceProvider>(ServiceProvider.class);

		BeanItemContainer<ModeEnum> modeContainer = new BeanItemContainer<ModeEnum>(ModeEnum.class);
		modeContainer.addAll(Arrays.asList(ModeEnum.values()));
		XERPComboBox modeComboBox = new XERPComboBox();
		modeComboBox.focus();
		modeComboBox.setRequired(true);
		modeComboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		modeComboBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		modeComboBox.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_MODE));
		modeComboBox.setContainerDataSource(modeContainer);
		modeComboBox.setItemCaptionPropertyId(ModeEnum.CAPTION_PROPERTY_ID);
		this.logEntryBinder.bind(modeComboBox, LogEntry.COLUMN_MODE);
		modeComboBox.addValueChangeListener(new ServiceProviderModeValueChangeListener(modeComboBox, spContainer));
		formLayout.addComponent(modeComboBox);
		modeComboBox.addShortcutListener(new DataEntryFoucsShortcutListener("ctrl+up", KeyCode.ARROW_UP,
				new int[] { ModifierKey.CTRL }, modeComboBox));

		ServiceProviderManager spManager = new ServiceProviderManager();
		try {
			List<ServiceProvider> employees = spManager.list(TypeEnum.INBOUND, this.logEntry.getMode());
			spContainer.addAll(employees);
		} catch (XERPException e) {
		}
		this.spComboBox.setRequired(true);
		this.spComboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_200_PX);
		this.spComboBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.spComboBox.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_SERVICE_PROVIDER));
		this.spComboBox.setContainerDataSource(spContainer);
		this.spComboBox.setItemCaptionPropertyId(ServiceProvider.COLUMN_NAME);
		this.spComboBox.setConverter(new ServiceProviderToLongConverter<Employee, Long>());
		this.logEntryBinder.bind(this.spComboBox, LogEntry.COLUMN_SERVICE_PROVIDER);
		try {
			ServiceProvider sp = new ServiceProviderManager().getBy(this.logEntry.getServiceProviderId());
			this.spComboBox.setValue(sp);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		formLayout.addComponent(this.spComboBox);

		XERPTextField noOfShipmentField = new XERPTextField();
		noOfShipmentField.setRequired(true);
		noOfShipmentField.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_SHIPMENT));
		noOfShipmentField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_40_PX);
		noOfShipmentField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		noOfShipmentField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_ALIGN_RIGHT);
		this.logEntryBinder.bind(noOfShipmentField, LogEntry.COLUMN_NO_OF_SHIP);
		formLayout.addComponent(noOfShipmentField);

		XERPTextArea remarksField = new XERPTextArea();
		remarksField.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_REMARKS));
		remarksField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_200_PX);
		remarksField.setHeight("50px");
		remarksField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.logEntryBinder.bind(remarksField, LogEntry.COLUMN_REMARKS);
		formLayout.addComponent(remarksField);

		return logEntryLayout;
	}

	@Override
	public void onUpdate(LogEntry logEntry) {
		this.logEntry = logEntry;
		this.init();
		this.logEntryBinder.setItemDataSource(this.logEntry);
		this.inBatchNoField.setEnabled(false);
		String[] deleteActions = new String[] { MailRoomPermissionEnum.DELETE_INBOUND_LOG.asPermission().getName() };
		this.deleteButton.setVisible(
				this.logEntry != null && this.logEntry.getId() > 0 && XERPUtil.hasPermissions(deleteActions));
		this.inboundLogDeleteButtonClickListener.setLogEntry(this.logEntry);
	}

	@Override

	public void onSelectionChange(LogEntry logEntry) {
		this.logEntry = logEntry;
		this.logEntryBinder.setItemDataSource(logEntry);
		this.inBatchNoField.setEnabled(false);
		String[] deleteActions = new String[] { MailRoomPermissionEnum.DELETE_INBOUND_LOG.asPermission().getName() };
		this.deleteButton.setVisible(
				this.logEntry != null && this.logEntry.getId() > 0 && XERPUtil.hasPermissions(deleteActions));
		this.inboundLogDeleteButtonClickListener.setLogEntry(this.logEntry);
	}

}
