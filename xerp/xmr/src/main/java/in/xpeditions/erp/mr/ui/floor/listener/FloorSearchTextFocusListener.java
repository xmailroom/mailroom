/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

/**
 * @author xpeditions
 *
 */
public class FloorSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2656576707840323694L;
	private FloorSearchTextShortCutListener countrySearchTextShortCutListener;

	public FloorSearchTextFocusListener(FloorSearchTextShortCutListener countrySearchTextShortCutListener) {
		this.countrySearchTextShortCutListener = countrySearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(countrySearchTextShortCutListener);
	}
}