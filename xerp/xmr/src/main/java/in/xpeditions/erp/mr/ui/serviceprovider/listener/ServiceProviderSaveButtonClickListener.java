/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1724832640663001003L;
	
	private ServiceProviderDataChangeObserver countryDataChangeObserver;
	private BeanFieldGroup<ServiceProvider> binder;
	private boolean canEdit;

	public ServiceProviderSaveButtonClickListener(ServiceProviderDataChangeObserver countryDataChangeObserver, BeanFieldGroup<ServiceProvider> binder, boolean canEdit) {
		this.countryDataChangeObserver = countryDataChangeObserver;
		this.binder = binder;
		this.canEdit = canEdit;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		ServiceProviderManager serviceProviderManager = new ServiceProviderManager();
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		ServiceProvider serviceProvider = this.binder.getItemDataSource().getBean();
		if (serviceProvider.getName() == null || serviceProvider.getName().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.SERVICE_PROVIDER_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.SERVICE_PROVIER_NAME_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		try {
			
			if (serviceProvider.getId() > 0) {
				if (canEdit) {
					serviceProviderManager.update(serviceProvider);
					Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_SERVICE_PROVIDER_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				serviceProviderManager.save(serviceProvider);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_SERVICE_PROVIDER_SAVED_SUCCESSFULLY));
			}
			this.countryDataChangeObserver.notifyUpdate(serviceProvider);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.SERVICE_PROVIDER_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DUPILCATE_VALUE_NAME) + " '" + e.getTitle() + "' "+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}  catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.SERVICE_PROVIDER_SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} 
		event.getButton().setEnabled(true);
	}
}