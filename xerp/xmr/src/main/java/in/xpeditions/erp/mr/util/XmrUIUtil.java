/**
 * 
 */
package in.xpeditions.erp.mr.util;

import java.util.ResourceBundle;

import org.vaadin.addons.lazyquerycontainer.LazyQueryContainer;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;

import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreListQueryFactory;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentListQueryFactory;
import in.xpeditions.erp.mr.ui.division.listener.DivisionListQueryFactory;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeListQueryFactory;
import in.xpeditions.erp.mr.ui.floor.listener.FloorListQueryFactory;
import in.xpeditions.erp.mr.ui.serviceprovider.listener.ServiceProviderListQueryFactory;

/**
 * @author Saran
 *
 */
public class XmrUIUtil {

	public static void fillDivisionList(Table table, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		table.removeAllItems();

		QueryFactory queryFactory = new DivisionListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.DIVISION_NAME,
				String.class, "", true, true);
		container.addContainerProperty(XMailroomTableItemConstants.DIVISION_CODE,
				String.class, "", true, true);

		table.setContainerDataSource(container);
		table.setVisibleColumns(XMailroomTableItemConstants.DIVISION_NAME,
				XMailroomTableItemConstants.DIVISION_CODE);

		table
				.setColumnHeader(
						XMailroomTableItemConstants.DIVISION_NAME,
						bundle.getString(MailRoomMessageConstants.DIVISION_LIST_HEADER_NAME));
		table
				.setColumnHeader(
						XMailroomTableItemConstants.DIVISION_CODE,
						bundle.getString(MailRoomMessageConstants.DIVISION_LIST_HEADER_CODE));

		table.setPageLength(PAGE_LENGTH);
	}

	public static void fillDepartmentList(Table table, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		table.removeAllItems();

		QueryFactory queryFactory = new DepartmentListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.DEPARTMENT_NAME,
				String.class, "", true, true);
		container.addContainerProperty(XMailroomTableItemConstants.DEPARTMENT_CODE,
				String.class, "", true, true);

		table.setContainerDataSource(container);
		table.setVisibleColumns(XMailroomTableItemConstants.DEPARTMENT_NAME,
				XMailroomTableItemConstants.DEPARTMENT_CODE);

		table.setColumnHeader(
						XMailroomTableItemConstants.DEPARTMENT_NAME,
						bundle.getString(MailRoomMessageConstants.DEPARTMENT_LIST_HEADER_NAME));
		table.setColumnHeader(
						XMailroomTableItemConstants.DEPARTMENT_CODE,
						bundle.getString(MailRoomMessageConstants.DEPARTMENT_LIST_HEADER_CODE));

		table.setPageLength(PAGE_LENGTH);
	}

	public static void fillCostcentreList(Table table, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		table.removeAllItems();

		QueryFactory queryFactory = new CostcentreListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.COSTCENTRE_NAME,
				String.class, "", true, true);
		container.addContainerProperty(XMailroomTableItemConstants.COSTCENTRE_CODE,
				String.class, "", true, true);

		table.setContainerDataSource(container);
		table.setVisibleColumns(XMailroomTableItemConstants.COSTCENTRE_NAME,
				XMailroomTableItemConstants.COSTCENTRE_CODE);

		table.setColumnHeader(
						XMailroomTableItemConstants.COSTCENTRE_NAME,
						bundle.getString(MailRoomMessageConstants.COSTCENTRE_LIST_HEADER_NAME));
		table.setColumnHeader(
						XMailroomTableItemConstants.COSTCENTRE_CODE,
						bundle.getString(MailRoomMessageConstants.COSTCENTRE_LIST_HEADER_CODE));

		table.setPageLength(PAGE_LENGTH);
	}

	public static void fillFloorList(Table table, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		table.removeAllItems();

		QueryFactory queryFactory = new FloorListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.FLOOR_NAME,
				String.class, "", true, true);

		table.setContainerDataSource(container);
		table.setVisibleColumns(XMailroomTableItemConstants.FLOOR_NAME);

		table
				.setColumnHeader(
						XMailroomTableItemConstants.FLOOR_NAME,
						bundle.getString(MailRoomMessageConstants.FLOOR_LIST_HEADER_NAME));

		table.setPageLength(PAGE_LENGTH);
	}

	public static void fillEmployeeList(Table pagedTable, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		pagedTable.removeAllItems();

		QueryFactory queryFactory = new EmployeeListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_NAME,
				String.class, "", true, true);
		container.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_DEPARTMENT,
				String.class, "", true, true);

		pagedTable.setContainerDataSource(container);
		pagedTable.setVisibleColumns(XMailroomTableItemConstants.EMPLOYEE_NAME, XMailroomTableItemConstants.EMPLOYEE_DEPARTMENT);

		pagedTable
				.setColumnHeader(
						XMailroomTableItemConstants.EMPLOYEE_NAME,
						bundle.getString(MailRoomMessageConstants.EMPLOYEE_LIST_HEADER_NAME));
		
		pagedTable
		.setColumnHeader(
				XMailroomTableItemConstants.EMPLOYEE_DEPARTMENT,
				bundle.getString(MailRoomMessageConstants.EMPLOYEE_LIST_HEADER_DEPARTMENT_NAME));

		pagedTable.setPageLength(PAGE_LENGTH);
	}
	
	public static void fillServiceProviderList(Table pagedTable, String searchString) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		int PAGE_LENGTH = 10;

		pagedTable.removeAllItems();

		QueryFactory queryFactory = new ServiceProviderListQueryFactory(searchString);
		LazyQueryContainer container = new LazyQueryContainer(queryFactory,
				null, PAGE_LENGTH, true);

		container.addContainerProperty(XMailroomTableItemConstants.SERVICE_PROVIDER_NAME,
				String.class, "", true, true);
		container.addContainerProperty(XMailroomTableItemConstants.SERVICE_PROVIDER_TYPE,
				String.class, "", true, true);

		pagedTable.setContainerDataSource(container);
		pagedTable.setVisibleColumns(XMailroomTableItemConstants.SERVICE_PROVIDER_NAME,
				XMailroomTableItemConstants.SERVICE_PROVIDER_TYPE);

		pagedTable
				.setColumnHeader(
						XMailroomTableItemConstants.SERVICE_PROVIDER_NAME,
						bundle.getString(MailRoomMessageConstants.SERVICE_PROVIDER_LIST_HEADER_NAME));
		pagedTable
				.setColumnHeader(
						XMailroomTableItemConstants.SERVICE_PROVIDER_TYPE,
						bundle.getString(MailRoomMessageConstants.SERVICE_PROVIDER_LIST_HEADER_TYPE));

		pagedTable.setPageLength(PAGE_LENGTH);
	}
	
}
