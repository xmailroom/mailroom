package in.xpeditions.erp.mr.ui.inbound;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.Table.Align;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

import in.xpeditions.erp.acc.entity.District;
import in.xpeditions.erp.acc.manager.DistrictManager;
import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPCheckBox;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDateField;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPTextArea;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.converter.CityToLongConverter;
import in.xpeditions.erp.mr.converter.EmployeeToLongConverter;
import in.xpeditions.erp.mr.converter.ServiceProviderToLongConverter;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.From;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.To;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.manager.EmployeeManager;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.AddToButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.BatchNewItemHandler;
import in.xpeditions.erp.mr.ui.inbound.listener.BulkCheckBoxValueChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.BulkEntryValueChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.ClearButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.DataEntryFoucsShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.listener.EmployeeChangeButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.EmployeeComboFocusListener;
import in.xpeditions.erp.mr.ui.inbound.listener.EmployeeValueChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundSaveButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundValueChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.LogEntryChangeButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.NoOfDocsValueChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.RemoveToButtonClickListener;
import in.xpeditions.erp.mr.ui.inbound.listener.ServiceProviderModeValueChangeListener;

public class InboundEditContainer extends VerticalLayout implements InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2633519841373162221L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private Inbound inbound;

	private ResourceBundle bundle;

	private BeanFieldGroup<Inbound> inboundBinder;

	private XERPComboBox spComboBox;

	private XERPComboBox fromComboBox;

	private XERPTextField fromField;

	private Table toTable;

	private BeanFieldGroup<From> fromBinder;

	private XERPComboBox cityComboBox;

	private HashMap<Long, Employee> employeeMap;

	private HashMap<Long, District> cityMap;

	private BeanItemContainer<LogEntry> leContainer;

	private XERPComboBox inBatchNoComboBox;

	private XERPTextField awbField;

	private HashMap<BeanFieldGroup<To>, Integer> noOfDocsMap;

	private XERPTextField nosField;

	private XERPCheckBox bulkCheckBox;

	private XERPButton consignorEditButton;

	// private List<Inbound> inbounds = new ArrayList<Inbound>();

	public InboundEditContainer(InboundDataChangeObserver inboundDataChangeObserver, Inbound inbound) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.inboundDataChangeObserver.addListener(this);
		this.inbound = inbound;

		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.createEditContainer();
	}

	private void init() {
		this.noOfDocsMap = new HashMap<BeanFieldGroup<To>, Integer>();
		this.employeeMap = new HashMap<Long, Employee>();
		try {
			List<Employee> employees = new EmployeeManager().list();
			for (Employee employee : employees) {
				this.employeeMap.put(employee.getId(), employee);
			}
		} catch (XERPException e) {
			e.printStackTrace();
		}
		this.cityMap = new HashMap<Long, District>();
		try {
			List<District> cities = new DistrictManager().list();
			for (District city : cities) {
				this.cityMap.put(city.getId(), city);
			}
		} catch (XERPException e) {
			e.printStackTrace();
		}

		if (this.inbound == null) {
			this.inbound = new Inbound();
			this.inbound.setMode(ModeEnum.INTERBRANCH);
			this.inbound.setsType(TypeEnum.INBOUND);
			this.inbound.setDate(System.currentTimeMillis());
			this.inbound.setAwbNo("");
			this.inbound.setRemarks("");
			this.inbound.setNoOfDocs(1);

			From from = new From();
			from.setName("");
			from.setRefNo("");
			this.inbound.setxFrom(from);

			To to = new To();
			to.setName("");
			to.setIsrNo("");
			to.setRemarks("");
			this.inbound.setxTo(to);
		}
		// if (this.inbound.isBulk()) {
		// try {
		// this.inbounds = new
		// InboundManager().listBulk(this.inbound.getLogEntry());
		// } catch (XERPException e) {
		// e.printStackTrace();
		// }
		// }
	}

	private void createEditContainer() {
		this.init();
		this.toTable = new Table();
		this.nosField = new XERPTextField();
		VerticalLayout verticalLayout = new VerticalLayout();
		verticalLayout.setStyleName(" v-app xerp-valo-facebook xerphomeui xerp-overflow-auto");

		VerticalLayout dataEntryLayout = new VerticalLayout();
		verticalLayout.addComponent(dataEntryLayout);
		verticalLayout.setComponentAlignment(dataEntryLayout, Alignment.MIDDLE_CENTER);

		HorizontalLayout desigLayout = this.inboundLayout();
		dataEntryLayout.addComponent(desigLayout);

		VerticalLayout toLayout = this.toLayout();
		dataEntryLayout.addComponent(toLayout);

		nosField.addValueChangeListener(new BulkEntryValueChangeListener(nosField, this.toTable, this));
		bulkCheckBox.addValueChangeListener(
				new BulkCheckBoxValueChangeListener(nosField, this.toTable, this.bundle, this.noOfDocsMap));

		HorizontalLayout saveLayout = new HorizontalLayout();
		verticalLayout.addComponent(saveLayout);
		verticalLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_LEFT);
		saveLayout.setSpacing(true);
		saveLayout.setId("xerp-save-button-layout");

		String[] saveActions = new String[] { MailRoomPermissionEnum.ADD_INBOUND_LOG.asPermission().getName(),
				MailRoomPermissionEnum.ADD_INBOUND_LOG.asPermission().getName() };
		XERPFriendlyButton saveButton = new XERPFriendlyButton(saveActions);
		saveButton.setIcon(FontAwesome.SAVE);
		saveButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_SAVE));
		saveButton.setDisableOnClick(true);
		saveButton.setEnabled(true);
		saveButton.setImmediate(true);
		saveButton.addClickListener(new InboundSaveButtonClickListener(this.inboundDataChangeObserver, this.bundle,
				this.inboundBinder, this.fromBinder, this.toTable, this.nosField, this.noOfDocsMap));
		saveButton.setClickShortcut(KeyCode.ENTER, ModifierKey.CTRL);
		saveLayout.addComponent(saveButton);

		Button button = new Button();
		button.setDisableOnClick(true);
		button.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_CLEAR));
		button.setIcon(FontAwesome.TIMES_CIRCLE);
		button.addStyleName("small");
		button.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_PRINT);
		button.addClickListener(new ClearButtonClickListener(this.inboundDataChangeObserver));

		saveLayout.addComponent(button);

		if (this.inbound != null && this.inbound.getId() > 0) {
			String[] deleteActions = new String[] { MailRoomPermissionEnum.DELETE_INBOUND.asPermission().getName() };
			XERPFriendlyButton deleteButton = new XERPFriendlyButton(deleteActions);
			deleteButton.setIcon(FontAwesome.TRASH_O);
			deleteButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_DELETE));
			deleteButton.setDisableOnClick(true);
			deleteButton.setEnabled(true);
			deleteButton.setImmediate(true);
			deleteButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
			deleteButton.addClickListener(
					new InboundDeleteButtonClickListener(this.inboundDataChangeObserver, this.bundle, this.inbound));
			saveLayout.addComponent(deleteButton);
		}

		this.addComponent(verticalLayout);
	}

	private VerticalLayout toLayout() {
		VerticalLayout toLayout = new VerticalLayout();
		toLayout.setSpacing(true);
		toLayout.setMargin(new MarginInfo(false, false, true, false));

		this.toTable.setCaption("To");
		this.toTable.setWidth("1100px");
		this.toTable.setHeight("250px");

		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX),
				String.class, "", this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX), null,
				Align.LEFT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE),
				HorizontalLayout.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE),
				null, Align.LEFT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH),
				XERPTextField.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH), null,
				Align.LEFT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_SHIPMENT),
				XERPTextField.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_SHIPMENT),
				null, Align.RIGHT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO),
				XERPTextField.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO), null,
				Align.LEFT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS),
				XERPTextField.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS), null,
				Align.LEFT);
		this.toTable.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				HorizontalLayout.class, null, this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), null,
				Align.LEFT);

		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX), 50);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE), 280);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH), 200);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_SHIPMENT), 100);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO), 160);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS), 160);
		this.toTable.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), 137);

		toLayout.addComponent(this.toTable);

		// if (this.inbound.isBulk()) {
		// int i = 1;
		// for (Inbound inbound : this.inbounds) {
		// this.addToItem(i, inbound.getxTo(), inbound.getNoOfDocs());
		// }
		// } else {
		this.refreshToTable();
		// }

		this.toTable
				.setVisibleColumns(new Object[] { this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_INDEX),
						this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE),
						this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BRANCH),
						this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ISR_NO),
						this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS) });
		return toLayout;
	}

	private void refreshToTable() {
		this.toTable.removeAllItems();
		int i = 1;
		this.addToItem(i, this.inbound.getxTo(), this.inbound.getNoOfDocs());
	}

	/**
	 * @param i
	 */
	public void addToItem(int i, To to, int noOfDocs) {
		BeanFieldGroup<To> toBinder = new BeanFieldGroup<To>(To.class);
		to = this.initTo(to);
		toBinder.setItemDataSource(to);

		XERPTextField branchCombo = new XERPTextField();

		HorizontalLayout employeeLayout = new HorizontalLayout();

		BeanItemContainer<Employee> employeeContainer = new BeanItemContainer<Employee>(Employee.class);
		employeeContainer.addAll(this.employeeMap.values());
		XERPComboBox employeeCombo = new XERPComboBox();
		employeeCombo.setWidth("230px");
		employeeCombo.setRequired(true);
		employeeCombo.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		employeeCombo.setContainerDataSource(employeeContainer);
		employeeCombo.setConverter(new EmployeeToLongConverter<Employee, Long>(this.employeeMap));
		employeeCombo.setItemCaptionPropertyId(Employee.COLUMN_NAME);
		employeeCombo.setValue(this.employeeMap.get(this.inbound.getxTo().getEmployeeId()));
		employeeCombo.addValueChangeListener(new EmployeeValueChangeListener(branchCombo));
		EmployeeComboFocusListener employeeComboListener = new EmployeeComboFocusListener();
		employeeCombo.addFocusListener(employeeComboListener);
		toBinder.bind(employeeCombo, To.COLUMN_EMPLOYEE);
		employeeLayout.addComponent(employeeCombo);

		EmployeeChangeButtonClickListener employeeChangeButtonClickListener = new EmployeeChangeButtonClickListener(
				employeeCombo);
		XERPButton editButton = new XERPButton();
		editButton.setIcon(FontAwesome.EDIT);
		editButton.setStyleName(BaseTheme.BUTTON_LINK);
		editButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
		editButton.addClickListener(employeeChangeButtonClickListener);
		employeeLayout.addComponent(editButton);

		branchCombo.setEnabled(false);
		branchCombo.setWidth("190px");

		XERPTextField noOfDocsField = new XERPTextField();
		noOfDocsField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_50_PX);
		noOfDocsField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_ALIGN_RIGHT);
		noOfDocsField.setValue(noOfDocs + "");
		noOfDocsField.addValueChangeListener(new NoOfDocsValueChangeListener(this.noOfDocsMap, toBinder));
		this.noOfDocsMap.put(toBinder, noOfDocs);

		XERPTextField isrNoField = new XERPTextField();
		isrNoField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		isrNoField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		toBinder.bind(isrNoField, To.COLUMN_ISR_NO);

		XERPTextField remarksField = new XERPTextField();
		remarksField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		remarksField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		toBinder.bind(remarksField, To.COLUMN_REMARKS);

		HorizontalLayout actionLayout = new HorizontalLayout();
		actionLayout.setSpacing(true);

		XERPButton addButton = new XERPButton();
		addButton.setStyleName(BaseTheme.BUTTON_LINK);
		addButton.setIcon(FontAwesome.PLUS);
		addButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_FRIENDLY);
		addButton.addClickListener(new AddToButtonClickListener(this, this.toTable));
		actionLayout.addComponent(addButton);

		XERPButton deleteButton = new XERPButton();
		deleteButton.setStyleName(BaseTheme.BUTTON_LINK);
		deleteButton.setIcon(FontAwesome.MINUS);
		deleteButton.addClickListener(new RemoveToButtonClickListener(this, toBinder));
		deleteButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_DANGER);
		actionLayout.addComponent(deleteButton);

		this.toTable.addItem(new Object[] { i + "", employeeLayout, branchCombo, noOfDocsField, isrNoField,
				remarksField, actionLayout }, toBinder);

	}

	public void removeToItem(BeanFieldGroup<To> toBinder) {
		this.toTable.removeItem(toBinder);
		this.noOfDocsMap.remove(toBinder);
	}

	private To initTo(To to) {
		if (to == null) {
			to = new To();
			to.setIsrNo("");
			to.setName("");
			to.setRemarks("");
		}
		return to;
	}

	private HorizontalLayout inboundLayout() {

		this.inboundBinder = new BeanFieldGroup<Inbound>(Inbound.class);

		HorizontalLayout inboundLayout = new HorizontalLayout();

		FormLayout formLayout = new FormLayout();
		inboundLayout.addComponent(formLayout);

		HorizontalLayout dateLayout = new HorizontalLayout();
		formLayout.addComponent(dateLayout);
		dateLayout.setSpacing(true);
		dateLayout.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE));

		XERPDateField dateField = new XERPDateField();
		dateField = new XERPDateField();
		dateField.setRequired(true);
		dateField.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		dateField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_160_PX);
		dateField.setDateFormat(DataHolder.commonUserDateTimeFormat);
		dateField.setResolution(Resolution.SECOND);
		dateField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.inboundBinder.bind(dateField, Inbound.COLUMN_DATE);
		dateLayout.addComponent(dateField);

		Label awbLabel = new Label();
		awbLabel.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		awbLabel.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_AWB_NO));
		dateLayout.addComponent(awbLabel);
		dateLayout.setComponentAlignment(awbLabel, Alignment.MIDDLE_CENTER);

		this.awbField = new XERPTextField();
		this.awbField.setRequired(true);
		this.awbField.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.awbField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		this.awbField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.inboundBinder.bind(this.awbField, Inbound.COLUMN_AWB_NO);
		dateLayout.addComponent(awbField);
		awbField.addShortcutListener(new DataEntryFoucsShortcutListener("ctrl+right", KeyCode.ARROW_RIGHT,
				new int[] { ModifierKey.CTRL }, awbField));

		bulkCheckBox = new XERPCheckBox();
		bulkCheckBox.setVisible(this.inbound.isBulk());
		dateLayout.addComponent(bulkCheckBox);
		bulkCheckBox.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BULK));
		bulkCheckBox.setHeight("10px");
		dateLayout.setComponentAlignment(bulkCheckBox, Alignment.TOP_CENTER);
		this.inboundBinder.bind(bulkCheckBox, Inbound.COLUMN_BULK);

		nosField.setValue(this.inbound.getNoOfDocs() + "");
		nosField.setVisible(false);
		nosField.setInputPrompt("Nos.");
		nosField.setHeight("20px");
		nosField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_30_PX);
		nosField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		nosField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_ALIGN_RIGHT);
		dateLayout.addComponent(nosField);
		dateLayout.setComponentAlignment(nosField, Alignment.TOP_CENTER);

		HorizontalLayout batchLayout = new HorizontalLayout();
		batchLayout.setHeight("30px");
		formLayout.addComponent(batchLayout);
		batchLayout.setSpacing(true);
		batchLayout.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO));
		formLayout.addComponent(batchLayout);

		this.leContainer = new BeanItemContainer<LogEntry>(LogEntry.class);
		LogEntryManager logEntryManager = new LogEntryManager();
		try {
			List<LogEntry> logEntries = logEntryManager.listPending();
			this.leContainer.addAll(logEntries);
			if (!this.leContainer.containsId(this.inbound.getLogEntry())) {
				this.leContainer.addItem(this.inbound.getLogEntry());
			}
		} catch (XERPException e) {
		}

		XERPComboBox modeComboBox = new XERPComboBox();
		this.spComboBox = new XERPComboBox();
		this.inBatchNoComboBox = new XERPComboBox();

		this.inBatchNoComboBox.setRequired(true);
		this.inBatchNoComboBox.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.inBatchNoComboBox.setNewItemsAllowed(true);
		this.inBatchNoComboBox.setNewItemHandler(new BatchNewItemHandler(leContainer, inBatchNoComboBox));
		this.inBatchNoComboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_160_PX);
		this.inBatchNoComboBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.inBatchNoComboBox.setContainerDataSource(leContainer);
		this.inBatchNoComboBox.setItemCaptionPropertyId(LogEntry.COLUMN_BATCH_NO);
		this.inBatchNoComboBox.addValueChangeListener(
				new InboundValueChangeListener(this.inBatchNoComboBox, modeComboBox, this.spComboBox));
		this.inboundBinder.bind(this.inBatchNoComboBox, Inbound.COLUMN_LOG_ENTRY);
		batchLayout.addComponent(this.inBatchNoComboBox);
		batchLayout.setComponentAlignment(inBatchNoComboBox, Alignment.TOP_CENTER);

		/*LogEntryComboFocusListener logEntryComboFocusListener = new LogEntryComboFocusListener();
		this.inBatchNoComboBox.addFocusListener(logEntryComboFocusListener);*/

		LogEntryChangeButtonClickListener logEntryChangeButtonClickListener = new LogEntryChangeButtonClickListener(
				this.inBatchNoComboBox);
		XERPButton editButton = new XERPButton();
		editButton.setIcon(FontAwesome.EDIT);
		editButton.setStyleName(BaseTheme.BUTTON_LINK);
		editButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
		editButton.addClickListener(logEntryChangeButtonClickListener);
		batchLayout.addComponent(editButton);

		Label mLabel = new Label();
		mLabel.setWidth("55px");
		mLabel.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_MODE));
		mLabel.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		batchLayout.addComponent(mLabel);
		batchLayout.setComponentAlignment(mLabel, Alignment.MIDDLE_CENTER);

		this.fromComboBox = new XERPComboBox();
		this.consignorEditButton = new XERPButton();
		this.fromField = new XERPTextField();
		BeanItemContainer<ServiceProvider> spContainer = new BeanItemContainer<ServiceProvider>(ServiceProvider.class);
		BeanItemContainer<ModeEnum> modeContainer = new BeanItemContainer<ModeEnum>(ModeEnum.class);

		modeContainer.addAll(Arrays.asList(ModeEnum.values()));
		modeComboBox.setRequired(true);
		modeComboBox.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		modeComboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		modeComboBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		modeComboBox.setContainerDataSource(modeContainer);
		modeComboBox.setItemCaptionPropertyId(ModeEnum.CAPTION_PROPERTY_ID);
		modeComboBox.addValueChangeListener(new ServiceProviderModeValueChangeListener(modeComboBox, spContainer,
				this.fromComboBox, this.fromField, this.consignorEditButton));
		this.inboundBinder.bind(modeComboBox, Inbound.COLUMN_MODE);
		batchLayout.addComponent(modeComboBox);

		Label sLabel = new Label();
		sLabel.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_SERVICE_PROVIDER));
		sLabel.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		batchLayout.addComponent(sLabel);
		batchLayout.setComponentAlignment(sLabel, Alignment.MIDDLE_CENTER);

		ServiceProviderManager spManager = new ServiceProviderManager();
		try {
			List<ServiceProvider> sps = spManager.list(TypeEnum.INBOUND, this.inbound.getMode());
			spContainer.addAll(sps);
		} catch (XERPException e) {
		}
		this.spComboBox.setRequired(true);
		this.spComboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_160_PX);
		this.spComboBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.spComboBox.setContainerDataSource(spContainer);
		this.spComboBox.setItemCaptionPropertyId(ServiceProvider.COLUMN_NAME);
		this.spComboBox.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.spComboBox.setConverter(new ServiceProviderToLongConverter<Employee, Long>());
		this.inboundBinder.bind(this.spComboBox, Inbound.COLUMN_SERVICE_PROVIDER);
		batchLayout.addComponent(this.spComboBox);

		HorizontalLayout rLayout = new HorizontalLayout();
		rLayout.setSpacing(true);
		formLayout.addComponent(rLayout);
		rLayout.setCaption(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REMARKS));

		XERPTextArea remarksField = new XERPTextArea();
		remarksField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_200_PX);
		remarksField.setHeight("50px");
		remarksField.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.inboundBinder.bind(remarksField, Inbound.COLUMN_REMARKS);
		rLayout.addComponent(remarksField);

		this.inboundBinder.setItemDataSource(this.inbound);
		if (this.inbound.getLogEntry() != null && this.inbound.getLogEntry().getId() > 0) {
			modeComboBox.setEnabled(false);
			this.spComboBox.setEnabled(false);
		}

		HorizontalLayout consignmentLayout = this.consignmentLayout();

		formLayout.addComponent(consignmentLayout);

		this.awbField.setReadOnly(this.inbound.isBulk());

		return inboundLayout;
	}

	/**
	 * @return
	 */
	private HorizontalLayout consignmentLayout() {

		this.fromBinder = new BeanFieldGroup<From>(From.class);

		HorizontalLayout consignmentLayout = new HorizontalLayout();
		consignmentLayout.setCaption("From");
		consignmentLayout.setSpacing(true);

		boolean isInterBranch = this.inbound.getMode().equals(ModeEnum.INTERBRANCH);

		BeanItemContainer<Employee> employeeContainer = new BeanItemContainer<Employee>(Employee.class);
		employeeContainer.addAll(this.employeeMap.values());
		this.fromComboBox.setInputPrompt(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_CONSIGNOR));
		this.fromComboBox.setWidth("230px");
		this.fromComboBox.setVisible(isInterBranch);
		this.fromComboBox.setRequired(isInterBranch);
		this.fromComboBox.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.fromComboBox.setContainerDataSource(employeeContainer);
		this.fromComboBox.setItemCaptionPropertyId(Employee.COLUMN_NAME);
		this.fromComboBox.setValue(this.employeeMap.get(this.inbound.getxFrom().getEmployeeId()));
		this.fromComboBox.setConverter(new EmployeeToLongConverter<Employee, Long>(this.employeeMap));
		this.fromBinder.bind(fromComboBox, From.COLUMN_EMPLOYEE);
		consignmentLayout.addComponent(this.fromComboBox);
		EmployeeComboFocusListener employeeComboListener = new EmployeeComboFocusListener();
		this.fromComboBox.addFocusListener(employeeComboListener);

		EmployeeChangeButtonClickListener employeeChangeButtonClickListener = new EmployeeChangeButtonClickListener(
				this.fromComboBox);
		
		this.consignorEditButton.setIcon(FontAwesome.EDIT);
		this.consignorEditButton.setStyleName(BaseTheme.BUTTON_LINK);
		this.consignorEditButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
		this.consignorEditButton.addClickListener(employeeChangeButtonClickListener);
		this.consignorEditButton.setVisible(isInterBranch);
		consignmentLayout.addComponent(this.consignorEditButton);

		this.fromField.setInputPrompt(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_CONSIGNOR));
		this.fromField.setWidth("230px");
		this.fromField.setVisible(!isInterBranch);
		this.fromField.setRequired(!isInterBranch);
		this.fromField.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.fromBinder.bind(this.fromField, From.COLUMN_FROM_NAME);
		consignmentLayout.addComponent(this.fromField);

		BeanItemContainer<District> cityContainer = new BeanItemContainer<District>(District.class);
		cityContainer.addAll(this.cityMap.values());
		this.cityComboBox = new XERPComboBox();
		this.cityComboBox.setInputPrompt(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_CITY));
		this.cityComboBox.setContainerDataSource(cityContainer);
		this.cityComboBox.setItemCaptionPropertyId(District.COLUMN_NAME);
		this.cityComboBox.setRequired(true);
		this.cityComboBox.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.cityComboBox.setValue(this.cityMap.get(this.inbound.getxFrom().getCityId()));
		this.cityComboBox.setConverter(new CityToLongConverter<Employee, Long>(this.cityMap));
		this.fromBinder.bind(this.cityComboBox, From.COLUMN_CITY);
		consignmentLayout.addComponent(this.cityComboBox);

		XERPTextField refField = new XERPTextField();
		refField.setInputPrompt(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_REF_NO));
		this.fromBinder.bind(refField, From.COLUMN_REF_NO);
		consignmentLayout.addComponent(refField);

		this.fromBinder.setItemDataSource(this.inbound.getxFrom());

		return consignmentLayout;
	}

	@Override
	public void onUpdate(Inbound inbound) {
		this.inbound = null;
		this.removeAllComponents();
		this.createEditContainer();
	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		this.inbound = inbound;
		this.removeAllComponents();
		this.createEditContainer();
	}

	@SuppressWarnings("unchecked")
	public void removeItem(int i) {
		List<?> itemIds = (List<?>) this.toTable.getItemIds();
		this.removeToItem((BeanFieldGroup<To>) itemIds.get(i));
	}

	public void setLogEntry(LogEntry logEntry) {
		this.inBatchNoComboBox.setValue(logEntry);
		this.awbField.focus();
	}

	@Override
	public void onEmployeeChange(Employee employee) {

	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		// TODO Auto-generated method stub

	}

}
