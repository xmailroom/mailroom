/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import java.io.Serializable;

import in.xpeditions.erp.mr.entity.ServiceProvider;

/**
 * @author xpeditions
 *
 */
public interface ServiceProviderDataChangeListener extends Serializable {

	public void onUpdate(ServiceProvider serviceProvider);

	public void onSelectionChange(ServiceProvider serviceProvider);

	public void onSearchTextValueChange(String value);

	public void onEditButtonClick();

}
