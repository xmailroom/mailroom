/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDataChangeObserver;

/**
 * @author xpeditions
 *
 */
public class FloorHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8373592946196286721L;
	
	public FloorHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        FloorDataChangeObserver floorDataChangeObserver = new FloorDataChangeObserver();
        
        FloorListContainer countryListContainer = new FloorListContainer(floorDataChangeObserver);
        containerSplitter.setFirstComponent(countryListContainer);
        
        FloorEditContainer countryEditContainer = new FloorEditContainer(floorDataChangeObserver);
        containerSplitter.setSecondComponent(countryEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
