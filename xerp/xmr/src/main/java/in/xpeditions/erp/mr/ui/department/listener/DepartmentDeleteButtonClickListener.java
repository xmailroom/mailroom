/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DepartmentDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1190581082926644152L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private Department department;

	public DepartmentDeleteButtonClickListener(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		String pleaseConfirmTxt = bundle.getString(MailRoomMessageConstants.DEPARTMENT_DELETE_CONFIRM_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = bundle.getString(MailRoomMessageConstants.DEPARTMENT_DELETE_CONFIRM_MESSAGE);
		String captionYes = bundle.getString(MailRoomMessageConstants.DEPARTMENT_DELETE_CONFIRM_BUTTON_CAPTION_YES);
		String captionNo = bundle.getString(MailRoomMessageConstants.DEPARTMENT_DELETE_CONFIRM_BUTTON_CAPTION_NO);
		DepartmentDeleteConfirmListener countryDeleteConfirmListener = new DepartmentDeleteConfirmListener(this.department, this.departmentDataChangeObserver);
		ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt, doYouWantToDeleteTxt + this.department.getName() + "?", captionYes, captionNo, countryDeleteConfirmListener);
		event.getButton().setEnabled(true);
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
