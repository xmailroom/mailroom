package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class DeliverySheetDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5891461053097580477L;

	private ResourceBundle bundle;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private DeliverySheet deliverySheet;

	public DeliverySheetDeleteConfirmListener(ResourceBundle bundle,
			DeliverySheetDataChangeObserver deliverySheetDataChangeObserver, DeliverySheet deliverySheet) {
		this.bundle = bundle;
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheet = deliverySheet;
	}

	@Override
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			DeliverySheetManager deliverySheetManager = new DeliverySheetManager();
			try {
				deliverySheetManager.delete(this.deliverySheet);
				Notification.show(this.bundle
						.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DELETED_SUCCESSFULLY));
				this.deliverySheetDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_FOREIGN_KEY_VALUE)
						+ " '" + this.deliverySheet.getNumber() + "' "
						+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_HAS_REFERENCE);
				String caption = this.bundle
						.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DELETE_FAILED);
				Notification.show(caption, "<br/>" + subMessage, Type.ERROR_MESSAGE);
			} catch (XERPException e) {
				String subMessage = "<br/>" + e.getMessage();
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DELETE_FAILED),
						subMessage, Type.ERROR_MESSAGE);

			}
		}
	}

}
