/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public interface DivisionDAOManager {

	public void save(Division division) throws XERPException;

	public void update(Division division) throws XERPException;

	public List<Division> listDivisions(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public Long getDivisionCount(String searchString) throws XERPException;

	public void delete(Division division) throws XERPException;

	public List<Division> getDivisionByBranch(Branch branch) throws XERPException;

}
