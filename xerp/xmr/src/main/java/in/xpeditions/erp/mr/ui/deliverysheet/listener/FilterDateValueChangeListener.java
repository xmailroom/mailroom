package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.mr.ui.deliverysheet.DeliverySheetEditContainer;

public class FilterDateValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4281446267229055779L;

	private DeliverySheetEditContainer deliverySheetEditContainer;

	public FilterDateValueChangeListener(DeliverySheetEditContainer deliverySheetEditContainer) {
		this.deliverySheetEditContainer = deliverySheetEditContainer;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		this.deliverySheetEditContainer.refreshTable();
	}

}
