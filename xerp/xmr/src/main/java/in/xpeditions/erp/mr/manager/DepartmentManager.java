/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.DepartmentDAOManager;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class DepartmentManager {

	public List<Department> listDepartments(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		return departmentDAOManager.listDepartments(startIndex, count, sortProperties, sortPropertyAscendingStates,
				searchString);
	}

	public Long getDepartmentCount(String searchString) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		return departmentDAOManager.getDepartmentCount(searchString);
	}

	public void save(Department department) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		departmentDAOManager.save(department);
	}

	public void update(Department department) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		departmentDAOManager.update(department);
	}

	public void delete(Department department) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		departmentDAOManager.delete(department);
	}

	public List<Department> listByDivision(Division division) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		return departmentDAOManager.listByDivision(division);
	}

	public Department getByName(String departmentName) throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		return departmentDAOManager.getByName(departmentName);
	}
	
	public List<Department> list() throws XERPException {
		DepartmentDAOManager departmentDAOManager = (DepartmentDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT);
		return departmentDAOManager.list();
	}

}
