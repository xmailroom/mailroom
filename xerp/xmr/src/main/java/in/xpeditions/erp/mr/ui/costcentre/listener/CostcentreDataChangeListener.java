/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.io.Serializable;

import in.xpeditions.erp.mr.entity.CostCentre;

/**
 * @author Saran
 *
 */
public interface CostcentreDataChangeListener extends Serializable {

	public void onSelectionChange(CostCentre costCentre);

	public void onUpdate(CostCentre costCentre);

	public void onSearchTextValueChange(String value);

	public void onEditButtonClick();

}
