package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.InboundDAOManager;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

public class InboundManager {

	public void save(Inbound inbound) throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		inboundDAOManager.save(inbound);
	}
	
	public void bulkSave(List<Inbound> inbounds) throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		inboundDAOManager.bulkSave(inbounds);
	}

	public void update(Inbound inbound) throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		inboundDAOManager.update(inbound);
	}

	public List<Inbound> list() throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		return inboundDAOManager.list();
	}

	public Inbound getBy(Inbound inbound) throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		return inboundDAOManager.getBy(inbound);
	}

	public void delete(Inbound inbound) throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		inboundDAOManager.delete(inbound);
	}

	public List<Object[]> listSQL() throws XERPException {
		InboundDAOManager inboundDAOManager = (InboundDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND);
		return inboundDAOManager.listSQL();
	}

}
