/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.io.Serializable;

import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author xpeditions
 *
 */
public interface FloorDataChangeListener extends Serializable {

	public void onUpdate(Floor floor);

	public void onSelectionChange(Floor floor);

	public void onSearchTextValueChange(String value);

	public void onEditButtonClick();

}
