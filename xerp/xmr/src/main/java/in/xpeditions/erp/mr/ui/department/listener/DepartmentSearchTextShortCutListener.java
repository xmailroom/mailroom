/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DepartmentSearchTextShortCutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -883010857398250405L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	
	public DepartmentSearchTextShortCutListener(String prompt, int keyCode,
			DepartmentDataChangeObserver departmentDataChangeObserver) {
		super(prompt, keyCode, null);
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if ((target instanceof XERPTextField)) {
			XERPTextField field = (XERPTextField) target;
			String value = field.getValue();
			this.departmentDataChangeObserver.notifySearchTextValueChange(value);
		}
	}

}
