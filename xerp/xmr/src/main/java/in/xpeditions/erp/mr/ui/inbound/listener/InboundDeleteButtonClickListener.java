package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.manager.InboundManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class InboundDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7867681842642052336L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private Inbound inbound;

	private ResourceBundle bundle;

	public InboundDeleteButtonClickListener(InboundDataChangeObserver inboundDataChangeObserver, ResourceBundle bundle,
			Inbound inbound) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.bundle = bundle;
		this.inbound = inbound;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			this.inbound = new InboundManager().getBy(this.inbound);
		} catch (XERPException e) {
		}
		String heading = this.bundle.getString(MailRoomMessageConstants.CONFIRM_INBOUND_DELETE_WINDOW_CAPTION);
		String message = this.bundle.getString(MailRoomMessageConstants.CONFIRM_INBOUND_DELETE_MESSAGE);
		String captionYes = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_YES);
		String captionNo = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_NO);

		if (this.inbound.isBulk()) {
			String dMessage = this.bundle.getString(MailRoomMessageConstants.CONFIRM_MESSAGE_DELETE_BULK_ENTY) + "\n"
					+ message + "?";
			message = dMessage;
		} else {
			message += " '" + this.inbound.getAwbNo() + "' Dated '"
					+ XERPUtil.formatDate(this.inbound.getDate(), DataHolder.commonUserDateTimeFormat) + "' ?";
		}

		event.getButton().setEnabled(true);
		InboundDeleteConfirmListener inboundDeleteConfirmListener = new InboundDeleteConfirmListener(this.bundle,
				this.inboundDataChangeObserver, this.inbound);
		ConfirmDialog dialog = ConfirmDialog.show(UI.getCurrent(), heading, message, captionYes, captionNo,
				inboundDeleteConfirmListener);
		dialog.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_STYLE_DANGER);
		dialog.getOkButton().addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
	}

}
