/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public class EmployeeListItemClickListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2481617277924694069L;
	private EmployeeDataChangeObserver countryDataChangeObserver;

	public EmployeeListItemClickListener(EmployeeDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<Employee> nestingBeanItem = (NestingBeanItem<Employee>) event.getItem();
		Employee employee = nestingBeanItem.getBean();
		this.countryDataChangeObserver.notifySelectionChange(employee);
	}
}
