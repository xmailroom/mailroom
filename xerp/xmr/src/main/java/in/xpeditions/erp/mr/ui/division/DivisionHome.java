/**
 * 
 */
package in.xpeditions.erp.mr.ui.division;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.division.listener.DivisionDataChangeObserver;

/**
 * @author Saran
 *
 */
public class DivisionHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2156662724976881990L;
	
	public DivisionHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        DivisionDataChangeObserver divisionDataChangeObserver = new DivisionDataChangeObserver();
        DivisionListContainer divisionListContainer = new DivisionListContainer(divisionDataChangeObserver);
        containerSplitter.setFirstComponent(divisionListContainer);
        
        DivisionEditContainer divisionEditContainer = new DivisionEditContainer(divisionDataChangeObserver);
        containerSplitter.setSecondComponent(divisionEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
