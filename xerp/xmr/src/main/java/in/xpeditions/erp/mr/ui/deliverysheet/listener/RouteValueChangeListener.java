package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Label;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.ui.deliverysheet.DeliverySheetEditContainer;

public class RouteValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1492291041031356142L;

	private DeliverySheetEditContainer deliverySheetEditContainer;

	private XERPComboBox departmantCombo;

	private Label dLabel;

	public RouteValueChangeListener(DeliverySheetEditContainer deliverySheetEditContainer, XERPComboBox departmantCombo,
			Label dLabel) {
		this.deliverySheetEditContainer = deliverySheetEditContainer;
		this.departmantCombo = departmantCombo;
		this.dLabel = dLabel;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Floor floor = (Floor) event.getProperty().getValue();
		if (floor != null) {
			this.departmantCombo.setVisible(true);
			this.dLabel.setVisible(true);
		} else {
			this.departmantCombo.setVisible(false);
			this.dLabel.setVisible(false);
		}
		this.deliverySheetEditContainer.refreshTable();
	}

}
