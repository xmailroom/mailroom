/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public class EmployeeMoveDepartmentComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4565289468235060652L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private XERPComboBox costCentreCombo;

	public EmployeeMoveDepartmentComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			XERPComboBox costCentreCombo) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.costCentreCombo = costCentreCombo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.
	 * Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox departmentCombo = (XERPComboBox) event.getProperty();
		Department department = (Department) departmentCombo.getValue();
		if(department != null) {
			this.employeeDataChangeObserver.notiyDepartmentMoveChange(department, this.costCentreCombo);
		} else {
			costCentreCombo.setReadOnly(false);
			costCentreCombo.removeAllItems();
		}
	}

}
