package in.xpeditions.erp.mr.ui.inbound.log;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPTableFilterDecorator;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogCellStyleGenerator;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryDataChangeListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryListItemChangeListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryListItemClickListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryListItemShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.log.listener.TableFoucsShortcutListener;

public class InboundLogEntryListContainer extends VerticalLayout implements InboundLogEntryDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3431147801166567076L;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private FilterTable table;

	private ResourceBundle bundle;

	private InboundLogEntryListItemClickListener inboundLogEntryListItemClickListener;

	private IndexedContainer container;

	private InboundLogEntryListItemShortcutListener inboundLogEntryListItemShortcutListener;

	public InboundLogEntryListContainer(InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver) {
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.inboundLogEntryDataChangeObserver.addListener(this);

		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent().getPage()
				.setTitle(this.bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND_LOG));
		this.createListLayout();
	}

	private void createListLayout() {
		boolean canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_INBOUND_LOG.asPermission().getName());

		this.table = new FilterTable();
		this.addComponent(this.table);
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.setFilterBarVisible(true);
		this.table.setSortEnabled(true);
		this.table.setFilterDecorator(new XERPTableFilterDecorator());
		this.table.setWidth("100%");
		this.table.setHeight("280px");
		this.table.setImmediate(true);
		this.table.setVisible(canView);
		this.table.setPageLength(15);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);
		this.table.addShortcutListener(new TableFoucsShortcutListener("ctrl+down", KeyCode.ARROW_DOWN,
				new int[] { ModifierKey.CTRL }, this.table));
		this.table.setCellStyleGenerator(new InboundLogCellStyleGenerator());

		container = new IndexedContainer();

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_INDEX),
				Integer.class, 0);

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_BATCH_NO),
				String.class, "");

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_DATE),
				String.class, "");

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_MODE),
				String.class, "");

		container.addContainerProperty(
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_SERVICE_PROVIDER), String.class, "");

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_SHIPMENT),
				Integer.class, 0);

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_ENTRY),
				Integer.class, 0);

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING),
				Integer.class, 0);

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_REMARKS),
				String.class, "");

		container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				HorizontalLayout.class, null);

		this.table.setContainerDataSource(container);

		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_INDEX),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_BATCH_NO),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_DATE),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_MODE),
				Align.LEFT);
		this.table.setColumnAlignment(
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_SERVICE_PROVIDER), Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_SHIPMENT),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_ENTRY),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_REMARKS),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), Align.LEFT);

		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_INDEX), 60);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_BATCH_NO), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_DATE), 125);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_MODE), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_SERVICE_PROVIDER),
				101);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_SHIPMENT), 98);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_NO_OF_ENTRY), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_REMARKS), 200);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), 50);

		this.inboundLogEntryListItemShortcutListener = new InboundLogEntryListItemShortcutListener("", KeyCode.ENTER,
				new int[] { ModifierKey.ALT }, this, this.bundle);

		this.inboundLogEntryListItemClickListener = new InboundLogEntryListItemClickListener(this.bundle,
				this.inboundLogEntryDataChangeObserver, this);

		this.table.addValueChangeListener(new InboundLogEntryListItemChangeListener(
				this.inboundLogEntryDataChangeObserver, this.inboundLogEntryListItemClickListener,
				this.inboundLogEntryListItemShortcutListener, this.table));
		this.table.addItemClickListener(inboundLogEntryListItemClickListener);

		this.refreshTable();
	}

	private void refreshTable() {
		this.container.removeAllItems();
		LogEntryManager logEntryManager = new LogEntryManager();
		List<Object[]> objects = new ArrayList<Object[]>();
		try {
			objects = logEntryManager.listInbound();
		} catch (XERPException e) {
		}
		LogEntry logEntry = null;
		int i = 1;
		for (Object[] object : objects) {
			// BeanFieldGroup<LogEntry> logEntryBinder = new
			// BeanFieldGroup<LogEntry>(LogEntry.class);
			logEntry = this.init(logEntry, object);
			// logEntryBinder.setItemDataSource(logEntry);
			int pending = logEntry.getNoOfShip() - logEntry.getNoOfEntry();
			String serviceProviderName = object[9] + "";

			HorizontalLayout actionLayout = new HorizontalLayout();
			Label label = new Label();
			label.addShortcutListener(this.inboundLogEntryListItemShortcutListener);
			actionLayout.addComponent(label);

			this.table.addItem(new Object[] { i++, logEntry.getBatchNo() + "",
					XERPUtil.formatDate(logEntry.getDate(), DataHolder.commonUserDateTimeFormat),
					logEntry.getMode().getName(), serviceProviderName, logEntry.getNoOfShip(),
					logEntry.getNoOfEntry(), pending, logEntry.getRemarks(), actionLayout }, logEntry);
		}

	}

	private LogEntry init(LogEntry logEntry, Object[] object) {
		logEntry = new LogEntry();
		int i = 0;
		logEntry.setId(XERPUtil.sum(object[i++], 0).longValue());
		logEntry.setBatchNo(XERPUtil.sum(object[i++], 0).longValue());
		logEntry.setDate(XERPUtil.sum(object[i++], 0).longValue());
		logEntry.setMode(ModeEnum.get(XERPUtil.sum(object[i++], 0).intValue()));
		logEntry.setNoOfShip(XERPUtil.sum(object[i++], 0).intValue());
		logEntry.setNoOfEntry(XERPUtil.sum(object[i++], 0).intValue());
		logEntry.setRemarks(object[i++] + "");
		logEntry.setsType(TypeEnum.get(XERPUtil.sum(object[i++], 0).intValue()));
		logEntry.setServiceProviderId(XERPUtil.sum(object[i++], 0).longValue());
		i++;// Service Provider Name
		return logEntry;
	}

	@Override
	public void onUpdate(LogEntry logEntry) {
		this.refreshTable();
		this.table.setCurrentPageFirstItemId(logEntry);
		this.table.select(logEntry);
		this.table.focus();
	}

	@Override
	public void onSelectionChange(LogEntry logEntry) {

	}

}
