/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class CostCentreEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4465112557777554395L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostCentreEditButtonClickListener(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.costcentreDataChangeObserver.notifyEditButtonClick();
		event.getButton().setEnabled(true);
	}

}
