/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.ServiceProvider;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderListItemClickListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2481617277924694069L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;

	public ServiceProviderListItemClickListener(ServiceProviderDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<ServiceProvider> nestingBeanItem = (NestingBeanItem<ServiceProvider>) event.getItem();
		ServiceProvider serviceProvider = nestingBeanItem.getBean();
		this.countryDataChangeObserver.notifySelectionChange(serviceProvider);
	}
}
