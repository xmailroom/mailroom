package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = From.TABLE_NAME)
public class From implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1310809757037191331L;

	public static final String TABLE_NAME = "xmr_from";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_EMPLOYEE = "employeeId";

	public static final String COLUMN_FROM_NAME = "name";

	public static final String COLUMN_CITY = "cityId";

	public static final String COLUMN_REF_NO = "refNo";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = From.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@Column(name = From.COLUMN_EMPLOYEE)
	private long employeeId;

	@Column(name = From.COLUMN_FROM_NAME)
	private String name;

	@JoinColumn(name = From.COLUMN_CITY, nullable = false)
	private long cityId;

	@Column(name = From.COLUMN_REF_NO)
	private String refNo;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the employeeId
	 */
	public long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 *            the employeeId to set
	 */
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the cityId
	 */
	public long getCityId() {
		return cityId;
	}

	/**
	 * @param cityId
	 *            the cityId to set
	 */
	public void setCityId(long cityId) {
		this.cityId = cityId;
	}

	/**
	 * @return the refNo
	 */
	public String getRefNo() {
		return refNo;
	}

	/**
	 * @param refNo
	 *            the refNo to set
	 */
	public void setRefNo(String refNo) {
		this.refNo = refNo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		From other = (From) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
