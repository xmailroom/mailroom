/**
 * 
 */
package in.xpeditions.erp.mr.entity.util;

/**
 * @author Saran
 *
 */
public enum GenderEnum {

	MALE("Male"), FEMALE("Female");  
	  
	private String name;

	GenderEnum(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }

    @Override
    public String toString() {
    	return this.name;
    }
	
}
