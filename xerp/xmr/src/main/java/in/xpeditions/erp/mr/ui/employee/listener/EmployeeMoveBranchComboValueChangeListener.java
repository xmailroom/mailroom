/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPComboBox;

/**
 * @author Saran
 *
 */
public class EmployeeMoveBranchComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6555451697753804965L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private XERPComboBox divisionCombo;

	public EmployeeMoveBranchComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			XERPComboBox divisionCombo) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.divisionCombo = divisionCombo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.
	 * Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox branchCombo = (XERPComboBox) event.getProperty();
		Branch branch = (Branch) branchCombo.getValue();
		if (branch != null) {
			this.employeeDataChangeObserver.notifyOnBranchMoveChange(branch, this.divisionCombo);
		} else {
			this.divisionCombo.setReadOnly(false);
			this.divisionCombo.removeAllItems();
		}
	}

}
