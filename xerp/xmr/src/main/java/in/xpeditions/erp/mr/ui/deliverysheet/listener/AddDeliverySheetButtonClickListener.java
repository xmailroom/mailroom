package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.ui.deliverysheet.DeliverySheetEditWindow;

public class AddDeliverySheetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8977512647383594044L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	public AddDeliverySheetButtonClickListener(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		DeliverySheetEditWindow deliverySheetEditWindow = new DeliverySheetEditWindow(
				this.deliverySheetDataChangeObserver, null);
		deliverySheetEditWindow.open();
	}

}
