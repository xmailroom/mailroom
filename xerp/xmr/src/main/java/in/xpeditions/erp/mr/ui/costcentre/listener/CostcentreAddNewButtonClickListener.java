/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class CostcentreAddNewButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4815236512129262203L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostcentreAddNewButtonClickListener(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.costcentreDataChangeObserver.notifyUpdate(null);
		button.setEnabled(true);
	}

}
