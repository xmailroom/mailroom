/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.ServiceProviderDAOManager;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class ServiceProviderManager {

	public void delete(ServiceProvider serviceProvider) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		serviceProviderDAOManager.delete(serviceProvider);
	}

	public void save(ServiceProvider serviceProvider) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		serviceProviderDAOManager.save(serviceProvider);
	}

	public void update(ServiceProvider serviceProvider) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		serviceProviderDAOManager.update(serviceProvider);
	}

	public List<ServiceProvider> listServiceProviders(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		return serviceProviderDAOManager.listServiceProviders(startIndex, count, sortProperties,
				sortPropertyAscendingStates, searchString);
	}

	public Long getServiceProviderCount(String searchString) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		return serviceProviderDAOManager.getCount(searchString);
	}

	public ServiceProvider getBy(Long id) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		return serviceProviderDAOManager.getBy(id);
	}

	public List<ServiceProvider> list(TypeEnum type, ModeEnum mode) throws XERPException {
		ServiceProviderDAOManager serviceProviderDAOManager = (ServiceProviderDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER);
		return serviceProviderDAOManager.list(type, mode);
	}

}
