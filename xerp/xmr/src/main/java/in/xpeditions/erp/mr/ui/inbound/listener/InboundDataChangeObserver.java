package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;

public class InboundDataChangeObserver {

	private List<InboundDataChangeListener> inboundDataChangeListeners = new ArrayList<InboundDataChangeListener>();;

	public void addListener(InboundDataChangeListener inboundDataChangeListener) {
		this.inboundDataChangeListeners.add(inboundDataChangeListener);
	}

	public void notifyUpdate(Inbound inbound) {
		for (InboundDataChangeListener inboundDataChangeListener : this.inboundDataChangeListeners) {
			inboundDataChangeListener.onUpdate(inbound);
		}
	}

	public void notifySelectionChange(Inbound inbound) {
		for (InboundDataChangeListener inboundDataChangeListener : this.inboundDataChangeListeners) {
			inboundDataChangeListener.onSelectionChange(inbound);
		}
	}

	public void notifyEmployeeChange(Employee employee) {
		for (InboundDataChangeListener inboundDataChangeListener : this.inboundDataChangeListeners) {
			inboundDataChangeListener.onEmployeeChange(employee);
		}
	}

	public void notifyLogEntryChange(LogEntry logEntry) {
		for (InboundDataChangeListener inboundDataChangeListener : this.inboundDataChangeListeners) {
			inboundDataChangeListener.onLogEntryChange(logEntry);
		}
	}

}
