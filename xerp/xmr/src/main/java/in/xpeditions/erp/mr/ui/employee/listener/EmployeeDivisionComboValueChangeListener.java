/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author Saran
 *
 */
public class EmployeeDivisionComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6712719999771702289L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private BeanFieldGroup<Employee> binder;

	public EmployeeDivisionComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			BeanFieldGroup<Employee> binder) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.binder = binder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.
	 * Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox divisionCombo = (XERPComboBox) event.getProperty();
		Division division = (Division) divisionCombo.getValue();
		if (division != null) {
			this.employeeDataChangeObserver.notifyDivisionChange(division);
		} else {
			XERPComboBox departmentCombo = (XERPComboBox) this.binder.getField(XMailroomFormItemConstants.EMPLOYEE_DEPARTMENT);
			departmentCombo.setReadOnly(false);
			departmentCombo.removeAllItems();
			
		}
	}

}
