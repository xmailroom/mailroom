package in.xpeditions.erp.mr.ui.inbound.log.listener;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

public class ClearButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2148588788934401782L;

	private BeanFieldGroup<LogEntry> logEntryBinder;

	private XERPFriendlyButton deleteButton;

	public ClearButtonClickListener(BeanFieldGroup<LogEntry> logEntryBinder, XERPFriendlyButton deleteButton) {
		this.logEntryBinder = logEntryBinder;
		this.deleteButton = deleteButton;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		LogEntry logEntry = new LogEntry();
		logEntry.setMode(ModeEnum.COURIER);
		logEntry.setsType(TypeEnum.INBOUND);
		logEntry.setNoOfShip(1);
		logEntry.setNoOfEntry(0);
		logEntry.setDate(System.currentTimeMillis());
		logEntry.setRemarks("");
		this.logEntryBinder.setItemDataSource(logEntry);
		event.getButton().setEnabled(true);
		this.deleteButton.setVisible(false);
	}

}
