/**
 * 
 */
package in.xpeditions.erp.mr.entity.util;

/**
 * @author Saran
 *
 */
public enum ModeEnum {
	
	COURIER("Courier"), POSTAL("Postal"), INTERBRANCH("Inter Branch");

	public static final String CAPTION_PROPERTY_ID = "name";

	private String name;

	private ModeEnum(String name) {
		this.setName(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static ModeEnum get(int i) {
		return ModeEnum.values()[i];
	}

}
