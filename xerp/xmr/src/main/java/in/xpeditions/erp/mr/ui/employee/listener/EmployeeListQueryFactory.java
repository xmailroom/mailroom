/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author xpeditions
 *
 */
public class EmployeeListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -778041010293651816L;
	private String searchString;

	public EmployeeListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		EmployeeListQuery countryListQuery = new EmployeeListQuery(searchString, queryDefinition);
		return countryListQuery;
	}
}