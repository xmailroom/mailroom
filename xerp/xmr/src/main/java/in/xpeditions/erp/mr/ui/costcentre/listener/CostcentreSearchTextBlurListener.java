/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class CostcentreSearchTextBlurListener implements BlurListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1945383919650038360L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;
	private CostcentreSearchTextShortCutListener costcentreSearchTextShortCutListener;

	public CostcentreSearchTextBlurListener(CostcentreDataChangeObserver costcentreDataChangeObserver,
			CostcentreSearchTextShortCutListener costcentreSearchTextShortCutListener) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
		this.costcentreSearchTextShortCutListener = costcentreSearchTextShortCutListener;
	}

	@Override
	public void blur(BlurEvent event) {
		XERPTextField searchTextComp = (XERPTextField) event.getComponent();
		searchTextComp.removeShortcutListener(costcentreSearchTextShortCutListener);
		String searchString = searchTextComp.getValue();
		this.costcentreDataChangeObserver.notifySearchTextValueChange(searchString);
	}

}
