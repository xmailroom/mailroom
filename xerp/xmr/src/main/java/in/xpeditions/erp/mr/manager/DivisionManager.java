/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.acc.util.XAccUtil;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.history.util.XHistory;
import in.xpeditions.erp.commons.history.util.XHistoryAction;
import in.xpeditions.erp.commons.history.util.XHistoryType;
import in.xpeditions.erp.commons.manager.XHistoryManager;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.DivisionDAOManager;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class DivisionManager {

	public void save(Division division) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		divisionDAOManager.save(division);

		XHistory xHistory = XAccUtil.createObjectHistory(division.getId(), XHistoryType.DIVISION,
				XHistoryAction.INSERT);
		XHistoryManager historyManager = new XHistoryManager();
		historyManager.save(xHistory);
	}

	public void update(Division division) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		divisionDAOManager.update(division);

		XHistory xHistory = XAccUtil.createObjectHistory(division.getId(), XHistoryType.DIVISION,
				XHistoryAction.UPDATE);
		XHistoryManager historyManager = new XHistoryManager();
		historyManager.save(xHistory);
	}

	public List<Division> listDivisions(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		return divisionDAOManager.listDivisions(startIndex, count, sortProperties, sortPropertyAscendingStates,
				searchString);
	}

	public Long getDivisionCount(String searchString) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		return divisionDAOManager.getDivisionCount(searchString);
	}

	public void delete(Division division) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		divisionDAOManager.delete(division);
	}

	public List<Division> getDivisionByBracnch(Branch branch) throws XERPException {
		DivisionDAOManager divisionDAOManager = (DivisionDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION);
		return divisionDAOManager.getDivisionByBranch(branch);
	}

}
