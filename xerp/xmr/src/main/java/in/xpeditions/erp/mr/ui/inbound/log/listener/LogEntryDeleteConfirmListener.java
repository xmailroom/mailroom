package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class LogEntryDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4902178264126771190L;

	private ResourceBundle bundle;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private LogEntry logEntry;

	public LogEntryDeleteConfirmListener(ResourceBundle bundle,
			InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver, LogEntry logEntry) {
		this.bundle = bundle;
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.logEntry = logEntry;
	}

	@Override
	public void onClose(ConfirmDialog dialog) {
		if (dialog.isConfirmed()) {
			LogEntryManager logEntryManager = new LogEntryManager();
			try {
				logEntryManager.delete(this.logEntry);
				Notification.show(
						this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DELETED_SUCCESSFULLY));
				this.inboundLogEntryDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_FOREIGN_KEY_VALUE)
						+ " '" + this.logEntry.getBatchNo() + "' " + "' Dated '"
						+ XERPUtil.formatDate(this.logEntry.getDate(), DataHolder.commonUserDateTimeFormat) + "' ?"
						+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_HAS_REFERENCE);
				Notification.show(this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DELETE_FAILED),
						subMessage, Type.ERROR_MESSAGE);
			} catch (XERPException e) {
				String subMessage = "<br/>" + e.getMessage();
				Notification.show(
						this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DELETE_FAILED),
						subMessage, Type.ERROR_MESSAGE);

			}
		}
	}

}
