/**
 * 
 */
package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.dao.manager.DivisionDAOManager;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class DivisionDAOManagerHibernate implements DivisionDAOManager {

	@Override
	public void save(Division division) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.save(division);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void update(Division division) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.update(division);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Division> listDivisions(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		String hql = "select d from Division d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " d.name LIKE '" + searchString + "%' OR d.code LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		String sortCrit = "";
		if (sortProperties != null) {
			for (int i = 0; i < sortProperties.length; i++) {
				Object sObject = sortProperties[i];
				if (sObject.equals(XMailroomTableItemConstants.DIVISION_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " d.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				} 
				if (sObject.equals(XMailroomTableItemConstants.DIVISION_CODE)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " d.code " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				}
			}
			sortCrit = sortCrit.isEmpty() ? "" : " ORDER BY " + sortCrit;
		}
		if(sortCrit.isEmpty()) {
			sortCrit = " ORDER BY d.name";
		}
		hql += sortCrit;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			if (count > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(count);
			}
			List<Division> divisions = query.list();
			return divisions;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Long getDivisionCount(String searchString) throws XERPException {
		String hql = "select count(d) from Division d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " d.name LIKE '" + searchString + "%' OR d.code LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(Division division) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.delete(division);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Division> getDivisionByBranch(Branch branch) throws XERPException {
		String hql = "select d from Division d where d.branch = :branch";
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("branch", branch);
			List<Division> divisions = query.list();
			return divisions;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
