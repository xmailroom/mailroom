package in.xpeditions.erp.mr.ui.menu.handler;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.sp.SPHome;

public class SPMenuHandler implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8704494777848780542L;
	private MailRoomMenuChangeHandler menuChangeHandler;

	public SPMenuHandler(MailRoomMenuChangeHandler menuChangeHandler) {
		this.menuChangeHandler = menuChangeHandler;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		CustomComponent spHome = new SPHome();

		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());

		String breadCrumb = bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM) + " "
				+ bundle.getString(MailRoomMessageConstants.COMMON_MENU_PATH_SEPARATOR) + " "
				+ bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_SP);

		this.menuChangeHandler.handleMenuChange(breadCrumb, spHome);
	}

}
