/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.manager.DivisionManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DivisionSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2082871621035348398L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private BeanFieldGroup<Division> binder;
	private boolean canEdit;

	public DivisionSaveButtonClickListener(DivisionDataChangeObserver divisionDataChangeObserver,
			BeanFieldGroup<Division> binder, boolean canEdit) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
		this.binder = binder;
		this.canEdit = canEdit;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		DivisionManager divisionManager = new DivisionManager();
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		Division division = this.binder.getItemDataSource().getBean();
		if (division.getName() == null || division.getName().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.DIVISION_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.DIVISION_NAME_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		if (division.getCode() == null || division.getCode().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.DIVISION_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.DIVISION_CODE_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		try {

			if (division.getId() > 0) {
				if (canEdit) {
					divisionManager.update(division);
					Notification
							.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				divisionManager.save(division);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_SAVED_SUCCESSFULLY));
			}
			this.divisionDataChangeObserver.notifyUpdate(division);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.DIVISION_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_DUPILCATE_VALUE_NAME) + " '"
					+ e.getTitle() + "' " + bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}
		event.getButton().setEnabled(true);
	}

}
