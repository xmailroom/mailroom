package in.xpeditions.erp.mr.dao.manager;

import java.sql.Date;
import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.entity.DeliverySheetDetail;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Floor;

public interface DeliverySheetDAOManager {

	void save(DeliverySheet deliverySheet) throws XERPException;

	void update(DeliverySheet deliverySheet) throws XERPException;

	List<DeliverySheet> list() throws XERPException;

	DeliverySheet getBy(DeliverySheet deliverySheet) throws XERPException;

	void delete(DeliverySheet deliverySheet) throws XERPException;

	List<Object[]> listSQL() throws XERPException;

	List<DeliverySheetDetail> listDeliverySheetDetail(DeliverySheet deliverySheet) throws XERPException;

	List<Object[]> listDeliverySheetDetailSQL(DeliverySheet deliverySheet) throws XERPException;

	List<Object[]> listReadyForDeliverySQL(Date from, Date to, Floor floor, Department department) throws XERPException;

	DeliverySheetDetail getDeliverySheetDetail(DeliverySheetDetail deliverySheetDetail) throws XERPException;

}
