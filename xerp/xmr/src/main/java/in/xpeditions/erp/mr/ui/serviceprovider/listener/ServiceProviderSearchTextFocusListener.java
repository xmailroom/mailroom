/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2656576707840323694L;
	private ServiceProviderSearchTextShortCutListener countrySearchTextShortCutListener;

	public ServiceProviderSearchTextFocusListener(ServiceProviderSearchTextShortCutListener countrySearchTextShortCutListener) {
		this.countrySearchTextShortCutListener = countrySearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(countrySearchTextShortCutListener);
	}
}