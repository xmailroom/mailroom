package in.xpeditions.erp.mr.util;

import java.util.Calendar;
import java.util.ResourceBundle;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.InboundHome;
import in.xpeditions.erp.mr.ui.inbound.log.InboundLogEntryListContainer;

public class InboundLogEntryUtil {

	/**
	 * @param inboundLogEntryListContainer
	 * @param bundle
	 * 
	 */
	public static void takeAction(LogEntry logEntry, InboundLogEntryListContainer inboundLogEntryListContainer,
			ResourceBundle bundle) {
		String[] editActions = { MailRoomPermissionEnum.ADD_INBOUND.asPermission().getName() };
		if (XERPUtil.hasPermissions(editActions)) {
			if (logEntry != null) {
				int pending = logEntry.getNoOfShip() - logEntry.getNoOfEntry();
				if (pending > 0) {
					VerticalLayout inboundLogHomeParent = (VerticalLayout) inboundLogEntryListContainer.getParent()
							.getParent().getParent();
					inboundLogHomeParent.removeAllComponents();
					VerticalLayout xerpHomeUI = (VerticalLayout) inboundLogHomeParent.getParent();

					HorizontalLayout breadCrumb = (HorizontalLayout) xerpHomeUI.getComponent(1);
					Label selectedMenuText = (Label) breadCrumb.getComponent(0);

					String breadCrumbs = bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND) + " "
							+ bundle.getString(MailRoomMessageConstants.COMMON_MENU_PATH_SEPARATOR) + " "
							+ bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND);
					selectedMenuText.setValue(breadCrumbs);

					InboundHome inboundHome = new InboundHome();
					inboundLogHomeParent.addComponent(inboundHome);
					inboundHome.getInboundEditContainer().setLogEntry(logEntry);
				} else {
					String caption = bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_NO_PENDING);
					String description = "\n"
							+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_NO_PENDING_REASON);
					Notification.show(caption, description, Type.WARNING_MESSAGE);
					return;
				}
			}
		} else {
			String caption = bundle.getString(MailRoomMessageConstants.ERROR_PERMISSION);
			String description = "\n" + bundle.getString(MailRoomMessageConstants.PERMISSION_REQUIRED);
			Notification.show(caption, description, Type.WARNING_MESSAGE);
			return;
		}
	}

	public static long getDifferenceInDays(long startTime, long endTime) {

		Calendar sCalendar = Calendar.getInstance();
		sCalendar.setTimeInMillis(startTime);

		Calendar eCalendar = Calendar.getInstance();
		eCalendar.setTimeInMillis(endTime);

		long diff = sCalendar.getTimeInMillis() - eCalendar.getTimeInMillis();

		long diffDays = diff / 1000 / 60 / 60 / 24;

		return diffDays;
	}

}
