/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import com.vaadin.data.Item;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.manager.CostCentreManager;

/**
 * @author Saran
 *
 */
public class CostCentreListQuery implements Query, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3676885280981521515L;
	private String searchString;
	private QueryDefinition queryDefinition;

	public CostCentreListQuery(String searchString, QueryDefinition queryDefinition) {
		this.searchString = searchString;
		this.queryDefinition = queryDefinition;
	}

	@Override
	public Item constructItem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAllItems() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Item> loadItems(int startIndex, int count) {
		CostCentreManager costCentreManager = new CostCentreManager();
		try {
			Object[] sortProperties = this.queryDefinition.getSortPropertyIds();
			boolean[] sortPropertyAscendingStates = this.queryDefinition.getSortPropertyAscendingStates();
			List<CostCentre> costCentres = costCentreManager.listCostCentres(startIndex, count, sortProperties, sortPropertyAscendingStates, this.searchString);
			List<Item> items = new ArrayList<Item>(costCentres.size());
			for (CostCentre costCentre : costCentres) {
				items.add(new NestingBeanItem<CostCentre>(costCentre, this.queryDefinition.getMaxNestedPropertyDepth(), this.queryDefinition.getPropertyIds()));
			}
			return items;	
		}
			catch (XERPException e) {
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public void saveItems(List<Item> arg0, List<Item> arg1, List<Item> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int size() {
		CostCentreManager costCentreManagerManager = new CostCentreManager();
		try {
			Long countOf = costCentreManagerManager.getCostCentreCount(searchString);
			return countOf.intValue();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
