/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author Saran
 *
 */
public interface EmployeeDAOManager {

	public Long getEmployeeCount(String searchString) throws XERPException;

	public List<Employee> listEmployees(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public void save(Employee employee) throws XERPException;

	public void update(Employee employee) throws XERPException;

	public void delete(Employee employee) throws XERPException;

	public void bulkSave(Floor floor, Division division, Department department, CostCentre costCentre,
			Employee employee) throws XERPException;

	public List<Employee> list() throws XERPException;
	

}
