/**
 * 
 */
package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.dao.manager.DepartmentDAOManager;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class DepartmentDAOManagerHibernate implements DepartmentDAOManager {

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> listDepartments(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		String hql = "select d from Department d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " d.name LIKE '" + searchString + "%' OR d.code LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		String sortCrit = "";
		if (sortProperties != null) {
			for (int i = 0; i < sortProperties.length; i++) {
				Object sObject = sortProperties[i];
				if (sObject.equals(XMailroomTableItemConstants.DEPARTMENT_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " d.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				} 
				if (sObject.equals(XMailroomTableItemConstants.DEPARTMENT_CODE)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " d.code " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				}
			}
			sortCrit = sortCrit.isEmpty() ? "" : " ORDER BY " + sortCrit;
		}
		if(sortCrit.isEmpty()) {
			sortCrit = " ORDER BY d.name";
		}
		hql += sortCrit;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			if (count > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(count);
			}
			List<Department> departments = query.list();
			return departments;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Long getDepartmentCount(String searchString) throws XERPException {
		String hql = "select count(d) from Department d";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " d.name LIKE '" + searchString + "%' OR d.code LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void save(Department department) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.save(department);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void update(Department department) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.update(department);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void delete(Department department) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.delete(department);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> listByDivision(Division division) throws XERPException {
		String hql = "select d from Department d where d.division=:division";
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("division", division);
			List<Department> departments = query.list();
			return departments;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Department getByName(String departmentName) throws XERPException {
		String hql = "select d from Department d inner join d.floors where d.name=:departmentName";
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("departmentName", departmentName);
			return (Department) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Department> list() throws XERPException {
		String hql = "SELECT f FROM " + Department.class.getSimpleName() + " f order by " + Department.COLUMN_NAME;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
