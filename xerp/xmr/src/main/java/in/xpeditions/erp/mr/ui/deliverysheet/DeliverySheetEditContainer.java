package in.xpeditions.erp.mr.ui.deliverysheet;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.shared.ui.datefield.Resolution;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.acc.util.XAccUtil;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDateField;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.export.ExportUIContainer;
import in.xpeditions.erp.commons.ui.export.listener.ExportActionListener;
import in.xpeditions.erp.commons.ui.export.listener.ExportActionObserver;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.entity.DeliverySheetDetail;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.util.DeliveryStatus;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.manager.FloorManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetEditCloseListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetSaveClickListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.FilterDateValueChangeListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.RouteValueChangeListener;

public class DeliverySheetEditContainer extends VerticalLayout implements ExportActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4408130218575530789L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private DeliverySheet deliverySheet;

	private ResourceBundle bundle;

	private BeanFieldGroup<DeliverySheet> deliverySheetBinder;

	private DeliverySheetEditWindow deliverySheetEditWindow;

	private FilterTable table;

	private BeanItemContainer<Floor> floorContainer;

	private IndexedContainer container;

	private List<Object[]> objects;

	private BeanItemContainer<Department> departmentContainer;

	private XERPComboBox routeCombo;

	private XERPComboBox departmantCombo;

	private XERPDateField fromField;

	private XERPDateField toField;

	public DeliverySheetEditContainer(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			DeliverySheetEditWindow deliverySheetEditWindow, DeliverySheet deliverySheet) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheetEditWindow = deliverySheetEditWindow;
		this.deliverySheet = deliverySheet;

		this.setStyleName(" v-app xerp-valo-facebook xerphomeui xerp-overflow-auto");
		this.setSizeFull();
		MarginInfo marginInfo = new MarginInfo(false, true, false, true);
		this.setMargin(marginInfo);

		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		this.createEditContainer();
	}

	private void createEditContainer() {
		this.init();
		VerticalLayout verticalLayout = new VerticalLayout();
		VerticalLayout dataEntryLayout = new VerticalLayout();
		verticalLayout.addComponent(dataEntryLayout);
		dataEntryLayout.setSpacing(true);

		VerticalLayout desigLayout = this.deliverySheetLayout();
		dataEntryLayout.addComponent(desigLayout);

		HorizontalLayout saveLayout = new HorizontalLayout();
		saveLayout.setMargin(new MarginInfo(true, false, false, true));
		verticalLayout.addComponent(saveLayout);
		verticalLayout.setComponentAlignment(saveLayout, Alignment.BOTTOM_RIGHT);

		saveLayout.setSpacing(true);
		saveLayout.setId("xerp-save-button-layout");

		String[] saveActions = new String[] { MailRoomPermissionEnum.ADD_DELIVERY_SHEET.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_DELIVERY_SHEET.asPermission().getName() };
		XERPFriendlyButton saveButton = new XERPFriendlyButton(saveActions);

		saveButton.setIcon(FontAwesome.SAVE);
		saveButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_SAVE));
		saveButton.setDisableOnClick(true);
		saveButton.setEnabled(true);
		saveButton.setImmediate(true);
		saveLayout.addComponent(saveButton);

		ClickListener deliverySheetSaveClickListener = new DeliverySheetSaveClickListener(this.bundle,
				this.deliverySheetEditWindow, this.deliverySheetDataChangeObserver, this.deliverySheetBinder,
				this.table);
		saveButton.addClickListener(deliverySheetSaveClickListener);

		saveButton.setClickShortcut(KeyCode.ENTER);

		Button button = new Button();
		button.setDisableOnClick(true);
		button.setCaption("Close");
		button.setIcon(FontAwesome.TIMES_CIRCLE);
		button.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		button.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
		ClickListener deliverySheetEditCloseListener = new DeliverySheetEditCloseListener(this.deliverySheetEditWindow);
		button.addClickListener(deliverySheetEditCloseListener);
		saveLayout.addComponent(button);

		String[] printActions = new String[] { MailRoomPermissionEnum.PRINT_DELIVERY_SHEET.asPermission().getName() };
		if (XERPUtil.hasPermissions(printActions)) {
			ExportActionObserver exportActionObserver = new ExportActionObserver();
			exportActionObserver.addExportActionListener(this);
			ExportUIContainer exportUIContainer = new ExportUIContainer(exportActionObserver);
			saveLayout.addComponent(exportUIContainer);
			saveLayout.setComponentAlignment(exportUIContainer, Alignment.TOP_RIGHT);
			saveLayout.setExpandRatio(exportUIContainer, .1f);
			saveLayout.addComponent(exportUIContainer);
		}

		this.addComponent(verticalLayout);
	}

	private VerticalLayout deliverySheetLayout() {
		VerticalLayout wDesigLayout = new VerticalLayout();
		wDesigLayout.setSpacing(true);

		this.deliverySheetBinder = new BeanFieldGroup<DeliverySheet>(DeliverySheet.class);
		this.deliverySheetBinder.setBuffered(true);
		this.deliverySheetBinder.setItemDataSource(this.deliverySheet);

		HorizontalLayout desigLayout = new HorizontalLayout();
		wDesigLayout.addComponent(desigLayout);
		desigLayout.setSpacing(true);
		FormLayout formLayout = new FormLayout();
		formLayout.setSpacing(true);
		desigLayout.addComponent(formLayout);

		HorizontalLayout filterLayout = new HorizontalLayout();
		filterLayout.setCaption(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_FILTER_FROM));
		filterLayout.setSpacing(true);
		formLayout.addComponent(filterLayout);

		this.fromField = new XERPDateField();
		this.fromField.setResolution(Resolution.DAY);
		this.fromField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_100_PX);
		this.fromField.setDateFormat(DataHolder.commonUserDateFormat);
		this.fromField.setValue(new Date(System.currentTimeMillis()));
		filterLayout.addComponent(this.fromField);

		Label toLabel = new Label();
		toLabel.setValue(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_FILTER_TO));
		filterLayout.addComponent(toLabel);

		this.toField = new XERPDateField();
		this.toField.setResolution(Resolution.DAY);
		this.toField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_100_PX);
		this.toField.setDateFormat(DataHolder.commonUserDateFormat);
		this.toField.setValue(new Date(System.currentTimeMillis()));
		filterLayout.addComponent(this.toField);

		FilterDateValueChangeListener filterDateValueChangeListener = new FilterDateValueChangeListener(this);
		this.fromField.addValueChangeListener(filterDateValueChangeListener);
		this.toField.addValueChangeListener(filterDateValueChangeListener);

		Label rLabel = new Label();
		rLabel.setValue(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_FILTER_ROUTE));
		filterLayout.addComponent(rLabel);

		this.floorContainer = new BeanItemContainer<Floor>(Floor.class);

		this.refreshFloor();

		this.routeCombo = new XERPComboBox();
		this.routeCombo.setContainerDataSource(this.floorContainer);
		this.routeCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		this.routeCombo.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.routeCombo.setItemCaptionPropertyId(Floor.COLUMN_NAME);
		filterLayout.addComponent(this.routeCombo);

		this.departmentContainer = new BeanItemContainer<Department>(Department.class);
		this.refreshDepartment();

		Label dLabel = new Label();
		dLabel.setVisible(false);
		dLabel.setValue(this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_FILTER_DEPARTMENT));
		filterLayout.addComponent(dLabel);

		this.departmantCombo = new XERPComboBox();
		this.departmantCombo.setContainerDataSource(this.departmentContainer);
		this.departmantCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		this.departmantCombo.setRequiredError(this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED));
		this.departmantCombo.setItemCaptionPropertyId(Floor.COLUMN_NAME);
		this.departmantCombo.setVisible(false);
		filterLayout.addComponent(this.departmantCombo);

		this.routeCombo.addValueChangeListener(new RouteValueChangeListener(this, this.departmantCombo, dLabel));

		this.table = new FilterTable();
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.setWidth("100%");
		this.table.setHeight("500px");
		this.table.setImmediate(true);
		this.table.setPageLength(5);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);
		this.table.setMultiSelect(true);
		this.table.setFooterVisible(true);

		wDesigLayout.addComponent(this.table);

		this.container = new IndexedContainer();

		this.container.addContainerProperty(Inbound.COLUMN_AWB_NO, String.class, "");
		this.container.addContainerProperty(Inbound.COLUMN_DATE, String.class, "");
		this.container.addContainerProperty(Inbound.COLUMN_MODE, ModeEnum.class, ModeEnum.INTERBRANCH);
		this.container.addContainerProperty(Inbound.COLUMN_SERVICE_PROVIDER, String.class, "");
		this.container.addContainerProperty(Inbound.COLUMN_FROM, String.class, "");
		this.container.addContainerProperty(Inbound.COLUMN_TO, String.class, "");
		this.container.addContainerProperty(Employee.COLUMN_DEPARTMENT, String.class, "");
		this.container.addContainerProperty(Inbound.COLUMN_NO_OF_DOCS, Integer.class, 0);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				HorizontalLayout.class, null);

		this.table.setColumnHeader(Inbound.COLUMN_AWB_NO,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_AWB_NO));
		this.table.setColumnHeader(Inbound.COLUMN_DATE,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE));
		this.table.setColumnHeader(Inbound.COLUMN_MODE,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_MODE));
		this.table.setColumnHeader(Inbound.COLUMN_SERVICE_PROVIDER,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_SERVICE_PROVIDER));
		this.table.setColumnHeader(Inbound.COLUMN_FROM,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_CONSIGNOR));
		this.table.setColumnHeader(Inbound.COLUMN_TO,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE));
		this.table.setColumnHeader(Employee.COLUMN_DEPARTMENT,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_EMPLOYEE_DEPARTMENT));
		this.table.setColumnHeader(Inbound.COLUMN_NO_OF_DOCS,
				this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_NO_OF_DOCS));
		this.table.setColumnHeader(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION));

		this.table.setColumnAlignment(Inbound.COLUMN_NO_OF_DOCS, Align.RIGHT);

		this.table.setColumnWidth(Inbound.COLUMN_AWB_NO, 150);
		this.table.setColumnWidth(Inbound.COLUMN_DATE, 150);
		this.table.setColumnWidth(Inbound.COLUMN_MODE, 100);
		this.table.setColumnWidth(Inbound.COLUMN_SERVICE_PROVIDER, 150);
		this.table.setColumnWidth(Inbound.COLUMN_FROM, 200);
		this.table.setColumnWidth(Inbound.COLUMN_TO, 200);
		this.table.setColumnWidth(Employee.COLUMN_DEPARTMENT, 200);
		this.table.setColumnWidth(Inbound.COLUMN_NO_OF_DOCS, 100);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), 165);

		this.table.setContainerDataSource(this.container);

		this.refreshDeliverySheetTable();

		return wDesigLayout;
	}

	/**
	 * 
	 */
	private void refreshFloor() {
		FloorManager floorManager = new FloorManager();
		try {
			this.floorContainer.addAll(floorManager.listAll());
		} catch (XERPException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 
	 */
	private void refreshDepartment() {
		DepartmentManager departmentManager = new DepartmentManager();
		try {
			this.departmentContainer.addAll(departmentManager.list());
		} catch (XERPException e) {
			e.printStackTrace();
		}
	}

	private void refreshDeliverySheetTable() {
		this.table.removeAllItems();
		for (Object[] object : this.objects) {
			DeliverySheetDetail deliverySheetDetail = this.init(object);
			BeanFieldGroup<DeliverySheetDetail> deliverySheetDetailBinder = new BeanFieldGroup<DeliverySheetDetail>(
					DeliverySheetDetail.class);
			deliverySheetDetailBinder.setItemDataSource(deliverySheetDetail);
			HorizontalLayout horizontalLayout = new HorizontalLayout();
			int i = 5;
			this.table
					.addItem(
							new Object[] { deliverySheetDetail.getInbound().getAwbNo() + "",
									XERPUtil.formatDate(deliverySheetDetail.getInbound().getDate(),
											DataHolder.commonUserDateTimeFormat),
					deliverySheetDetail.getInbound().getMode(), object[i++] + "", object[i++] + "", object[i++] + "",
					object[i++] + "", deliverySheetDetail.getInbound().getNoOfDocs(), horizontalLayout },
					deliverySheetDetailBinder);
		}
		this.summaryDeliverySheetTable();
	}

	private DeliverySheetDetail init(Object[] object) {
		DeliverySheetDetail deliverySheetDetail = new DeliverySheetDetail();
		int i = 0;
		deliverySheetDetail.setId(XERPUtil.sum(object[i++], 0).longValue());
		if (deliverySheetDetail.getId() > 0) {
			try {
				deliverySheetDetail = new DeliverySheetManager().getDeliverySheetDetail(deliverySheetDetail);
			} catch (XERPException e) {
				e.printStackTrace();
			}
		} else {
			deliverySheetDetail.setDeliverySheet(this.deliverySheet);
			deliverySheetDetail.setRemarks("");
			deliverySheetDetail.setDeliveryStatus(DeliveryStatus.UNDELIVERED);
			Inbound inbound = new Inbound();
			inbound.setId(XERPUtil.sum(object[i++], 0).longValue());
			inbound.setAwbNo(object[i++] + "");
			inbound.setDate(XERPUtil.sum(object[i++], 0).longValue());
			inbound.setMode(ModeEnum.get(XERPUtil.sum(object[i++], 0).intValue()));
			i++;
			i++;
			i++;
			i++;
			inbound.setNoOfDocs(XERPUtil.sum(object[i++], 0).intValue());
			deliverySheetDetail.setInbound(inbound);
		}
		return deliverySheetDetail;
	}

	public void summaryDeliverySheetTable() {
		Collection<?> itemIds = this.table.getItemIds();
		long noOfDocs = 0;
		for (Object itemId : itemIds) {
			@SuppressWarnings("unchecked")
			BeanFieldGroup<DeliverySheetDetail> deliverySheetDetailBinder = (BeanFieldGroup<DeliverySheetDetail>) itemId;
			DeliverySheetDetail deliverySheetDetail = deliverySheetDetailBinder.getItemDataSource().getBean();
			noOfDocs += deliverySheetDetail.getInbound().getNoOfDocs();
		}
		this.table.setColumnFooter(Inbound.COLUMN_NO_OF_DOCS, noOfDocs + "");
	}

	public void removeDeliverySheetDetail(BeanFieldGroup<DeliverySheetDetail> deliverySheetDetailBinder) {
		if (this.table.size() > 1) {
			this.table.removeItem(deliverySheetDetailBinder);
			this.summaryDeliverySheetTable();
		}
	}

	// private HorizontalLayout
	// createActionLayout(BeanFieldGroup<DeliverySheetDetail>
	// deliverySheetDetailBinder) {
	// HorizontalLayout horizontalLayout = new HorizontalLayout();
	//
	// XERPButton addButton = new XERPButton();
	// addButton.setStyleName(BaseTheme.BUTTON_LINK);
	// addButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
	// addButton.setIcon(FontAwesome.PLUS);
	// horizontalLayout.addComponent(addButton);
	//
	// XERPButton removeButton = new XERPButton();
	// removeButton.setStyleName(BaseTheme.BUTTON_LINK);
	// removeButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
	// removeButton.setIcon(FontAwesome.MINUS);
	// horizontalLayout.addComponent(removeButton);
	//
	// return horizontalLayout;
	// }

	private void init() {
		this.objects = new ArrayList<Object[]>();
		if (this.deliverySheet == null || this.deliverySheet.getId() == 0) {
			this.deliverySheet = new DeliverySheet();
			this.deliverySheet.setReceivedBy("");
			this.deliverySheet.setReceivedBy("");
			this.deliverySheet.setDeliverySheetDetails(new ArrayList<DeliverySheetDetail>());
			try {
				this.objects = new DeliverySheetManager().listReadyForDeliverySQL(
						new java.sql.Date(System.currentTimeMillis()), new java.sql.Date(System.currentTimeMillis()),
						null, null);
			} catch (XERPException e) {
				e.printStackTrace();
			}
		} else {
			try {
				this.objects = new DeliverySheetManager().listDeliverySheetDetailSQL(this.deliverySheet);
			} catch (XERPException e) {
				e.printStackTrace();
			}
		}

	}

	@Override
	public File onExportXLS() throws XERPException {
		return null;
	}

	@Override
	public File onExportPDF() throws XERPException {
		Map<String, Object> paramMap = new HashMap<String, Object>();
		return XAccUtil.exportTableIntoPDF(this.table, "xmr-delivery-sheet.html", paramMap);
	}

	@Override
	public File onPrint() throws XERPException {
		// TODO Auto-generated method stub
		return null;
	}

	public void refreshTable() {
		java.sql.Date from = null;
		java.sql.Date to = null;
		Floor floor = null;
		Department department = null;
		if (this.fromField != null && this.fromField.getValue() != null) {
			from = new java.sql.Date(this.fromField.getValue().getTime());
		}
		if (this.toField != null && this.toField.getValue() != null) {
			to = new java.sql.Date(this.toField.getValue().getTime());
		}
		if (this.routeCombo != null && this.routeCombo.getValue() != null) {
			floor = (Floor) this.routeCombo.getValue();
		}
		if (this.departmantCombo != null && this.departmantCombo.getValue() != null) {
			department = (Department) this.departmantCombo.getValue();
		}
		try {
			this.objects = new DeliverySheetManager().listReadyForDeliverySQL(from, to, floor, department);
			this.refreshDeliverySheetTable();
		} catch (XERPException e) {
			e.printStackTrace();
		}
	}

}
