package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.mr.entity.DeliverySheet;

public class DeliverySheetListItemChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2846636880190330826L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private FilterTable table;

	public DeliverySheetListItemChangeListener(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			FilterTable table) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.table = table;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		try {
			DeliverySheet deliverySheet = (DeliverySheet) event.getProperty().getValue();
			if (this.table.isSelected(deliverySheet)) {
				this.deliverySheetDataChangeObserver.notifySelectionChange(deliverySheet);
			}
		} catch (ClassCastException e) {
			return;
		}
	}

}
