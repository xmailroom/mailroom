/**
 * 
 */
package in.xpeditions.erp.mr.importdata.ui;

import java.util.ResourceBundle;

import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.UI;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.MultiFileUpload;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadStateWindow;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.importdata.ui.listener.XAccMasterDataImportButtonClickListener;
import in.xpeditions.erp.mr.importdata.ui.listener.XERPMasterDataFileUploadHandler;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class XERPInstallMasterUI extends UI {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8278688332168390143L;

	@Override
	protected void init(VaadinRequest request) {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());

		HorizontalLayout mainLayout = new HorizontalLayout();
		mainLayout.setMargin(true);
		mainLayout.setSpacing(true);
		
		Label masterLabel = new Label();
		masterLabel.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_XACC_MASTER_DATA_IMPORT_CHOOSE_MASTER));
		mainLayout.addComponent(masterLabel);
		
		XERPComboBox masterCombo = new XERPComboBox();
		masterCombo.addItem("Country");
		masterCombo.addItem("Employee");
		mainLayout.addComponent(masterCombo);

		UploadStateWindow fileUploadStateWindow = new UploadStateWindow();
		fileUploadStateWindow.setOverallProgressVisible(true);

		XAccMasterDataImportButtonClickListener importButtonClickListener = new XAccMasterDataImportButtonClickListener(masterCombo);

		NativeButton importButton = new NativeButton();
		String buttonCaption = bundle
				.getString(MailRoomMessageConstants.BUTTON_IMPORT_MASTER_DATA);
		importButton.setCaption(buttonCaption);
		importButton.addClickListener(importButtonClickListener);
		importButton.setIcon(FontAwesome.DOWNLOAD);
		importButton.setDisableOnClick(true);
		importButton.setVisible(false);

		UploadFinishedHandler xerpRefactorDataFileUploadHandler = new XERPMasterDataFileUploadHandler(
				importButtonClickListener, importButton);

		MultiFileUpload dataFileUpload = new MultiFileUpload(
				xerpRefactorDataFileUploadHandler, fileUploadStateWindow, false);
		dataFileUpload.setVisible(true);
		String fileCaption = bundle
				.getString(MailRoomMessageConstants.LABEL_XACC_MASTER_DATA_IMPORT_BROWSE);
		dataFileUpload.getSmartUpload().setUploadButtonCaptions(fileCaption,
				fileCaption);

		mainLayout.addComponent(dataFileUpload);

		mainLayout.addComponent(importButton);

		setContent(mainLayout);
	}

}
