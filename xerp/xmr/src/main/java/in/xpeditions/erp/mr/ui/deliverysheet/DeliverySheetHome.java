package in.xpeditions.erp.mr.ui.deliverysheet;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;

public class DeliverySheetHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8123222755656129733L;

	public DeliverySheetHome() {
		this.setSizeFull();
		this.init();
	}

	private void init() {
		DeliverySheetDataChangeObserver deliverySheetDataChangeObserver = new DeliverySheetDataChangeObserver();
		DeliverySheetListContainer deliverySheetListContainer = new DeliverySheetListContainer(
				deliverySheetDataChangeObserver);
		DeliverySheetViewContainer deliverySheetViewContainer = new DeliverySheetViewContainer(
				deliverySheetDataChangeObserver);

		VerticalLayout verticalLayout = new VerticalLayout(deliverySheetListContainer, deliverySheetViewContainer);
		verticalLayout.setExpandRatio(deliverySheetListContainer, 1);
		verticalLayout.setSpacing(true);
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		verticalLayout.setMargin(marginInfo);
		verticalLayout.setMargin(true);

		this.setStyleName("xerp-overflow-auto");
		this.setHeight("100%");

		this.setCompositionRoot(verticalLayout);
	}

}
