package in.xpeditions.erp.mr.ui.inbound;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;

public class InboundHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1041389266884676946L;

	private InboundEditContainer inboundEditContainer;

	public InboundHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		InboundDataChangeObserver inboundDataChangeObserver = new InboundDataChangeObserver();

		HorizontalSplitPanel horizontalSplitPanel = new HorizontalSplitPanel();
		horizontalSplitPanel.setSplitPosition(25);

		VerticalLayout leftLayout = new VerticalLayout();
		leftLayout.setMargin(new MarginInfo(true, false, false, true));
		horizontalSplitPanel.addComponent(leftLayout);

		InboundListContainer inboundListContainer = new InboundListContainer(inboundDataChangeObserver);
		leftLayout.addComponent(inboundListContainer);
		leftLayout.addComponent(inboundListContainer);
		leftLayout.setComponentAlignment(inboundListContainer, Alignment.MIDDLE_CENTER);

		this.inboundEditContainer = new InboundEditContainer(inboundDataChangeObserver, null);
		this.inboundEditContainer.setMargin(new MarginInfo(false, false, false, true));
		horizontalSplitPanel.addComponent(getInboundEditContainer());
		this.setCompositionRoot(horizontalSplitPanel);
	}

	/**
	 * @return the inboundEditContainer
	 */
	public InboundEditContainer getInboundEditContainer() {
		return inboundEditContainer;
	}

	/**
	 * @param inboundEditContainer
	 *            the inboundEditContainer to set
	 */
	public void setInboundEditContainer(InboundEditContainer inboundEditContainer) {
		this.inboundEditContainer = inboundEditContainer;
	}

}
