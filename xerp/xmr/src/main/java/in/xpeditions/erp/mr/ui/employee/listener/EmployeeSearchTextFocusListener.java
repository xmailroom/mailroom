/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

/**
 * @author xpeditions
 *
 */
public class EmployeeSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2656576707840323694L;
	private EmployeeSearchTextShortCutListener countrySearchTextShortCutListener;

	public EmployeeSearchTextFocusListener(EmployeeSearchTextShortCutListener countrySearchTextShortCutListener) {
		this.countrySearchTextShortCutListener = countrySearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(countrySearchTextShortCutListener);
	}
}