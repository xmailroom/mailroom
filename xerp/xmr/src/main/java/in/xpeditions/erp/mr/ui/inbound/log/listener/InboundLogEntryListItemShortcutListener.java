package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.log.InboundLogEntryListContainer;
import in.xpeditions.erp.mr.util.InboundLogEntryUtil;

public class InboundLogEntryListItemShortcutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6033224786014434856L;

	private LogEntry logEntry;

	private InboundLogEntryListContainer inboundLogEntryListContainer;

	private ResourceBundle bundle;

	public InboundLogEntryListItemShortcutListener(String caption, int keyCode, int[] modifierKeys,
			InboundLogEntryListContainer inboundLogEntryListContainer, ResourceBundle bundle) {
		super(caption, keyCode, modifierKeys);
		this.inboundLogEntryListContainer = inboundLogEntryListContainer;
		this.bundle = bundle;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		InboundLogEntryUtil.takeAction(this.logEntry, this.inboundLogEntryListContainer, this.bundle);
	}

	public void setFocusedItem(LogEntry logEntry) {
		this.logEntry = logEntry;
	}

}
