package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class DeliverySheetDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3861853387534972827L;

	private ResourceBundle bundle;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private DeliverySheet deliverySheet;

	public DeliverySheetDeleteButtonClickListener(ResourceBundle bundle,
			DeliverySheetDataChangeObserver deliverySheetDataChangeObserver, DeliverySheet deliverySheet) {
		this.bundle = bundle;
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheet = deliverySheet;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			this.deliverySheet = new DeliverySheetManager().getBy(this.deliverySheet);
		} catch (XERPException e) {
		}
		String pleaseConfirmTxt = this.bundle
				.getString(MailRoomMessageConstants.CONFIRM_DELIVERY_SHEET_DELETE_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = this.bundle
				.getString(MailRoomMessageConstants.CONFIRM_DELIVERY_SHEET_DELETE_MESSAGE);
		String captionYes = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_YES);
		String captionNo = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_NO);

		event.getButton().setEnabled(true);
		DeliverySheetDeleteConfirmListener deliverySheetDeleteConfirmListener = new DeliverySheetDeleteConfirmListener(
				this.bundle, this.deliverySheetDataChangeObserver, this.deliverySheet);
		ConfirmDialog dialog = ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt,
				doYouWantToDeleteTxt + " '" + this.deliverySheet.getNumber() + "' ?", captionYes, captionNo,
				deliverySheetDeleteConfirmListener);
		dialog.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_STYLE_DANGER);
		dialog.getOkButton().addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
	}

}
