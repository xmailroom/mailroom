package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = LogEntry.TABLE_NAME)
public class LogEntry implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8688032202610320074L;

	public static final String TABLE_NAME = "xmr_log_entry";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_BATCH_NO = "batchNo";

	public static final String COLUMN_DATE = "date";

	public static final String COLUMN_MODE = "mode";

	public static final String COLUMN_TYPE = "sType";

	public static final String COLUMN_SERVICE_PROVIDER = "serviceProviderId";

	public static final String COLUMN_NO_OF_SHIP = "noOfShip";

	public static final String COLUMN_NO_OF_ENTRY = "noOfEntry";

	public static final String COLUMN_REMARKS = "remarks";

	public static final String COLUMN_CREATED_TIME = "createdTime";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = LogEntry.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@Column(name = LogEntry.COLUMN_BATCH_NO, unique = true, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long batchNo;

	@Column(name = LogEntry.COLUMN_DATE, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long date;

	@Column(name = LogEntry.COLUMN_MODE, nullable = false)
	private ModeEnum mode;

	@Column(name = LogEntry.COLUMN_TYPE, nullable = false)
	private TypeEnum sType;

	@Column(name = LogEntry.COLUMN_SERVICE_PROVIDER, nullable = false)
	private long serviceProviderId;

	@Column(name = LogEntry.COLUMN_NO_OF_SHIP, nullable = false, columnDefinition = "INT DEFAULT 0")
	private int noOfShip;

	@Column(name = LogEntry.COLUMN_NO_OF_ENTRY, nullable = false, columnDefinition = "INT DEFAULT 0")
	private int noOfEntry;

	@Column(name = LogEntry.COLUMN_REMARKS, columnDefinition = "TEXT DEFAULT NULL")
	private String remarks;

	@Column(name = LogEntry.COLUMN_CREATED_TIME, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long createdTime;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the batchNo
	 */
	public long getBatchNo() {
		return batchNo;
	}

	/**
	 * @param batchNo
	 *            the batchNo to set
	 */
	public void setBatchNo(long batchNo) {
		this.batchNo = batchNo;
	}

	/**
	 * @return the date
	 */
	public long getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(long date) {
		this.date = date;
	}

	/**
	 * @return the mode
	 */
	public ModeEnum getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	/**
	 * @return the sTypeEnum
	 */
	public TypeEnum getsType() {
		return sType;
	}

	/**
	 * @param sTypeEnum
	 *            the sTypeEnum to set
	 */
	public void setsType(TypeEnum sTypeEnum) {
		this.sType = sTypeEnum;
	}

	/**
	 * @return the serviceProviderId
	 */
	public long getServiceProviderId() {
		return serviceProviderId;
	}

	/**
	 * @param serviceProviderId
	 *            the serviceProviderId to set
	 */
	public void setServiceProviderId(long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}

	/**
	 * @return the noOfShip
	 */
	public int getNoOfShip() {
		return noOfShip;
	}

	/**
	 * @param noOfShip
	 *            the noOfShip to set
	 */
	public void setNoOfShip(int noOfShip) {
		this.noOfShip = noOfShip;
	}

	/**
	 * @return the noOfEntry
	 */
	public int getNoOfEntry() {
		return noOfEntry;
	}

	/**
	 * @param noOfEntry
	 *            the noOfEntry to set
	 */
	public void setNoOfEntry(int noOfEntry) {
		this.noOfEntry = noOfEntry;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the createdTime
	 */
	public long getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime
	 *            the createdTime to set
	 */
	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		LogEntry other = (LogEntry) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
