package in.xpeditions.erp.mr.ui.menu.handler;

import java.io.Serializable;

import com.vaadin.ui.Component;
import com.vaadin.ui.Label;
import com.vaadin.ui.Layout;

public class MailRoomMenuChangeHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1162417322431747077L;

	private Layout container;

	private Label breadCumbLabel;

	public MailRoomMenuChangeHandler(Layout container, Label breadCumbLabel) {
		this.container = container;
		this.breadCumbLabel = breadCumbLabel;
	}

	public void handleMenuChange(String title, Component component) {
		this.container.removeAllComponents();
		this.container.addComponent(component);
		this.breadCumbLabel.setValue(title);
	}

}
