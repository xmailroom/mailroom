package in.xpeditions.erp.mr.entity.util;

public enum SP {
	BLUEDART("Blue Dart", Mode.COURIER, Type.O),
	DTDC("DTDC", Mode.COURIER, Type.O),
	OB("OB", Mode.INTER_BRANCH, Type.O),
	IB("IB", Mode.INTER_BRANCH, Type.I),
	FEDEX("Fedex", Mode.COURIER, Type.O),
	DHL("Dhl", Mode.COURIER, Type.O),
	FIRST_FLIGHT("First Flight", Mode.COURIER, Type.O),
	OVERNITE("Overnite", Mode.COURIER, Type.O),
	PROFESSIONAL("Professional", Mode.COURIER, Type.O),
	;
	
	private String name;
	
	private Mode mode;
	
	private Type type;

	private SP(String name, Mode mode, Type type) {
		this.name = name;
		this.setMode(mode);
		this.setType(type);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the mode
	 */
	public Mode getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(Mode mode) {
		this.mode = mode;
	}

	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(Type type) {
		this.type = type;
	}
}
