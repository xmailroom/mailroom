/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public class DepartmentDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5437708514599625364L;
	
	private List<DepartmentDataChangeListener> departmentDataChangeListeners;

	public DepartmentDataChangeObserver() {
		this.departmentDataChangeListeners = new ArrayList<DepartmentDataChangeListener>();
	}

	public void add(DepartmentDataChangeListener departmentDataChangeListener) {
		this.departmentDataChangeListeners.add(departmentDataChangeListener);
	}

	public void remove(DepartmentDataChangeListener departmentDataChangeListener) {
		this.departmentDataChangeListeners.remove(departmentDataChangeListener);
	}

	public void notifyUpdate(Department department) {
		for (DepartmentDataChangeListener departmentDataChangeListener : departmentDataChangeListeners) {
			departmentDataChangeListener.onUpdate(department);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (DepartmentDataChangeListener departmentDataChangeListener : departmentDataChangeListeners) {
			departmentDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifySelectionChange(Department department) {
		for (DepartmentDataChangeListener departmentDataChangeListener : departmentDataChangeListeners) {
			departmentDataChangeListener.onSelectionChange(department);
		}
	}

	public void notifyEditButtonClick() {
		for (DepartmentDataChangeListener departmentDataChangeListener : departmentDataChangeListeners) {
			departmentDataChangeListener.onEditButtonClick();
		}
	}

}
