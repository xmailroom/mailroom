/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5428318104135590926L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;

	public ServiceProviderEditButtonClickListener(
			ServiceProviderDataChangeObserver countryDataChangeObserver) {
				this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.countryDataChangeObserver.notifyEditButtonClick();
		event.getButton().setEnabled(true);
	}

}
