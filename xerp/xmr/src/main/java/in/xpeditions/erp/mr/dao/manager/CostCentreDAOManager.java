/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public interface CostCentreDAOManager {

	public void save(CostCentre costCentre) throws XERPException;

	public void update(CostCentre costCentre) throws XERPException;

	public Long getCostCentreCount(String searchString) throws XERPException;

	public List<CostCentre> listCostCentres(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public void delete(CostCentre costCentre) throws XERPException;

	public List<CostCentre> listByDepartment(Department department) throws XERPException;

	public CostCentre getByName(String costCenterName) throws XERPException;

}
