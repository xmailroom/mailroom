/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class CostcentreSearchTextShortCutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7317625129533600676L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostcentreSearchTextShortCutListener(String prompt, int keyCode,
			CostcentreDataChangeObserver costcentreDataChangeObserver) {
		super(prompt, keyCode, null);
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if ((target instanceof XERPTextField)) {
			XERPTextField field = (XERPTextField) target;
			String value = field.getValue();
			this.costcentreDataChangeObserver.notifySearchTextValueChange(value);
		}
	}

}
