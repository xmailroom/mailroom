package in.xpeditions.erp.mr.entity.util;

public enum Mode {

	COURIER("Courier"), POSTAL("Postal"), INTER_BRANCH("Inter Branch");

	public static final String CAPTION_PROPERTY_ID = "name";

	private String name;

	private Mode(String name) {
		this.setName(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static Mode get(int i) {
		return Mode.values()[i];
	}

}