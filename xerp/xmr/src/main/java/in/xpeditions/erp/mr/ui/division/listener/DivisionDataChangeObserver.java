/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public class DivisionDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6347849551095247601L;

	private List<DivisionDataChangeListener> divisionDataChangeListeners;

	public DivisionDataChangeObserver() {
		this.divisionDataChangeListeners = new ArrayList<DivisionDataChangeListener>();
	}

	public void add(DivisionDataChangeListener divisionDataChangeListener) {
		this.divisionDataChangeListeners.add(divisionDataChangeListener);
	}

	public void remove(DivisionDataChangeListener divisionDataChangeListener) {
		this.divisionDataChangeListeners.remove(divisionDataChangeListener);
	}

	public void notifyEditButtonClick() {
		for (DivisionDataChangeListener divisionDataChangeListener : divisionDataChangeListeners) {
			divisionDataChangeListener.onEditButtonClick();
		}
	}

	public void notifyUpdate(Division division) {
		for (DivisionDataChangeListener divisionDataChangeListener : divisionDataChangeListeners) {
			divisionDataChangeListener.onUpdate(division);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (DivisionDataChangeListener divisionDataChangeListener : divisionDataChangeListeners) {
			divisionDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifySelectionChange(Division division) {
		for (DivisionDataChangeListener divisionDataChangeListener : divisionDataChangeListeners) {
			divisionDataChangeListener.onSelectionChange(division);
		}
	}

}
