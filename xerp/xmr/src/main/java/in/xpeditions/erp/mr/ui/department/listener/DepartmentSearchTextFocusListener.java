/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DepartmentSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8965244498716491505L;
	private DepartmentSearchTextShortCutListener departmentSearchTextShortCutListener;

	public DepartmentSearchTextFocusListener(
			DepartmentSearchTextShortCutListener departmentSearchTextShortCutListener) {
		this.departmentSearchTextShortCutListener = departmentSearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(departmentSearchTextShortCutListener);
	}

}
