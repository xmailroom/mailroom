/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import in.xpeditions.erp.acc.resources.i18n.I18NAccResourceBundle;
import in.xpeditions.erp.acc.resources.i18n.XAccMessageConstants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.manager.CostCentreManager;

/**
 * @author Saran
 *
 */
public class CostCentreDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1347759631664228513L;
	private CostCentre costCentre;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostCentreDeleteConfirmListener(CostCentre costCentre,
			CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costCentre = costCentre;
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void onClose(ConfirmDialog confirmDialog) {
		ResourceBundle bundle = I18NAccResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		if (confirmDialog.isConfirmed()) {
			CostCentreManager costCentreManager = new CostCentreManager();
			try {
				costCentreManager.delete(costCentre);
				Notification.show(bundle.getString(XAccMessageConstants.NOTIFICATION_STATE_DELETED_SUCCESSFULLY));
				this.costcentreDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = bundle.getString(XAccMessageConstants.NOTIFICATION_STATE_FOREIGN_KEY_VALUE) + " '" + costCentre.getName() + "' "+ bundle.getString(XAccMessageConstants.NOTIFICATION_STATE_HAS_REFERENCE);
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(XAccMessageConstants.NOTIFICATION_STATE_DELETE_FAILED), "<br/>" + subMessage , Notification.Type.ERROR_MESSAGE, true).show(page);
			} catch (XERPException e) {
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(XAccMessageConstants.NOTIFICATION_STATE_DELETE_FAILED), "<br/>" + e.getMessage() , Notification.Type.ERROR_MESSAGE, true).show(page);
			}
		}
	}

}
