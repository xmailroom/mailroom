/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;

import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author xpeditions
 *
 */
public class FloorListItemClickListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2481617277924694069L;
	private FloorDataChangeObserver countryDataChangeObserver;

	public FloorListItemClickListener(FloorDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void itemClick(ItemClickEvent event) {
		NestingBeanItem<Floor> nestingBeanItem = (NestingBeanItem<Floor>) event.getItem();
		Floor floor = nestingBeanItem.getBean();
		this.countryDataChangeObserver.notifySelectionChange(floor);
	}
}
