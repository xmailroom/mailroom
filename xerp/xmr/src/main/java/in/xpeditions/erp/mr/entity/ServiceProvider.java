/**
 * 
 */
package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

import in.xpeditions.erp.commons.annotations.XERPPrimaryEntity;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

/**
 * @author Saran
 *
 */
@XERPPrimaryEntity
@Entity
@Table (name = ServiceProvider.TABLE_NAME, uniqueConstraints = { @UniqueConstraint(columnNames = {ServiceProvider.COLUMN_NAME,
		ServiceProvider.COLUMN_TYPE }) })
public class ServiceProvider implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -144535000591297915L;
	
	public static final String TABLE_NAME = "mr_serviceprovider";
	
	public static final String COLUMN_ID = "id";
	
	private static final String COLUMN_MODE = "mode";
	
	public static final String COLUMN_TYPE = "type";
	
	public static final String COLUMN_NAME = "name";
	
	private static final String COLUMN_CONTACT_NAME = "contactName";
	
	private static final String COLUMN_MOBILE = "mobileNumber";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = ServiceProvider.COLUMN_ID, unique = true, nullable = false)
	private long id;
	
	@Column(name = ServiceProvider.COLUMN_MODE)
	private ModeEnum mode;
	
	@Column(name = ServiceProvider.COLUMN_TYPE)
	private TypeEnum type;
	
	@Column(name = ServiceProvider.COLUMN_NAME)
	private String name;
	
	@Column(name = ServiceProvider.COLUMN_CONTACT_NAME)
	private String contactName;

	@Column(name = ServiceProvider.COLUMN_MOBILE)
	private String mobileNumber;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the mode
	 */
	public ModeEnum getMode() {
		return mode;
	}

	/**
	 * @param mode the mode to set
	 */
	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	/**
	 * @return the type
	 */
	public TypeEnum getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(TypeEnum type) {
		this.type = type;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the contactName
	 */
	public String getContactName() {
		return contactName;
	}

	/**
	 * @param contactName the contactName to set
	 */
	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	/**
	 * @return the mobileNumber
	 */
	public String getMobileNumber() {
		return mobileNumber;
	}

	/**
	 * @param mobileNumber the mobileNumber to set
	 */
	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ServiceProvider other = (ServiceProvider) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
