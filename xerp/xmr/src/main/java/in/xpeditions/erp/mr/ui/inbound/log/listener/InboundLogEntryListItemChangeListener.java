package in.xpeditions.erp.mr.ui.inbound.log.listener;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.mr.entity.LogEntry;

public class InboundLogEntryListItemChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3500765411800676770L;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private FilterTable table;

	private InboundLogEntryListItemClickListener inboundLogEntryListItemClickListener;

	private InboundLogEntryListItemShortcutListener inboundLogEntryListItemShortcutListener;

	public InboundLogEntryListItemChangeListener(InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver,
			InboundLogEntryListItemClickListener inboundLogEntryListItemClickListener, InboundLogEntryListItemShortcutListener inboundLogEntryListItemShortcutListener, FilterTable table) {
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.inboundLogEntryListItemClickListener = inboundLogEntryListItemClickListener;
		this.inboundLogEntryListItemShortcutListener = inboundLogEntryListItemShortcutListener;
		this.table = table;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		LogEntry logEntry = (LogEntry) event.getProperty().getValue();
		if (this.table.isSelected(logEntry)) {
			this.inboundLogEntryListItemClickListener.setFocusedItem(logEntry);
			this.inboundLogEntryDataChangeObserver.notifySelectionChange(logEntry);
			this.inboundLogEntryListItemShortcutListener.setFocusedItem(logEntry);
		}
	}

}
