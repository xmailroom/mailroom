/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class DivisionAddNewButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7439026405440178352L;
	private DivisionDataChangeObserver divisionDataChangeObserver;

	public DivisionAddNewButtonClickListener(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.divisionDataChangeObserver.notifyUpdate(null);
		button.setEnabled(true);
	}

}
