/**
 * 
 */
package in.xpeditions.erp.mr.converter;

import java.util.HashMap;
import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public class EmployeeToLongConverter<T1, T2> implements Converter<Object, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5612943022187500154L;

	private HashMap<Long, Employee> employeeMap;

	public EmployeeToLongConverter(HashMap<Long, Employee> employeeMap) {
		this.employeeMap = employeeMap;
	}

	@Override
	public Long convertToModel(Object value, Class<? extends Long> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value != null) {
			long id = ((Employee) value).getId();
			return id;
		}
		return 0L;
	}

	@Override
	public Employee convertToPresentation(Long value, Class<? extends Object> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == 0L) {
			return null;
		}
		return this.employeeMap.get(value);
	}

	@Override
	public Class<Long> getModelType() {
		return Long.class;
	}

	@Override
	public Class<Object> getPresentationType() {
		return Object.class;
	}

}
