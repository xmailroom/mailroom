package in.xpeditions.erp.mr.resources.i18n;

public class MailRoomMessageConstants {

	/* Menu */
	public static final String COMMON_MENU_PATH_SEPARATOR = "common_menu_path_separator";

	public static final String HOME_MENU_MAIL_ROOM = "home_menu_mail_room";

	public static final String HOME_MENU_MAIL_ROOM_INBOUND_LOG = "home_menu_mail_room_inbound_log";

	public static final String HOME_MENU_MAIL_ROOM_INBOUND = "home_menu_mail_room_inbound";

	public static final String HOME_MENU_MAIL_ROOM_SP = "home_menu_mail_room_sp";

	public static final String HOME_MENU_MAIL_ROOM_DELIVERY_SHEET = "home_menu_mail_room_delivery_sheet";

	/* Common */
	public static final String BUTTON_DELETE = "button_delete";
	public static final String BUTTON_SAVE = "button_save";
	public static final String BUTTON_CLOSE = "button_close";
	public static final String BUTTON_CLEAR = "button_clear";
	public static final String BUTTON_OK = "button_ok";
	public static final String BUTTON_PRINT = "button_print";
	public static final String LABEL_ACTION = "label_action";

	public static final String ERROR_PERMISSION = "error_permission";
	public static final String ERROR_REQUIRED = "error_required";
	public static final String PERMISSION_REQUIRED = "permission_required";

	public static final String NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT = "notification_please_ensure_provided_information_are_correct";

	public static final String CONFIRM_DELETE_BUTTON_CAPTION_YES = "confirm_delete_button_caption_yes";
	public static final String CONFIRM_DELETE_BUTTON_CAPTION_NO = "confirm_delete_button_caption_no";

	public static final String NOTIFICATION_FOREIGN_KEY_VALUE = "notification_foreign_key_value";
	public static final String NOTIFICATION_HAS_REFERENCE = "notification_has_reference";

	/* Service Provider */
	public static final String NOTIFICATION_SP_SAVED_SUCCESSFULLY = "notification_sp_saved_successfully";
	public static final String NOTIFICATION_SP_SAVE_FAILED = "notification_sp_save_failed";
	public static final String NOTIFICATION_SP_DUPILCATE_VALUE_NAME = "notification_sp_dupilcate_value_name";
	public static final String NOTIFICATION_SP_ALREADY_EXISTS = "notification_sp_already_exists";
	public static final String NOTIFICATION_SP_DELETED_SUCCESSFULLY = "notification_sp_deleted_successfully";
	public static final String NOTIFICATION_SP_DELETE_FAILED = "notification_sp_delete_failed";
	public static final String NOTIFICATION_SP_REQUIRED = "notification_sp_required";

	/* Inbound Log */
	public static final String CONFIRM_INBOUND_LOG_DELETE_WINDOW_CAPTION = "confirm_inbound_log_delete_window_caption";
	public static final String CONFIRM_INBOUND_LOG_DELETE_MESSAGE = "confirm_inbound_log_delete_message";

	public static final String LABEL_INBOUND_LOG_INDEX = "label_inbound_log_index";
	public static final String LABEL_INBOUND_LOG_SERVICE_PROVIDER = "label_inbound_log_service_provider";
	public static final String LABEL_INBOUND_LOG_DATE = "label_inbound_log_date";
	public static final String LABEL_INBOUND_LOG_BATCH_NO = "label_inbound_log_batch_no";
	public static final String LABEL_INBOUND_LOG_MODE = "label_inbound_log_mode";
	public static final String LABEL_INBOUND_LOG_NO_OF_SHIPMENT = "label_inbound_log_no_of_shipment";
	public static final String LABEL_INBOUND_LOG_REMARKS = "label_inbound_log_remarks";
	public static final String LABEL_INBOUND_LOG_NO_OF_ENTRY = "label_inbound_log_no_of_entry";
	public static final String LABEL_INBOUND_LOG_PENDING = "label_inbound_log_pending";

	public static final String NOTIFICATION_INBOUND_LOG_SAVED_SUCCESSFULLY = "notification_inbound_log_saved_successfully";
	public static final String NOTIFICATION_INBOUND_LOG_SAVE_FAILED = "notification_inbound_log_save_failed";
	public static final String NOTIFICATION_INBOUND_LOG_DUPILCATE_VALUE_NAME = "notification_inbound_log_dupilcate_value_name";
	public static final String NOTIFICATION_INBOUND_LOG_ALREADY_EXISTS = "notification_inbound_log_already_exists";
	public static final String NOTIFICATION_INBOUND_LOG_DELETED_SUCCESSFULLY = "notification_inbound_log_deleted_successfully";
	public static final String NOTIFICATION_INBOUND_LOG_DELETE_FAILED = "notification_inbound_log_delete_failed";
	public static final String NOTIFICATION_INBOUND_LOG_REQUIRED = "notification_inbound_log_required";
	public static final String NOTIFICATION_UNABLE_TO_DELETE_INBOUND_LOG_REASON_1 = "notification_unable_to_delete_inbound_log_reason_1";

	/* Inbound */

	public static final String CONFIRM_INBOUND_DELETE_WINDOW_CAPTION = "confirm_inbound_delete_window_caption";
	public static final String CONFIRM_INBOUND_DELETE_MESSAGE = "confirm_inbound_delete_message";
	public static final String CONFIRM_MESSAGE_DELETE_BULK_ENTY = "confirm_message_delete_bulk_enty";

	public static final String LABEL_INBOUND_INDEX = "label_inbound_index";
	public static final String LABEL_INBOUND_SERVICE_PROVIDER = "label_inbound_service_provider";
	public static final String LABEL_INBOUND_DATE = "label_inbound_date";
	public static final String LABEL_INBOUND_BATCH_NO = "label_inbound_batch_no";
	public static final String LABEL_INBOUND_MODE = "label_inbound_mode";
	public static final String LABEL_INBOUND_NO_OF_SHIPMENT = "label_inbound_no_of_shipment";
	public static final String LABEL_INBOUND_REMARKS = "label_inbound_remarks";
	public static final String LABEL_INBOUND_NO_OF_ENTRY = "label_inbound_no_of_entry";
	public static final String LABEL_INBOUND_AWB_NO = "label_inbound_awb_no";
	public static final String LABEL_INBOUND_BULK = "label_inbound_bulk";
	public static final String LABEL_INBOUND_CONSIGNOR = "label_inbound_consignor";
	public static final String LABEL_INBOUND_CITY = "label_inbound_city";
	public static final String LABEL_INBOUND_REF_NO = "label_inbound_ref_no";
	public static final String LABEL_INBOUND_EMPLOYEE = "label_inbound_employee";
	public static final String LABEL_INBOUND_BRANCH = "label_inbound_branch";
	public static final String LABEL_INBOUND_ISR_NO = "label_inbound_isr_no";
	public static final String LABEL_INBOUND_NO_OF_DOCS = "label_inbound_no_of_docs";

	public static final String NOTIFICATION_INBOUND_NO_PENDING = "notification_inbound_no_pending";
	public static final String NOTIFICATION_INBOUND_NO_PENDING_REASON = "notification_inbound_no_pending_reason";
	public static final String NOTIFICATION_INBOUND_SAVED_SUCCESSFULLY = "notification_inbound_saved_successfully";
	public static final String NOTIFICATION_INBOUND_SAVE_FAILED = "notification_inbound_save_failed";
	public static final String NOTIFICATION_INBOUND_DUPILCATE_VALUE_NAME = "notification_inbound_dupilcate_value_name";
	public static final String NOTIFICATION_INBOUND_ALREADY_EXISTS = "notification_inbound_already_exists";
	public static final String NOTIFICATION_INBOUND_DELETED_SUCCESSFULLY = "notification_inbound_deleted_successfully";
	public static final String NOTIFICATION_INBOUND_DELETE_FAILED = "notification_inbound_delete_failed";
	public static final String NOTIFICATION_INBOUND_REQUIRED = "notification_inbound_required";
	public static final String NOTIFICATION_INBOUND_FROM_REQUIRED = "notification_inbound_from_required";
	public static final String NOTIFICATION_INBOUND_TO_REQUIRED = "notification_inbound_to_required";
	public static final String NOTIFICATION_INBOUND_BULK_ENTRIES_NOT_MATCHED = "notification_inbound_bulk_entries_not_matched";
	public static final String NOTIFICATION_INBOUND_BULK_ENTRIES_NOT_MATCHED_REASON = "notification_inbound_bulk_entries_not_matched_reason";

	public static final String NOTIFICATION_INBOUND_ENTRY_ALREADY_COMPLETED = "Inbound detail entry already completed for selected Log / It may saved from some other location or saved by some other user";

	public static final String WINDOW_HEADING_SELECT_EMPLOYEE = "window_heading_select_employee";
	public static final String WINDOW_HEADING_SELECT_LOG_ENTRY = "window_heading_select_log_entry";

	public static final String LABEL_INBOUND_ES_SL_NO = "label_inbound_es_sl_no";
	public static final String LABEL_INBOUND_ES_NAME = "label_inbound_es_name";
	public static final String LABEL_INBOUND_ES_BRANCH = "label_inbound_es_branch";
	public static final String LABEL_INBOUND_ES_CITY = "label_inbound_es_city";

	/* Delivery Sheet */
	public static final String BUTTON_CAPTION_ADD_DELIVERY_SHEET = "button_caption_add_delivery_sheet";

	public static final String CONFIRM_DELIVERY_SHEET_DELETE_WINDOW_CAPTION = "confirm_delivery_sheet_delete_window_caption";
	public static final String CONFIRM_DELIVERY_SHEET_DELETE_MESSAGE = "confirm_delivery_sheet_delete_message";

	public static final String COLUMN_NO_OF_INBOUND = "column_no_of_inbound";
	public static final String COLUMN_NO_OF_DOCS = "column_no_of_docs";
	public static final String COLUMN_NO_OF_INBOUND_DELIVERED = "column_no_of_inbound_delivered";

	public static final String CAPTION_DELIVERY_SHEET_NUMBER = "caption_delivery_sheet_number";
	public static final String CAPTION_DELIVERY_SHEET_DATE = "caption_delivery_sheet_date";
	public static final String CAPTION_DELIVERY_SHEET_NO_OF_INBOUND = "caption_delivery_sheet_no_of_inbound";
	public static final String CAPTION_DELIVERY_NO_OF_DOCS = "caption_delivery_no_of_docs";
	public static final String CAPTION_DELIVERY_NO_OF_INBOUND_DELIVERED = "caption_delivery_no_of_inbound_delivered";
	public static final String CAPTION_DELIVERY_SHEET_RECEIVED_BY = "caption_delivery_sheet_received_by";
	public static final String CAPTION_DELIVERY_SHEET_RECEIVED_TIME = "caption_delivery_sheet_received_time";

	public static final String LABEL_INBOUND_EMPLOYEE_DEPARTMENT = "label_inbound_employee_department";

	public static final String NOTIFICATION_DELIVERY_SHEET_ALREADY_EXISTS = "notification_delivery_sheet_already_exists";
	public static final String NOTIFICATION_DELIVERY_SHEET_SAVE_FAILED = "notification_delivery_sheet_save_failed";
	public static final String NOTIFICATION_DELIVERY_SHEET_SAVED_SUCCESSFULLY = "notification_delivery_sheet_saved_successfully";
	public static final String NOTIFICATION_DELIVERY_SHEET_REQUIRED = "notification_delivery_sheet_required";
	public static final String NOTIFICATION_DELIVERY_SHEET_DETAIL_REQUIRED = "notification_delivery_sheet_detail_required";
	public static final String NOTIFICATION_DELIVERY_SHEET_DELETED_SUCCESSFULLY = "notification_delivery_sheet_deleted_successfully";
	public static final String NOTIFICATION_DELIVERY_SHEET_DELETE_FAILED = "notification_delivery_sheet_delete_failed";

	public static final String CAPTION_DELIVERY_SHEET_FILTER_FROM = "caption_delivery_sheet_filter_from";
	public static final String CAPTION_DELIVERY_SHEET_FILTER_TO = "caption_delivery_sheet_filter_to";
	public static final String CAPTION_DELIVERY_SHEET_FILTER_ROUTE = "caption_delivery_sheet_filter_route";
	public static final String CAPTION_DELIVERY_SHEET_FILTER_DEPARTMENT = "caption_delivery_sheet_filter_department";
	
	// Master Message Constants
	public static final String HOME_MENU_MASTERS_DEPARTMENT = "home_menu_masters_department";
	public static final String HOME_MENU_MASTERS_COSTCENTRE = "home_menu_masters_costcentre";
	public static final String HOME_MENU_ACCOUNT_DIVISION = "home_menu_account_division";
	public static final String LABEL_CAPTION_DIVISION_NAME = "label_caption_division_name";
	public static final String TEXTFIELD_INPUT_PROMPT_DIVISION_NAME = "textfield_input_prompt_division_name";
	public static final String DIVISION_NAME_REQUIRED = "division_name_required";
	public static final String LABEL_CAPTION_DIVISION_CODE = "label_caption_division_code";
	public static final String TEXTFIELD_INPUT_PROMPT_DIVISION_CODE = "textfield_input_prompt_division_code";
	public static final String DIVISION_CODE_REQUIRED = "division_code_required";
	public static final String DIVISION_SAVE_FAILED = "division_save_failed";
	public static final String NOTIFICATION_DIVISION_UPDATED_SUCCESSFULLY = "notification_division_updated_successfully";
	public static final String NOTIFICATION_DIVISION_SAVED_SUCCESSFULLY = "notification_division_saved_successfully";
	public static final String NOTIFICATION_DIVISION_DUPILCATE_VALUE_NAME = "notification_division_dupilcate_value_name";
	public static final String NOTIFICATION_DIVISION_ALREADY_EXISTS = "notification_division_already_exists";
	public static final String HOME_MENU_MASTERS_DIVISION = "home_menu_masters_division";
	
	public static final String DIVISION_LIST_HEADER_NAME = "division_list_header_name";
	public static final String DIVISION_LIST_HEADER_CODE = "division_list_header_code";
	
	public static final String DIVISION_DELETE_CONFIRM_WINDOW_CAPTION = "division_delete_confirm_window_caption";
	public static final String DIVISION_DELETE_CONFIRM_MESSAGE = "division_delete_confirm_message";
	public static final String DIVISION_DELETE_CONFIRM_BUTTON_CAPTION_YES = "division_delete_confirm_button_caption_yes";
	public static final String DIVISION_DELETE_CONFIRM_BUTTON_CAPTION_NO = "division_delete_confirm_button_caption_no";
	public static final String NOTIFICATION_DIVISION_DELETED_SUCCESSFULLY = "notification_division_deleted_successfully";
	public static final String NOTIFICATION_DIVISION_FOREIGN_KEY_VALUE = "notification_division_foreign_key_value";
	public static final String NOTIFICATION_DIVISION_HAS_REFERENCE = "notification_division_has_reference";
	public static final String NOTIFICATION_DIVISION_DELETE_FAILED = "notification_division_delete_failed";
	
	public static final String DEPARTMENT_LIST_HEADER_NAME = "department_list_header_name";
	public static final String DEPARTMENT_LIST_HEADER_CODE = "department_list_header_code";
	
	public static final String LABEL_CAPTION_DEPARTMENT_DIVISION = "label_caption_department_division";
	public static final String LABEL_CAPTION_DEPARTMENT_CODE = "label_caption_department_code";
	public static final String LABEL_CAPTION_DEPARTMENT_NAME = "label_caption_department_name";
	
	public static final String TEXTFIELD_INPUT_PROMPT_DEPARTMENT_CODE = "textfield_input_prompt_department_code";
	public static final String TEXTFIELD_INPUT_PROMPT_DEPARTMENT_NAME = "textfield_input_prompt_department_name";
	
	public static final String DEPARTMENT_DIVISION_NAME_REQUIRED = "department_division_name_required";
	public static final String DEPARTMENT_CODE_REQUIRED = "department_code_required";
	public static final String DEPARTMENT_NAME_REQUIRED = "department_name_required";
	public static final String LIST_HEADER_DEPARTMENT_FLOOR = "list_header_department_floor";
	public static final String LIST_HEADER_DEPARTMENT_FLOOR_NAME = "list_header_department_floor_name";
	
	public static final String DEPARTMENT_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT = "department_please_ensure_provided_information_are_correct";
	public static final String DEPARTMENT_PLEASE_SELECT_FLOOR = "department_please_select_floor";
	
	public static final String NOTIFICATION_DEPARTMENT_UPDATED_SUCCESSFULLY = "notification_department_updated_successfully";
	public static final String NOTIFICATION_DEPARTMENT_SAVED_SUCCESSFULLY = "notification_department_saved_successfully";
	public static final String DEPARTMENT_SAVE_FAILED = "department_save_failed";
	public static final String NOTIFICATION_DEPARTMENT_DUPILCATE_VALUE_NAME = "notification_department_dupilcate_value_name";
	public static final String NOTIFICATION_DEPARTMENT_ALREADY_EXISTS = "notification_department_already_exists";
	
	public static final String DEPARTMENT_DELETE_CONFIRM_WINDOW_CAPTION = "department_delete_confirm_window_caption";
	public static final String DEPARTMENT_DELETE_CONFIRM_MESSAGE = "department_delete_confirm_message";
	public static final String DEPARTMENT_DELETE_CONFIRM_BUTTON_CAPTION_YES = "department_delete_confirm_button_caption_yes";
	public static final String DEPARTMENT_DELETE_CONFIRM_BUTTON_CAPTION_NO = "department_delete_confirm_button_caption_no";
	
	public static final String NOTIFICATION_DEPARTMENT_DELETED_SUCCESSFULLY = "notification_department_deleted_successfully";
	public static final String NOTIFICATION_DEPARTMENT_FOREIGN_KEY_VALUE = "notification_department_foreign_key_value";
	public static final String NOTIFICATION_DEPARTMENT_HAS_REFERENCE = "notification_department_has_reference";
	public static final String NOTIFICATION_DEPARTMENT_DELETE_FAILED = "notification_department_delete_failed";
	
	public static final String COSTCENTRE_LIST_HEADER_NAME = "costcentre_list_header_name";
	public static final String COSTCENTRE_LIST_HEADER_CODE = "costcentre_list_header_code";
	public static final String BUTTON_ADD_NEW_DIVISION = "button_add_new_division";
	public static final String BUTTON_ADD_NEW_DEPARTMENT = "button_add_new_department";
	public static final String BUTTON_ADD_NEW_COSTCENTRE = "button_add_new_costcentre";
	
	public static final String LABEL_CAPTION_COSTCENTRE_DEPARTMENT = "label_caption_costcentre_department";
	public static final String LABEL_CAPTION_COSTCETRE_NAME = "label_caption_costcetre_name";
	public static final String LABEL_CAPTION_COSTCENTRE_CODE = "label_caption_costcentre_code";
	
	public static final String COSTCENTRE_SAVE_FAILED = "costcentre_save_failed";
	public static final String COSTCENTRE_DEPARTMENT_NAME_REQUIRED = "costcentre_department_name_required";
	public static final String COSTCENTRE_DELETE_CONFIRM_WINDOW_CAPTION = "costcentre_delete_confirm_window_caption";
	public static final String COSTCENTRE_DELETE_CONFIRM_MESSAGE = "costcentre_delete_confirm_message";
	public static final String COSTCENTRE_DELETE_CONFIRM_BUTTON_CAPTION_YES = "costcentre_delete_confirm_button_caption_yes";
	public static final String COSTCENTRE_DELETE_CONFIRM_BUTTON_CAPTION_NO = "costcentre_delete_confirm_button_caption_no";
	
	public static final String BUTTON_ADD_NEW_FLOOR = "button_add_new_floor";
	public static final String FLOOR_LIST_HEADER_NAME = "floor_list_header_name";
	public static final String HOME_MENU_ACCOUNT_FLOOR = "home_menu_account_floor";
	public static final String HOME_MENU_MASTERS_FLOOR = "home_menu_masters_floor";
	public static final String FLOOR_SAVE_FAILED = "floor_save_failed";
	public static final String NOTIFICATION_FLOOR_UPDATED_SUCCESSFULLY = "notification_floor_updated_successfully";
	public static final String NOTIFICATION_FLOOR_SAVED_SUCCESSFULLY = "notification_floor_saved_successfully";
	
	public static final String BUTTON_ADD_NEW_EMPLOYEE = "button_add_new_employee";
	
	public static final String EMPLOYEE_LIST_HEADER_NAME = "employee_list_header_name";
	public static final String EMPLOYEE_LIST_HEADER_CODE = "employee_list_header_code";
	
	public static final String LABEL_CAPTION_EMPLOYEE_NAME = "label_caption_employee_name";
	public static final String EMPLOYEE_SAVE_FAILED = "employee_save_failed";
	public static final String EMPLOYEE_NAME_REQUIRED = "employee_name_required";
	public static final String EMPLOYEE_ID_REQUIRED = "employee_id_required";
	public static final String HOME_MENU_MASTERS_EMPLOYEE = "home_menu_masters_employee";
	public static final String EMPLOYEE_BRANCH_REQUIRED = "employee_branch_required";
	public static final String LABEL_CAPTION_EMPLOYEE_BRANCH = "label_caption_employee_branch";
	public static final String LABAEL_CAPTION_EMPLOYEE_DIVISION = "labael_caption_employee_division";
	public static final String EMPLOYEE_DIVISION_REQUIRED = "employee_division_required";
	public static final String LABEL_CAPTION_EMPLOYEE_DEPARTMENT = "label_caption_employee_department";
	public static final String EMPLOYEE_DEPARTMENT_REQUIRED = "employee_department_required";
	public static final String LABEL_CAPTION_EMPLOYEE_COSTCENTRE = "label_caption_employee_costcentre";
	public static final String EMPLOYEE_COSTCENTRE_REQUIRED = "employee_costcentre_required";
	public static final String LABEL_CAPTION_EMPLOYEE_NATURE = "label_caption_employee_nature";
	public static final String LABEL_CAPTION_EMPLOYEE_ID = "label_caption_employee_id";
	public static final String LABEL_CAPTION_EMAIL = "label_caption_email";
	public static final String EMPLOYEE_EMAIL_REQUIRED = "employee_email_required";
	public static final String LABEL_CAPTION_EXENSION = "label_caption_exension";
	public static final String LABEL_CAPTION_CABIN = "label_caption_cabin";
	public static final String LABEL_CAPTION_BUILDING = "label_caption_building";
	public static final String LABEL_CAPTION_LOCATION = "label_caption_location";
	public static final String LABEL_CAPTION_MOBILE = "label_caption_mobile";
	public static final String LABEL_CAPTION_EMPLOYEE_GENDER = "label_caption_employee_gender";
	public static final String LABEL_CAPTION_BAND = "label_caption_band";
	public static final String LABEL_CAPTION_EMPLOYEE_ACTIVE = "label_caption_employee_active";
	public static final String LIST_HEADER_EMPLOYEE_FLOOR_SELECT = "list_header_employee_floor_select";
	public static final String LIST_HEADER_EMPLOYEE_FLOOR_NAME = "list_header_employee_floor_name";
	public static final String EMPLOYEE_PLEASE_SELECT_FLOOR = "employee_please_select_floor";
	public static final String EMPLOYEE_PLEASE_SELECT_ANY_ONE_FLOOR = "employee_please_select_any_one_floor";
	public static final String EMPLOYEE_LIST_HEADER_DEPARTMENT_NAME = "employee_list_header_department_name";
	public static final String HOME_MENU_MASTERS_SERVICE_PROVIDER = "home_menu_masters_service_provider";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_NAME = "label_caption_service_provider_name";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_MODE = "label_caption_service_provider_mode";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_MODE_CANNOT_EMPTY = "label_caption_service_provider_mode_cannot_empty";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_TYPE = "label_caption_service_provider_type";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_TYPE_CANNOT_EMPTY = "label_caption_service_provider_type_cannot_empty";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_CONTACT_NAME = "label_caption_service_provider_contact_name";
	public static final String LABEL_CAPTION_SERVICE_PROVIDER_MOBILE_NUMBER = "label_caption_service_provider_mobile_number";
	public static final String SERVICE_PROVIDER_SAVE_FAILED = "service_provider_save_failed";
	public static final String SERVICE_PROVIER_NAME_REQUIRED = "service_provier_name_required";
	public static final String NOTIFICATION_SERVICE_PROVIDER_SAVED_SUCCESSFULLY = "notification_service_provider_saved_successfully";
	public static final String NOTIFICATION_SERVICE_PROVIDER_UPDATED_SUCCESSFULLY = "notification_service_provider_updated_successfully";
	public static final String SERVICE_PROVIDER_LIST_HEADER_NAME = "service_provider_list_header_name";
	public static final String SERVICE_PROVIDER_LIST_HEADER_TYPE = "service_provider_list_header_type";
	public static final String BUTTON_IMPORT_MASTER_DATA = "button_import_master_data";
	public static final String XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE = "xacc_master_data_import_failed_message";
	public static final String LABEL_XACC_MASTER_DATA_IMPORT_BROWSE = "label_xacc_master_data_import_browse";
	public static final String XACC_MASTER_DATA_IMPORT_FAILED_PLEASE_SELECT_FILE = "xacc_master_data_import_failed_please_select_file";
	public static final String XACC_MASTER_DATA_IMPORT_ACCOUNT_SUCCESS = "xacc_master_data_import_account_success";
	public static final String LABEL_XACC_MASTER_DATA_IMPORT_CHOOSE_MASTER = "label_xacc_master_data_import_choose_master";
	public static final String XACC_MASTER_DATA_IMPORT_FAILED_PLEASE_CHOOSE_MASTER_TYPE = "xacc_master_data_import_failed_please_choose_master_type";
	public static final String BUTTON_MOVE_EMPLOYEE = "button_move_employee";
	public static final String EMPLOYEE_MOVE_WINDOW_CAPTION = "employee_move_window_caption";
	public static final String BUTTON_UPDATE_COUNTRY = "button_update_country";
	public static final String BUTTON_EMPLOYEE_MOVE_CLOSE = "button_employee_move_close";
	public static final String BUTTON_SAVE_EDIT = "button_save_edit";
	public static final String NAME_REQUIRED = "name_required";
	public static final String BUTTON_RESET = "button_reset";
	public static final String BUTTON_ADD_NEW = "button_add_new";
	public static final String TEXTFIELD_INPUT_PROMPT_SEARCH = "textfield_input_prompt_search";
	public static final String NOTIFICATION_UPDATED_SUCCESSFULLY = "notification_updated_successfully";
	public static final String USER_PERMISSION_DENIED = "user_permission_denied";
	public static final String USER_EDIT_PERMISSION_DENIED = "user_edit_permission_denied";
	public static final String NOTIFICATION_SAVED_SUCCESSFULLY = "notification_saved_successfully";
	public static final String NOTIFICATION_DUPILCATE_VALUE_NAME = "notification_dupilcate_value_name";
	public static final String NOTIFICATION_ALREADY_EXISTS = "notification_already_exists";
	public static final String SAVE_FAILED = "save_failed";
	public static final String PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT = "please_ensure_provided_information_are_correct";
	public static final String HOME_MENU_MASTERS = "home_menu_masters";

}
