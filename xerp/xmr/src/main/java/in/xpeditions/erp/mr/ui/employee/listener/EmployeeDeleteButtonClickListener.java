/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.acc.resources.i18n.I18NAccResourceBundle;
import in.xpeditions.erp.acc.resources.i18n.XAccMessageConstants;
import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public class EmployeeDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -518236348814363646L;
	private EmployeeDataChangeObserver countryDataChangeObserver;
	private Employee employee;

	public EmployeeDeleteButtonClickListener(EmployeeDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		ResourceBundle bundle = I18NAccResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		String pleaseConfirmTxt = bundle.getString(XAccMessageConstants.COUNTRY_DELETE_CONFIRM_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = bundle.getString(XAccMessageConstants.COUNTRY_DELETE_CONFIRM_MESSAGE);
		String captionYes = bundle.getString(XAccMessageConstants.COUNTRY_DELETE_CONFIRM_BUTTON_CAPTION_YES);
		String captionNo = bundle.getString(XAccMessageConstants.COUNTRY_DELETE_CONFIRM_BUTTON_CAPTION_NO);
		EmployeeDeleteConfirmListener countryDeleteConfirmListener = new EmployeeDeleteConfirmListener(this.employee, this.countryDataChangeObserver);
		ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt, doYouWantToDeleteTxt + this.employee.getName() + "?", captionYes, captionNo, countryDeleteConfirmListener);
		event.getButton().setEnabled(true);
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}
}