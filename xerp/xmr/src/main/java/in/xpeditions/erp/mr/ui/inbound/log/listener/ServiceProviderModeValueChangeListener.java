package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.List;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;

public class ServiceProviderModeValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7698028172114919392L;

	private XERPComboBox modeComboBox;

	private BeanItemContainer<ServiceProvider> spContainer;

	public ServiceProviderModeValueChangeListener(XERPComboBox modeComboBox,
			BeanItemContainer<ServiceProvider> spContainer) {
		this.modeComboBox = modeComboBox;
		this.spContainer = spContainer;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		ModeEnum mode = (ModeEnum) this.modeComboBox.getValue();
		this.spContainer.removeAllItems();
		if (mode != null) {
			try {
				List<ServiceProvider> sps = new ServiceProviderManager().list(TypeEnum.INBOUND, mode);
				this.spContainer.addAll(sps);
			} catch (XERPException e) {
			}
		}
	}

}
