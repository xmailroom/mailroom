/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.ServiceProvider;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088973296473642423L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;
	private ServiceProvider serviceProvider;

	public ServiceProviderResetButtonClickListener(ServiceProviderDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.countryDataChangeObserver.notifySelectionChange(this.serviceProvider);
		event.getButton().setEnabled(true);
	}

	public void setServiceProvider(ServiceProvider serviceProvider) {
		this.serviceProvider = serviceProvider;
	}

}
