/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DepartmentSearchTextBlurListener implements BlurListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4268581586475050596L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private DepartmentSearchTextShortCutListener departmentSearchTextShortCutListener;

	public DepartmentSearchTextBlurListener(DepartmentDataChangeObserver departmentDataChangeObserver,
			DepartmentSearchTextShortCutListener departmentSearchTextShortCutListener) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
		this.departmentSearchTextShortCutListener = departmentSearchTextShortCutListener;
	}

	@Override
	public void blur(BlurEvent event) {
		XERPTextField searchTextComp = (XERPTextField) event.getComponent();
		searchTextComp.removeShortcutListener(departmentSearchTextShortCutListener);
		String searchString = searchTextComp.getValue();
		this.departmentDataChangeObserver.notifySearchTextValueChange(searchString);
	}

}
