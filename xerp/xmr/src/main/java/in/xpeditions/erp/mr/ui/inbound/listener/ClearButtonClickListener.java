package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

public class ClearButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2148588788934401782L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	public ClearButtonClickListener(InboundDataChangeObserver inboundDataChangeObserver) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		this.inboundDataChangeObserver.notifyUpdate(null);
	}

}
