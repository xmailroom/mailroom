/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import in.xpeditions.erp.acc.entity.Country;
import in.xpeditions.erp.acc.entity.District;
import in.xpeditions.erp.acc.ui.district.listener.DistrictDataChangeObserver;
import in.xpeditions.erp.acc.util.XAccFormItemConstants;
import in.xpeditions.erp.commons.component.XERPComboBox;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;

/**
 * @author xpeditions
 *
 */
public class FloorComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5483083136432572711L;
	private DistrictDataChangeObserver districtDataChangeObserver;
	private BeanFieldGroup<District> binder;

	public FloorComboValueChangeListener(DistrictDataChangeObserver districtDataChangeObserver, BeanFieldGroup<District> binder) {
		this.districtDataChangeObserver = districtDataChangeObserver;
		this.binder = binder;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Country country = (Country) event.getProperty().getValue();
		if (country != null) {
			this.districtDataChangeObserver.notifyOnCompanyValueChange(country);
		} else {
			XERPComboBox stateField = ((XERPComboBox) this.binder
					.getField(XAccFormItemConstants.DISTRICT_STATE));
			stateField.setReadOnly(false);
			stateField.removeAllItems();
		}
	}
}