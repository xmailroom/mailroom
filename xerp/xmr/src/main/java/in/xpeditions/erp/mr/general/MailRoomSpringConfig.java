package in.xpeditions.erp.mr.general;

import org.springframework.context.annotation.Bean;

import in.xpeditions.erp.commons.annotations.XERPSpringConfig;
import in.xpeditions.erp.mr.dao.manager.CostCentreDAOManager;
import in.xpeditions.erp.mr.dao.manager.DeliverySheetDAOManager;
import in.xpeditions.erp.mr.dao.manager.DepartmentDAOManager;
import in.xpeditions.erp.mr.dao.manager.DivisionDAOManager;
import in.xpeditions.erp.mr.dao.manager.EmployeeDAOManager;
import in.xpeditions.erp.mr.dao.manager.FloorDAOManager;
import in.xpeditions.erp.mr.dao.manager.InboundDAOManager;
import in.xpeditions.erp.mr.dao.manager.LogEntryDAOManager;
import in.xpeditions.erp.mr.dao.manager.ServiceProviderDAOManager;
import in.xpeditions.erp.mr.hibernate.manager.CostCentreDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.DeliverySheetDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.DepartmentDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.DivisionDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.EmployeeDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.FloorDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.InboundDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.LogEntryDAOManagerHibernate;
import in.xpeditions.erp.mr.hibernate.manager.ServiceProviderDAOManagerHibernate;

@XERPSpringConfig
public class MailRoomSpringConfig {

	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_LOG_ENTRY)
	public LogEntryDAOManager createLogEntryDAOManager() {
		return new LogEntryDAOManagerHibernate();
	}

	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_INBOUND)
	public InboundDAOManager createInboundDAOManager() {
		return new InboundDAOManagerHibernate();
	}

	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_DIVISION)
	public DivisionDAOManager createDivisionDAOManagerHibernate() {
		return new DivisionDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_DEPARTMENT)
	public DepartmentDAOManager createDepartmentDAOManagerHibernate() {
		return new DepartmentDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR)
	public FloorDAOManager createFloorDAOManagerHibernate() {
		return new FloorDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_COSTCENTRE)
	public CostCentreDAOManager createCostCentreDAOManagerHibernate() {
		return new CostCentreDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE)
	public EmployeeDAOManager createEmployeeDAOManagerHibernate() {
		return new EmployeeDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_SERVICE_PROVIDER)
	public ServiceProviderDAOManager createServiceProviderDAOManagerHibernate() {
		return new ServiceProviderDAOManagerHibernate();
	}
	
	@Bean(name = MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET)
	public DeliverySheetDAOManager createDeliverySheetDAOManager() {
		return new DeliverySheetDAOManagerHibernate();
	}

}
