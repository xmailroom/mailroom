/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author xpeditions
 *
 */
public class FloorDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1632562329617975130L;
	
	private List<FloorDataChangeListener> floorDataChangeListeners;
	
	public FloorDataChangeObserver() {
		this.floorDataChangeListeners = new ArrayList<FloorDataChangeListener>();
	}
	
	public void add(FloorDataChangeListener floorDataChangeListener) {
		this.floorDataChangeListeners.add(floorDataChangeListener);
	}
	
	public void remove(FloorDataChangeListener floorDataChangeListener) {
		this.floorDataChangeListeners.remove(floorDataChangeListener);
	}

	public void notifyUpdate(Floor floor) {
		for (FloorDataChangeListener floorDataChangeListener : this.floorDataChangeListeners) {
			floorDataChangeListener.onUpdate(floor);
		}
	}

	public void notifySelectionChange(Floor floor) {
		for (FloorDataChangeListener floorDataChangeListener : this.floorDataChangeListeners) {
			floorDataChangeListener.onSelectionChange(floor);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (FloorDataChangeListener floorDataChangeListener : this.floorDataChangeListeners) {
			floorDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifyEditButtonClick() {
		for (FloorDataChangeListener floorDataChangeListener : this.floorDataChangeListeners) {
			floorDataChangeListener.onEditButtonClick();
		}
	}
}