/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import com.vaadin.data.Item;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.manager.DivisionManager;

/**
 * @author Saran
 *
 */
public class DivisionListQuery implements Query, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6084607578905174731L;
	private String searchString;
	private QueryDefinition queryDefinition;

	public DivisionListQuery(String searchString, QueryDefinition queryDefinition) {
		this.searchString = searchString;
		this.queryDefinition = queryDefinition;
	}

	@Override
	public Item constructItem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAllItems() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Item> loadItems(int startIndex, int count) {
		DivisionManager divisionManager = new DivisionManager();
		try {
			Object[] sortProperties = this.queryDefinition.getSortPropertyIds();
			boolean[] sortPropertyAscendingStates = this.queryDefinition.getSortPropertyAscendingStates();
			List<Division> divisions = divisionManager.listDivisions(startIndex, count, sortProperties, sortPropertyAscendingStates, this.searchString);
			List<Item> items = new ArrayList<Item>(divisions.size());
			for (Division division : divisions) {
				items.add(new NestingBeanItem<Division>(division, this.queryDefinition.getMaxNestedPropertyDepth(), this.queryDefinition.getPropertyIds()));
			}
			return items;	
		}
			catch (XERPException e) {
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public void saveItems(List<Item> arg0, List<Item> arg1, List<Item> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int size() {
		DivisionManager divisionManager = new DivisionManager();
		try {
			Long countOf = divisionManager.getDivisionCount(searchString);
			return countOf.intValue();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
