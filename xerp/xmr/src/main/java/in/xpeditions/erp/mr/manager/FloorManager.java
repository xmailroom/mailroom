/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.FloorDAOManager;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class FloorManager {

	public List<Floor> listBy(Department department) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.listBy(department);
	}

	public List<Floor> listAll() throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.listAll();
	}

	public List<Floor> listFloors(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.listFloors(startIndex, count, sortProperties, sortPropertyAscendingStates, searchString);
	}

	public Long getFloorCount(String searchString) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.getFloorCount(searchString);
	}

	public void update(Floor floor) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		floorDAOManager.update(floor);
	}

	public void save(Floor floor) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		floorDAOManager.save(floor);
	}

	public void delete(Floor floor) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		floorDAOManager.delete(floor);
	}

	public Floor getByEmployee(Employee employee) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.getByEmployee(employee);
	}

	public Floor getByName(String floorName) throws XERPException {
		FloorDAOManager floorDAOManager = (FloorDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_FLOOR);
		return floorDAOManager.getByName(floorName);
	}

}
