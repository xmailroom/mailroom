package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Table;

import in.xpeditions.erp.mr.ui.inbound.InboundEditContainer;

public class AddToButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8471673228519619894L;

	private InboundEditContainer inboundEditContainer;

	private Table toTable;

	public AddToButtonClickListener(InboundEditContainer inboundEditContainer, Table toTable) {
		this.inboundEditContainer = inboundEditContainer;
		this.toTable = toTable;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		this.inboundEditContainer.addToItem(this.toTable.size() + 1, null, 1);
	}

}
