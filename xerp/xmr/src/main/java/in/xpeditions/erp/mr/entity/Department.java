/**
 * 
 */
package in.xpeditions.erp.mr.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPPrimaryEntity;

/**
 * @author Saran
 *
 */
@XERPPrimaryEntity
@Entity
@Table (name = Department.TABLE_NAME)
public class Department implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2298738201432281418L;
	
	public static final String TABLE_NAME = "mr_department";
	
	private static final String COLUMN_ID = "id";

	private static final String COLUMN_CODE = "code";

	public static final String COLUMN_NAME = "name";
	
	public static final String COLUMN_DIVISION = "division";

	private static final String JOIN_TABLE_DEPARTMENT_FLOOR = "mr_department_floor";

	private static final String JOIN_COLUMN_DEPARTMENT_ID = "departmentId";

	private static final String INVERSE_JOIN_COLUMN_FLOOR_ID = "floorId";
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Department.COLUMN_ID, unique = true, nullable = false)
	private long id;
	
	@Column (name = Department.COLUMN_CODE, unique = true)
	private String code;
	
	@Column (name = Department.COLUMN_NAME, unique = true, nullable = false)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = Department.COLUMN_DIVISION)
	private Division division;
	
	@ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	@JoinTable(name = Department.JOIN_TABLE_DEPARTMENT_FLOOR, joinColumns = { @JoinColumn(name = Department.JOIN_COLUMN_DEPARTMENT_ID) }, inverseJoinColumns = { @JoinColumn(name = Department.INVERSE_JOIN_COLUMN_FLOOR_ID) })
	private List<Floor> floors;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the code
	 */
	public String getCode() {
		return code;
	}

	/**
	 * @param code the code to set
	 */
	public void setCode(String code) {
		this.code = code;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the division
	 */
	public Division getDivision() {
		return division;
	}

	/**
	 * @param division the division to set
	 */
	public void setDivision(Division division) {
		this.division = division;
	}

	/**
	 * @return the floors
	 */
	public List<Floor> getFloors() {
		return floors;
	}

	/**
	 * @param floors the floors to set
	 */
	public void setFloors(List<Floor> floors) {
		this.floors = floors;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Department other = (Department) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
