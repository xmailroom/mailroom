package in.xpeditions.erp.mr.ui.inbound.log.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPComboBox;

public class DataEntryFoucsShortcutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2139512120099008669L;

	private XERPComboBox modeComboBox;

	public DataEntryFoucsShortcutListener(String caption, int keyCode, int[] modifierKeys, XERPComboBox modeComboBox) {
		super(caption, keyCode, modifierKeys);
		this.modeComboBox = modeComboBox;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.modeComboBox.focus();
	}

}
