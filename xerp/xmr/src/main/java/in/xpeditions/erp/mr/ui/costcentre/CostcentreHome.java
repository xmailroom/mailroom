/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreDataChangeObserver;

/**
 * @author Saran
 *
 */
public class CostcentreHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1760707200799244065L;
	
	public CostcentreHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        CostcentreDataChangeObserver costcentreDataChangeObserver = new CostcentreDataChangeObserver();
        CostcentreListContainer costcentreListContainer = new CostcentreListContainer(costcentreDataChangeObserver);
        containerSplitter.setFirstComponent(costcentreListContainer);
        
        CostcentreEditContainer costcentreEditContainer = new CostcentreEditContainer(costcentreDataChangeObserver);
        containerSplitter.setSecondComponent(costcentreEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
