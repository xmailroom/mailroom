/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.manager.DivisionManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DivisionDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3051913408097550696L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private Division division;

	public DivisionDeleteConfirmListener(Division division, DivisionDataChangeObserver divisionDataChangeObserver) {
		this.division = division;
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void onClose(ConfirmDialog confirmDialog) {
		if (confirmDialog.isConfirmed()) {
			ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
			DivisionManager divisionManager = new DivisionManager();
			try {
				divisionManager.delete(division);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_DELETED_SUCCESSFULLY));
				this.divisionDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_FOREIGN_KEY_VALUE) + " '" + division.getName() + "' "+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_HAS_REFERENCE);
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_DELETE_FAILED), "<br/>" + subMessage , Notification.Type.ERROR_MESSAGE, true).show(page);
			} catch (XERPException e) {
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DIVISION_DELETE_FAILED), "<br/>" + e.getMessage() , Notification.Type.ERROR_MESSAGE, true).show(page);
			}
		}
	}

}
