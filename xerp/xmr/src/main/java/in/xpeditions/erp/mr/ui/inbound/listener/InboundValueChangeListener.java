package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;

public class InboundValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7841583065065943566L;

	private XERPComboBox inBatchNoComboBox;

	private XERPComboBox modeComboBox;

	private XERPComboBox spComboBox;

	public InboundValueChangeListener(XERPComboBox inBatchNoComboBox, XERPComboBox modeComboBox,
			XERPComboBox spComboBox) {
		this.inBatchNoComboBox = inBatchNoComboBox;
		this.modeComboBox = modeComboBox;
		this.spComboBox = spComboBox;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		System.out.println("Log entry change listener");
		LogEntry logEntry = (LogEntry) this.inBatchNoComboBox.getValue();
		if (logEntry == null || logEntry.getId() == 0) {
			this.modeComboBox.setEnabled(true);
			this.spComboBox.setEnabled(true);
		} else {
			ServiceProviderManager spManager = new ServiceProviderManager();
			ServiceProvider sp = null;
			try {
				sp = spManager.getBy(logEntry.getServiceProviderId());
			} catch (XERPException e) {
				e.printStackTrace();
			}
			this.modeComboBox.setValue(logEntry.getMode());
			this.spComboBox.setValue(sp);
			this.modeComboBox.setEnabled(false);
			this.spComboBox.setEnabled(false);
		}
	}

}
