package in.xpeditions.erp.mr.ui.inbound.listener;

import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;

public interface InboundDataChangeListener {

	void onUpdate(Inbound inbound);

	void onSelectionChange(Inbound inbound);

	void onEmployeeChange(Employee employee);

	void onLogEntryChange(LogEntry logEntry);

}
