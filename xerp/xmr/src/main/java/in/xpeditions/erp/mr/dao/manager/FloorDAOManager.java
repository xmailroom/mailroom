/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author Saran
 *
 */
public interface FloorDAOManager {

	public List<Floor> listBy(Department department) throws XERPException;

	public List<Floor> listAll() throws XERPException;

	public List<Floor> listFloors(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public Long getFloorCount(String searchString) throws XERPException;

	public void update(Floor floor) throws XERPException;

	public void save(Floor floor) throws XERPException;

	public void delete(Floor floor) throws XERPException;

	public Floor getByEmployee(Employee employee) throws XERPException;

	public Floor getByName(String floorName) throws XERPException;

}
