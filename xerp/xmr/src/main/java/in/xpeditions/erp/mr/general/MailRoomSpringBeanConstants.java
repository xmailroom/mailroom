package in.xpeditions.erp.mr.general;

public class MailRoomSpringBeanConstants {

	public static final String DAO_MANAGER_LOG_ENTRY = "logEntryDAOManager";

	public static final String DAO_MANAGER_INBOUND = "inboundDAOManager";

	public static final String DAO_MANAGER_DIVISION = "divisionDAOManager";

	public static final String DAO_MANAGER_DEPARTMENT = "departmentDAOManager";

	public static final String DAO_MANAGER_FLOOR = "floorDAOManager";

	public static final String DAO_MANAGER_COSTCENTRE = "costCentreDAOManager";

	public static final String DAO_MANAGER_EMPLOYEE = "employeeDAOManager";

	public static final String DAO_MANAGER_SERVICE_PROVIDER = "serviceProviderDAOManager";
	
	public static final String DAO_MANAGER_DELIVERY_SHEET = "deliverySheetDAOManager";
}
