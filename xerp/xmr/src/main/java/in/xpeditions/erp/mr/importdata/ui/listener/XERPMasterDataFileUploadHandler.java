/**
 * 
 */
package in.xpeditions.erp.mr.importdata.ui.listener;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.NativeButton;
import com.vaadin.ui.Notification;
import com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler;

import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class XERPMasterDataFileUploadHandler implements UploadFinishedHandler {

	private XAccMasterDataImportButtonClickListener importButtonClickListener;
	private NativeButton importButton;

	public XERPMasterDataFileUploadHandler(XAccMasterDataImportButtonClickListener importButtonClickListener,
			NativeButton importButton) {
		this.importButtonClickListener = importButtonClickListener;
		this.importButton = importButton;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.wcs.wcslib.vaadin.widget.multifileupload.ui.UploadFinishedHandler#
	 * handleFile(java.io.InputStream, java.lang.String, java.lang.String, long)
	 */
	@Override
	public void handleFile(InputStream stream, String fileName,
			String mimeType, long arg3) {
		try {
			String seperator = System.getProperty("file.separator");
			File file = new File(DataHolder.tempFolder + seperator + System.currentTimeMillis()
					+ fileName.substring(fileName.lastIndexOf(".")));
			XERPUtil.writeToFile(stream, file);
			this.importButton.setVisible(file != null);
			importButtonClickListener.setImportFile(file);
		} catch (IOException e) {
			ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
			String message = bundle.getString(MailRoomMessageConstants.XACC_MASTER_DATA_IMPORT_FAILED_MESSAGE);
			Notification.show(message, "\n" + e.getMessage(), Notification.Type.ERROR_MESSAGE);
		}
	}

}
