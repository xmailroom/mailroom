/**
 * 
 */
package in.xpeditions.erp.mr.entity.util;

/**
 * @author Saran
 *
 */
public enum TypeEnum {
	
	INBOUND("Inbound"), OUTBOUND("Outbound");  
	  
	private String name;

	private TypeEnum(String name) {
		this.setName(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static TypeEnum get(int i) {
		return TypeEnum.values()[i];
	}

}
