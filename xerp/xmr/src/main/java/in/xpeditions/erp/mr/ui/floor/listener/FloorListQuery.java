/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import com.vaadin.data.Item;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.FloorManager;

/**
 * @author xpeditions
 *
 */
public class FloorListQuery implements Query, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 976287006875128175L;
	private String searchString;
	private QueryDefinition queryDefinition;

	public FloorListQuery(String searchString, QueryDefinition queryDefinition) {
		this.searchString = searchString;
		this.queryDefinition = queryDefinition;
	}

	@Override
	public Item constructItem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAllItems() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Item> loadItems(int startIndex, int count) {
		FloorManager floorManager = new FloorManager();
		try {
			Object[] sortProperties = this.queryDefinition.getSortPropertyIds();
			boolean[] sortPropertyAscendingStates = this.queryDefinition.getSortPropertyAscendingStates();
			List<Floor> floors = floorManager.listFloors(startIndex, count, sortProperties, sortPropertyAscendingStates, this.searchString);
			List<Item> items = new ArrayList<Item>(floors.size());
			for (Floor floor : floors) {
				items.add(new NestingBeanItem<Floor>(floor, this.queryDefinition.getMaxNestedPropertyDepth(), this.queryDefinition.getPropertyIds()));
			}
			return items;	
		}
			catch (XERPException e) {
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public void saveItems(List<Item> arg0, List<Item> arg1, List<Item> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int size() {
		FloorManager floorManager = new FloorManager();
		try {
			Long countOf = floorManager.getFloorCount(searchString);
			return countOf.intValue();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		return 0;
	}
}