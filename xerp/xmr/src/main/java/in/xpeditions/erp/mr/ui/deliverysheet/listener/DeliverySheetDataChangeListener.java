package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.mr.entity.DeliverySheet;

public interface DeliverySheetDataChangeListener {

	void onUpdate(DeliverySheet deliverySheet);

	void onEditButtonClick(DeliverySheet deliverySheet);

	void onSelectionChange(DeliverySheet deliverySheet);

	void onConfirm(BeanFieldGroup<DeliverySheet> deliverySheetBinder);

}
