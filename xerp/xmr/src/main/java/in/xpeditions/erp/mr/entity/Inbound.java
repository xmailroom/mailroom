package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = Inbound.TABLE_NAME)
public class Inbound implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2282471757945320499L;

	public static final String TABLE_NAME = "xmr_inbound";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_AWB_NO = "awbNo";

	public static final String COLUMN_BULK = "bulk";

	public static final String COLUMN_LOG_ENTRY = "logEntry";

	public static final String COLUMN_DATE = "date";

	public static final String COLUMN_MODE = "mode";

	public static final String COLUMN_TYPE = "sType";

	public static final String COLUMN_SERVICE_PROVIDER = "serviceProviderId";

	public static final String COLUMN_REMARKS = "remarks";

	public static final String COLUMN_FROM = "xFrom";

	public static final String COLUMN_TO = "xTo";

	public static final String COLUMN_NO_OF_DOCS = "noOfDocs";

	public static final String COLUMN_CREATED_TIME = "createdTime";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = Inbound.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@Column(name = Inbound.COLUMN_AWB_NO, nullable = false)
	private String awbNo;

	@Column(name = Inbound.COLUMN_BULK, nullable = false, columnDefinition = "boolean default false")
	private boolean bulk;

	@ManyToOne
	@JoinColumn(name = Inbound.COLUMN_LOG_ENTRY, nullable = false)
	private LogEntry logEntry;

	@Column(name = Inbound.COLUMN_DATE, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long date;

	@Column(name = Inbound.COLUMN_MODE, nullable = false)
	private ModeEnum mode;

	@Column(name = Inbound.COLUMN_TYPE, nullable = false)
	private TypeEnum sType;

	@Column(name = Inbound.COLUMN_SERVICE_PROVIDER, nullable = false)
	private long serviceProviderId;

	@Column(name = Inbound.COLUMN_REMARKS, columnDefinition = "TEXT DEFAULT NULL")
	private String remarks;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = Inbound.COLUMN_FROM, nullable = false)
	private From xFrom;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = Inbound.COLUMN_TO, nullable = false)
	private To xTo;

	@Column(name = Inbound.COLUMN_NO_OF_DOCS, nullable = false, columnDefinition = "INT DEFAULT 1")
	private int noOfDocs;

	@Column(name = Inbound.COLUMN_CREATED_TIME, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long createdTime;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the awbNo
	 */
	public String getAwbNo() {
		return awbNo;
	}

	/**
	 * @param awbNo
	 *            the awbNo to set
	 */
	public void setAwbNo(String awbNo) {
		this.awbNo = awbNo;
	}

	/**
	 * @return the bulk
	 */
	public boolean isBulk() {
		return bulk;
	}

	/**
	 * @param bulk
	 *            the bulk to set
	 */
	public void setBulk(boolean bulk) {
		this.bulk = bulk;
	}

	/**
	 * @return the logEntry
	 */
	public LogEntry getLogEntry() {
		return logEntry;
	}

	/**
	 * @param logEntry
	 *            the logEntry to set
	 */
	public void setLogEntry(LogEntry logEntry) {
		this.logEntry = logEntry;
	}

	/**
	 * @return the date
	 */
	public long getDate() {
		return date;
	}

	/**
	 * @param date
	 *            the date to set
	 */
	public void setDate(long date) {
		this.date = date;
	}

	/**
	 * @return the mode
	 */
	public ModeEnum getMode() {
		return mode;
	}

	/**
	 * @param mode
	 *            the mode to set
	 */
	public void setMode(ModeEnum mode) {
		this.mode = mode;
	}

	/**
	 * @return the sType
	 */
	public TypeEnum getsType() {
		return sType;
	}

	/**
	 * @param sType
	 *            the sType to set
	 */
	public void setsType(TypeEnum sType) {
		this.sType = sType;
	}

	/**
	 * @return the serviceProviderId
	 */
	public long getServiceProviderId() {
		return serviceProviderId;
	}

	/**
	 * @param serviceProviderId
	 *            the serviceProviderId to set
	 */
	public void setServiceProviderId(long serviceProviderId) {
		this.serviceProviderId = serviceProviderId;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/**
	 * @return the xFrom
	 */
	public From getxFrom() {
		return xFrom;
	}

	/**
	 * @param xFrom
	 *            the xFrom to set
	 */
	public void setxFrom(From xFrom) {
		this.xFrom = xFrom;
	}

	/**
	 * @return the xTo
	 */
	public To getxTo() {
		return xTo;
	}

	/**
	 * @param xTo
	 *            the xTo to set
	 */
	public void setxTo(To xTo) {
		this.xTo = xTo;
	}

	/**
	 * @return the noOfDocs
	 */
	public int getNoOfDocs() {
		return noOfDocs;
	}

	/**
	 * @param noOfDocs
	 *            the noOfDocs to set
	 */
	public void setNoOfDocs(int noOfDocs) {
		this.noOfDocs = noOfDocs;
	}

	/**
	 * @return the createdTime
	 */
	public long getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime
	 *            the createdTime to set
	 */
	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Inbound other = (Inbound) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
