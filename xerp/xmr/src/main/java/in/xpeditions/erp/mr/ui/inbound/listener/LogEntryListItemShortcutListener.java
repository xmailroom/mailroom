package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.LogEntrySelectionWindow;

public class LogEntryListItemShortcutListener extends ShortcutListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4589767299046014332L;

	private LogEntry logEntry;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private LogEntrySelectionWindow logEntrySelectionWindow;

	public LogEntryListItemShortcutListener(String caption, int keyCode, int[] modifierKeys,
			InboundDataChangeObserver inboundDataChangeObserver, LogEntrySelectionWindow logEntrySelectionWindow) {
		super(caption, keyCode, modifierKeys);
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.logEntrySelectionWindow = logEntrySelectionWindow;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.inboundDataChangeObserver.notifyLogEntryChange(this.logEntry);
		this.logEntrySelectionWindow.close();
	}

	public void setFocusedItem(LogEntry logEntry) {
		this.logEntry = logEntry;

	}

	@Override
	public void itemClick(ItemClickEvent event) {
		if (event.isDoubleClick()) {
			this.inboundDataChangeObserver.notifyLogEntryChange(this.logEntry);
			this.logEntrySelectionWindow.close();
		}
	}

}
