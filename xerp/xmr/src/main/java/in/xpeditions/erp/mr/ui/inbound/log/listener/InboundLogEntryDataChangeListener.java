package in.xpeditions.erp.mr.ui.inbound.log.listener;

import in.xpeditions.erp.mr.entity.LogEntry;

public interface InboundLogEntryDataChangeListener {

	void onUpdate(LogEntry logEntry);

	void onSelectionChange(LogEntry logEntry);

}
