package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.To;
import in.xpeditions.erp.mr.ui.inbound.InboundEditContainer;

public class RemoveToButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1340808271925404551L;

	private InboundEditContainer inboundEditContainer;

	private BeanFieldGroup<To> toBinder;

	public RemoveToButtonClickListener(InboundEditContainer inboundEditContainer, BeanFieldGroup<To> toBinder) {
		this.inboundEditContainer = inboundEditContainer;
		this.toBinder = toBinder;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		this.inboundEditContainer.removeToItem(this.toBinder);
	}

}
