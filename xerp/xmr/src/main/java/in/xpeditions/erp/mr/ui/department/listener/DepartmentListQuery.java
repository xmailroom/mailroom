/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import com.vaadin.data.Item;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.manager.DepartmentManager;

/**
 * @author Saran
 *
 */
public class DepartmentListQuery implements Query, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1198606121321835980L;
	private String searchString;
	private QueryDefinition queryDefinition;

	public DepartmentListQuery(String searchString, QueryDefinition queryDefinition) {
		this.searchString = searchString;
		this.queryDefinition = queryDefinition;
	}

	@Override
	public Item constructItem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAllItems() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Item> loadItems(int startIndex, int count) {
		DepartmentManager departmentManager = new DepartmentManager();
		try {
			Object[] sortProperties = this.queryDefinition.getSortPropertyIds();
			boolean[] sortPropertyAscendingStates = this.queryDefinition.getSortPropertyAscendingStates();
			List<Department> departments = departmentManager.listDepartments(startIndex, count, sortProperties, sortPropertyAscendingStates, this.searchString);
			List<Item> items = new ArrayList<Item>(departments.size());
			for (Department department : departments) {
				items.add(new NestingBeanItem<Department>(department, this.queryDefinition.getMaxNestedPropertyDepth(), this.queryDefinition.getPropertyIds()));
			}
			return items;	
		}
			catch (XERPException e) {
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public void saveItems(List<Item> arg0, List<Item> arg1, List<Item> arg2) {
		// TODO Auto-generated method stub

	}

	@Override
	public int size() {
		DepartmentManager departmentManager = new DepartmentManager();
		try {
			Long countOf = departmentManager.getDepartmentCount(searchString);
			return countOf.intValue();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		return 0;
	}

}
