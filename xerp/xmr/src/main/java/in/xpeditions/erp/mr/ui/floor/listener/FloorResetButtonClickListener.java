/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.Floor;

/**
 * @author xpeditions
 *
 */
public class FloorResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088973296473642423L;
	private FloorDataChangeObserver floorDataChangeObserver;
	private Floor floor;

	public FloorResetButtonClickListener(FloorDataChangeObserver floorDataChangeObserver) {
		this.floorDataChangeObserver = floorDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.floorDataChangeObserver.notifySelectionChange(this.floor);
		event.getButton().setEnabled(true);
	}

	public void setFloor(Floor floor) {
		this.floor = floor;
	}

}
