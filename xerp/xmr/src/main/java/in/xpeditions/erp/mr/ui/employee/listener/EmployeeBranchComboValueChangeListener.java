/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author Saran
 *
 */
public class EmployeeBranchComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7989181729063655242L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private BeanFieldGroup<Employee> binder;

	public EmployeeBranchComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver, BeanFieldGroup<Employee> binder) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.binder = binder;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox branchCombo = (XERPComboBox) event.getProperty();
		Branch branch = (Branch) branchCombo.getValue();
		if (branch != null) {
			this.employeeDataChangeObserver.notifyOnBranchChange(branch);
		} else {
			XERPComboBox divisionCombo = (XERPComboBox) this.binder.getField(XMailroomFormItemConstants.EMPLOYEE_DIVISION);
			divisionCombo.setReadOnly(false);
			divisionCombo.removeAllItems();
		}
	}

}
