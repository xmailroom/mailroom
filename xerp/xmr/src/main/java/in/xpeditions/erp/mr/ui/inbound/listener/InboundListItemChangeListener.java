package in.xpeditions.erp.mr.ui.inbound.listener;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.manager.InboundManager;

public class InboundListItemChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7277201029797374092L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private InboundListItemShortcutListener inboundListItemShortcutListener;

	private InboundListItemClickListener inboundListItemClickListener;

	private FilterTable table;

	public InboundListItemChangeListener(InboundDataChangeObserver inboundDataChangeObserver,
			InboundListItemClickListener inboundListItemClickListener,
			InboundListItemShortcutListener inboundListItemShortcutListener, FilterTable table) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.inboundListItemClickListener = inboundListItemClickListener;
		this.inboundListItemShortcutListener = inboundListItemShortcutListener;
		this.table = table;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Inbound inbound = (Inbound) event.getProperty().getValue();
		if (this.table.isSelected(inbound)) {
			try {
				inbound = new InboundManager().getBy(inbound);
			} catch (XERPException e) {
				e.printStackTrace();
			}
			this.inboundListItemClickListener.setFocusedItem(inbound);
			this.inboundDataChangeObserver.notifySelectionChange(inbound);
			this.inboundListItemShortcutListener.setFocusedItem(inbound);
		}
	}

}
