/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author Saran
 *
 */
public class EmployeeDepartmentComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3442741891595940677L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private BeanFieldGroup<Employee> binder;

	public EmployeeDepartmentComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			BeanFieldGroup<Employee> binder) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.binder = binder;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.
	 * Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox departmentCombo = (XERPComboBox) event.getProperty();
		Department department = (Department) departmentCombo.getValue();
		if(department != null) {
			this.employeeDataChangeObserver.notiyDepartmentChange(department);
		} else {
			XERPComboBox costCentreCombo = (XERPComboBox) this.binder.getField(XMailroomFormItemConstants.EMPLOYEE_COSTCENTRE);
			costCentreCombo.setReadOnly(false);
			costCentreCombo.removeAllItems();
		}
	}

}
