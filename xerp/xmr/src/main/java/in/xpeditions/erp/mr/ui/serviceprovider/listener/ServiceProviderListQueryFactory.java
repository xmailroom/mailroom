/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -778041010293651816L;
	private String searchString;

	public ServiceProviderListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		ServiceProviderListQuery countryListQuery = new ServiceProviderListQuery(searchString, queryDefinition);
		return countryListQuery;
	}
}