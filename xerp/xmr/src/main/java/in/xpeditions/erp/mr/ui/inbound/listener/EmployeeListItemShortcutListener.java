package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.ui.inbound.EmployeeSelectionWindow;

public class EmployeeListItemShortcutListener extends ShortcutListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4589767299046014332L;

	private Employee employee;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private EmployeeSelectionWindow employeeSelectionWindow;

	public EmployeeListItemShortcutListener(String caption, int keyCode, int[] modifierKeys,
			InboundDataChangeObserver inboundDataChangeObserver, EmployeeSelectionWindow employeeSelectionWindow) {
		super(caption, keyCode, modifierKeys);
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.employeeSelectionWindow = employeeSelectionWindow;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.inboundDataChangeObserver.notifyEmployeeChange(this.employee);
		this.employeeSelectionWindow.close();
	}

	public void setFocusedItem(Employee employee) {
		this.employee = employee;

	}

	@Override
	public void itemClick(ItemClickEvent event) {
		if (event.isDoubleClick()) {
			this.inboundDataChangeObserver.notifyEmployeeChange(this.employee);
			this.employeeSelectionWindow.close();
		}
	}

}
