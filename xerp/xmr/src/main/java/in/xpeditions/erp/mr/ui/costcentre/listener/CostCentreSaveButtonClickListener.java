/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.manager.CostCentreManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class CostCentreSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5416199381034026990L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;
	private BeanFieldGroup<CostCentre> binder;
	private boolean canEdit;

	public CostCentreSaveButtonClickListener(CostcentreDataChangeObserver costcentreDataChangeObserver,
			BeanFieldGroup<CostCentre> binder, boolean canEdit) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
		this.binder = binder;
		this.canEdit = canEdit;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		CostCentre costCentre = this.binder.getItemDataSource().getBean();
		CostCentreManager costcentreManager = new CostCentreManager();
		if (costCentre.getDepartment() == null || costCentre.getDepartment().getId() == 0) {
			message = bundle.getString(MailRoomMessageConstants.COSTCENTRE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.COSTCENTRE_DEPARTMENT_NAME_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		if (costCentre.getName() == null || costCentre.getName().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.COSTCENTRE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NAME_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		try {
			if (costCentre.getId() > 0) {
				if (this.canEdit) {
					costcentreManager.update(costCentre);
					Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				costcentreManager.save(costCentre);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_SAVED_SUCCESSFULLY));
			}
			this.costcentreDataChangeObserver.notifyUpdate(costCentre);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.COSTCENTRE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DUPILCATE_VALUE_NAME) + " '" + e.getTitle() + "' "+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}  catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} 
		event.getButton().setEnabled(true);
	}
}

