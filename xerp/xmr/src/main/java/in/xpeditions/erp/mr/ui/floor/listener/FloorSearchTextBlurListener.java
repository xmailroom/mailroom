/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;

/**
 * @author xpeditions
 *
 */
public class FloorSearchTextBlurListener implements BlurListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6390075732871681376L;
	private FloorDataChangeObserver countryDataChangeObserver;
	private FloorSearchTextShortCutListener countrySearchTextShortCutListener;

	public FloorSearchTextBlurListener(FloorDataChangeObserver countryDataChangeObserver, FloorSearchTextShortCutListener countrySearchTextShortCutListener) {
		this.countryDataChangeObserver = countryDataChangeObserver;
		this.countrySearchTextShortCutListener = countrySearchTextShortCutListener;
	}

	@Override
	public void blur(BlurEvent event) {
		XERPTextField searchTextComp = (XERPTextField) event.getComponent();
		searchTextComp.removeShortcutListener(countrySearchTextShortCutListener);
		String searchString = searchTextComp.getValue();
		this.countryDataChangeObserver.notifySearchTextValueChange(searchString);
	}

}
