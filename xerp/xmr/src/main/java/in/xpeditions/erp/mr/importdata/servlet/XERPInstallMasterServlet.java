/**
 * 
 */
package in.xpeditions.erp.mr.importdata.servlet;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinServlet;

import in.xpeditions.erp.mr.importdata.ui.XERPInstallMasterUI;

/**
 * @author Saran
 *
 */
@WebServlet(value = { "/master/install/*" }, asyncSupported = true)
@VaadinServletConfiguration(productionMode = false, ui = XERPInstallMasterUI.class, widgetset = "in.xpeditions.erp.home.general.AppWidgetSet", heartbeatInterval = 60, closeIdleSessions = false)
public class XERPInstallMasterServlet extends VaadinServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4068157233103608592L;

}
