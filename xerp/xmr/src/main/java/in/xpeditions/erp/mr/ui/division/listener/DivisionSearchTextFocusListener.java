/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DivisionSearchTextFocusListener implements FocusListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1311655836857523073L;
	private DivisionSearchTextShortCutListener divisionSearchTextShortCutListener;

	public DivisionSearchTextFocusListener(DivisionSearchTextShortCutListener divisionSearchTextShortCutListener) {
		this.divisionSearchTextShortCutListener = divisionSearchTextShortCutListener;
	}

	@Override
	public void focus(FocusEvent event) {
		XERPTextField textField = (XERPTextField) event.getComponent();
		textField.addShortcutListener(divisionSearchTextShortCutListener);
	}

}
