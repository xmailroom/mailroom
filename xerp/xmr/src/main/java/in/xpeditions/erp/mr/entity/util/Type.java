package in.xpeditions.erp.mr.entity.util;

public enum Type {

	I("I"), O("O");

	private String name;

	private Type(String name) {
		this.setName(name);
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	public static Type get(int i) {
		return Type.values()[i];
	}

}
