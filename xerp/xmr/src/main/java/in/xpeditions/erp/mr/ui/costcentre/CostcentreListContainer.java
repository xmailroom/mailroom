/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreDataChangeListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreDataChangeObserver;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreListItemClicskListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.costcentre.listener.CostcentreSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author Saran
 *
 */
public class CostcentreListContainer extends VerticalLayout implements CostcentreDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5554329005624446938L;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	private Table table;
	private String searchString;
	
	public CostcentreListContainer(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
		this.costcentreDataChangeObserver.add(this);
		
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_COSTCENTRE.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_COSTCENTRE.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_COSTCENTRE.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		CostcentreListItemClicskListener costcentreListItemClickListener = new CostcentreListItemClicskListener(this.costcentreDataChangeObserver);
		table.addItemClickListener(costcentreListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void refreshTable(String searchString2) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillCostcentreList(table, searchString);
		}
	}

	private void createSearchSection(ResourceBundle bundle) {
		CostcentreAddNewButtonClickListener costcentreAddNewButtonClickListener = new CostcentreAddNewButtonClickListener(this.costcentreDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_COSTCENTRE.asPermission().getName()};
		XERPPrimaryButton newCostCentreButton = new XERPPrimaryButton(actions);
		newCostCentreButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_ADD_NEW));
		newCostCentreButton.setSizeFull();
		newCostCentreButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newCostCentreButton.addClickListener(costcentreAddNewButtonClickListener);
		this.addComponent(newCostCentreButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		CostcentreSearchTextShortCutListener costcentreSearchTextShortCutListener = new CostcentreSearchTextShortCutListener(prompt, KeyCode.ENTER, this.costcentreDataChangeObserver);
		searchTextField.addShortcutListener(costcentreSearchTextShortCutListener);
		
		CostcentreSearchTextBlurListener costcentreSearchTextBlurListener = new CostcentreSearchTextBlurListener(this.costcentreDataChangeObserver, costcentreSearchTextShortCutListener);
		searchTextField.addBlurListener(costcentreSearchTextBlurListener);
		
		CostcentreSearchTextFocusListener costcentreSearchTextFocusListener = new CostcentreSearchTextFocusListener(costcentreSearchTextShortCutListener);
		searchTextField.addFocusListener(costcentreSearchTextFocusListener);
		this.addComponent(searchTextField);
	}

	@Override
	public void onSelectionChange(CostCentre costCentre) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpdate(CostCentre costCentre) {
		refreshTable(this.searchString);
	}

	@Override
	public void onSearchTextValueChange(String searchString) {
		this.searchString = searchString;
		refreshTable(this.searchString);
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}

}
