/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPCheckBox;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.EmployeeManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class EmployeeUpdateButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5719079464366335723L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private BeanFieldGroup<Employee> binder;
	private boolean canEdit;
	private XERPComboBox branchCombo;
	private XERPComboBox divisionCombo;
	private XERPComboBox departmentCombo;
	private XERPComboBox costCentreCombo;
	private Table moveFloorTable;

	public EmployeeUpdateButtonClickListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			BeanFieldGroup<Employee> binder, boolean canEdit, XERPComboBox branchCombo, XERPComboBox divisionCombo,
			XERPComboBox departmentCombo, XERPComboBox costCentreCombo, Table moveFloorTable) {
				this.employeeDataChangeObserver = employeeDataChangeObserver;
				this.binder = binder;
				this.canEdit = canEdit;
				this.branchCombo = branchCombo;
				this.divisionCombo = divisionCombo;
				this.departmentCombo = departmentCombo;
				this.costCentreCombo = costCentreCombo;
				this.moveFloorTable = moveFloorTable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		EmployeeManager employeeManager = new EmployeeManager();
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		Employee employee = this.binder.getItemDataSource().getBean();
		
		Branch branch = (Branch) this.branchCombo.getValue();
		Division division = (Division) this.divisionCombo.getValue();
		Department department = (Department) this.departmentCombo.getValue();
		CostCentre costCentre = (CostCentre) this.costCentreCombo.getValue();
		
		if (branch == null || branch.getId() == 0) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_BRANCH_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		employee.setBranch(branch);
		if (division == null || division.getId() == 0) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_DIVISION_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		employee.setDivision(division);
		if (department == null || department.getId() == 0) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_DEPARTMENT_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		employee.setDepartment(department);
		if (costCentre == null || costCentre.getId() == 0) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_COSTCENTRE_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		employee.setCostCetre(costCentre);
		if (employee.getEmployeeId() == null || employee.getEmployeeId().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_ID_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		if (employee.getEmailId() == null || employee.getEmailId().isEmpty()) {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.EMPLOYEE_EMAIL_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		Collection<XERPCheckBox> itemIds = (Collection<XERPCheckBox>) this.moveFloorTable.getItemIds();
		
		List<Floor> floors = new ArrayList<Floor>();
		if(itemIds != null && itemIds.size() > 0) {
			for (XERPCheckBox xerpCheckBox : itemIds) {
				Boolean isChecked = xerpCheckBox.getValue();
				if (isChecked) {
					Floor floor = (Floor) xerpCheckBox.getData();
					floors.add(floor);
				}
			}
		} else {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_PLEASE_SELECT_FLOOR);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		if (floors != null && floors.size() > 0) {
			if (floors.size() > 1) {
				message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_PLEASE_SELECT_ANY_ONE_FLOOR);
				Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
				event.getButton().setEnabled(true);
				return;
			} else {
				Floor floor = floors.get(0);
				employee.setFloor(floor);
			}
		} else {
			message = bundle.getString(MailRoomMessageConstants.EMPLOYEE_PLEASE_SELECT_ANY_ONE_FLOOR);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		
		try {
			
			if (employee.getId() > 0) {
				if (canEdit) {
					employeeManager.update(employee);
					Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				employeeManager.save(employee);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_SAVED_SUCCESSFULLY));
			}
			this.employeeDataChangeObserver.notifyUpdate(employee);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DUPILCATE_VALUE_NAME) + " '" + e.getTitle() + "' "+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}  catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} 
		event.getButton().setEnabled(true);
	}

}
