/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee;

import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;

import in.xpeditions.erp.acc.ui.util.XAccUIConstants;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDataChangeObserver;

/**
 * @author xpeditions
 *
 */
public class EmployeeHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8373592946196286721L;
	
	public EmployeeHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		HorizontalSplitPanel containerSplitter = new HorizontalSplitPanel();
        containerSplitter.setSizeFull();
        containerSplitter.setSplitPosition(XAccUIConstants.HORIZONTAL_SPLITTER_SPLIT_POSITION);
        
        EmployeeDataChangeObserver employeeDataChangeObserver = new EmployeeDataChangeObserver();
        
        EmployeeListContainer employeeListContainer = new EmployeeListContainer(employeeDataChangeObserver);
        containerSplitter.setFirstComponent(employeeListContainer);
        
        EmployeeEditContainer employeeEditContainer = new EmployeeEditContainer(employeeDataChangeObserver);
        containerSplitter.setSecondComponent(employeeEditContainer);
        
        this.setCompositionRoot(containerSplitter);
	}

}
