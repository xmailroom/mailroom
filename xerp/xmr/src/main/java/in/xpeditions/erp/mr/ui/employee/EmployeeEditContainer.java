/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.OptionGroup;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.acc.manager.BranchManager;
import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPCheckBox;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPDangerButton;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.XERPWindow;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.entity.util.EmployeeNature;
import in.xpeditions.erp.mr.entity.util.GenderEnum;
import in.xpeditions.erp.mr.manager.CostCentreManager;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.manager.DivisionManager;
import in.xpeditions.erp.mr.manager.FloorManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeBranchComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDataChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDataChangeObserver;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDepartmentComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeDivisionComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeEditButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeMoveBranchComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeMoveCloseButtonCickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeMoveDepartmentButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeMoveDepartmentComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeMoveDivisionComboValueChangeListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeResetButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeSaveButtonClickListener;
import in.xpeditions.erp.mr.ui.employee.listener.EmployeeUpdateButtonClickListener;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author xpeditions
 * 
 */
public class EmployeeEditContainer extends VerticalLayout implements
		EmployeeDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484020088110920562L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private BeanFieldGroup<Employee> binder;
	private Employee employee;
	private XERPFriendlyButton saveButton;
	private EmployeeDeleteButtonClickListener employeeDeleteButtonClickListener;
	private EmployeeResetButtonClickListener employeeResetButtonClickListener;
	private boolean canAdd;
	private boolean canEdit;
	private boolean canView;
	private XERPDangerButton editButton;
	private XERPDangerButton deleteButton;
	private Table floorTable;
	private Floor employeeFloor;
	private XERPPrimaryButton moveButton;
	private EmployeeMoveDepartmentButtonClickListener employeeMoveDepartmentButtonClickListener;
	private XERPWindow moveEmployeeWindow;
	private Table moveFloorTable;

	public EmployeeEditContainer(
			EmployeeDataChangeObserver employeeDataChangeObserver) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.employeeDataChangeObserver.add(this);
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent()
				.getPage()
				.setTitle(
						bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS_EMPLOYEE));
		canAdd = XERPUtil.hasPermission(MailRoomPermissionEnum.ADD_EMPLOYEE
				.asPermission().getName());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_EMPLOYEE
				.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_EMPLOYEE
				.asPermission().getName());
		/* this.setSizeFull(); */
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		this.setMargin(marginInfo);
		createEditContainer(bundle);
	}

	private void createEditContainer(ResourceBundle bundle) {
		this.binder = new BeanFieldGroup<Employee>(Employee.class);
		this.binder.setBuffered(true);

		this.employee = initialize();

		HorizontalLayout editBarLayout = new HorizontalLayout();
		MarginInfo editMarginInfo = new MarginInfo(true, true, false, false);
		editBarLayout.setMargin(editMarginInfo);
		editBarLayout.setVisible(false);
		this.addComponent(editBarLayout);
		this.setComponentAlignment(editBarLayout, Alignment.TOP_RIGHT);

		String editActions[] = {
				MailRoomPermissionEnum.EDIT_EMPLOYEE.asPermission().getName(),
				MailRoomPermissionEnum.DELETE_EMPLOYEE.asPermission()
						.getName() };
		EmployeeEditButtonClickListener countryEditButtonClickListener = new EmployeeEditButtonClickListener(
				this.employeeDataChangeObserver);
		editButton = new XERPDangerButton(editActions);
		createFormButton(editButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE_EDIT),
				editBarLayout, FontAwesome.EDIT,
				countryEditButtonClickListener, true);		
		
		HorizontalLayout containerLayout = new HorizontalLayout();
		containerLayout.setSizeFull();
		containerLayout.setSpacing(true);
		this.addComponent(containerLayout);
		
		FormLayout leftFLayout = new FormLayout();
		containerLayout.addComponent(leftFLayout);
		
		BranchManager branchManager = new BranchManager();
		List<Branch> branches = null;
		try {
			branches = branchManager.listAllBranches();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		BeanItemContainer<Branch> branchContainer = new BeanItemContainer<Branch>(Branch.class);
		branchContainer.addAll(branches);
		
		XERPComboBox branchCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_BRANCH),
				XMailroomFormItemConstants.EMPLOYEE_BRANCH, branchContainer, XMailroomFormItemConstants.EMPLOYEE_BRANCH_NAME,
				null, null, true, bundle.getString(MailRoomMessageConstants.EMPLOYEE_BRANCH_REQUIRED));
		
		ValueChangeListener branchComboValueChangeListener = new EmployeeBranchComboValueChangeListener(this.employeeDataChangeObserver, this.binder);
		branchCombo.addValueChangeListener(branchComboValueChangeListener);
		leftFLayout.addComponent(branchCombo);
		
		BeanItemContainer<Division> divisionContainer = new BeanItemContainer<Division>(Division.class);
		
		XERPComboBox divisionCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABAEL_CAPTION_EMPLOYEE_DIVISION),
				XMailroomFormItemConstants.EMPLOYEE_DIVISION, divisionContainer,
				XMailroomFormItemConstants.EMPLOYEE_DIVISION_NAME, null, null, true,
				bundle.getString(MailRoomMessageConstants.EMPLOYEE_DIVISION_REQUIRED));
		ValueChangeListener divisionComboValueChangeListener = new EmployeeDivisionComboValueChangeListener(this.employeeDataChangeObserver, this.binder);
		divisionCombo.addValueChangeListener(divisionComboValueChangeListener);
		leftFLayout.addComponent(divisionCombo);
		
		BeanItemContainer<Department> departmantContainer = new BeanItemContainer<Department>(Department.class);
		
		XERPComboBox departmentCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_DEPARTMENT),
				XMailroomFormItemConstants.EMPLOYEE_DEPARTMENT, departmantContainer,
				XMailroomFormItemConstants.EMPLOYEE_DEPARTMENT_NAME, null, null, true,
				bundle.getString(MailRoomMessageConstants.EMPLOYEE_DEPARTMENT_REQUIRED));
		ValueChangeListener departmentComboValueChangeListener = new EmployeeDepartmentComboValueChangeListener(this.employeeDataChangeObserver, this.binder);
		departmentCombo.addValueChangeListener(departmentComboValueChangeListener);
		leftFLayout.addComponent(departmentCombo);
		
		
		BeanItemContainer<CostCentre> costCentreContainer = new BeanItemContainer<CostCentre>(CostCentre.class);
		
		XERPComboBox costCentreCombo = createFormCombobox(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_COSTCENTRE),
				XMailroomFormItemConstants.EMPLOYEE_COSTCENTRE, costCentreContainer,
				XMailroomFormItemConstants.EMPLOYEE_COSTCENTRE_NAME, null, null, true,
				bundle.getString(MailRoomMessageConstants.EMPLOYEE_COSTCENTRE_REQUIRED));
		leftFLayout.addComponent(costCentreCombo);
		
		BeanItemContainer<EmployeeNature> natureContainer = new BeanItemContainer<EmployeeNature>(EmployeeNature.class);
		natureContainer.addAll(Arrays.asList(EmployeeNature.values()));
		
		OptionGroup natureOptions = new OptionGroup();
		natureOptions.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		natureOptions.addStyleName("horizontal");
		natureOptions.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_NATURE));
		natureOptions.setContainerDataSource(natureContainer);
		this.binder.bind(natureOptions, XMailroomFormItemConstants.EMMPLOYEE_NATURE);
		leftFLayout.addComponent(natureOptions);
				
		XERPTextField nameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_NAME),
				XMailroomFormItemConstants.EMPLOYEE_NAME,
				Employee.COLUMN_NAME,
				true, bundle.getString(MailRoomMessageConstants.NAME_REQUIRED));
		nameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		nameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		leftFLayout.addComponent(nameField);

		XERPTextField employeeIdField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_ID),
				XMailroomFormItemConstants.EMPLOYEE_ID,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_ID),
				true, bundle.getString(MailRoomMessageConstants.EMPLOYEE_ID_REQUIRED));
		employeeIdField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		employeeIdField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		leftFLayout.addComponent(employeeIdField);
		
		XERPTextField emailField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMAIL),
				XMailroomFormItemConstants.EMPLOYEE_EMAIL,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_ID),
				true, bundle.getString(MailRoomMessageConstants.EMPLOYEE_EMAIL_REQUIRED));
		emailField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		emailField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		leftFLayout.addComponent(emailField);
		
		FormLayout rightFLayout = new FormLayout();
		containerLayout.addComponent(rightFLayout);
		
		XERPTextField extensionField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EXENSION),
				XMailroomFormItemConstants.EMPLOYEE_EXTENSION,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EXENSION),
				false, null);
		extensionField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		extensionField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(extensionField);
		
		XERPTextField cabinField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_CABIN),
				XMailroomFormItemConstants.EMPLOYEE_CABIN,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_CABIN),
				false, null);
		cabinField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		cabinField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(cabinField);
		
		XERPTextField buildingNameField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_BUILDING),
				XMailroomFormItemConstants.EMPLOYEE_BUILDING,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_BUILDING),
				false, null);
		buildingNameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		buildingNameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(buildingNameField);
		
		XERPTextField locationField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_LOCATION),
				XMailroomFormItemConstants.EMPLOYEE_LOCATION,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_LOCATION),
				false, null);
		locationField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		locationField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(locationField);
		
		XERPTextField mobileField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_MOBILE),
				XMailroomFormItemConstants.EMPLOYEE_MOBILE,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_MOBILE),
				false, null);
		mobileField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		mobileField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(mobileField);
		
		BeanItemContainer<GenderEnum> genderContainer = new BeanItemContainer<GenderEnum>(GenderEnum.class);
		genderContainer.addAll(Arrays.asList(GenderEnum.values()));
		
		OptionGroup genderOptions = new OptionGroup();
		genderOptions.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		genderOptions.addStyleName("horizontal");
		genderOptions.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_GENDER));
		genderOptions.setContainerDataSource(genderContainer);
		this.binder.bind(genderOptions, XMailroomFormItemConstants.EMMPLOYEE_GENDER);
		rightFLayout.addComponent(genderOptions);
		
		XERPTextField bandField = createFormTextField(
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_BAND),
				XMailroomFormItemConstants.EMPLOYEE_BAND,
				bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_BAND),
				false, null);
		bandField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		bandField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		rightFLayout.addComponent(bandField);
		
		XERPCheckBox activeCheckBox = new XERPCheckBox();
		activeCheckBox.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_ACTIVE));
		activeCheckBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.binder.bind(activeCheckBox, XMailroomFormItemConstants.EMPLOYEE_ACTIVE);
		rightFLayout.addComponent(activeCheckBox);
		
		HorizontalLayout tableLayout = new HorizontalLayout();
		this.addComponent(tableLayout);
		floorTable = new Table();
		tableLayout.addComponent(floorTable);
		floorTable.setWidth("250px");
		floorTable.setHeight("250px");
		
		floorTable.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, XERPCheckBox.class, null);
		floorTable.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_FLOOR_NAME, String.class, "");
		
		floorTable.setColumnHeader(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, bundle.getString(MailRoomMessageConstants.LIST_HEADER_EMPLOYEE_FLOOR_SELECT));
		floorTable.setColumnHeader(XMailroomTableItemConstants.EMPLOYEE_FLOOR_NAME, bundle.getString(MailRoomMessageConstants.LIST_HEADER_EMPLOYEE_FLOOR_NAME));
		
		floorTable.setColumnWidth(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, 50);

		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		/* buttonBarLayoutContainer.setWidth("100%"); */
		this.addComponent(buttonBarLayoutContainer);
		this.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(false);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		EmployeeSaveButtonClickListener countrySaveButtonClickListener = new EmployeeSaveButtonClickListener(
				this.employeeDataChangeObserver, this.binder, this.canEdit, this.floorTable);
		String saveActions[] = {
				MailRoomPermissionEnum.ADD_EMPLOYEE.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_EMPLOYEE.asPermission().getName() };
		this.saveButton = new XERPFriendlyButton(saveActions);
		createFormButton(saveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE),
				buttonBarLayout, FontAwesome.SAVE,
				countrySaveButtonClickListener, true);
		this.saveButton.setClickShortcut(KeyCode.ENTER, null);
		
		this.employeeDeleteButtonClickListener = new EmployeeDeleteButtonClickListener(
				this.employeeDataChangeObserver);
		String deleteActions[] = { MailRoomPermissionEnum.DELETE_EMPLOYEE
				.asPermission().getName() };
		deleteButton = new XERPDangerButton(deleteActions);
		createFormButton(deleteButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_DELETE),
				buttonBarLayout, FontAwesome.TRASH_O,
				employeeDeleteButtonClickListener, true);
		
		this.employeeMoveDepartmentButtonClickListener = new EmployeeMoveDepartmentButtonClickListener(this.employeeDataChangeObserver);
		String moveActions[] = { MailRoomPermissionEnum.MOVE_EMPLOYEE
				.asPermission().getName() };
		this.moveButton = new XERPPrimaryButton(moveActions);
		
		createFormButton(moveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_MOVE_EMPLOYEE),
				buttonBarLayout, FontAwesome.TRASH_O,
				employeeMoveDepartmentButtonClickListener, true);
		
		
		this.employeeResetButtonClickListener = new EmployeeResetButtonClickListener(
				this.employeeDataChangeObserver);
		String resetActions[] = {
				MailRoomPermissionEnum.ADD_EMPLOYEE.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_EMPLOYEE.asPermission().getName() };
		XERPPrimaryButton resetButton = new XERPPrimaryButton(resetActions);
		createFormButton(resetButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_RESET),
				buttonBarLayout, FontAwesome.UNDO,
				employeeResetButtonClickListener, true);

		handleSelectionChange(this.employee);
	}

	private void handleSelectionChange(Employee employee) {
		if (employee == null) {
			employee = initialize();
		}
		this.employee = employee;
		this.binder.setItemDataSource(employee);
		if (employee.getId() > 0) {
			HorizontalLayout editBarLayout = (HorizontalLayout) this
					.getComponent(0);
			editBarLayout.setVisible(true);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(3);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(false);
			
		}
		Department department = employee.getDepartment();
		refreshFloorTable(department);
		employeeContainerFieldProperties(true);
		handleButtons(employee);
	}

	private void refreshFloorTable(Department department) {
		this.floorTable.removeAllItems();
		if (department != null) {
			
			FloorManager floorManager = new FloorManager(); 
			try {
				if (employee != null && employee.getId() > 0) {
					this.employeeFloor = floorManager.getByEmployee(employee);
				} else {
					this.employeeFloor = null;
				}
			} catch (XERPException e) {
				e.printStackTrace();
			}
			
			List<Floor> allFloors = null; 
			try {
				allFloors = floorManager.listBy(department);
			} catch (XERPException e) {
				e.printStackTrace();
			}
			
			if (allFloors != null && allFloors.size() > 0) {
				for (Floor floor : allFloors) {
					Object[] objects = new Object[2];
					XERPCheckBox floorCheckBox = new XERPCheckBox();
					floorCheckBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
					floorCheckBox.setData(floor);
					
					if (this.employeeFloor != null) {
						if (this.employeeFloor.getId() == floor.getId()) {
							floorCheckBox.setValue(true);
						}
					}
					objects[0] = floorCheckBox;
					
					String floorName = floor.getName();
					objects[1] = floorName;
					
					this.floorTable.addItem(objects, floorCheckBox);
				}
			}
		}
	}

	private void employeeContainerFieldProperties(boolean readOnly) {
		Collection<Field<?>> fields = this.binder.getFields();
		for (Field<?> field : fields) {
			field.setReadOnly(readOnly);
		}
		this.floorTable.setEnabled(!readOnly);
	}

	private void handleButtons(Employee employee) {
		boolean isExistCountry = employee.getId() > 0;
		this.deleteButton.setVisible(isExistCountry);
		this.moveButton.setVisible(isExistCountry);
		if (isExistCountry) {
			this.employeeDeleteButtonClickListener.setEmployee(employee);
		}
		this.employeeResetButtonClickListener.setEmployee(employee);
	}

	private XERPTextField createFormTextField(String caption,
			String propertyId, String inputPrompt, boolean isRequired,
			String errorMessage) {
		XERPTextField textField = new XERPTextField();
		textField.setCaption(caption);
		textField.setRequired(isRequired);
		textField.setRequiredError(errorMessage);
		textField.setImmediate(true);
		textField.setInputPrompt(inputPrompt);
		binder.bind(textField, propertyId);
		return textField;
	}
	
	private XERPComboBox createFormCombobox(String caption, String propertyId,
			BeanItemContainer<?> itemContainer, String itemCaptionPropertyId,
			Layout layout, String defaultValue, boolean isRequired, String errorMessage) {
		XERPComboBox comboBox = new XERPComboBox();
		comboBox.setCaption(caption);
		comboBox.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		comboBox.setNullSelectionAllowed(false);
		comboBox.setImmediate(true);
		comboBox.setContainerDataSource(itemContainer);
		comboBox.setRequired(isRequired);
		if (isRequired) {
			comboBox.setRequiredError(errorMessage);
		}
		comboBox.setItemCaptionPropertyId(itemCaptionPropertyId);
		this.binder.bind(comboBox, propertyId);
		if (defaultValue != null && !defaultValue.isEmpty()) {
			comboBox.setValue(defaultValue);
		}
		if (layout != null) {
			layout.addComponent(comboBox);
		}
		return comboBox;
	}

	private void createFormButton(XERPButton button, String caption,
			Layout layout, FontAwesome icon, ClickListener buttonClickListener,
			boolean isEnabled) {
		button.setCaption(caption);
		button.setDisableOnClick(true);
		button.addClickListener(buttonClickListener);
		button.setIcon(icon);
		button.setEnabled(isEnabled);
		button.setImmediate(true);
		layout.addComponent(button);
	}

	private Employee initialize() {
		Employee employee = new Employee();
		employee.setName("");
		employee.setBand("");
		employee.setBuildingName("");
		employee.setCabin("");
		/*employee.setBranch(new Branch());
		employee.setCostCetre(new CostCentre());
		employee.setDepartment(new Department());
		employee.setDivision(new Division());*/
		employee.setFloor(new Floor());
		employee.setEmailId("");
		employee.setEmployeeId("");
		employee.setEmployeeNature(EmployeeNature.PERMANENT);
		employee.setExtension("");
		employee.setLocation("");
		employee.setMobileNumber(0);
		employee.setActive(true);
		return employee;
	}

	@Override
	public void onUpdate(Employee employee) {
		if (employee != null && employee.getId() > 0) {
			handleSelectionChange(employee);
			if (this.moveEmployeeWindow != null) {
				this.moveEmployeeWindow.forceClose();
			}
		} else {
			employee = initialize();
			this.employee = employee;
			this.binder.setItemDataSource(employee);
			HorizontalLayout editBarContainer = (HorizontalLayout) this
					.getComponent(0);
			editBarContainer.setVisible(false);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(3);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(true);
			refreshFloorTable(null);
			onEditButtonClick();
		}
	}

	@Override
	public void onSelectionChange(Employee employee) {
		handleSelectionChange(employee);
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEditButtonClick() {
		boolean readOnly = canView && (canEdit || canAdd) ? !canView : canView;
		employeeContainerFieldProperties(readOnly);
		HorizontalLayout buttonBarContainer = (HorizontalLayout) this
				.getComponent(3);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
				.getComponent(0);
		buttonBarLayout.setVisible(true);
		handleButtons(employee);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onBranchChange(Branch branch) {
		DivisionManager divisionManager = new DivisionManager();
		List<Division> divisions = null;
		try {
			divisions = divisionManager.getDivisionByBracnch(branch);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		XERPComboBox divisionField = ((XERPComboBox) this.binder
				.getField(XMailroomFormItemConstants.EMPLOYEE_DIVISION));
		divisionField.setReadOnly(false);
		divisionField.removeAllItems();
		BeanItemContainer<Division> divisionContainer = ((BeanItemContainer<Division>) divisionField
				.getContainerDataSource());
		divisionContainer.addAll(divisions);
		divisionField.setContainerDataSource(divisionContainer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDivisionChange(Division division) {
		DepartmentManager departmentManager = new DepartmentManager();
		List<Department> departments = null;
		try {
			departments = departmentManager.listByDivision(division);
		} catch (XERPException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		XERPComboBox departmentField = ((XERPComboBox) this.binder
				.getField(XMailroomFormItemConstants.EMPLOYEE_DEPARTMENT));
		departmentField.setReadOnly(false);
		departmentField.removeAllItems();
		BeanItemContainer<Department> deartmentContainer = ((BeanItemContainer<Department>) departmentField
				.getContainerDataSource());
		deartmentContainer.addAll(departments);
		departmentField.setContainerDataSource(deartmentContainer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDepartmentChange(Department department) {
		CostCentreManager costCentreManager = new CostCentreManager();
		List<CostCentre> costCentres = null;
		try {
			costCentres = costCentreManager.listByDepartment(department);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		XERPComboBox costCentreContainerField = ((XERPComboBox) this.binder
				.getField(XMailroomFormItemConstants.EMPLOYEE_COSTCENTRE));
		costCentreContainerField.setReadOnly(false);
		costCentreContainerField.removeAllItems();
		BeanItemContainer<CostCentre> costCentreContainer = ((BeanItemContainer<CostCentre>) costCentreContainerField
				.getContainerDataSource());
		costCentreContainer.addAll(costCentres);
		costCentreContainerField.setContainerDataSource(costCentreContainer);
		
		refreshFloorTable(department);
		
	}

	@Override
	public void onMoveEmployee() {
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		
		this.moveEmployeeWindow = new XERPWindow();
		this.moveEmployeeWindow.setCaption(bundle.getString(MailRoomMessageConstants.EMPLOYEE_MOVE_WINDOW_CAPTION));
		this.moveEmployeeWindow.setImmediate(true);
		this.moveEmployeeWindow.setModal(true);
		this.moveEmployeeWindow.setWidth("700px");
		this.moveEmployeeWindow.setHeight("500px");
		this.moveEmployeeWindow.center();
		
		VerticalLayout layout = new VerticalLayout();
		layout.setSizeFull();
		layout.setSpacing(true);
		layout.setMargin(true);
		this.moveEmployeeWindow.setContent(layout);
		
		Branch branch = this.employee.getBranch();
		Division division = this.employee.getDivision();
		
		FormLayout formLayout = new FormLayout();
		layout.addComponent(formLayout);
		
		BranchManager branchManager = new BranchManager();
		List<Branch> branches = null;
		try {
			branches = branchManager.listAllBranches();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		BeanItemContainer<Branch> branchContainer = new BeanItemContainer<Branch>(Branch.class);
		branchContainer.addAll(branches);
		
		XERPComboBox branchCombo = new XERPComboBox();
		branchCombo.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_BRANCH));
		branchCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		branchCombo.setNullSelectionAllowed(false);
		branchCombo.setImmediate(true);
		branchCombo.setContainerDataSource(branchContainer);
		branchCombo.setRequired(true);
		branchCombo.setRequiredError(bundle.getString(MailRoomMessageConstants.EMPLOYEE_BRANCH_REQUIRED));
		branchCombo.setItemCaptionPropertyId(XMailroomFormItemConstants.EMPLOYEE_BRANCH_NAME);
		
		XERPComboBox divisionCombo = new XERPComboBox();
		
		BeanItemContainer<Division> divisionContainer = new BeanItemContainer<Division>(Division.class);
		
		divisionCombo.setCaption(bundle.getString(MailRoomMessageConstants.LABAEL_CAPTION_EMPLOYEE_DIVISION));
		divisionCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		divisionCombo.setNullSelectionAllowed(false);
		divisionCombo.setImmediate(true);
		divisionCombo.setContainerDataSource(divisionContainer);
		divisionCombo.setRequired(true);
		divisionCombo.setRequiredError(bundle.getString(MailRoomMessageConstants.EMPLOYEE_DIVISION_REQUIRED));
		divisionCombo.setItemCaptionPropertyId(XMailroomFormItemConstants.EMPLOYEE_DIVISION_NAME);
		
		ValueChangeListener branchComboValueChangeListener = new EmployeeMoveBranchComboValueChangeListener(this.employeeDataChangeObserver, divisionCombo);
		branchCombo.addValueChangeListener(branchComboValueChangeListener);
		branchCombo.setValue(branch);
		formLayout.addComponent(branchCombo);
		
		formLayout.addComponent(divisionCombo);
		
		BeanItemContainer<Department> departmantContainer = new BeanItemContainer<Department>(Department.class);
		
		XERPComboBox departmentCombo = new XERPComboBox();
		
		departmentCombo.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_DEPARTMENT));
		departmentCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		departmentCombo.setNullSelectionAllowed(false);
		departmentCombo.setImmediate(true);
		departmentCombo.setContainerDataSource(departmantContainer);
		departmentCombo.setRequired(true);
		departmentCombo.setRequiredError(bundle.getString(MailRoomMessageConstants.EMPLOYEE_DEPARTMENT_REQUIRED));
		departmentCombo.setItemCaptionPropertyId(XMailroomFormItemConstants.EMPLOYEE_DEPARTMENT_NAME);
		
		formLayout.addComponent(departmentCombo);
		
		BeanItemContainer<CostCentre> costCentreContainer = new BeanItemContainer<CostCentre>(CostCentre.class);
		
		ValueChangeListener divisionComboValueChangeListener = new EmployeeMoveDivisionComboValueChangeListener(this.employeeDataChangeObserver, departmentCombo);
		divisionCombo.addValueChangeListener(divisionComboValueChangeListener);
		divisionCombo.setValue(division);
		
		XERPComboBox costCentreCombo = new XERPComboBox();
		costCentreCombo.setCaption(bundle.getString(MailRoomMessageConstants.LABEL_CAPTION_EMPLOYEE_COSTCENTRE));
		costCentreCombo.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_150_PX);
		costCentreCombo.setNullSelectionAllowed(false);
		costCentreCombo.setImmediate(true);
		costCentreCombo.setContainerDataSource(costCentreContainer);
		costCentreCombo.setRequired(true);
		costCentreCombo.setRequiredError(bundle.getString(MailRoomMessageConstants.EMPLOYEE_COSTCENTRE_REQUIRED));
		costCentreCombo.setItemCaptionPropertyId(XMailroomFormItemConstants.EMPLOYEE_COSTCENTRE_NAME);
		
		ValueChangeListener departmentComboValueChangeListener = new EmployeeMoveDepartmentComboValueChangeListener(this.employeeDataChangeObserver, costCentreCombo);
		departmentCombo.addValueChangeListener(departmentComboValueChangeListener);
		
		formLayout.addComponent(costCentreCombo);
		
		moveFloorTable = new Table();
		layout.addComponent(moveFloorTable);
		moveFloorTable.setWidth("250px");
		moveFloorTable.setHeight("250px");
		
		moveFloorTable.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, XERPCheckBox.class, null);
		moveFloorTable.addContainerProperty(XMailroomTableItemConstants.EMPLOYEE_FLOOR_NAME, String.class, "");
		
		moveFloorTable.setColumnHeader(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, bundle.getString(MailRoomMessageConstants.LIST_HEADER_EMPLOYEE_FLOOR_SELECT));
		moveFloorTable.setColumnHeader(XMailroomTableItemConstants.EMPLOYEE_FLOOR_NAME, bundle.getString(MailRoomMessageConstants.LIST_HEADER_EMPLOYEE_FLOOR_NAME));
		
		moveFloorTable.setColumnWidth(XMailroomTableItemConstants.EMPLOYEE_FLOOR_SELECT, 50);
		
		layout.addComponent(moveFloorTable);
		layout.setComponentAlignment(moveFloorTable, Alignment.MIDDLE_CENTER);
		
		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		/* buttonBarLayoutContainer.setWidth("100%"); */
		layout.addComponent(buttonBarLayoutContainer);
		layout.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(true);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		EmployeeUpdateButtonClickListener countrySaveButtonClickListener = new EmployeeUpdateButtonClickListener(
				this.employeeDataChangeObserver, this.binder, this.canEdit, branchCombo, divisionCombo, departmentCombo, costCentreCombo, this.moveFloorTable);
		String updateActions[] = {
				MailRoomPermissionEnum.ADD_EMPLOYEE.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_EMPLOYEE.asPermission().getName() };
		XERPFriendlyButton updateButton = new XERPFriendlyButton(updateActions);
		createFormButton(updateButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_UPDATE_COUNTRY),
				buttonBarLayout, FontAwesome.SAVE,
				countrySaveButtonClickListener, true);
		updateButton.setClickShortcut(KeyCode.ENTER, null);
		
		EmployeeMoveCloseButtonCickListener employeeMoveCloseButtonCickListener = new EmployeeMoveCloseButtonCickListener(this.employeeDataChangeObserver);
		
		XERPButton closeButton = new XERPButton();
		closeButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_EMPLOYEE_MOVE_CLOSE));
		closeButton.addClickListener(employeeMoveCloseButtonCickListener);
		buttonBarLayout.addComponent(closeButton);
		
//		refreshMoveFloorTable(department);
		
		UI.getCurrent().addWindow(moveEmployeeWindow);
		this.moveEmployeeWindow.focus();
	}
	
	private void refreshMoveFloorTable(Department department) {
		moveFloorTable.removeAllItems();
		if (department != null) {
			
			FloorManager floorManager = new FloorManager(); 
			try {
				if (employee != null && employee.getId() > 0) {
					this.employeeFloor = floorManager.getByEmployee(employee);
				} else {
					this.employeeFloor = null;
				}
			} catch (XERPException e) {
				e.printStackTrace();
			}
			
			List<Floor> allFloors = null; 
			try {
				allFloors = floorManager.listBy(department);
			} catch (XERPException e) {
				e.printStackTrace();
			}
			
			if (allFloors != null && allFloors.size() > 0) {
				for (Floor floor : allFloors) {
					Object[] objects = new Object[2];
					XERPCheckBox floorCheckBox = new XERPCheckBox();
					floorCheckBox.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
					floorCheckBox.setData(floor);
					
					if (this.employeeFloor != null) {
						if (this.employeeFloor.getId() == floor.getId()) {
							floorCheckBox.setValue(true);
						}
					}
					objects[0] = floorCheckBox;
					
					String floorName = floor.getName();
					objects[1] = floorName;
					
					moveFloorTable.addItem(objects, floorCheckBox);
				}
			}
		}
	}

	@Override
	public void onMoveWindowClose() {
		this.moveEmployeeWindow.forceClose();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onBranchMoveChange(Branch branch, XERPComboBox divisionCombo) {
		DivisionManager divisionManager = new DivisionManager();
		List<Division> divisions = null;
		try {
			divisions = divisionManager.getDivisionByBracnch(branch);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		divisionCombo.setReadOnly(false);
		divisionCombo.removeAllItems();
		BeanItemContainer<Division> divisionContainer = ((BeanItemContainer<Division>) divisionCombo
				.getContainerDataSource());
		divisionContainer.addAll(divisions);
		divisionCombo.setContainerDataSource(divisionContainer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDivisionMoveChange(Division division, XERPComboBox departmentCombo) {
		DepartmentManager departmentManager = new DepartmentManager();
		List<Department> departments = null;
		try {
			departments = departmentManager.listByDivision(division);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		departmentCombo.setReadOnly(false);
		departmentCombo.removeAllItems();
		BeanItemContainer<Department> deartmentContainer = ((BeanItemContainer<Department>) departmentCombo
				.getContainerDataSource());
		deartmentContainer.addAll(departments);
		departmentCombo.setContainerDataSource(deartmentContainer);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void onDepartmentMoveChange(Department department, XERPComboBox costCentreCombo) {
		CostCentreManager costCentreManager = new CostCentreManager();
		List<CostCentre> costCentres = null;
		try {
			costCentres = costCentreManager.listByDepartment(department);
		} catch (XERPException e) {
			e.printStackTrace();
		}
		
		costCentreCombo.setReadOnly(false);
		costCentreCombo.removeAllItems();
		BeanItemContainer<CostCentre> costCentreContainer = ((BeanItemContainer<CostCentre>) costCentreCombo
				.getContainerDataSource());
		costCentreContainer.addAll(costCentres);
		costCentreCombo.setContainerDataSource(costCentreContainer);
		
		refreshMoveFloorTable(department);
	}
}