package in.xpeditions.erp.mr.ui.menu.handler;

import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.log.InboundLogHome;

public class InboundLogMenuHandler implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4475508544392443249L;
	private MailRoomMenuChangeHandler menuChangeHandler;

	public InboundLogMenuHandler(MailRoomMenuChangeHandler menuChangeHandler) {
		this.menuChangeHandler = menuChangeHandler;
	}

	@Override
	public void menuSelected(MenuItem selectedItem) {
		// Split Panel
		CustomComponent inboundHome = new InboundLogHome();

		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());

		String breadCrumb = bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM) + " "
				+ bundle.getString(MailRoomMessageConstants.COMMON_MENU_PATH_SEPARATOR) + " "
				+ bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_INBOUND_LOG);

		this.menuChangeHandler.handleMenuChange(breadCrumb, inboundHome);
	}

}
