package in.xpeditions.erp.mr.permissions;

import in.xpeditions.erp.commons.XERPPermissionProvider;
import in.xpeditions.erp.commons.annotations.XERPPermissions;
import in.xpeditions.erp.commons.permission.util.XERPPermission;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPPermissions
public class HallmarkPermissionProvider implements XERPPermissionProvider {

	/* (non-Javadoc)
	 * @see in.xpeditions.erp.commons.XERPPermissionProvider#retrievePermissions()
	 */
	@Override
	public XERPPermission[] retrievePermissions() {
		MailRoomPermissionEnum[] values = MailRoomPermissionEnum.values();
		XERPPermission[] permissions = new XERPPermission[values.length];
		for (int i = 0; i < values.length; i++) {
			MailRoomPermissionEnum accPermissionEnum = values[i];
			permissions[i] = accPermissionEnum.asPermission();
		}
		return permissions;
	}

}
