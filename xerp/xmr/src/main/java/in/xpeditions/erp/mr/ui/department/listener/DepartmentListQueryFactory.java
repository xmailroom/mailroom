/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.io.Serializable;

import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;
import org.vaadin.addons.lazyquerycontainer.QueryFactory;

/**
 * @author Saran
 *
 */
public class DepartmentListQueryFactory implements QueryFactory, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1888390591117020046L;
	private String searchString;

	public DepartmentListQueryFactory(String searchString) {
		this.searchString = searchString;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.vaadin.addons.lazyquerycontainer.QueryFactory#constructQuery(org.
	 * vaadin.addons.lazyquerycontainer.QueryDefinition)
	 */
	@Override
	public Query constructQuery(QueryDefinition queryDefinition) {
		queryDefinition.setMaxNestedPropertyDepth(3);
		DepartmentListQuery divisionListQuery = new DepartmentListQuery(searchString, queryDefinition);
		return divisionListQuery;
	}

}
