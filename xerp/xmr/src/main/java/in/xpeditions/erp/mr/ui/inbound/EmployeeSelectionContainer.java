package in.xpeditions.erp.mr.ui.inbound;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPTableFilterDecorator;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.manager.EmployeeManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.EmployeeListItemChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.EmployeeListItemShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.listener.TableFoucsShortcutListener;

public class EmployeeSelectionContainer extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3668644096860638052L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private EmployeeSelectionWindow employeeSelectionWindow;

	private Employee employee;

	private VerticalLayout dataEntryLayout;

	public FilterTable table;

	private IndexedContainer container;

	private ResourceBundle bundle;

	private EmployeeListItemShortcutListener employeeListItemShortcutListener;

	public EmployeeSelectionContainer(InboundDataChangeObserver inboundDataChangeObserver, ResourceBundle bundle,
			EmployeeSelectionWindow employeeSelectionWindow, Employee employee) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.bundle = bundle;
		this.employeeSelectionWindow = employeeSelectionWindow;
		this.employee = employee;
		this.setStyleName(" v-app xerp-valo-facebook xerphomeui xerp-overflow-auto");
		MarginInfo marginInfo = new MarginInfo(true, true, false, true);
		this.setMargin(marginInfo);
		this.createEmployeeSelectionLayout();
	}

	private void createEmployeeSelectionLayout() {

		this.init();
		VerticalLayout verticalLayout = new VerticalLayout();
		dataEntryLayout = new VerticalLayout();
		verticalLayout.addComponent(dataEntryLayout);
		dataEntryLayout.setSpacing(true);

		HorizontalLayout desigLayout = this.employeeSelection();
		dataEntryLayout.addComponent(desigLayout);

		this.addComponent(verticalLayout);
	}

	private HorizontalLayout employeeSelection() {
		HorizontalLayout desigLayout = new HorizontalLayout();
		desigLayout.setSpacing(true);
		FormLayout formLayout = new FormLayout();
		formLayout.setSpacing(true);
		desigLayout.addComponent(formLayout);

		this.table = new FilterTable();
		desigLayout.addComponent(this.table);
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.addStyleName("table-xls");
		this.table.setFilterBarVisible(true);
		this.table.setSortEnabled(true);
		this.table.setFilterDecorator(new XERPTableFilterDecorator());
		this.table.setWidth("700px");
		this.table.setHeight("375px");
		this.table.setImmediate(true);
		this.table.setPageLength(15);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);
		this.table.addShortcutListener(new TableFoucsShortcutListener("ctrl+down", KeyCode.ARROW_DOWN,
				new int[] { ModifierKey.CTRL }, this.table));

		this.employeeListItemShortcutListener = new EmployeeListItemShortcutListener("", KeyCode.ENTER, null,
				this.inboundDataChangeObserver, this.employeeSelectionWindow);

		EmployeeListItemChangeListener employeeListItemChangeListener = new EmployeeListItemChangeListener(
				this.employeeListItemShortcutListener);
		this.table.addValueChangeListener(employeeListItemChangeListener);

		this.table.addItemClickListener(this.employeeListItemShortcutListener);

		this.container = new IndexedContainer();
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO),
				Label.class, null);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_NAME),
				String.class, "");
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_BRANCH),
				String.class, "");
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_CITY),
				String.class, "");

		this.table.setContainerDataSource(container);

		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_NAME),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_BRANCH),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_CITY),
				Align.LEFT);

		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_NAME), 200);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_BRANCH), 135);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_CITY), 135);

		this.refreshTable();

		return desigLayout;
	}

	private void refreshTable() {
		this.container.removeAllItems();
		EmployeeManager employeeManager = new EmployeeManager();
		List<Employee> employees = new ArrayList<Employee>();
		try {
			employees = employeeManager.list();
		} catch (XERPException e) {
		}
		long i = 1;
		for (Employee employee : employees) {
			Label iL = new Label();
			iL.setValue(i++ + "");
			iL.addShortcutListener(employeeListItemShortcutListener);
			this.table.addItem(new Object[] { iL, employee.getName(), employee.getBranch().getName(), "" }, employee);
		}
		if (this.employee != null) {
			this.table.select(this.employee);
		}
		this.table.focus();
	}

	private void init() {
		// TODO Auto-generated method stub

	}

}
