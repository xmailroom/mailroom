/**
 * 
 */
package in.xpeditions.erp.mr.util;

/**
 * @author Saran
 *
 */
public class XMailroomFormItemConstants {

	public static final String DIVISION_NAME = "name";
	public static final String DIVISION_CODE = "code";
	public static final String DIVISION_BRANCH = "branch";
	public static final String DIVISION_BRANCH_NAME = "name";

	public static final String DEPARTMENT_DIVISION = "division";
	public static final String DEPARTMENT_DIVISION_NAME = "name";
	public static final String DEPARTMENT_CODE = "code";

	public static final String COSTCENTRE_DEPARTMENT = "department";
	public static final String COSTCENTRE_DEPARTMENT_NAME = "name";
	public static final String COSTCENTRE_NAME = "name";
	public static final String COSTCENTRE_CODE = "code";

	public static final String FLOOR_NAME = "name";

	public static final String EMPLOYEE_NAME = "name";
	public static final String EMPLOYEE_BRANCH = "branch";
	public static final String EMPLOYEE_BRANCH_NAME = "name";
	public static final String EMPLOYEE_DIVISION = "division";
	public static final String EMPLOYEE_DIVISION_NAME = "name";
	public static final String EMPLOYEE_DEPARTMENT = "department";
	public static final String EMPLOYEE_DEPARTMENT_NAME = "name";
	public static final String EMPLOYEE_COSTCENTRE = "costCetre";
	public static final String EMPLOYEE_COSTCENTRE_NAME = "name";
	public static final String EMMPLOYEE_NATURE = "employeeNature";
	public static final String EMPLOYEE_ID = "employeeId";
	public static final String EMPLOYEE_EMAIL = "emailId";
	public static final String EMPLOYEE_EXTENSION = "extension";
	public static final String EMPLOYEE_CABIN = "cabin";
	public static final String EMPLOYEE_BUILDING = "buildingName";
	public static final String EMPLOYEE_LOCATION = "location";
	public static final String EMPLOYEE_MOBILE = "mobileNumber";
	public static final String EMMPLOYEE_GENDER = "gender";
	public static final String EMPLOYEE_BAND = "band";
	public static final String EMPLOYEE_ACTIVE = "active";

	public static final String SERVICE_PROVIDER_MODE = "mode";
	public static final String SERVICE_PROVIDER_TYPE = "type";
	public static final String SERVICE_PROVIDER_NAME = "name";
	public static final String SERVICE_PROVIDER_CONTACT_NAME = "contactName";
	public static final String SERVICE_PROVIDER_MOBILE_NUMBER = "mobileNumber";
	
}
