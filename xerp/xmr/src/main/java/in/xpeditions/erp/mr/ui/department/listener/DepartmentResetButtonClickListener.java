/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.Department;

/**
 * @author Saran
 *
 */
public class DepartmentResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8915086930146362073L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private Department department;

	public DepartmentResetButtonClickListener(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.departmentDataChangeObserver.notifySelectionChange(this.department);
		event.getButton().setEnabled(true);
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

}
