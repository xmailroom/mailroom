/**
 * 
 */
package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.dao.manager.EmployeeDAOManager;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class EmployeeDAOManagerHibernate implements EmployeeDAOManager {

	@Override
	public Long getEmployeeCount(String searchString) throws XERPException {
		String hql = "select count(e) from Employee e";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " e.name LIKE '" + searchString + "%' ";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> listEmployees(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		String hql = "select e from Employee e";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " e.name LIKE '" + searchString + "%' ";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		String sortCrit = "";
		if (sortProperties != null) {
			for (int i = 0; i < sortProperties.length; i++) {
				Object sObject = sortProperties[i];
				if (sObject.equals(XMailroomTableItemConstants.EMPLOYEE_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " e.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				} 
			}
			sortCrit = sortCrit.isEmpty() ? "" : " ORDER BY " + sortCrit;
		}
		if(sortCrit.isEmpty()) {
			sortCrit = " ORDER BY e.name";
		}
		hql += sortCrit;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			if (count > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(count);
			}
			List<Employee> employees = query.list();
			return employees;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void save(Employee employee) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.save(employee);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void update(Employee employee) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.update(employee);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void delete(Employee employee) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.delete(employee);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void bulkSave(Floor floor, Division division, Department department, CostCentre costCentre,
			Employee employee) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			
			session.saveOrUpdate(floor);
			session.saveOrUpdate(division);
			session.saveOrUpdate(department);
			session.saveOrUpdate(costCentre);
			session.save(employee);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> list() throws XERPException {
		String hql = "select e from Employee e where e."+Employee.COLUMN_ACTIVE +" IS TRUE";
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
