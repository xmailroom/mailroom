package in.xpeditions.erp.mr.ui.sp;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.HorizontalSplitPanel;
import com.vaadin.ui.VerticalLayout;

public class SPHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1041389266884676946L;

	public SPHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		// SPDataChangeObserver inboundDataChangeObserver = new
		// SPDataChangeObserver();

		HorizontalSplitPanel horizontalSplitPanel = new HorizontalSplitPanel();
		horizontalSplitPanel.setSplitPosition(25);

		VerticalLayout leftLayout = new VerticalLayout();
		leftLayout.setMargin(new MarginInfo(true, false, false, true));
		horizontalSplitPanel.addComponent(leftLayout);

		// SPListContainer inboundListContainer = new
		// SPListContainer(inboundDataChangeObserver);
		// leftLayout.addComponent(inboundListContainer);
		// leftLayout.addComponent(inboundListContainer);
		// leftLayout.setComponentAlignment(inboundListContainer,
		// Alignment.MIDDLE_CENTER);

		// this.inboundEditContainer = new
		// SPEditContainer(inboundDataChangeObserver, null);
		// this.inboundEditContainer.setMargin(new MarginInfo(false, false,
		// false, true));
		// horizontalSplitPanel.addComponent(getSPEditContainer());
		this.setCompositionRoot(horizontalSplitPanel);
	}

}
