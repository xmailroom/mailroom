package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.ui.Table;

import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.ui.inbound.InboundEditContainer;

public class BulkEntryValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5946600949567883400L;

	private XERPTextField nosField;

	private Table toTable;

	private InboundEditContainer inboundEditContainer;

	public BulkEntryValueChangeListener(XERPTextField nosField, Table toTable,
			InboundEditContainer inboundEditContainer) {
		this.nosField = nosField;
		this.toTable = toTable;
		this.inboundEditContainer = inboundEditContainer;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		int nos = XERPUtil.sum(this.nosField.getValue(), 0).intValue();
		if (this.toTable.size() == nos) {
			return;
		}
		if (this.toTable.size() > nos) {
			for (int i = this.toTable.size() - 1; i >= nos; i--) {
				this.inboundEditContainer.removeItem(i);
			}
		} else if (this.toTable.size() < nos) {
			for (int i = this.toTable.size() + 1; i <= nos; i++) {
				this.inboundEditContainer.addToItem(i, null, 1);
			}
		}
	}

}
