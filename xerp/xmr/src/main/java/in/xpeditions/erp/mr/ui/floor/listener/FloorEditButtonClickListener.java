/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author xpeditions
 *
 */
public class FloorEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5428318104135590926L;
	private FloorDataChangeObserver countryDataChangeObserver;

	public FloorEditButtonClickListener(
			FloorDataChangeObserver countryDataChangeObserver) {
				this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.countryDataChangeObserver.notifyEditButtonClick();
		button.setEnabled(true);
	}

}
