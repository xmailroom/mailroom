/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class DepartmentEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1860615080381243841L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;

	public DepartmentEditButtonClickListener(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.departmentDataChangeObserver.notifyEditButtonClick();
		button.setEnabled(true);
	}

}
