/**
 * 
 */
package in.xpeditions.erp.mr.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.EmployeeDAOManager;
import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

/**
 * @author Saran
 *
 */
public class EmployeeManager {

	public Long getEmployeeCount(String searchString) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		return employeeDAOManager.getEmployeeCount(searchString);
	}

	public List<Employee> listEmployees(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		return employeeDAOManager.listEmployees(startIndex, count, sortProperties, sortPropertyAscendingStates,
				searchString);
	}

	public void save(Employee employee) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		employeeDAOManager.save(employee);
	}

	public void update(Employee employee) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		employeeDAOManager.update(employee);
	}

	public void delete(Employee employee) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		employeeDAOManager.delete(employee);
	}

	public void bulkSave(Floor floor, Division division, Department department, CostCentre costCentre,
			Employee employee) throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		employeeDAOManager.bulkSave(floor, division, department, costCentre, employee);
	}

	public List<Employee> list() throws XERPException {
		EmployeeDAOManager employeeDAOManager = (EmployeeDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_EMPLOYEE);
		return employeeDAOManager.list();
	}

}
