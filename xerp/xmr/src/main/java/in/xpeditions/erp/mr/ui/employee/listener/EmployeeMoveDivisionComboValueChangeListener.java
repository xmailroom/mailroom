/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public class EmployeeMoveDivisionComboValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3276199391246799067L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;
	private XERPComboBox departmentCombo;

	public EmployeeMoveDivisionComboValueChangeListener(EmployeeDataChangeObserver employeeDataChangeObserver,
			XERPComboBox departmentCombo) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
		this.departmentCombo = departmentCombo;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.vaadin.data.Property.ValueChangeListener#valueChange(com.vaadin.data.
	 * Property.ValueChangeEvent)
	 */
	@Override
	public void valueChange(ValueChangeEvent event) {
		XERPComboBox divisionCombo = (XERPComboBox) event.getProperty();
		Division division = (Division) divisionCombo.getValue();
		if (division != null) {
			this.employeeDataChangeObserver.notifyDivisionMoveChange(division, this.departmentCombo);
		} else {
			departmentCombo.setReadOnly(false);
			departmentCombo.removeAllItems();
			
		}
	}

}
