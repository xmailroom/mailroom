package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.ui.deliverysheet.DeliverySheetEditWindow;

public class DeliverySheetEditCloseListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7587106615782045489L;

	private DeliverySheetEditWindow deliverySheetEditWindow;

	public DeliverySheetEditCloseListener(DeliverySheetEditWindow deliverySheetEditWindow) {
		this.deliverySheetEditWindow = deliverySheetEditWindow;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		if (this.deliverySheetEditWindow != null) {
			this.deliverySheetEditWindow.close();
		}
	}

}
