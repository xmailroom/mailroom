package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Inbound;

public interface InboundDAOManager {

	void save(Inbound inbound) throws XERPException;

	void update(Inbound inbound) throws XERPException;

	List<Inbound> list() throws XERPException;

	Inbound getBy(Inbound inbound) throws XERPException;

	void delete(Inbound inbound) throws XERPException;

	List<Object[]> listSQL() throws XERPException;

	void bulkSave(List<Inbound> inbounds) throws XERPException;

}
