/**
 * 
 */
package in.xpeditions.erp.mr.ui.division;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.division.listener.DivisionAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.division.listener.DivisionDataChangeListener;
import in.xpeditions.erp.mr.ui.division.listener.DivisionDataChangeObserver;
import in.xpeditions.erp.mr.ui.division.listener.DivisionListItemClickListener;
import in.xpeditions.erp.mr.ui.division.listener.DivisionSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.division.listener.DivisionSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.division.listener.DivisionSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author Saran
 *
 */
public class DivisionListContainer extends VerticalLayout implements DivisionDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1377088942076898412L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	private Table table;
	private String searchString;

	public DivisionListContainer(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
		this.divisionDataChangeObserver.add(this);
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_DIVISION.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_DIVISION.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_DIVISION.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		DivisionListItemClickListener countryListItemClickListener = new DivisionListItemClickListener(this.divisionDataChangeObserver);
		table.addItemClickListener(countryListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void refreshTable(String searchString) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillDivisionList(table, searchString);
		}
	}

	private void createSearchSection(ResourceBundle bundle) {
		DivisionAddNewButtonClickListener divisionAddNewButtonClickListener = new DivisionAddNewButtonClickListener(this.divisionDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_DIVISION.asPermission().getName()};
		XERPPrimaryButton newDivisionButton = new XERPPrimaryButton(actions);
		newDivisionButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_ADD_NEW));
		newDivisionButton.setSizeFull();
		newDivisionButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newDivisionButton.addClickListener(divisionAddNewButtonClickListener);
		this.addComponent(newDivisionButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		DivisionSearchTextShortCutListener divisionSearchTextShortCutListener = new DivisionSearchTextShortCutListener(prompt, KeyCode.ENTER, this.divisionDataChangeObserver);
		searchTextField.addShortcutListener(divisionSearchTextShortCutListener);
		
		DivisionSearchTextBlurListener divisionSearchTextBlurListener = new DivisionSearchTextBlurListener(this.divisionDataChangeObserver, divisionSearchTextShortCutListener);
		searchTextField.addBlurListener(divisionSearchTextBlurListener);
		
		DivisionSearchTextFocusListener countrySearchTextFocusListener = new DivisionSearchTextFocusListener(divisionSearchTextShortCutListener);
		searchTextField.addFocusListener(countrySearchTextFocusListener);
		this.addComponent(searchTextField);
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onUpdate(Division division) {
		refreshTable(this.searchString);
	}

	@Override
	public void onSearchTextValueChange(String searchString) {
		this.searchString = searchString;
		refreshTable(this.searchString);
	}

	@Override
	public void onSelectionChange(Division division) {
		// TODO Auto-generated method stub
		
	}

}
