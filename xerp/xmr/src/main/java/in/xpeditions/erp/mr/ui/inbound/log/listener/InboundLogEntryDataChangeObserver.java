package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.LogEntry;

public class InboundLogEntryDataChangeObserver {

	List<InboundLogEntryDataChangeListener> inboundLogEntryDataChangeListener = new ArrayList<InboundLogEntryDataChangeListener>();

	public void addListener(InboundLogEntryDataChangeListener inboundLogEntryDataChangeListener) {
		this.inboundLogEntryDataChangeListener.add(inboundLogEntryDataChangeListener);
	}

	public void notifyUpdate(LogEntry logEntry) {
		for (InboundLogEntryDataChangeListener inboundLogEntryDataChangeListener : inboundLogEntryDataChangeListener) {
			inboundLogEntryDataChangeListener.onUpdate(logEntry);
		}
	}

	public void notifySelectionChange(LogEntry logEntry) {
		for (InboundLogEntryDataChangeListener inboundLogEntryDataChangeListener : inboundLogEntryDataChangeListener) {
			inboundLogEntryDataChangeListener.onSelectionChange(logEntry);
		}
	}

}
