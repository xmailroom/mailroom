package in.xpeditions.erp.mr.ui.deliverysheet;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.util.HierarchicalContainer;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.themes.BaseTheme;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTableFilterDecorator;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.AddDeliverySheetButtonClickListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDataChangeObserver;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetEditButtonClickListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetListItemChangeListener;
import in.xpeditions.erp.mr.ui.deliverysheet.listener.DeliverySheetListItemClickListener;

public class DeliverySheetListContainer extends VerticalLayout implements DeliverySheetDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2871014401183025388L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private ResourceBundle bundle;

	private boolean canEdit;

	private boolean canView;

	private boolean canDelete;

	private VerticalLayout deliverySheetVerticalLayout;

	private FilterTable table;

	private HierarchicalContainer container;

	public DeliverySheetListContainer(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheetDataChangeObserver.addListener(this);
		this.bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());

		this.canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_DELIVERY_SHEET.asPermission().getName());
		this.canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_DELIVERY_SHEET.asPermission().getName());
		this.canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_DELIVERY_SHEET.asPermission().getName());

		UI.getCurrent().getPage()
				.setTitle(this.bundle.getString(MailRoomMessageConstants.HOME_MENU_MAIL_ROOM_DELIVERY_SHEET));
		this.deliverySheetListLayout();

	}

	private void deliverySheetListLayout() {
		this.deliverySheetVerticalLayout = new VerticalLayout();
		this.deliverySheetVerticalLayout.setSpacing(true);
		this.addComponent(this.deliverySheetVerticalLayout);
		String[] actions = { MailRoomPermissionEnum.ADD_DELIVERY_SHEET.asPermission().getName() };
		XERPPrimaryButton addNewButton = new XERPPrimaryButton(actions);
		addNewButton.setCaption(this.bundle.getString(MailRoomMessageConstants.BUTTON_CAPTION_ADD_DELIVERY_SHEET));
		addNewButton.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_200_PX);
		addNewButton.setIcon(FontAwesome.PLUS_CIRCLE);
		addNewButton.addClickListener(new AddDeliverySheetButtonClickListener(this.deliverySheetDataChangeObserver));
		addNewButton.setClickShortcut(KeyCode.N, ModifierKey.CTRL);
		this.deliverySheetVerticalLayout.addComponent(addNewButton);
		this.desigTable();
	}

	private void desigTable() {

		this.table = new FilterTable();
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.setFilterBarVisible(true);
		this.table.setSortEnabled(true);
		this.table.setFilterDecorator(new XERPTableFilterDecorator());
		this.table.setWidth("1000px");
		this.table.setHeight("240px");
		this.table.setImmediate(true);
		this.table.setVisible(this.canEdit || this.canView || this.canDelete);
		this.table.setPageLength(5);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);

		this.deliverySheetVerticalLayout.addComponent(this.table);

		this.container = new HierarchicalContainer();

		this.container.addContainerProperty(DeliverySheet.COLUMN_NUMBER, String.class, "");
		this.container.addContainerProperty(DeliverySheet.COLUMN_DATE, String.class, "");
		this.container.addContainerProperty(DeliverySheet.COLUMN_RECEIVED_BY, String.class, "");
		this.container.addContainerProperty(DeliverySheet.COLUMN_RECEIVED_TIME, String.class, "");
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND),
				Long.class, 0);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_DOCS),
				Long.class, 0);
		this.container.addContainerProperty(
				this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND_DELIVERED), Long.class, 0);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				HorizontalLayout.class, null);

		this.table.setColumnHeader(DeliverySheet.COLUMN_NUMBER,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_NUMBER));
		this.table.setColumnHeader(DeliverySheet.COLUMN_DATE,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_DATE));
		this.table.setColumnHeader(DeliverySheet.COLUMN_RECEIVED_BY,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_RECEIVED_BY));
		this.table.setColumnHeader(DeliverySheet.COLUMN_RECEIVED_TIME,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_RECEIVED_TIME));
		this.table.setColumnHeader(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_SHEET_NO_OF_INBOUND));
		this.table.setColumnHeader(MailRoomMessageConstants.COLUMN_NO_OF_DOCS,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_NO_OF_DOCS));
		this.table.setColumnHeader(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND_DELIVERED,
				this.bundle.getString(MailRoomMessageConstants.CAPTION_DELIVERY_NO_OF_INBOUND_DELIVERED));
		this.table.setColumnHeader(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION),
				this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION));

		this.table.setColumnWidth(DeliverySheet.COLUMN_NUMBER, 150);
		this.table.setColumnWidth(DeliverySheet.COLUMN_DATE, 100);
		this.table.setColumnWidth(DeliverySheet.COLUMN_RECEIVED_BY, 150);
		this.table.setColumnWidth(DeliverySheet.COLUMN_RECEIVED_TIME, 130);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND), 100);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_DOCS), 100);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.COLUMN_NO_OF_INBOUND_DELIVERED), 100);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), 160);

		this.table.setContainerDataSource(this.container);
		this.table.setFilterFieldVisible(this.bundle.getString(MailRoomMessageConstants.LABEL_ACTION), false);

		ItemClickListener deliverySheetListItemClickListener = new DeliverySheetListItemClickListener(this.bundle,
				this.deliverySheetDataChangeObserver);
		this.table.addItemClickListener(deliverySheetListItemClickListener);

		ValueChangeListener deliverySheetListItemChangeListener = new DeliverySheetListItemChangeListener(
				this.deliverySheetDataChangeObserver, this.table);
		this.table.addValueChangeListener(deliverySheetListItemChangeListener);

		this.refreshTable();

	}

	private void refreshTable() {
		this.container.removeAllItems();
		DeliverySheetManager deliverySheetManager = new DeliverySheetManager();
		List<Object[]> objects = new ArrayList<Object[]>();
		try {
			objects = deliverySheetManager.listSQL();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		for (Object[] object : objects) {
			DeliverySheet deliverySheet = this.init(object);
			HorizontalLayout horizontalLayout = this.actionLayout(deliverySheet);
			int i = 5;
			this.table.addItem(new Object[] { deliverySheet.getNumber() + "",
					XERPUtil.formatDate(deliverySheet.getDate(), DataHolder.commonUserDateTimeFormat),
					deliverySheet.getReceivedBy(),
					XERPUtil.formatDate(deliverySheet.getReceivedTime(), DataHolder.commonUserDateTimeFormat),
					XERPUtil.sum(object[i++], 0).longValue(), XERPUtil.sum(object[i++], 0).longValue(),
					XERPUtil.sum(object[i++], 0).longValue(), horizontalLayout }, deliverySheet);
		}

	}

	private DeliverySheet init(Object[] object) {
		DeliverySheet deliverySheet = new DeliverySheet();
		int i = 0;
		deliverySheet.setId(XERPUtil.sum(object[i++], 0).longValue());
		deliverySheet.setNumber(XERPUtil.sum(object[i++], 0).longValue());
		deliverySheet.setDate(XERPUtil.sum(object[i++], 0).longValue());
		deliverySheet.setReceivedBy(object[i++] + "");
		deliverySheet.setReceivedTime(XERPUtil.sum(object[i++], 0).longValue());
		return deliverySheet;
	}

	private HorizontalLayout actionLayout(DeliverySheet deliverySheet) {
		HorizontalLayout actionLayout = new HorizontalLayout();
		actionLayout.setSpacing(true);

		String[] actionEdit = { MailRoomPermissionEnum.EDIT_DELIVERY_SHEET.asPermission().getName() };
		XERPButton editButton = new XERPButton(actionEdit);
		editButton.setDescription("Edit");
		editButton.setIcon(FontAwesome.EDIT);
		editButton.addStyleName(BaseTheme.BUTTON_LINK);
		editButton.addStyleName("small");
		editButton.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_PRIMARY);
		ClickListener deliverySheetEditButtonClickListener = new DeliverySheetEditButtonClickListener(this.bundle,
				this.deliverySheetDataChangeObserver, deliverySheet);
		editButton.addClickListener(deliverySheetEditButtonClickListener);
		actionLayout.addComponent(editButton);

		String[] actionDelete = { MailRoomPermissionEnum.DELETE_DELIVERY_SHEET.asPermission().getName() };
		XERPButton delete = new XERPButton(actionDelete);
		delete.setDescription("Delete");
		delete.setIcon(FontAwesome.TRASH_O);
		delete.addStyleName(BaseTheme.BUTTON_LINK);
		delete.addStyleName("small");
		delete.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_COLOR_DANGER);
		ClickListener deliverySheetDeleteButtonClickListener = new DeliverySheetDeleteButtonClickListener(this.bundle,
				this.deliverySheetDataChangeObserver, deliverySheet);
		delete.addClickListener(deliverySheetDeleteButtonClickListener);
		actionLayout.addComponent(delete);

		return actionLayout;
	}

	@Override
	public void onUpdate(DeliverySheet deliverySheet) {
		this.refreshTable();
		this.table.select(deliverySheet);
	}

	@Override
	public void onEditButtonClick(DeliverySheet deliverySheet) {
		try {
			deliverySheet = new DeliverySheetManager().getBy(deliverySheet);
			// if
			// (deliverySheet.getBookingStatus().equals(BookingStatus.CONFIRMED))
			// {
			// String message = this.bundle
			// .getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_UNABLE_TO_EDIT);
			// String subMessage = this.bundle
			// .getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_UNABLE_TO_EDIT_REASON);
			// Notification.show(message, "\n" + subMessage,
			// Notification.Type.ERROR_MESSAGE);
			// return;
			// }
			// if (deliverySheet.getBookingStatus().ordinal() >
			// BookingStatus.BOOKED.ordinal()) {
			// String message = this.bundle
			// .getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_UNABLE_TO_EDIT);
			// String subMessage = this.bundle
			// .getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_UNABLE_TO_EDIT_REASON_1);
			// Notification.show(message, "\n" + subMessage,
			// Notification.Type.ERROR_MESSAGE);
			// return;
			// }
			deliverySheet.setDeliverySheetDetails(new DeliverySheetManager().listDeliverySheetDetail(deliverySheet));
		} catch (XERPException e) {
			e.printStackTrace();
		}
		DeliverySheetEditWindow deliverySheetEditWindow = new DeliverySheetEditWindow(
				this.deliverySheetDataChangeObserver, deliverySheet);
		deliverySheetEditWindow.open();
	}

	@Override
	public void onSelectionChange(DeliverySheet deliverySheet) {

	}

	@Override
	public void onConfirm(BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		// TODO Auto-generated method stub

	}

}
