package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.EmployeeSelectionWindow;

public class EmployeeChangeButtonClickListener implements ClickListener, InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3352023123667284996L;

	private XERPComboBox employeeComboBox;

	public EmployeeChangeButtonClickListener(XERPComboBox employeeComboBox) {
		this.employeeComboBox = employeeComboBox;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Employee employee = (Employee) this.employeeComboBox.getValue();
		InboundDataChangeObserver inboundDataChangeObserver = new InboundDataChangeObserver();
		inboundDataChangeObserver.addListener(this);
		EmployeeSelectionWindow employeeSelectionWindow = new EmployeeSelectionWindow(inboundDataChangeObserver,
				employee);
		employeeSelectionWindow.open();
	}

	@Override
	public void onUpdate(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEmployeeChange(Employee employee) {
		this.employeeComboBox.setValue(employee);
	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		// TODO Auto-generated method stub
		
	}

}
