package in.xpeditions.erp.mr.ui.inbound;

import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.util.IndexedContainer;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.event.ShortcutAction.ModifierKey;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.CustomTable.Align;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPTableFilterDecorator;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.inbound.listener.InboundDataChangeObserver;
import in.xpeditions.erp.mr.ui.inbound.listener.LogEntryListItemChangeListener;
import in.xpeditions.erp.mr.ui.inbound.listener.LogEntryListItemShortcutListener;
import in.xpeditions.erp.mr.ui.inbound.listener.TableFoucsShortcutListener;

public class LogEntrySelectionContainer extends VerticalLayout {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3668644096860638052L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private LogEntrySelectionWindow logEntrySelectionWindow;

	private LogEntry logEntry;

	private VerticalLayout dataEntryLayout;

	public FilterTable table;

	private IndexedContainer container;

	private ResourceBundle bundle;

	private LogEntryListItemShortcutListener logEntryListItemShortcutListener;

	public LogEntrySelectionContainer(InboundDataChangeObserver inboundDataChangeObserver, ResourceBundle bundle,
			LogEntrySelectionWindow logEntrySelectionWindow, LogEntry logEntry) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.bundle = bundle;
		this.logEntrySelectionWindow = logEntrySelectionWindow;
		this.logEntry = logEntry;
		this.setStyleName(" v-app xerp-valo-facebook xerphomeui xerp-overflow-auto");
		MarginInfo marginInfo = new MarginInfo(true, true, false, true);
		this.setMargin(marginInfo);
		this.createLogEntrySelectionLayout();
	}

	private void createLogEntrySelectionLayout() {

		this.init();
		VerticalLayout verticalLayout = new VerticalLayout();
		dataEntryLayout = new VerticalLayout();
		verticalLayout.addComponent(dataEntryLayout);
		dataEntryLayout.setSpacing(true);

		HorizontalLayout desigLayout = this.logEntrySelection();
		dataEntryLayout.addComponent(desigLayout);

		this.addComponent(verticalLayout);
	}

	private HorizontalLayout logEntrySelection() {
		HorizontalLayout desigLayout = new HorizontalLayout();
		desigLayout.setSpacing(true);
		FormLayout formLayout = new FormLayout();
		formLayout.setSpacing(true);
		desigLayout.addComponent(formLayout);

		this.table = new FilterTable();
		desigLayout.addComponent(this.table);
		this.table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		this.table.addStyleName("table-xls");
		this.table.setFilterBarVisible(true);
		this.table.setSortEnabled(true);
		this.table.setFilterDecorator(new XERPTableFilterDecorator());
		this.table.setWidth("700px");
		this.table.setHeight("375px");
		this.table.setImmediate(true);
		this.table.setPageLength(15);
		this.table.setSelectable(true);
		this.table.setColumnReorderingAllowed(true);
		this.table.addShortcutListener(new TableFoucsShortcutListener("ctrl+down", KeyCode.ARROW_DOWN,
				new int[] { ModifierKey.CTRL }, this.table));

		this.logEntryListItemShortcutListener = new LogEntryListItemShortcutListener("", KeyCode.ENTER, null,
				this.inboundDataChangeObserver, this.logEntrySelectionWindow);

		LogEntryListItemChangeListener logEntryListItemChangeListener = new LogEntryListItemChangeListener(
				this.logEntryListItemShortcutListener);
		this.table.addValueChangeListener(logEntryListItemChangeListener);

		this.table.addItemClickListener(this.logEntryListItemShortcutListener);

		this.container = new IndexedContainer();
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO),
				Label.class, null);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO),
				Long.class, 0);
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE),
				String.class, "");
		this.container.addContainerProperty(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING),
				Integer.class, 0);

		this.table.setContainerDataSource(container);

		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO),
				Align.RIGHT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO),
				Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE), Align.LEFT);
		this.table.setColumnAlignment(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING),
				Align.RIGHT);

		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_ES_SL_NO), 75);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_BATCH_NO), 200);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_DATE), 135);
		this.table.setColumnWidth(this.bundle.getString(MailRoomMessageConstants.LABEL_INBOUND_LOG_PENDING), 100);

		this.refreshTable();

		return desigLayout;
	}

	private void refreshTable() {
		this.container.removeAllItems();
		LogEntryManager logEntryManager = new LogEntryManager();
		List<LogEntry> logEntrys = new ArrayList<LogEntry>();
		try {
			logEntrys = logEntryManager.listPending();
		} catch (XERPException e) {
		}
		long i = 1;
		for (LogEntry logEntry : logEntrys) {
			Label iL = new Label();
			iL.setValue(i++ + "");
			iL.addShortcutListener(logEntryListItemShortcutListener);
			this.table.addItem(new Object[] { iL, logEntry.getBatchNo(),
					XERPUtil.formatDate(logEntry.getDate(), DataHolder.commonUserDateTimeFormat),
					(logEntry.getNoOfShip() - logEntry.getNoOfEntry()) }, logEntry);
		}
		if (this.logEntry != null) {
			this.table.select(this.logEntry);
		}
		this.table.focus();
	}

	private void init() {
		// TODO Auto-generated method stub

	}

}
