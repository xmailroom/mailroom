/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor;

import java.util.Collection;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Field;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Layout;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPButton;
import in.xpeditions.erp.commons.component.XERPDangerButton;
import in.xpeditions.erp.commons.component.XERPFriendlyButton;
import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDataChangeListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDataChangeObserver;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDeleteButtonClickListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorEditButtonClickListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorResetButtonClickListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorSaveButtonClickListener;
import in.xpeditions.erp.mr.util.XMailroomFormItemConstants;

/**
 * @author xpeditions
 * 
 */
public class FloorEditContainer extends VerticalLayout implements
		FloorDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4484020088110920562L;
	private FloorDataChangeObserver floorDataChangeObserver;
	private BeanFieldGroup<Floor> binder;
	private Floor floor;
	private XERPFriendlyButton saveButton;
	private FloorDeleteButtonClickListener floorDeleteButtonClickListener;
	private FloorResetButtonClickListener floorResetButtonClickListener;
	private boolean canAdd;
	private boolean canEdit;
	private boolean canView;
	private XERPDangerButton editButton;
	private XERPDangerButton deleteButton;

	public FloorEditContainer(
			FloorDataChangeObserver floorDataChangeObserver) {
		this.floorDataChangeObserver = floorDataChangeObserver;
		this.floorDataChangeObserver.add(this);
		ResourceBundle bundle = I18MailRoomResourceBundle
				.createBundle(VaadinSession.getCurrent().getLocale());
		UI.getCurrent()
				.getPage()
				.setTitle(
						bundle.getString(MailRoomMessageConstants.HOME_MENU_ACCOUNT_FLOOR));
		canAdd = XERPUtil.hasPermission(MailRoomPermissionEnum.ADD_FLOOR
				.asPermission().getName());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_FLOOR
				.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_FLOOR
				.asPermission().getName());
		/* this.setSizeFull(); */
		MarginInfo marginInfo = new MarginInfo(false, false, false, true);
		this.setMargin(marginInfo);
		createEditContainer(bundle);
	}

	private void createEditContainer(ResourceBundle bundle) {
		this.binder = new BeanFieldGroup<Floor>(Floor.class);
		this.binder.setBuffered(true);

		this.floor = initialize();

		HorizontalLayout editBarLayout = new HorizontalLayout();
		MarginInfo editMarginInfo = new MarginInfo(true, true, false, false);
		editBarLayout.setMargin(editMarginInfo);
		editBarLayout.setVisible(false);
		this.addComponent(editBarLayout);
		this.setComponentAlignment(editBarLayout, Alignment.TOP_RIGHT);

		String editActions[] = {
				MailRoomPermissionEnum.EDIT_FLOOR.asPermission().getName(),
				MailRoomPermissionEnum.DELETE_FLOOR.asPermission()
						.getName() };
		FloorEditButtonClickListener floorEditButtonClickListener = new FloorEditButtonClickListener(
				this.floorDataChangeObserver);
		editButton = new XERPDangerButton(editActions);
		createFormButton(editButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE_EDIT),
				editBarLayout, FontAwesome.EDIT,
				floorEditButtonClickListener, true);		

		FormLayout formLayout = new FormLayout();

		XERPTextField nameField = createFormTextField(
				Floor.COLUMN_NAME,
				XMailroomFormItemConstants.FLOOR_NAME,
				Floor.COLUMN_NAME,
				true, bundle.getString(MailRoomMessageConstants.NAME_REQUIRED));
		nameField
				.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		nameField.setWidth(XERPUIConstants.UI_COMPONENTS_WIDTH_300_PX);
		formLayout.addComponent(nameField);

		this.addComponent(formLayout);

		HorizontalLayout buttonBarLayoutContainer = new HorizontalLayout();
		MarginInfo marginInfo = new MarginInfo(true, true, true, false);
		buttonBarLayoutContainer.setMargin(marginInfo);
		buttonBarLayoutContainer.setSizeFull();
		/* buttonBarLayoutContainer.setWidth("100%"); */
		this.addComponent(buttonBarLayoutContainer);
		this.setComponentAlignment(buttonBarLayoutContainer,
				Alignment.BOTTOM_RIGHT);

		HorizontalLayout buttonBarLayout = new HorizontalLayout();
		buttonBarLayout.setVisible(false);
		buttonBarLayout.setSpacing(true);
		buttonBarLayoutContainer.addComponent(buttonBarLayout);
		buttonBarLayoutContainer.setComponentAlignment(buttonBarLayout,
				Alignment.BOTTOM_RIGHT);

		FloorSaveButtonClickListener countrySaveButtonClickListener = new FloorSaveButtonClickListener(
				this.floorDataChangeObserver, this.binder, this.canEdit);
		String saveActions[] = {
				MailRoomPermissionEnum.ADD_FLOOR.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_FLOOR.asPermission().getName() };
		this.saveButton = new XERPFriendlyButton(saveActions);
		createFormButton(saveButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_SAVE),
				buttonBarLayout, FontAwesome.SAVE,
				countrySaveButtonClickListener, true);
		this.saveButton.setClickShortcut(KeyCode.ENTER, null);
		
		this.floorDeleteButtonClickListener = new FloorDeleteButtonClickListener(
				this.floorDataChangeObserver);
		String deleteActions[] = { MailRoomPermissionEnum.DELETE_FLOOR
				.asPermission().getName() };
		deleteButton = new XERPDangerButton(deleteActions);
		createFormButton(deleteButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_DELETE),
				buttonBarLayout, FontAwesome.TRASH_O,
				floorDeleteButtonClickListener, true);

		this.floorResetButtonClickListener = new FloorResetButtonClickListener(
				this.floorDataChangeObserver);
		String resetActions[] = {
				MailRoomPermissionEnum.ADD_FLOOR.asPermission().getName(),
				MailRoomPermissionEnum.EDIT_FLOOR.asPermission().getName() };
		XERPPrimaryButton resetButton = new XERPPrimaryButton(resetActions);
		createFormButton(resetButton,
				bundle.getString(MailRoomMessageConstants.BUTTON_RESET),
				buttonBarLayout, FontAwesome.UNDO,
				floorResetButtonClickListener, true);

		handleSelectionChange(this.floor);
	}

	private void handleSelectionChange(Floor floor) {
		if (floor == null) {
			floor = initialize();
		}
		this.floor = floor;
		this.binder.setItemDataSource(floor);
		if (floor.getId() > 0) {
			HorizontalLayout editBarLayout = (HorizontalLayout) this
					.getComponent(0);
			editBarLayout.setVisible(true);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(false);
		}
		countryContainerFieldProperties(true);
		handleButtons(floor);
	}

	private void countryContainerFieldProperties(boolean readOnly) {
		Collection<Field<?>> fields = this.binder.getFields();
		for (Field<?> field : fields) {
			field.setReadOnly(readOnly);
		}
	}

	private void handleButtons(Floor floor) {
		boolean isExistCountry = floor.getId() > 0;
		this.deleteButton.setVisible(isExistCountry);
		if (isExistCountry) {
			this.floorDeleteButtonClickListener.setCountry(floor);
		}
		this.floorResetButtonClickListener.setFloor(floor);
	}

	private XERPTextField createFormTextField(String caption,
			String propertyId, String inputPrompt, boolean isRequired,
			String errorMessage) {
		XERPTextField textField = new XERPTextField();
		textField.setCaption(caption);
		textField.setRequired(isRequired);
		textField.setRequiredError(errorMessage);
		textField.setImmediate(true);
		textField.setInputPrompt(inputPrompt);
		binder.bind(textField, propertyId);
		return textField;
	}

	private void createFormButton(XERPButton button, String caption,
			Layout layout, FontAwesome icon, ClickListener buttonClickListener,
			boolean isEnabled) {
		button.setCaption(caption);
		button.setDisableOnClick(true);
		button.addClickListener(buttonClickListener);
		button.setIcon(icon);
		button.setEnabled(isEnabled);
		button.setImmediate(true);
		layout.addComponent(button);
	}

	private Floor initialize() {
		Floor floor = new Floor();
		floor.setName("");
		return floor;
	}

	@Override
	public void onUpdate(Floor floor) {
		if (floor != null && floor.getId() > 0) {
			handleSelectionChange(floor);
		} else {
			floor = initialize();
			this.floor = floor;
			this.binder.setItemDataSource(floor);
			HorizontalLayout editBarContainer = (HorizontalLayout) this
					.getComponent(0);
			editBarContainer.setVisible(false);
			HorizontalLayout buttonBarContainer = (HorizontalLayout) this
					.getComponent(2);
			HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
					.getComponent(0);
			buttonBarLayout.setVisible(true);
			onEditButtonClick();
		}
	}

	@Override
	public void onSelectionChange(Floor floor) {
		handleSelectionChange(floor);
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEditButtonClick() {
		boolean readOnly = canView && (canEdit || canAdd) ? !canView : canView;
		countryContainerFieldProperties(readOnly);
		HorizontalLayout buttonBarContainer = (HorizontalLayout) this
				.getComponent(2);
		HorizontalLayout buttonBarLayout = (HorizontalLayout) buttonBarContainer
				.getComponent(0);
		buttonBarLayout.setVisible(true);
		handleButtons(floor);
	}
}