/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public class EmployeeResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5088973296473642423L;
	private EmployeeDataChangeObserver countryDataChangeObserver;
	private Employee employee;

	public EmployeeResetButtonClickListener(EmployeeDataChangeObserver countryDataChangeObserver) {
		this.countryDataChangeObserver = countryDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.countryDataChangeObserver.notifySelectionChange(this.employee);
		event.getButton().setEnabled(true);
	}

	public void setEmployee(Employee employee) {
		this.employee = employee;
	}

}
