/**
 * 
 */
package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.mr.dao.manager.ServiceProviderDAOManager;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.util.XMailroomTableItemConstants;

/**
 * @author Saran
 *
 */
public class ServiceProviderDAOManagerHibernate implements ServiceProviderDAOManager {

	@Override
	public void delete(ServiceProvider serviceProvider) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.delete(serviceProvider);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void save(ServiceProvider serviceProvider) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.save(serviceProvider);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@Override
	public void update(ServiceProvider serviceProvider) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try{
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			tx = session.beginTransaction();
			session.update(serviceProvider);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if(tx != null){
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch(Exception e){
			if(tx != null){
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if(session != null && session.isOpen()){
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ServiceProvider> listServiceProviders(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException {
		String hql = "select s from ServiceProvider s";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " s.name LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		String sortCrit = "";
		if (sortProperties != null) {
			for (int i = 0; i < sortProperties.length; i++) {
				Object sObject = sortProperties[i];
				if (sObject.equals(XMailroomTableItemConstants.SERVICE_PROVIDER_NAME)) {
					if(!sortCrit.isEmpty()){
						sortCrit = sortCrit + ",";
					}
					sortCrit += " s.name " + (sortPropertyAscendingStates[i] ? "ASC" : "DESC");
					continue;
				} 
			}
			sortCrit = sortCrit.isEmpty() ? "" : " ORDER BY " + sortCrit;
		}
		if(sortCrit.isEmpty()) {
			sortCrit = " ORDER BY s.name";
		}
		hql += sortCrit;
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			if (count > 0) {
				query.setFirstResult(startIndex);
				query.setMaxResults(count);
			}
			List<ServiceProvider> serviceProviders = query.list();
			return serviceProviders;
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public Long getCount(String searchString) throws XERPException {
		String hql = "select count(s) from ServiceProvider s";
		String criteria = "";
		if (searchString != null && !searchString.isEmpty()) {
			criteria += " s.name LIKE '" + searchString + "%'";
		}
		criteria = criteria.isEmpty() ? criteria : (" WHERE " + criteria);
		hql += criteria;
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			return (Long) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public ServiceProvider getBy(Long id) throws XERPException {
		String hql = "select s from ServiceProvider s WHERE s.id=:id";
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("id", id);
			return (ServiceProvider) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ServiceProvider> list(TypeEnum type, ModeEnum mode) throws XERPException {
		String hql = "select s from ServiceProvider s WHERE s.type=:type and s.mode=:mode";
		
		Session session = null;
		try {
			session = HibernateUtil.getSession(DataHolder.common_db_name);
			Query query = session.createQuery(hql);
			query.setParameter("mode", mode);
			query.setParameter("type", type);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
