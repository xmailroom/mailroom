package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.mr.entity.Employee;

public class EmployeeListItemChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8526965701738077347L;

	private EmployeeListItemShortcutListener employeeListItemShortcutListener;

	public EmployeeListItemChangeListener(EmployeeListItemShortcutListener employeeListItemShortcutListener) {
		this.employeeListItemShortcutListener = employeeListItemShortcutListener;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		Employee employee = (Employee) event.getProperty().getValue();
		if (employee != null) {
			this.employeeListItemShortcutListener.setFocusedItem(employee);
		}
	}

}
