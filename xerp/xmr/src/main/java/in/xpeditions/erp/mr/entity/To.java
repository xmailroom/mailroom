package in.xpeditions.erp.mr.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = To.TABLE_NAME)
public class To implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1310809757037191331L;

	public static final String TABLE_NAME = "xmr_to";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_EMPLOYEE = "employeeId";

	public static final String COLUMN_TO_NAME = "name";

	public static final String COLUMN_CITY = "city";

	public static final String COLUMN_ISR_NO = "isrNo";

	public static final String COLUMN_REMARKS = "remarks";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = To.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@Column(name = To.COLUMN_EMPLOYEE)
	private long employeeId;

	@Column(name = To.COLUMN_TO_NAME)
	private String name;

	@Column(name = To.COLUMN_ISR_NO)
	private String isrNo;

	@Column(name = To.COLUMN_REMARKS)
	private String remarks;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the employeeId
	 */
	public long getEmployeeId() {
		return employeeId;
	}

	/**
	 * @param employeeId
	 *            the employeeId to set
	 */
	public void setEmployeeId(long employeeId) {
		this.employeeId = employeeId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the isrNo
	 */
	public String getIsrNo() {
		return isrNo;
	}

	/**
	 * @param isrNo
	 *            the isrNo to set
	 */
	public void setIsrNo(String isrNo) {
		this.isrNo = isrNo;
	}

	/**
	 * @return the remarks
	 */
	public String getRemarks() {
		return remarks;
	}

	/**
	 * @param remarks
	 *            the remarks to set
	 */
	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		To other = (To) obj;
		if (id != other.id)
			return false;
		return true;
	}

}
