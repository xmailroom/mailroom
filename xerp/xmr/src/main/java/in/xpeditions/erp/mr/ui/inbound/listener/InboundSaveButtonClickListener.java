package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;

import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.From;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.To;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.manager.InboundManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.util.InboundUtil;

public class InboundSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6692446945510350816L;

	private InboundDataChangeObserver inboundDataChangeObserver;

	private ResourceBundle bundle;

	private BeanFieldGroup<Inbound> inboundBinder;

	private BeanFieldGroup<From> fromBinder;

	private Table toTable;

	private HashMap<BeanFieldGroup<To>, Integer> noOfDocsMap;

	private XERPTextField nosField;

	public InboundSaveButtonClickListener(InboundDataChangeObserver inboundDataChangeObserver, ResourceBundle bundle,
			BeanFieldGroup<Inbound> inboundBinder, BeanFieldGroup<From> fromBinder, Table toTable,
			XERPTextField nosField, HashMap<BeanFieldGroup<To>, Integer> noOfShipMap) {
		this.inboundDataChangeObserver = inboundDataChangeObserver;
		this.bundle = bundle;
		this.inboundBinder = inboundBinder;
		this.fromBinder = fromBinder;
		this.toTable = toTable;
		this.nosField = nosField;
		this.noOfDocsMap = noOfShipMap;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		String message = "", subMessage = "";
		try {
			this.inboundBinder.commit();
		} catch (CommitException e) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}
		Inbound inbound = this.inboundBinder.getItemDataSource().getBean();

		try {
			this.fromBinder.commit();
			From from = this.fromBinder.getItemDataSource().getBean();
			if (inbound.getMode().equals(ModeEnum.INTERBRANCH)) {
				if (from.getEmployeeId() == 0) {
					message = this.bundle.getString(
							MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
					subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_FROM_REQUIRED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					button.setEnabled(true);
					return;
				}

				if (from.getCityId() == 0) {
					message = this.bundle.getString(
							MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
					subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_FROM_REQUIRED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					button.setEnabled(true);
					return;
				}
			}
			inbound.setxFrom(from);
		} catch (CommitException e1) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_FROM_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}
		int bulkNos = 0, noOfEntries = 0;
		if (this.nosField != null) {
			bulkNos = XERPUtil.sum(this.nosField.getValue(), 0).intValue();
		}
		List<To> tos = new ArrayList<To>();
		List<?> itemIds = (List<?>) this.toTable.getItemIds();
		for (Object itemId : itemIds) {
			@SuppressWarnings("unchecked")
			BeanFieldGroup<To> toBinder = (BeanFieldGroup<To>) itemId;
			try {
				toBinder.commit();
				To to = toBinder.getItemDataSource().getBean();
				noOfEntries += XERPUtil.sum(this.noOfDocsMap.get(toBinder), 0).intValue();
				if (inbound.getMode().equals(ModeEnum.INTERBRANCH))
					if (to.getEmployeeId() == 0) {
						message = this.bundle.getString(
								MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
						subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_TO_REQUIRED);
						Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
						button.setEnabled(true);
						return;
					}
				tos.add(to);
			} catch (CommitException e) {
				message = this.bundle.getString(
						MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
				subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_FROM_REQUIRED);
				Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
				button.setEnabled(true);
				return;
			}
		}
		if (inbound.isBulk()) {
			if (bulkNos != noOfEntries) {
				message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_BULK_ENTRIES_NOT_MATCHED);
				subMessage = this.bundle
						.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_BULK_ENTRIES_NOT_MATCHED_REASON);
				Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
				button.setEnabled(true);
				return;
			}
		}
		if (inbound.getServiceProviderId() == 0) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}
		InboundManager inboundManager = new InboundManager();
		try {
			if (inbound.getId() > 0) {
				inboundManager.update(inbound);
			} else {
				List<Inbound> inbounds = new ArrayList<Inbound>();
				inbound.setCreatedTime(System.currentTimeMillis());
				for (Object itemId : itemIds) {
					@SuppressWarnings("unchecked")
					BeanFieldGroup<To> toBinder = (BeanFieldGroup<To>) itemId;
					int noOfDocs = XERPUtil.sum(this.noOfDocsMap.get(itemId), 0).intValue();
					Inbound iiInbound = InboundUtil.copy(inbound, toBinder.getItemDataSource().getBean(), noOfDocs);
					inbounds.add(iiInbound);
				}
				inboundManager.bulkSave(inbounds);
			}
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_SAVED_SUCCESSFULLY);
			Notification.show(message, Notification.Type.WARNING_MESSAGE);
			this.inboundDataChangeObserver.notifyUpdate(inbound);
		} catch (XERPConstraintViolationException e) {
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_SAVE_FAILED);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DUPILCATE_VALUE_NAME)
					+ " '" + e.getTitle() + "' "
					+ this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);

		} catch (XERPException e) {
			message = "XERPException";
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			button.setEnabled(true);
			return;
		}

		button.setEnabled(true);
	}

}
