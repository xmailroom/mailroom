/**
 * 
 */
package in.xpeditions.erp.mr.ui.floor;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.floor.listener.FloorAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDataChangeListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorDataChangeObserver;
import in.xpeditions.erp.mr.ui.floor.listener.FloorListItemClickListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.floor.listener.FloorSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author xpeditions
 *
 */
public class FloorListContainer extends VerticalLayout implements FloorDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7372471904016649781L;
	
	private FloorDataChangeObserver floorDataChangeObserver;
	private Table table;
	private String searchString;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	
	public FloorListContainer(FloorDataChangeObserver floorDataChangeObserver) {
		this.floorDataChangeObserver = floorDataChangeObserver;
		this.floorDataChangeObserver.add(this);
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_FLOOR.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_FLOOR.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_FLOOR.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		FloorListItemClickListener floorListItemClickListener = new FloorListItemClickListener(this.floorDataChangeObserver);
		table.addItemClickListener(floorListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void createSearchSection(ResourceBundle bundle) {
		FloorAddNewButtonClickListener floorAddNewButtonClickListener = new FloorAddNewButtonClickListener(this.floorDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_FLOOR.asPermission().getName()};
		XERPPrimaryButton newCountryButton = new XERPPrimaryButton(actions);
		newCountryButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_ADD_NEW));
		newCountryButton.setSizeFull();
		newCountryButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newCountryButton.addClickListener(floorAddNewButtonClickListener);
		this.addComponent(newCountryButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		FloorSearchTextShortCutListener floorSearchTextShortCutListener = new FloorSearchTextShortCutListener(prompt, KeyCode.ENTER, this.floorDataChangeObserver);
		searchTextField.addShortcutListener(floorSearchTextShortCutListener);
		
		FloorSearchTextBlurListener countrySearchTextBlurListener = new FloorSearchTextBlurListener(this.floorDataChangeObserver, floorSearchTextShortCutListener);
		searchTextField.addBlurListener(countrySearchTextBlurListener);
		
		FloorSearchTextFocusListener countrySearchTextFocusListener = new FloorSearchTextFocusListener(floorSearchTextShortCutListener);
		searchTextField.addFocusListener(countrySearchTextFocusListener);
		this.addComponent(searchTextField);
	}
	
	private void refreshTable(String searchString) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillFloorList(table, searchString);
		}
	}


	@Override
	public void onUpdate(Floor floor) {
		refreshTable(this.searchString);
//		if (country != null && country.getId() > 0) {
//		}
	}

	@Override
	public void onSelectionChange(Floor floor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSearchTextValueChange(String searchString) {
		this.searchString = searchString;
		refreshTable(this.searchString);
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}
}