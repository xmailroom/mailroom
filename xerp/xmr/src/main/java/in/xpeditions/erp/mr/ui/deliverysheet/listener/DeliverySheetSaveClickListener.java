package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import org.tepi.filtertable.FilterTable;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;

import in.xpeditions.erp.acc.entity.User;
import in.xpeditions.erp.commons.general.Constants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.entity.DeliverySheetDetail;
import in.xpeditions.erp.mr.manager.DeliverySheetManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.DeliverySheetEditWindow;
import in.xpeditions.erp.mr.ui.deliverysheet.ReceivedByEditWindow;

public class DeliverySheetSaveClickListener implements ClickListener, DeliverySheetDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2059490889614288298L;

	private ResourceBundle bundle;

	private DeliverySheetEditWindow deliverySheetEditWindow;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private BeanFieldGroup<DeliverySheet> deliverySheetBinder;

	private FilterTable deliverySheetTable;

	private ClickEvent event;

	public DeliverySheetSaveClickListener(ResourceBundle bundle, DeliverySheetEditWindow deliverySheetEditWindow,
			DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			BeanFieldGroup<DeliverySheet> deliverySheetBinder, FilterTable deliverySheetTable) {
		this.bundle = bundle;
		this.deliverySheetEditWindow = deliverySheetEditWindow;
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheetBinder = deliverySheetBinder;
		this.deliverySheetTable = deliverySheetTable;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		this.event = event;
		event.getButton().setEnabled(false);
		String message = "", subMessage = "";
		DeliverySheet deliverySheet = null;
		try {
			this.deliverySheetBinder.commit();
			deliverySheet = this.deliverySheetBinder.getItemDataSource().getBean();
		} catch (CommitException e) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		deliverySheet.setDeliverySheetDetails(new ArrayList<DeliverySheetDetail>());
		Collection<?> aItemIds = this.deliverySheetTable.getItemIds();
		List<Object> itemIds = new ArrayList<Object>();
		for (Object itemId : aItemIds) {
			if (this.deliverySheetTable.isSelected(itemId)) {
				itemIds.add(itemId);
			}
		}
		if (itemIds.size() == 0) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DETAIL_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		for (Object itemId : itemIds) {
			@SuppressWarnings("unchecked")
			BeanFieldGroup<DeliverySheetDetail> deliverySheetDetailBinder = (BeanFieldGroup<DeliverySheetDetail>) itemId;
			try {
				deliverySheetDetailBinder.commit();
				DeliverySheetDetail deliverySheetDetail = deliverySheetDetailBinder.getItemDataSource().getBean();
				deliverySheet.getDeliverySheetDetails().add(deliverySheetDetail);
			} catch (CommitException e) {
				message = this.bundle.getString(
						MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
				subMessage = this.bundle
						.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DETAIL_REQUIRED);
				Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
				event.getButton().setEnabled(true);
				return;
			}
		}
		DeliverySheetDataChangeObserver deliverySheetDataChangeObserver = new DeliverySheetDataChangeObserver();
		deliverySheetDataChangeObserver.addListener(this);
		ReceivedByEditWindow receivedByEditWindow = new ReceivedByEditWindow(deliverySheetDataChangeObserver,
				deliverySheetBinder);
		receivedByEditWindow.open();
		event.getButton().setEnabled(true);
	}

	@Override
	public void onUpdate(DeliverySheet deliverySheet) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEditButtonClick(DeliverySheet deliverySheet) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSelectionChange(DeliverySheet deliverySheet) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onConfirm(BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		event.getButton().setEnabled(false);
		String message = "", subMessage = "";
		DeliverySheet deliverySheet = null;
		try {
			this.deliverySheetBinder.commit();
			deliverySheet = this.deliverySheetBinder.getItemDataSource().getBean();
		} catch (CommitException e) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		deliverySheet.setDeliverySheetDetails(new ArrayList<DeliverySheetDetail>());
		Collection<?> aItemIds = this.deliverySheetTable.getItemIds();
		List<Object> itemIds = new ArrayList<Object>();
		for (Object itemId : aItemIds) {
			if (this.deliverySheetTable.isSelected(itemId)) {
				itemIds.add(itemId);
			}
		}
		if (itemIds.size() == 0) {
			message = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DETAIL_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		for (Object itemId : itemIds) {
			@SuppressWarnings("unchecked")
			BeanFieldGroup<DeliverySheetDetail> deliverySheetDetailBinder = (BeanFieldGroup<DeliverySheetDetail>) itemId;
			try {
				deliverySheetDetailBinder.commit();
				DeliverySheetDetail deliverySheetDetail = deliverySheetDetailBinder.getItemDataSource().getBean();
				deliverySheet.getDeliverySheetDetails().add(deliverySheetDetail);
			} catch (CommitException e) {
				message = this.bundle.getString(
						MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
				subMessage = this.bundle
						.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_DETAIL_REQUIRED);
				Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
				event.getButton().setEnabled(true);
				return;
			}
		}
		DeliverySheetManager deliverySheetManager = new DeliverySheetManager();
		try {
			if (deliverySheet.getId() > 0) {
				deliverySheetManager.update(deliverySheet);
			} else {
				User loggedInUser = (User) XERPUtil.getSessionAttribute(Constants.SESSION_USER);
				deliverySheet.setCreatedUserId(loggedInUser.getId());
				deliverySheet.setCreatedTime(System.currentTimeMillis());
				deliverySheet.setReceivedTime(System.currentTimeMillis());
				deliverySheetManager.save(deliverySheet);
			}
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_SAVED_SUCCESSFULLY);
			Notification.show(message, Notification.Type.WARNING_MESSAGE);
			if (this.deliverySheetEditWindow != null) {
				this.deliverySheetEditWindow.forceClose();
			}
			this.deliverySheetDataChangeObserver.notifyUpdate(deliverySheet);
		} catch (XERPConstraintViolationException e) {
			message = this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_SAVE_FAILED);
			subMessage = e.getTitle() + "' "
					+ this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		} catch (XERPException e) {
			message = "XERPException";
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
	}

}
