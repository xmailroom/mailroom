package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ResourceBundle;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class DeliverySheetEditButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4062587783490013726L;
	private ResourceBundle bundle;
	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;
	private DeliverySheet deliverySheet;

	public DeliverySheetEditButtonClickListener(ResourceBundle bundle,
			DeliverySheetDataChangeObserver deliverySheetDataChangeObserver, DeliverySheet deliverySheet) {
		this.bundle = bundle;
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.deliverySheet = deliverySheet;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		String[] editActions = { MailRoomPermissionEnum.EDIT_DELIVERY_SHEET.asPermission().getName() };
		if (XERPUtil.hasPermissions(editActions)) {
			this.deliverySheetDataChangeObserver.notifyEditButtonClick(this.deliverySheet);
			;
		} else {
			Notification.show(this.bundle.getString(MailRoomMessageConstants.ERROR_PERMISSION),
					this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED), Type.WARNING_MESSAGE);
		}
	}

}
