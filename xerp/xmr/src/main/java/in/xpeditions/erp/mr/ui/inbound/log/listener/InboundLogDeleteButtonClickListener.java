package in.xpeditions.erp.mr.ui.inbound.log.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.manager.LogEntryManager;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class InboundLogDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5931707744795979618L;

	private InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver;

	private ResourceBundle bundle;

	private LogEntry logEntry;

	public InboundLogDeleteButtonClickListener(InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver,
			ResourceBundle bundle, LogEntry logEntry) {
		this.inboundLogEntryDataChangeObserver = inboundLogEntryDataChangeObserver;
		this.bundle = bundle;
		this.setLogEntry(logEntry);
	}

	@Override
	public void buttonClick(ClickEvent event) {
		try {
			this.setLogEntry(new LogEntryManager().getBy(this.getLogEntry()));
		} catch (XERPException e) {
		}
		if (this.getLogEntry().getNoOfEntry() > 0) {
			String subMessage = this.bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_UNABLE_TO_DELETE_INBOUND_LOG_REASON_1);
			Notification.show(this.bundle.getString(MailRoomMessageConstants.NOTIFICATION_INBOUND_LOG_DELETE_FAILED),
					subMessage, Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		String pleaseConfirmTxt = this.bundle
				.getString(MailRoomMessageConstants.CONFIRM_INBOUND_LOG_DELETE_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = this.bundle
				.getString(MailRoomMessageConstants.CONFIRM_INBOUND_LOG_DELETE_MESSAGE);
		String captionYes = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_YES);
		String captionNo = this.bundle.getString(MailRoomMessageConstants.CONFIRM_DELETE_BUTTON_CAPTION_NO);

		event.getButton().setEnabled(true);
		LogEntryDeleteConfirmListener logEntryDeleteConfirmListener = new LogEntryDeleteConfirmListener(this.bundle,
				this.inboundLogEntryDataChangeObserver, this.getLogEntry());
		ConfirmDialog dialog = ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt,
				doYouWantToDeleteTxt + " '" + this.getLogEntry().getBatchNo() + "' Dated '"
						+ XERPUtil.formatDate(this.getLogEntry().getDate(), DataHolder.commonUserDateTimeFormat)
						+ "' ?",
				captionYes, captionNo, logEntryDeleteConfirmListener);
		dialog.setStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_POPUP_WINDOW_STYLE_DANGER);
		dialog.getOkButton().addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_DANGER);
	}

	/**
	 * @return the logEntry
	 */
	public LogEntry getLogEntry() {
		return logEntry;
	}

	/**
	 * @param logEntry
	 *            the logEntry to set
	 */
	public void setLogEntry(LogEntry logEntry) {
		this.logEntry = logEntry;
	}

}
