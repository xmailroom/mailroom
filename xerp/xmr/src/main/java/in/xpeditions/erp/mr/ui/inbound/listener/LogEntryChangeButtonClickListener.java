package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.ui.inbound.LogEntrySelectionWindow;

public class LogEntryChangeButtonClickListener implements ClickListener, InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3352023123667284996L;

	private XERPComboBox logEntryComboBox;

	public LogEntryChangeButtonClickListener(XERPComboBox logEntryComboBox) {
		this.logEntryComboBox = logEntryComboBox;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		LogEntry logEntry = (LogEntry) this.logEntryComboBox.getValue();
		InboundDataChangeObserver inboundDataChangeObserver = new InboundDataChangeObserver();
		inboundDataChangeObserver.addListener(this);
		LogEntrySelectionWindow logEntrySelectionWindow = new LogEntrySelectionWindow(inboundDataChangeObserver,
				logEntry);
		logEntrySelectionWindow.open();
	}

	@Override
	public void onUpdate(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		this.logEntryComboBox.setValue(logEntry);
	}

	@Override
	public void onEmployeeChange(Employee employee) {
		// TODO Auto-generated method stub

	}

}
