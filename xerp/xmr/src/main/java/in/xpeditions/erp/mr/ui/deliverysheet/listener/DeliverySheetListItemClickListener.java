package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ResourceBundle;

import com.vaadin.data.util.BeanItem;
import com.vaadin.event.ItemClickEvent;
import com.vaadin.event.ItemClickEvent.ItemClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

public class DeliverySheetListItemClickListener implements ItemClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6452321241956666778L;

	private ResourceBundle bundle;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	public DeliverySheetListItemClickListener(ResourceBundle bundle,
			DeliverySheetDataChangeObserver deliverySheetDataChangeObserver) {
		this.bundle = bundle;
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
	}

	@Override
	public void itemClick(ItemClickEvent event) {
		if (event.isDoubleClick()) {
			try {
				BeanItem<DeliverySheet> beanItem = new BeanItem<DeliverySheet>((DeliverySheet) event.getItemId());
				DeliverySheet deliverySheet = beanItem.getBean();
				String[] editActions = { MailRoomPermissionEnum.EDIT_DELIVERY_SHEET.asPermission().getName() };
				if (XERPUtil.hasPermissions(editActions)) {
					this.deliverySheetDataChangeObserver.notifyEditButtonClick(deliverySheet);
				} else {
					Notification.show(this.bundle.getString(MailRoomMessageConstants.ERROR_PERMISSION),
							this.bundle.getString(MailRoomMessageConstants.ERROR_REQUIRED), Type.WARNING_MESSAGE);
				}
			} catch (ClassCastException e) {
			}
		}
	}

}
