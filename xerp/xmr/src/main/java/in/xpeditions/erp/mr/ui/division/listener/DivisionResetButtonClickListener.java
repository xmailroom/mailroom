/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public class DivisionResetButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2381188291894657717L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private Division division;

	public DivisionResetButtonClickListener(DivisionDataChangeObserver divisionDataChangeObserver) {
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		this.divisionDataChangeObserver.notifySelectionChange(this.division);
		event.getButton().setEnabled(true);
	}

	public void setDivision(Division division) {
		this.division = division;
	}

}
