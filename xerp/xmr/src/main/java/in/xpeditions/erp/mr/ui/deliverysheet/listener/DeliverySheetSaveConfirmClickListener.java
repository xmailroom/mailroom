package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.deliverysheet.ReceivedByEditWindow;

public class DeliverySheetSaveConfirmClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 636182440436401604L;

	private DeliverySheetDataChangeObserver deliverySheetDataChangeObserver;

	private BeanFieldGroup<DeliverySheet> deliverySheetBinder;

	private ReceivedByEditWindow receivedByEditWindow;

	public DeliverySheetSaveConfirmClickListener(DeliverySheetDataChangeObserver deliverySheetDataChangeObserver,
			ReceivedByEditWindow receivedByEditWindow, BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		this.deliverySheetDataChangeObserver = deliverySheetDataChangeObserver;
		this.receivedByEditWindow = receivedByEditWindow;
		this.deliverySheetBinder = deliverySheetBinder;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		String message = "", subMessage = "";
		try {
			this.deliverySheetBinder.commit();
		} catch (CommitException e) {
			message = bundle
					.getString(MailRoomMessageConstants.NOTIFICATION_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DELIVERY_SHEET_REQUIRED);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		this.deliverySheetDataChangeObserver.notifyConfirm(this.deliverySheetBinder);
		this.receivedByEditWindow.forceClose();
	}

}
