/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import in.xpeditions.erp.mr.entity.ServiceProvider;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderDataChangeObserver implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1632562329617975130L;
	
	private List<ServiceProviderDataChangeListener> countryDataChangeListeners;
	
	public ServiceProviderDataChangeObserver() {
		this.countryDataChangeListeners = new ArrayList<ServiceProviderDataChangeListener>();
	}
	
	public void add(ServiceProviderDataChangeListener countryDataChangeListener) {
		this.countryDataChangeListeners.add(countryDataChangeListener);
	}
	
	public void remove(ServiceProviderDataChangeListener countryDataChangeListener) {
		this.countryDataChangeListeners.remove(countryDataChangeListener);
	}

	public void notifyUpdate(ServiceProvider serviceProvider) {
		for (ServiceProviderDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onUpdate(serviceProvider);
		}
	}

	public void notifySelectionChange(ServiceProvider serviceProvider) {
		for (ServiceProviderDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onSelectionChange(serviceProvider);
		}
	}

	public void notifySearchTextValueChange(String value) {
		for (ServiceProviderDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onSearchTextValueChange(value);
		}
	}

	public void notifyEditButtonClick() {
		for (ServiceProviderDataChangeListener countryDataChangeListener : this.countryDataChangeListeners) {
			countryDataChangeListener.onEditButtonClick();
		}
	}
}