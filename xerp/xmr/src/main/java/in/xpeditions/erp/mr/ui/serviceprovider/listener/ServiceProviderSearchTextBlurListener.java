/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import in.xpeditions.erp.commons.component.XERPTextField;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderSearchTextBlurListener implements BlurListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6390075732871681376L;
	private ServiceProviderDataChangeObserver countryDataChangeObserver;
	private ServiceProviderSearchTextShortCutListener countrySearchTextShortCutListener;

	public ServiceProviderSearchTextBlurListener(ServiceProviderDataChangeObserver countryDataChangeObserver, ServiceProviderSearchTextShortCutListener countrySearchTextShortCutListener) {
		this.countryDataChangeObserver = countryDataChangeObserver;
		this.countrySearchTextShortCutListener = countrySearchTextShortCutListener;
	}

	@Override
	public void blur(BlurEvent event) {
		XERPTextField searchTextComp = (XERPTextField) event.getComponent();
		searchTextComp.removeShortcutListener(countrySearchTextShortCutListener);
		String searchString = searchTextComp.getValue();
		this.countryDataChangeObserver.notifySearchTextValueChange(searchString);
	}

}
