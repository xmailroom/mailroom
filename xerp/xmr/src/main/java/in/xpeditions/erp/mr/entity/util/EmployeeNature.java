/**
 * 
 */
package in.xpeditions.erp.mr.entity.util;

/**
 * @author Saran
 *
 */
public enum EmployeeNature {
	
	TEMPORARY("Temporary"), PERMANENT("Permanent");  
	  
	private String name;

	EmployeeNature(String name){
        this.name = name;
    }
    
    public String getName(){
        return this.name;
    }

    @Override
    public String toString() {
    	return this.name;
    }

}
