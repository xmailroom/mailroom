package in.xpeditions.erp.mr.ui.menu.handler;

import com.vaadin.event.ShortcutListener;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

public class MailRoomMenuShortcutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1410503347870705699L;

	private Command menuHandler;

	private MenuItem menuItem;

	public MailRoomMenuShortcutListener(String caption, int keyCode, int[] modifierKeys, Command menuHandler,
			MenuItem menuItem) {
		super(caption, keyCode, modifierKeys);
		this.menuHandler = menuHandler;
		this.menuItem = menuItem;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		this.menuHandler.menuSelected(menuItem);
	}

}
