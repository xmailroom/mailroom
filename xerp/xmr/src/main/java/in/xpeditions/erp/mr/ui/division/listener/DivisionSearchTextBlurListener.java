/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.event.FieldEvents.BlurEvent;
import com.vaadin.event.FieldEvents.BlurListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DivisionSearchTextBlurListener implements BlurListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 9018227094805061730L;
	private DivisionDataChangeObserver divisionDataChangeObserver;
	private DivisionSearchTextShortCutListener divisionSearchTextShortCutListener;

	public DivisionSearchTextBlurListener(DivisionDataChangeObserver divisionDataChangeObserver,
			DivisionSearchTextShortCutListener divisionSearchTextShortCutListener) {
				this.divisionDataChangeObserver = divisionDataChangeObserver;
				this.divisionSearchTextShortCutListener = divisionSearchTextShortCutListener;
	}

	@Override
	public void blur(BlurEvent event) {
		XERPTextField searchTextComp = (XERPTextField) event.getComponent();
		searchTextComp.removeShortcutListener(divisionSearchTextShortCutListener);
		String searchString = searchTextComp.getValue();
		this.divisionDataChangeObserver.notifySearchTextValueChange(searchString);
	}

}
