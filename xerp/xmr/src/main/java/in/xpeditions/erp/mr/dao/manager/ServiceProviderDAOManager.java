/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

/**
 * @author Saran
 *
 */
public interface ServiceProviderDAOManager {

	public void delete(ServiceProvider serviceProvider) throws XERPException;

	public void save(ServiceProvider serviceProvider) throws XERPException;

	public void update(ServiceProvider serviceProvider) throws XERPException;

	public List<ServiceProvider> listServiceProviders(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public Long getCount(String searchString) throws XERPException;

	public ServiceProvider getBy(Long id) throws XERPException;

	public List<ServiceProvider> list(TypeEnum type, ModeEnum mode) throws XERPException;

}
