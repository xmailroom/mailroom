package in.xpeditions.erp.mr.manager;

import java.sql.Date;
import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.spring.XERPSpringContextHelper;
import in.xpeditions.erp.mr.dao.manager.DeliverySheetDAOManager;
import in.xpeditions.erp.mr.entity.DeliverySheet;
import in.xpeditions.erp.mr.entity.DeliverySheetDetail;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.general.MailRoomSpringBeanConstants;

public class DeliverySheetManager {

	public void save(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		deliverySheetDAOManager.save(deliverySheet);
	}

	public void update(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		deliverySheetDAOManager.update(deliverySheet);
	}

	public List<DeliverySheet> list() throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.list();
	}

	public DeliverySheet getBy(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.getBy(deliverySheet);
	}

	public void delete(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		deliverySheetDAOManager.delete(deliverySheet);
	}

	public List<Object[]> listSQL() throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.listSQL();
	}

	public List<DeliverySheetDetail> listDeliverySheetDetail(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.listDeliverySheetDetail(deliverySheet);
	}

	public List<Object[]> listDeliverySheetDetailSQL(DeliverySheet deliverySheet) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.listDeliverySheetDetailSQL(deliverySheet);
	}

	public List<Object[]> listReadyForDeliverySQL(Date from, Date to, Floor floor, Department department)
			throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.listReadyForDeliverySQL(from, to, floor, department);
	}

	public DeliverySheetDetail getDeliverySheetDetail(DeliverySheetDetail deliverySheetDetail) throws XERPException {
		DeliverySheetDAOManager deliverySheetDAOManager = (DeliverySheetDAOManager) XERPSpringContextHelper
				.getBean(MailRoomSpringBeanConstants.DAO_MANAGER_DELIVERY_SHEET);
		return deliverySheetDAOManager.getDeliverySheetDetail(deliverySheetDetail);
	}

}
