package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.event.FieldEvents.FocusEvent;
import com.vaadin.event.FieldEvents.FocusListener;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Employee;
import in.xpeditions.erp.mr.entity.Inbound;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.ui.inbound.EmployeeSelectionWindow;

public class EmployeeComboFocusListener implements FocusListener, InboundDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -613927487631874156L;


	private XERPComboBox employeeComboBox;

	@Override
	public void focus(FocusEvent event) {
		this.employeeComboBox = (XERPComboBox) event.getSource();
		Employee employee = (Employee) this.employeeComboBox.getValue();
		if (employee == null) {
			InboundDataChangeObserver inboundDataChangeObserver = new InboundDataChangeObserver();
			inboundDataChangeObserver.addListener(this);
			EmployeeSelectionWindow employeeSelectionWindow = new EmployeeSelectionWindow(inboundDataChangeObserver,
					null);
			employeeSelectionWindow.open();
		}
	}

	@Override
	public void onUpdate(Inbound inbound) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onSelectionChange(Inbound inbound) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEmployeeChange(Employee employee) {
		this.employeeComboBox.setValue(employee);
	}

	@Override
	public void onLogEntryChange(LogEntry logEntry) {
		// TODO Auto-generated method stub
		
	}

}
