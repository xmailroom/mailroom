/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import com.vaadin.event.ShortcutListener;

import in.xpeditions.erp.commons.component.XERPTextField;

/**
 * @author Saran
 *
 */
public class DivisionSearchTextShortCutListener extends ShortcutListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3472535236715291903L;
	private DivisionDataChangeObserver divisionDataChangeObserver;

	public DivisionSearchTextShortCutListener(String prompt, int keyCode,
			DivisionDataChangeObserver divisionDataChangeObserver) {
		super(prompt, keyCode, null);
		this.divisionDataChangeObserver = divisionDataChangeObserver;
	}

	@Override
	public void handleAction(Object sender, Object target) {
		if ((target instanceof XERPTextField)) {
			XERPTextField field = (XERPTextField) target;
			String value = field.getValue();
			this.divisionDataChangeObserver.notifySearchTextValueChange(value);
		}
	}

}
