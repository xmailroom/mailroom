/**
 * 
 */
package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public interface DepartmentDAOManager {

	public List<Department> listDepartments(int startIndex, int count, Object[] sortProperties,
			boolean[] sortPropertyAscendingStates, String searchString) throws XERPException;

	public Long getDepartmentCount(String searchString) throws XERPException;

	public void save(Department department) throws XERPException;

	public void update(Department department) throws XERPException;

	public void delete(Department department) throws XERPException;

	public List<Department> listByDivision(Division division) throws XERPException;

	public Department getByName(String departmentName) throws XERPException;

	public List<Department> list() throws XERPException;

}