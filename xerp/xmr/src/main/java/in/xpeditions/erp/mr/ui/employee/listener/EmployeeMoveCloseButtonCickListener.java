/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class EmployeeMoveCloseButtonCickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4435743027324121184L;
	private EmployeeDataChangeObserver employeeDataChangeObserver;

	public EmployeeMoveCloseButtonCickListener(EmployeeDataChangeObserver employeeDataChangeObserver) {
		this.employeeDataChangeObserver = employeeDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.employeeDataChangeObserver.notifyMoveWindowClose();
		button.setEnabled(true);
	}

}
