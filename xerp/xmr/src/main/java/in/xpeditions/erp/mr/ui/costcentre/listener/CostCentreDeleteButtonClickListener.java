/**
 * 
 */
package in.xpeditions.erp.mr.ui.costcentre.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.UI;

import in.xpeditions.erp.mr.entity.CostCentre;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class CostCentreDeleteButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -378907163533295042L;
	private CostCentre costCentre;
	private CostcentreDataChangeObserver costcentreDataChangeObserver;

	public CostCentreDeleteButtonClickListener(CostcentreDataChangeObserver costcentreDataChangeObserver) {
		this.costcentreDataChangeObserver = costcentreDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		event.getButton().setEnabled(false);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		String pleaseConfirmTxt = bundle.getString(MailRoomMessageConstants.COSTCENTRE_DELETE_CONFIRM_WINDOW_CAPTION);
		String doYouWantToDeleteTxt = bundle.getString(MailRoomMessageConstants.COSTCENTRE_DELETE_CONFIRM_MESSAGE);
		String captionYes = bundle.getString(MailRoomMessageConstants.COSTCENTRE_DELETE_CONFIRM_BUTTON_CAPTION_YES);
		String captionNo = bundle.getString(MailRoomMessageConstants.COSTCENTRE_DELETE_CONFIRM_BUTTON_CAPTION_NO);
		CostCentreDeleteConfirmListener costCentreDeleteConfirmListener = new CostCentreDeleteConfirmListener(this.costCentre, this.costcentreDataChangeObserver);
		ConfirmDialog.show(UI.getCurrent(), pleaseConfirmTxt, doYouWantToDeleteTxt + this.costCentre.getName() + "?", captionYes, captionNo, costCentreDeleteConfirmListener);
		event.getButton().setEnabled(true);
	}

	public void setCostCentre(CostCentre costCentre) {
		this.costCentre = costCentre;
	}

}
