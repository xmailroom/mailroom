/**
 * 
 */
package in.xpeditions.erp.mr.ui.serviceprovider.listener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.vaadin.addons.lazyquerycontainer.NestingBeanItem;
import org.vaadin.addons.lazyquerycontainer.Query;
import org.vaadin.addons.lazyquerycontainer.QueryDefinition;

import com.vaadin.data.Item;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderListQuery implements Query, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 976287006875128175L;
	private String searchString;
	private QueryDefinition queryDefinition;

	public ServiceProviderListQuery(String searchString, QueryDefinition queryDefinition) {
		this.searchString = searchString;
		this.queryDefinition = queryDefinition;
	}

	@Override
	public Item constructItem() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean deleteAllItems() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public List<Item> loadItems(int startIndex, int count) {
		ServiceProviderManager serviceProviderManager = new ServiceProviderManager();
		try {
			Object[] sortProperties = this.queryDefinition.getSortPropertyIds();
			boolean[] sortPropertyAscendingStates = this.queryDefinition.getSortPropertyAscendingStates();
			List<ServiceProvider> serviceProviders = serviceProviderManager.listServiceProviders(startIndex, count, sortProperties, sortPropertyAscendingStates, this.searchString);
			List<Item> items = new ArrayList<Item>(serviceProviders.size());
			for (ServiceProvider serviceProvider : serviceProviders) {
				items.add(new NestingBeanItem<ServiceProvider>(serviceProvider, this.queryDefinition.getMaxNestedPropertyDepth(), this.queryDefinition.getPropertyIds()));
			}
			return items;	
		}
			catch (XERPException e) {
				e.printStackTrace();
			}
		return null;
	}

	@Override
	public void saveItems(List<Item> arg0, List<Item> arg1, List<Item> arg2) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public int size() {
		ServiceProviderManager serviceProviderManager = new ServiceProviderManager();
		try {
			Long countOf = serviceProviderManager.getServiceProviderCount(searchString);
			return countOf.intValue();
		} catch (XERPException e) {
			e.printStackTrace();
		}
		return 0;
	}
}