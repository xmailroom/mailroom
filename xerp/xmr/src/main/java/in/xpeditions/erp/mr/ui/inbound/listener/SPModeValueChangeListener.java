package in.xpeditions.erp.mr.ui.inbound.listener;

import java.util.List;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.data.util.BeanItemContainer;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.entity.util.Mode;
import in.xpeditions.erp.mr.entity.util.ModeEnum;
import in.xpeditions.erp.mr.entity.util.TypeEnum;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;

public class SPModeValueChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7698028172114919392L;

	private XERPComboBox modeComboBox;

	private BeanItemContainer<ServiceProvider> spContainer;

	private XERPComboBox fromComboBox;

	private XERPTextField fromField;

	public SPModeValueChangeListener(XERPComboBox modeComboBox, BeanItemContainer<ServiceProvider> spContainer,
			XERPComboBox fromComboBox, XERPTextField fromField) {
		this.modeComboBox = modeComboBox;
		this.spContainer = spContainer;
		this.fromComboBox = fromComboBox;
		this.fromField = fromField;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		System.out.println("Service provider change listener");
		ModeEnum mode = (ModeEnum) this.modeComboBox.getValue();
		this.spContainer.removeAllItems();
		if (mode != null) {
			try {
				List<ServiceProvider> sps = new ServiceProviderManager().list(TypeEnum.INBOUND, mode);
				this.spContainer.addAll(sps);
			} catch (XERPException e) {
			}
			boolean isInterBranch = mode.equals(Mode.INTER_BRANCH);
			this.fromComboBox.setVisible(isInterBranch);
			this.fromField.setVisible(!isInterBranch);
			this.fromComboBox.setRequired(isInterBranch);
			this.fromField.setRequired(!isInterBranch);
		}
	}

}
