package in.xpeditions.erp.mr.dao.manager;

import java.util.List;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.LogEntry;

public interface LogEntryDAOManager {

	void save(LogEntry logEntry) throws XERPException;

	void update(LogEntry logEntry) throws XERPException;

	List<LogEntry> list() throws XERPException;

	LogEntry getBy(LogEntry logEntry) throws XERPException;

	void delete(LogEntry logEntry) throws XERPException;

	List<Object[]> listInbound() throws XERPException;

	List<LogEntry> listPending() throws XERPException;

}
