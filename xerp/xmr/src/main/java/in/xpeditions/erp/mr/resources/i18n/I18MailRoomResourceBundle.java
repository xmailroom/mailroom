package in.xpeditions.erp.mr.resources.i18n;

import java.util.Locale;
import java.util.ResourceBundle;

import in.xpeditions.erp.mr.util.MailRoomConstants;

public class I18MailRoomResourceBundle {

	public static ResourceBundle createBundle(Locale locale) {
		return ResourceBundle.getBundle(MailRoomConstants.I18N_BASE_NAME, locale);
	}

}
