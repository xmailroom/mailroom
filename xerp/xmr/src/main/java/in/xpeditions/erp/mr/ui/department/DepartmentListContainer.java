/**
 * 
 */
package in.xpeditions.erp.mr.ui.department;

import java.util.ResourceBundle;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.server.FontAwesome;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Table;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.commons.component.XERPPrimaryButton;
import in.xpeditions.erp.commons.component.XERPTextField;
import in.xpeditions.erp.commons.ui.util.XERPUIConstants;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.permissions.MailRoomPermissionEnum;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentAddNewButtonClickListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDataChangeListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentDataChangeObserver;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentListItemClicskListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentSearchTextBlurListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentSearchTextFocusListener;
import in.xpeditions.erp.mr.ui.department.listener.DepartmentSearchTextShortCutListener;
import in.xpeditions.erp.mr.util.XmrUIUtil;

/**
 * @author Saran
 *
 */
public class DepartmentListContainer extends VerticalLayout implements DepartmentDataChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3606237433390020953L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private boolean canEdit;
	private boolean canView;
	private boolean canDelete;
	private Table table;
	private String searchString;
	
	public DepartmentListContainer(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
		this.departmentDataChangeObserver.add(this);
		this.setMargin(true);
		this.setSpacing(true);
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		canEdit = XERPUtil.hasPermission(MailRoomPermissionEnum.EDIT_DEPARTMENT.asPermission().getName());
		canView = XERPUtil.hasPermission(MailRoomPermissionEnum.VIEW_DEPARTMENT.asPermission().getName());
		canDelete = XERPUtil.hasPermission(MailRoomPermissionEnum.DELETE_DEPARTMENT.asPermission().getName());
		createSearchSection(bundle);
		createListSection(bundle);
	}

	private void createListSection(ResourceBundle bundle) {
		table = new Table();
		
		table.setWidth("100%");
		table.setImmediate(true);
		table.setSelectable(canEdit || canView || canDelete);
		table.setVisible(canEdit || canView || canDelete);
		table.addStyleName(XERPUIConstants.UI_COMPONENTS_STYLE_SMALL);
		DepartmentListItemClicskListener departmentListItemClickListener = new DepartmentListItemClicskListener(this.departmentDataChangeObserver);
		table.addItemClickListener(departmentListItemClickListener);
		this.addComponent(table);
		/*this.addComponent(table.createControls());*/
		refreshTable(this.searchString);
	}

	private void refreshTable(String searchString2) {
		if (canView || canEdit || canDelete) {
			XmrUIUtil.fillDepartmentList(table, searchString);
		}
	}

	private void createSearchSection(ResourceBundle bundle) {
		DepartmentAddNewButtonClickListener departmentAddNewButtonClickListener = new DepartmentAddNewButtonClickListener(this.departmentDataChangeObserver);
		String[] actions = {MailRoomPermissionEnum.ADD_DEPARTMENT.asPermission().getName()};
		XERPPrimaryButton newDivisionButton = new XERPPrimaryButton(actions);
		newDivisionButton.setCaption(bundle.getString(MailRoomMessageConstants.BUTTON_ADD_NEW));
		newDivisionButton.setSizeFull();
		newDivisionButton.setIcon(FontAwesome.PLUS_CIRCLE);
		newDivisionButton.addClickListener(departmentAddNewButtonClickListener);
		this.addComponent(newDivisionButton);
	
		XERPTextField searchTextField = new XERPTextField();
		searchTextField.setMaxLength(XERPUIConstants.UI_COMPONENTS_TEXT_FIELD_MAX_LENGTH);
		searchTextField.setImmediate(true);
		searchTextField.setSizeFull();
		searchTextField.focus();
		String prompt = bundle.getString(MailRoomMessageConstants.TEXTFIELD_INPUT_PROMPT_SEARCH);
		searchTextField.setInputPrompt(prompt);
		
		DepartmentSearchTextShortCutListener departmentSearchTextShortCutListener = new DepartmentSearchTextShortCutListener(prompt, KeyCode.ENTER, this.departmentDataChangeObserver);
		searchTextField.addShortcutListener(departmentSearchTextShortCutListener);
		
		DepartmentSearchTextBlurListener departmentSearchTextBlurListener = new DepartmentSearchTextBlurListener(this.departmentDataChangeObserver, departmentSearchTextShortCutListener);
		searchTextField.addBlurListener(departmentSearchTextBlurListener);
		
		DepartmentSearchTextFocusListener departmentSearchTextFocusListener = new DepartmentSearchTextFocusListener(departmentSearchTextShortCutListener);
		searchTextField.addFocusListener(departmentSearchTextFocusListener);
		this.addComponent(searchTextField);
	}

	@Override
	public void onUpdate(Department department) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSearchTextValueChange(String value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onSelectionChange(Department department) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onEditButtonClick() {
		// TODO Auto-generated method stub
		
	}

}
