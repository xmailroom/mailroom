package in.xpeditions.erp.mr.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import in.xpeditions.erp.commons.annotations.XERPSecondaryEntity;

/**
 * @author richardmtp@gmail.com
 *
 */
@XERPSecondaryEntity
@Entity
@Table(name = DeliverySheet.TABLE_NAME)
public class DeliverySheet implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2282471757945320499L;

	public static final String TABLE_NAME = "xmr_delivery_sheet";

	public static final String COLUMN_ID = "id";

	public static final String COLUMN_NUMBER = "number";

	public static final String COLUMN_DATE = "date";

	public static final String COLUMN_RECEIVED_BY = "receivedBy";

	public static final String COLUMN_RECEIVED_TIME = "receivedTime";

	public static final String COLUMN_CREATED_USER_ID = "createdUserId";

	public static final String COLUMN_CREATED_TIME = "createdTime";

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = DeliverySheet.COLUMN_ID, unique = true, nullable = false)
	private long id;

	@Column(name = DeliverySheet.COLUMN_NUMBER, unique = true, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long number;

	@Column(name = DeliverySheet.COLUMN_DATE, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long date;

	@Column(name = DeliverySheet.COLUMN_RECEIVED_BY, nullable = false)
	private String receivedBy;

	@Column(name = DeliverySheet.COLUMN_RECEIVED_TIME, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long receivedTime;

	@Column(name = DeliverySheet.COLUMN_CREATED_USER_ID, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long createdUserId;

	@Column(name = DeliverySheet.COLUMN_CREATED_TIME, nullable = false, columnDefinition = "BIGINT DEFAULT 0")
	private long createdTime;

	@OneToMany(cascade = { CascadeType.ALL }, mappedBy = DeliverySheetDetail.COLUMN_DELIVERY_SHEET)
	private List<DeliverySheetDetail> deliverySheetDetails;

	/**
	 * @return the id
	 */
	public long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * @return the number
	 */
	public long getNumber() {
		return number;
	}

	/**
	 * @param number the number to set
	 */
	public void setNumber(long number) {
		this.number = number;
	}

	/**
	 * @return the date
	 */
	public long getDate() {
		return date;
	}

	/**
	 * @param date the date to set
	 */
	public void setDate(long date) {
		this.date = date;
	}

	/**
	 * @return the receivedBy
	 */
	public String getReceivedBy() {
		return receivedBy;
	}

	/**
	 * @param receivedBy the receivedBy to set
	 */
	public void setReceivedBy(String receivedBy) {
		this.receivedBy = receivedBy;
	}

	/**
	 * @return the receivedTime
	 */
	public long getReceivedTime() {
		return receivedTime;
	}

	/**
	 * @param receivedTime the receivedTime to set
	 */
	public void setReceivedTime(long receivedTime) {
		this.receivedTime = receivedTime;
	}

	/**
	 * @return the createdUserId
	 */
	public long getCreatedUserId() {
		return createdUserId;
	}

	/**
	 * @param createdUserId the createdUserId to set
	 */
	public void setCreatedUserId(long createdUserId) {
		this.createdUserId = createdUserId;
	}

	/**
	 * @return the createdTime
	 */
	public long getCreatedTime() {
		return createdTime;
	}

	/**
	 * @param createdTime the createdTime to set
	 */
	public void setCreatedTime(long createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return the deliverySheetDetails
	 */
	public List<DeliverySheetDetail> getDeliverySheetDetails() {
		return deliverySheetDetails;
	}

	/**
	 * @param deliverySheetDetails the deliverySheetDetails to set
	 */
	public void setDeliverySheetDetails(List<DeliverySheetDetail> deliverySheetDetails) {
		this.deliverySheetDetails = deliverySheetDetails;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DeliverySheet other = (DeliverySheet) obj;
		if (id != other.id)
			return false;
		return true;
	}
	
}
