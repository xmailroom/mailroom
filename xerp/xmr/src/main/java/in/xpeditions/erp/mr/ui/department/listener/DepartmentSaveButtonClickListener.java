/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.ResourceBundle;

import com.vaadin.data.fieldgroup.BeanFieldGroup;
import com.vaadin.data.fieldgroup.FieldGroup.CommitException;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Table;

import in.xpeditions.erp.commons.component.XERPCheckBox;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Floor;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DepartmentSaveButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6849566587827536032L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;
	private BeanFieldGroup<Department> binder;
	private boolean canEdit;
	private Table floorTable;

	public DepartmentSaveButtonClickListener(DepartmentDataChangeObserver departmentDataChangeObserver,
			BeanFieldGroup<Department> binder, boolean canEdit, Table floorTable) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
		this.binder = binder;
		this.canEdit = canEdit;
		this.floorTable = floorTable;
	}

	@SuppressWarnings("unchecked")
	@Override
	public void buttonClick(ClickEvent event) {
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
		event.getButton().setEnabled(false);
		String message = "";
		String subMessage = "";
		DepartmentManager departmentManager = new DepartmentManager();
		try {
			this.binder.commit();
		} catch (CommitException e) {
			message = bundle.getString(MailRoomMessageConstants.DEPARTMENT_PLEASE_ENSURE_PROVIDED_INFORMATION_ARE_CORRECT);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		Department department = this.binder.getItemDataSource().getBean();
		Collection<XERPCheckBox> itemIds = (Collection<XERPCheckBox>) this.floorTable.getItemIds();
		
		List<Floor> floors = new ArrayList<Floor>();
		if(itemIds != null && itemIds.size() > 0) {
			for (XERPCheckBox xerpCheckBox : itemIds) {
				Boolean isChecked = xerpCheckBox.getValue();
				if (isChecked) {
					Floor floor = (Floor) xerpCheckBox.getData();
					floors.add(floor);
				}
			}
		} else {
			message = bundle.getString(MailRoomMessageConstants.DEPARTMENT_PLEASE_SELECT_FLOOR);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
			event.getButton().setEnabled(true);
			return;
		}
		
		if (floors != null && floors.size() > 0) {
			department.setFloors(floors);
		}
		
		try {

			if (department.getId() > 0) {
				if (canEdit) {
					departmentManager.update(department);
					Notification
							.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_UPDATED_SUCCESSFULLY));
				} else {
					message = bundle.getString(MailRoomMessageConstants.USER_PERMISSION_DENIED);
					subMessage = bundle.getString(MailRoomMessageConstants.USER_EDIT_PERMISSION_DENIED);
					Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
					event.getButton().setEnabled(true);
					return;
				}
			} else {
				departmentManager.save(department);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_SAVED_SUCCESSFULLY));
			}
			this.departmentDataChangeObserver.notifyUpdate(department);
		} catch (XERPConstraintViolationException e) {
			message = bundle.getString(MailRoomMessageConstants.DEPARTMENT_SAVE_FAILED);
			subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_DUPILCATE_VALUE_NAME) + " '"
					+ e.getTitle() + "' " + bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_ALREADY_EXISTS);
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		} catch (Exception e) {
			e.printStackTrace();
			message = bundle.getString(MailRoomMessageConstants.DEPARTMENT_SAVE_FAILED);
			subMessage = e.getMessage();
			Notification.show(message, "\n" + subMessage, Notification.Type.ERROR_MESSAGE);
		}
		event.getButton().setEnabled(true);
	}

}
