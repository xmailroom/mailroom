/**
 * 
 */
package in.xpeditions.erp.mr.converter;

import java.util.Locale;

import com.vaadin.data.util.converter.Converter;

import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.ServiceProvider;
import in.xpeditions.erp.mr.manager.ServiceProviderManager;

/**
 * @author xpeditions
 *
 */
public class ServiceProviderToLongConverter<T1, T2> implements Converter<Object, Long> {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5612943022187500154L;

	@Override
	public Long convertToModel(Object value, Class<? extends Long> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value != null) {
			long id = ((ServiceProvider) value).getId();
			return id;
		}
		return 0L;
	}

	@Override
	public ServiceProvider convertToPresentation(Long value, Class<? extends Object> targetType, Locale locale)
			throws com.vaadin.data.util.converter.Converter.ConversionException {
		if (value == 0L) {
			return null;
		}
		ServiceProviderManager serviceProviderManager = new ServiceProviderManager();
		ServiceProvider sp = null;
		try {
			sp = serviceProviderManager.getBy(value);
		} catch (XERPException e) {
		}
		return sp;
	}

	@Override
	public Class<Long> getModelType() {
		return Long.class;
	}

	@Override
	public Class<Object> getPresentationType() {
		return Object.class;
	}

}
