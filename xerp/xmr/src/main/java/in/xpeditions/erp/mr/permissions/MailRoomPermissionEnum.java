package in.xpeditions.erp.mr.permissions;

import in.xpeditions.erp.commons.permission.util.XERPPermission;

public enum MailRoomPermissionEnum {
	
	MENU_MAIL_ROOM_PERMISSIONS(null, "menu-mail-room-permissions", "Mail Room"),
	
	MENU_MAIL_ROOM_INBOUND_LOG(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-inbound-log", "Inbound Log"),
	ADD_INBOUND_LOG(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND_LOG, "add-inbound-log", "Add Inbound Log"),
	VIEW_INBOUND_LOG(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND_LOG, "view-inbound-log", "View Inbound Log"),
	EDIT_INBOUND_LOG(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND_LOG, "edit-inbound-log", "Edit Inbound Log"),
	DELETE_INBOUND_LOG(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND_LOG, "delete-inbound-log", "Delete Inbound Log"),
	
	MENU_MAIL_ROOM_INBOUND(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-inbound", "Inbound"),
	ADD_INBOUND(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND, "add-inbound", "Add Inbound"),
	VIEW_INBOUND(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND, "view-inbound", "View Inbound"),
	EDIT_INBOUND(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND, "edit-inbound", "Edit Inbound"),
	DELETE_INBOUND(MailRoomPermissionEnum.MENU_MAIL_ROOM_INBOUND, "delete-inbound", "Delete Inbound"),
	
	MENU_MAIL_ROOM_SP(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-sp", "Service Provider"),
	ADD_SP(MailRoomPermissionEnum.MENU_MAIL_ROOM_SP, "add-sp", "Add Service Provider"),
	VIEW_SP(MailRoomPermissionEnum.MENU_MAIL_ROOM_SP, "view-sp", "View Service Provider"),
	EDIT_SP(MailRoomPermissionEnum.MENU_MAIL_ROOM_SP, "edit-sp", "Edit Service Provider"),
	DELETE_SP(MailRoomPermissionEnum.MENU_MAIL_ROOM_SP, "delete-sp", "Delete Service Provider"),
	
	MENU_MAIL_ROOM_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-delivery-sheet", "Delivery Sheet"),
	ADD_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_DELIVERY_SHEET, "add-delivery-sheet", "Add Delivery Sheet"),
	VIEW_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_DELIVERY_SHEET, "view-delivery-sheet", "View Delivery Sheet"),
	EDIT_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_DELIVERY_SHEET, "edit-delivery-sheet", "Edit Delivery Sheet"),
	PRINT_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_DELIVERY_SHEET, "print-delivery-sheet", "Print Delivery Sheet"),
	DELETE_DELIVERY_SHEET(MailRoomPermissionEnum.MENU_MAIL_ROOM_DELIVERY_SHEET, "delete-delivery-sheet", "Delete Delivery Sheet"),
	
	MENU_MAIL_ROOM_DIVISION(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-division", "Division"),
	ADD_DIVISION(MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION, "add-division", "Add Division"),
	VIEW_DIVISION(MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION, "view-division", "View Division"),
	EDIT_DIVISION(MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION, "edit-division", "Edit Division"),
	DELETE_DIVISION(MailRoomPermissionEnum.MENU_MAIL_ROOM_DIVISION, "delete-division", "Delete Division"),
	
	MENU_MAIL_ROOM_DEPARTMENT(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-department", "Department"),
	ADD_DEPARTMENT(MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT, "add-department", "Add Department"),
	VIEW_DEPARTMENT(MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT, "view-department", "View Department"),
	EDIT_DEPARTMENT(MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT, "edit-department", "Edit Department"),
	DELETE_DEPARTMENT(MailRoomPermissionEnum.MENU_MAIL_ROOM_DEPARTMENT, "delete-department", "Delete Department"),
	
	MENU_MAIL_ROOM_COSTCENTRE(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-costcentre", "Costcentre"),
	ADD_COSTCENTRE(MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE, "add-costcentre", "Add Costcentre"),
	VIEW_COSTCENTRE(MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE, "view-costcentre", "View Costcentre"),
	EDIT_COSTCENTRE(MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE, "edit-costcentre", "Edit Costcentre"),
	DELETE_COSTCENTRE(MailRoomPermissionEnum.MENU_MAIL_ROOM_COSTCENTRE, "delete-costcentre", "Delete Costcentre"),
	
	MENU_MAIL_ROOM_FLOOR(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-costcentre", "Floor"),
	ADD_FLOOR(MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR, "add-floor", "Add Floor"),
	VIEW_FLOOR(MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR, "view-floor", "View Floor"),
	EDIT_FLOOR(MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR, "edit-floor", "Edit Floor"),
	DELETE_FLOOR(MailRoomPermissionEnum.MENU_MAIL_ROOM_FLOOR, "delete-floor", "Delete Floor"),
	
	MENU_MAIL_ROOM_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-employee", "Employee"),
	ADD_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE, "add-employee", "Add Employee"),
	VIEW_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE, "view-employee", "View Employee"),
	EDIT_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE, "edit-employee", "Edit Employee"),
	DELETE_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE, "delete-employee", "Delete Employee"),
	MOVE_EMPLOYEE(MailRoomPermissionEnum.MENU_MAIL_ROOM_EMPLOYEE, "move-employee", "Move Employee"),
	
	MENU_MAIL_ROOM_SERVICE_PROVIDER(MailRoomPermissionEnum.MENU_MAIL_ROOM_PERMISSIONS, "menu-account-service-provider", "Service Provider"),
	ADD_SERVICE_PROVIDER(MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER, "add-service-provider", "Add Service Provider"),
	VIEW_SERVICE_PROVIDER(MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER, "view--service-provider", "View Service Provider"),
	EDIT_SERVICE_PROVIDER(MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER, "edit--service-provider", "Edit Service Provider"),
	DELETE_SERVICE_PROVIDER(MailRoomPermissionEnum.MENU_MAIL_ROOM_SERVICE_PROVIDER, "delete-service-provider", "Delete Service Provider");
	
	
	private MailRoomPermissionEnum parent;
	private String name;
	private String displayName;

	private MailRoomPermissionEnum(MailRoomPermissionEnum parent, String name, String displayName) {
		this.parent = parent;
		this.name = name;
		this.displayName = displayName;
	}

	public XERPPermission asPermission() {
		XERPPermission permission = new XERPPermission();
		permission.setName(this.name);
		permission.setDisplayName(this.displayName);
		if (this.parent != null) {
			XERPPermission parent = new XERPPermission();
			parent.setName(this.parent.name);
			parent.setDisplayName(this.parent.displayName);
			permission.setParent(parent);
		}
		return permission;
	}

	public String getName() {
		return name;
	}
}
