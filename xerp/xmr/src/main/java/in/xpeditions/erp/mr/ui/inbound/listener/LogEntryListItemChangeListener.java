package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;

import in.xpeditions.erp.mr.entity.LogEntry;

public class LogEntryListItemChangeListener implements ValueChangeListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8526965701738077347L;

	private LogEntryListItemShortcutListener logEntryListItemShortcutListener;

	public LogEntryListItemChangeListener(LogEntryListItemShortcutListener logEntryListItemShortcutListener) {
		this.logEntryListItemShortcutListener = logEntryListItemShortcutListener;
	}

	@Override
	public void valueChange(ValueChangeEvent event) {
		LogEntry logEntry = (LogEntry) event.getProperty().getValue();
		if (logEntry != null) {
			this.logEntryListItemShortcutListener.setFocusedItem(logEntry);
		}
	}

}
