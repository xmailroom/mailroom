/**
 * 
 */
package in.xpeditions.erp.mr.ui.menu.handler;

import java.util.Locale;
import java.util.ResourceBundle;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.MenuBar.Command;
import com.vaadin.ui.MenuBar.MenuItem;

import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;
import in.xpeditions.erp.mr.ui.serviceprovider.ServiceProviderHome;

/**
 * @author Saran
 *
 */
public class ServiceProviderMenuHandler implements Command {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1666029025397150054L;
	private MailRoomMenuChangeHandler menuChangeHandler;

	public ServiceProviderMenuHandler(MailRoomMenuChangeHandler menuChangeHandler) {
		this.menuChangeHandler = menuChangeHandler;
	}

	/* (non-Javadoc)
	 * @see com.vaadin.ui.MenuBar.Command#menuSelected(com.vaadin.ui.MenuBar.MenuItem)
	 */
	@Override
	public void menuSelected(MenuItem selectedItem) {
		CustomComponent serviceProviderHome = new ServiceProviderHome();
		
		Locale locale = VaadinSession.getCurrent().getLocale();
		ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(locale);
        
        String breadCrumb = bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS) + " " + 
        					bundle.getString(MailRoomMessageConstants.COMMON_MENU_PATH_SEPARATOR) + " " + 
        					bundle.getString(MailRoomMessageConstants.HOME_MENU_MASTERS_SERVICE_PROVIDER);
        
        this.menuChangeHandler.handleMenuChange(breadCrumb, serviceProviderHome);
	}

}
