/**
 * 
 */
package in.xpeditions.erp.mr.ui.division.listener;

import java.io.Serializable;

import in.xpeditions.erp.mr.entity.Division;

/**
 * @author Saran
 *
 */
public interface DivisionDataChangeListener extends Serializable {

	public void onEditButtonClick();

	public void onUpdate(Division division);

	public void onSearchTextValueChange(String value);

	public void onSelectionChange(Division division);

}
