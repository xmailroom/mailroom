/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import java.util.ResourceBundle;

import org.vaadin.dialogs.ConfirmDialog;
import org.vaadin.dialogs.ConfirmDialog.Listener;

import com.vaadin.server.Page;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;

import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.manager.DepartmentManager;
import in.xpeditions.erp.mr.resources.i18n.I18MailRoomResourceBundle;
import in.xpeditions.erp.mr.resources.i18n.MailRoomMessageConstants;

/**
 * @author Saran
 *
 */
public class DepartmentDeleteConfirmListener implements Listener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2375273429079767847L;
	private Department department;
	private DepartmentDataChangeObserver departmentDataChangeObserver;

	public DepartmentDeleteConfirmListener(Department department,
			DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.department = department;
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void onClose(ConfirmDialog confirmDialog) {
		if (confirmDialog.isConfirmed()) {
			ResourceBundle bundle = I18MailRoomResourceBundle.createBundle(VaadinSession.getCurrent().getLocale());
			DepartmentManager departmentManager = new DepartmentManager();
			try {
				departmentManager.delete(department);
				Notification.show(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_DELETED_SUCCESSFULLY));
				this.departmentDataChangeObserver.notifyUpdate(null);
			} catch (XERPConstraintViolationException e) {
				String subMessage = bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_FOREIGN_KEY_VALUE)
						+ " '" + department.getName() + "' "
						+ bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_HAS_REFERENCE);
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_DELETE_FAILED),
						"<br/>" + subMessage, Notification.Type.ERROR_MESSAGE, true).show(page);
			} catch (XERPException e) {
				Page page = UI.getCurrent().getPage();
				new Notification(bundle.getString(MailRoomMessageConstants.NOTIFICATION_DEPARTMENT_DELETE_FAILED),
						"<br/>" + e.getMessage(), Notification.Type.ERROR_MESSAGE, true).show(page);
			}
		}
	}

}
