package in.xpeditions.erp.mr.hibernate.manager;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.exception.ConstraintViolationException;

import in.xpeditions.erp.commons.general.Constants;
import in.xpeditions.erp.commons.general.XERPConstraintViolationException;
import in.xpeditions.erp.commons.general.XERPException;
import in.xpeditions.erp.commons.hibernate.HibernateUtil;
import in.xpeditions.erp.commons.util.DataHolder;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.dao.manager.LogEntryDAOManager;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.ServiceProvider;

public class LogEntryDAOManagerHibernate implements LogEntryDAOManager {

	@Override
	public void save(LogEntry logEntry) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			String HQL = "select (IFNULL(MAX(le." + LogEntry.COLUMN_BATCH_NO + "), 0) + 1) from "
					+ LogEntry.class.getSimpleName() + " le";
			Query query = session.createQuery(HQL);
			Object nextNo = query.uniqueResult();
			logEntry.setBatchNo(XERPUtil.sum(nextNo, 0).longValue());
			session.save(logEntry);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void update(LogEntry logEntry) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.update(logEntry);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogEntry> list() throws XERPException {
		String hql = "SELECT le FROM " + LogEntry.class.getSimpleName() + " le";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public LogEntry getBy(LogEntry logEntry) throws XERPException {
		String hql = "SELECT le FROM " + LogEntry.class.getSimpleName() + " AS le WHERE le.id=:id";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(hql);
			query.setParameter("id", logEntry.getId());
			return (LogEntry) query.uniqueResult();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@Override
	public void delete(LogEntry logEntry) throws XERPException {
		Transaction tx = null;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			tx = session.beginTransaction();
			session.delete(logEntry);
			tx.commit();
		} catch (ConstraintViolationException e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPConstraintViolationException(e);
		} catch (Exception e) {
			if (tx != null) {
				tx.rollback();
			}
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> listInbound() throws XERPException {
		String db1 = DataHolder.common_db_name;
		String db2 = (String) XERPUtil.getSessionAttribute(Constants.SESSION_DBNAME);
		String s = "";
		s += " le." + LogEntry.COLUMN_ID + " as leId,";
		s += " le." + LogEntry.COLUMN_BATCH_NO + " as batchNo,";
		s += " le." + LogEntry.COLUMN_DATE + " as leDate,";
		s += " le." + LogEntry.COLUMN_MODE + " as leMode,";
		s += " le." + LogEntry.COLUMN_NO_OF_SHIP + " as nos,";
		s += " le." + LogEntry.COLUMN_NO_OF_ENTRY + " as noe,";
		s += " IFNULL(le." + LogEntry.COLUMN_REMARKS + ", '') as r,";
		s += " le." + LogEntry.COLUMN_TYPE + " as sType,";
		s += " sp." + ServiceProvider.COLUMN_ID + " as spId,";
		s += " sp." + ServiceProvider.COLUMN_NAME + " as spName";
		String SQL = " SELECT " + s + " FROM " + db2 + "." + LogEntry.TABLE_NAME + " le" + " INNER JOIN " + db1 + "."
				+ ServiceProvider.TABLE_NAME + " sp ON le." + LogEntry.COLUMN_SERVICE_PROVIDER + " = sp." + ServiceProvider.COLUMN_ID
				+ " order by le." + LogEntry.COLUMN_DATE + " DESC , le." + LogEntry.COLUMN_BATCH_NO + ", le."
				+ LogEntry.COLUMN_NO_OF_SHIP + ", le." + LogEntry.COLUMN_MODE;
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createSQLQuery(SQL);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<LogEntry> listPending() throws XERPException {
		String HQL = "SELECT le FROM " + LogEntry.class.getSimpleName() + " le where (le." + LogEntry.COLUMN_NO_OF_SHIP
				+ " - le." + LogEntry.COLUMN_NO_OF_ENTRY + ")>:pending";
		Session session = null;
		try {
			session = HibernateUtil.getSession();
			Query query = session.createQuery(HQL);
			query.setParameter("pending", 0);
			return query.list();
		} catch (Exception e) {
			throw new XERPException(e);
		} finally {
			if (session != null && session.isOpen()) {
				session.close();
			}
		}
	}

}
