package in.xpeditions.erp.mr.ui.deliverysheet.listener;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.data.fieldgroup.BeanFieldGroup;

import in.xpeditions.erp.mr.entity.DeliverySheet;

public class DeliverySheetDataChangeObserver {

	private List<DeliverySheetDataChangeListener> deliverySheetDataChangeListeners = new ArrayList<DeliverySheetDataChangeListener>();

	public void addListener(DeliverySheetDataChangeListener deliverySheetDataChangeListener) {
		this.deliverySheetDataChangeListeners.add(deliverySheetDataChangeListener);
	}

	public void notifyUpdate(DeliverySheet deliverySheet) {
		for (DeliverySheetDataChangeListener deliverySheetDataChangeListener : this.deliverySheetDataChangeListeners) {
			deliverySheetDataChangeListener.onUpdate(deliverySheet);
		}
	}

	public void notifySelectionChange(DeliverySheet deliverySheet) {
		for (DeliverySheetDataChangeListener deliverySheetDataChangeListener : this.deliverySheetDataChangeListeners) {
			deliverySheetDataChangeListener.onSelectionChange(deliverySheet);
		}
	}

	public void notifyEditButtonClick(DeliverySheet deliverySheet) {
		for (DeliverySheetDataChangeListener deliverySheetDataChangeListener : this.deliverySheetDataChangeListeners) {
			deliverySheetDataChangeListener.onEditButtonClick(deliverySheet);
		}
	}

	public void notifyConfirm(BeanFieldGroup<DeliverySheet> deliverySheetBinder) {
		for (DeliverySheetDataChangeListener deliverySheetDataChangeListener : this.deliverySheetDataChangeListeners) {
			deliverySheetDataChangeListener.onConfirm(deliverySheetBinder);
		}
	}

}
