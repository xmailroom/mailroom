/**
 * 
 */
package in.xpeditions.erp.mr.util;

/**
 * @author Saran
 *
 */
public class XMailroomTableItemConstants {


	public static final String DIVISION_NAME = "name";
	public static final String DIVISION_CODE = "code";

	public static final String DEPARTMENT_NAME = "name";
	public static final String DEPARTMENT_CODE = "code";

	public static final String DEPARTMENT_FLOOR_SELECT = "select";
	public static final String DEPARTMENT_FLOOR_NAME = "name";

	public static final String COSTCENTRE_NAME = "name";
	public static final String COSTCENTRE_CODE = "code";
	public static final String COSTCENTRE_DEPARTMENT_NAME = "department.name";

	public static final String FLOOR_NAME = "name";

	public static final String EMPLOYEE_NAME = "name";

	public static final String EMPLOYEE_FLOOR_SELECT = "select";
	public static final String EMPLOYEE_FLOOR_NAME = "name";
	public static final String EMPLOYEE_DEPARTMENT = "department.name";

	public static final String SERVICE_PROVIDER_NAME = "name";
	public static final String SERVICE_PROVIDER_TYPE = "type";
	
}
