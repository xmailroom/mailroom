package in.xpeditions.erp.mr.ui.inbound.log;

import com.vaadin.shared.ui.MarginInfo;
import com.vaadin.ui.Alignment;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;

import in.xpeditions.erp.mr.ui.inbound.log.listener.InboundLogEntryDataChangeObserver;

public class InboundLogHome extends CustomComponent {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7701941773800558780L;

	public InboundLogHome() {
		this.init();
		this.setSizeFull();
	}

	private void init() {
		InboundLogEntryDataChangeObserver inboundLogEntryDataChangeObserver = new InboundLogEntryDataChangeObserver();
		InboundLogEntryEditContainer inboundLogEntryEditContainer = new InboundLogEntryEditContainer(
				inboundLogEntryDataChangeObserver, null);
		inboundLogEntryEditContainer.setWidth("50%");

		VerticalLayout verticalLayout = new VerticalLayout();
		// verticalLayout.setMargin(new MarginInfo(false, false, false, true));
		verticalLayout.addComponent(inboundLogEntryEditContainer);
		verticalLayout.setComponentAlignment(inboundLogEntryEditContainer, Alignment.MIDDLE_CENTER);

		InboundLogEntryListContainer inboundLogEntryListContainer = new InboundLogEntryListContainer(
				inboundLogEntryDataChangeObserver);
		inboundLogEntryListContainer.setMargin(new MarginInfo(true, false, false, false));
		inboundLogEntryListContainer.setWidth("70%");
		verticalLayout.addComponent(inboundLogEntryListContainer);
		verticalLayout.setComponentAlignment(inboundLogEntryListContainer, Alignment.MIDDLE_CENTER);

		this.setCompositionRoot(verticalLayout);
	}

}
