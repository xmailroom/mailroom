/**
 * 
 */
package in.xpeditions.erp.mr.ui.employee.listener;

import java.io.Serializable;

import in.xpeditions.erp.acc.entity.Branch;
import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.mr.entity.Department;
import in.xpeditions.erp.mr.entity.Division;
import in.xpeditions.erp.mr.entity.Employee;

/**
 * @author xpeditions
 *
 */
public interface EmployeeDataChangeListener extends Serializable {

	public void onUpdate(Employee employee);

	public void onSelectionChange(Employee employee);

	public void onSearchTextValueChange(String value);

	public void onEditButtonClick();

	public void onBranchChange(Branch branch);

	public void onDivisionChange(Division division);

	public void onDepartmentChange(Department department);

	public void onMoveEmployee();

	public void onMoveWindowClose();

	public void onBranchMoveChange(Branch branch, XERPComboBox divisionCombo);

	public void onDivisionMoveChange(Division division, XERPComboBox departmentCombo);

	public void onDepartmentMoveChange(Department department, XERPComboBox costCentreCombo);

}
