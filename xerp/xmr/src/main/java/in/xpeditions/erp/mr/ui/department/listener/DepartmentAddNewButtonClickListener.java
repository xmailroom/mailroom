/**
 * 
 */
package in.xpeditions.erp.mr.ui.department.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;

/**
 * @author Saran
 *
 */
public class DepartmentAddNewButtonClickListener implements ClickListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7050442615983584364L;
	private DepartmentDataChangeObserver departmentDataChangeObserver;

	public DepartmentAddNewButtonClickListener(DepartmentDataChangeObserver departmentDataChangeObserver) {
		this.departmentDataChangeObserver = departmentDataChangeObserver;
	}

	@Override
	public void buttonClick(ClickEvent event) {
		Button button = event.getButton();
		button.setEnabled(false);
		this.departmentDataChangeObserver.notifyUpdate(null);
		button.setEnabled(true);
	}

}
