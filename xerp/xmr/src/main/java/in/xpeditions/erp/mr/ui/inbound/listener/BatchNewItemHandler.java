package in.xpeditions.erp.mr.ui.inbound.listener;

import com.vaadin.data.util.BeanItemContainer;
import com.vaadin.ui.AbstractSelect.NewItemHandler;

import in.xpeditions.erp.commons.component.XERPComboBox;
import in.xpeditions.erp.commons.util.XERPUtil;
import in.xpeditions.erp.mr.entity.LogEntry;
import in.xpeditions.erp.mr.entity.util.TypeEnum;

public class BatchNewItemHandler implements NewItemHandler {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1628506221166578929L;

	private BeanItemContainer<LogEntry> leContainer;

	private XERPComboBox inBatchNoComboBox;

	public BatchNewItemHandler(BeanItemContainer<LogEntry> leContainer, XERPComboBox inBatchNoComboBox) {
		this.leContainer = leContainer;
		this.inBatchNoComboBox = inBatchNoComboBox;
	}

	@Override
	public void addNewItem(String newItemCaption) {
		LogEntry logEntry = new LogEntry();
		logEntry.setsType(TypeEnum.INBOUND);
		logEntry.setBatchNo(XERPUtil.sum(newItemCaption, 0).longValue());
		this.leContainer.addItem(logEntry);
		this.inBatchNoComboBox.setContainerDataSource(this.leContainer);
		this.inBatchNoComboBox.setValue(logEntry);
	}

}
